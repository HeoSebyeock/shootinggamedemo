﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainSceneManager : MonoBehaviour
{
    enum EGameKind
    {
        SuperPororo,
        HulkCrong,
        SpiderLoopy,
        ThorPoby,
        BatMan,
        CaptainPetty,
        IronPororo,
    }
    public void CallGame(int kind)
    {
        SceneManager.LoadScene(((EGameKind)kind).ToString());
    }
}
