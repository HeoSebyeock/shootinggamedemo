﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatManGameManager : GameManager
{
    protected override void Start()
    {
        base.Start();
        currentGameKind = EGameKind.BatMan;
    }
}
