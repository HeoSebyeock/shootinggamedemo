﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public enum EGameKind
    {
        SuperPororo,
        HulkCrong,
        SpiderLoopy,
        ThorPoby,
        BatMan,
        CaptainPetty,
        IronPororo,
    }

    enum EGameStep
    {
        OP,
        One,
        Two,
        ED
    }

    [Header("PARENTS_PANEL")]
    [SerializeField]
    private Transform _gameTransformPanel;
    [SerializeField]
    private Transform _UITransformPanel;

    public Transform GameTransformPanel { get { return _gameTransformPanel; } }
    public Transform UITransformPanel { get { return _UITransformPanel; } }

    [Header("오프닝")]
    [SerializeField]
    private GameObject _openning;
    private UnityEvent _oppeningEvent;

    [Header("게임1")]
    [SerializeField]
    private GameObject _gameOne;

    [Header("게임2")]
    [SerializeField]
    private GameObject _gameTwo;

    [Header("게임3")]
    [SerializeField]
    private GameObject _gameThree;

    [Header("엔딩")]
    [SerializeField]
    private GameObject _endding;

    [Header("DEBUG TIMESCALE BUTTON")]
    public GameObject Timescalebutton;

    private EGameStep _currentGameStep = EGameStep.OP;
    static public EGameKind currentGameKind;
    static public UnityEvent GameEndCallBackEvent = new UnityEvent();

    protected virtual void Start()
    {
        GameInitSetting();
    }

    void GameInitSetting()
    {
        //Util.GetInstance.IsNotTouched = false;
        GameEndCallBackEvent.AddListener(GameEndCallBack);
    }

    void GameEndCallBack()
    {
        _currentGameStep++;
        gameObject.AddComponent<TransitionImageAction>().CreateTransitionImage("circle", TransitionImageAction.EType.ZoomInOut, Vector2.zero, 1, 0);
        gameObject.GetComponent<TransitionImageAction>().StartAction();
        StartCoroutine("CoGameEndCallBack");
    }

    IEnumerator CoGameEndCallBack()
    {
        Debug.Log("in game");
        yield return new WaitForSeconds(1);
        switch (_currentGameStep)
        {
            case EGameStep.One:
                {
                    _gameOne.SetActive(true);
                    _openning.SetActive(false);
                }
                break;

            case EGameStep.Two:
                {
                    _gameTwo.SetActive(true);
                    _gameOne.SetActive(false);
                }
                break;

            case EGameStep.ED:
                {
                    _gameTwo.SetActive(false);
                    //고쳐야함 임시로 스파이더 루피때문에 해놓음
                    if(_gameThree != null)
                    {
                        _gameThree.SetActive(false);
                    }
                    _endding.SetActive(true);
                    yield return new WaitForSeconds(7);
                    SceneManager.LoadScene(0);
                }
                break;
            default:
                break;
        }
    }

    public void TimeScaleButtonCallBack()
    {
        if(Time.timeScale == 1)
        {
            Time.timeScale = 5;
            Timescalebutton.transform.GetChild(0).GetComponent<Text>().text = "Speed x 5";
        }
        else
        {
            Time.timeScale = 1;
            Timescalebutton.transform.GetChild(0).GetComponent<Text>().text = "Normal";
        }
    }
}
