﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    /// <summary>
    /// 상위 폴더 종류
    /// </summary>
    public enum EFolderRootType
    {
        Images,
        Spines,
        Prefabs,
        ScriptableObjectData,
    }

    /// <summary>
    /// 하위 폴더 종류
    /// </summary>
    public enum EFolderLeafType
    {
        Common,
        Character,
        Effect,
        Etc,
        Game,
    }

    /// <summary>
    /// 오브젝트 종류
    /// </summary>
    public enum EObjectType
    {
        Prefab,
        Spine,
        SpriteRenderer,
        Image,
        SpriteMask,
    }

    /// <summary>
    /// 태그 종류
    /// </summary>
    public enum ETag
    {
        Untagged,
        NotMasking,
        //Super Pororo
        SuccessSun,
        BigSuccessSun,
        FailSun,
        //Hulk Crong
        Obstacle,
        Enemy,
        //Spider loopy
        RotateEnemy,
        PassCheck,
        SuperGoblin,
        //Thor poby
        Cloud,
        Collect,
        Wrong,
        //Batman eddy
        BoostItem,
        //Captain petty
        SuperItem,
        TouchAreaItem,
        EnemyNormalLaiser,
        HitLaiser,
        EnemyMissile,
        ActorAttack,
        //Iron Pororo
        FeverMode,
        Laiser,
        Actor,
        //ShootingGame
        EnemyBullet,
        ActorBullet,
    }

    //스프라이트 픽셀 : 게임 유닛
    public const int SPRITE_PIXELS_PER_UNIT = 1;
}
