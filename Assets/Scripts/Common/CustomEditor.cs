﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.Events;


/// <summary>
/// 파티클 커스텀 에디터
/// </summary>
//[CustomEditor(typeof(Particles))]
//public class CustomEditor_Particles : Editor
//{
//    private Particles _particles;

//    private void OnEnable()
//    {
//        if (AssetDatabase.Contains(target))
//        {
//            //_particles = null;
//        }
//        else
//        {
//            _particles = (Particles)target;
//        }
//    }

//    public override void OnInspectorGUI()
//    {
//        EditorGUILayout.LabelField("*** 파티클 정보 입력 ***");
//        EditorGUILayout.Space();

//        DefaultSetting();

//        switch (_particles.Type)
//        {
//            case Particles.EType.Spread:
//            case Particles.EType.YoYo:
//                {
//                    _particles.IsRandom = EditorGUILayout.Toggle("고정 / 랜덤", _particles.IsRandom);
//                }
//                break;

//            default:
//                break;
//        }
//    }

//    void DefaultSetting()
//    {
//        _particles.Type = (Particles.EType)EditorGUILayout.EnumPopup("파티클 종류", _particles.Type);
//        _particles.ImageName = EditorGUILayout.TextField("이미지 이름", _particles.ImageName);
//        _particles.MoveDistance = EditorGUILayout.FloatField("이동 거리", _particles.MoveDistance);
//        _particles.Scale = EditorGUILayout.FloatField("크기", _particles.Scale);
//        _particles.Count = EditorGUILayout.IntField("개수", _particles.Count);
//        _particles.FadeTime = EditorGUILayout.FloatField("페이드 시간", _particles.FadeTime);

//    }
//}

/// <summary>
/// 힌트 커스텀 에디터
/// </summary>

#if UNITY_EDITOR
[CustomEditor(typeof(HintImageAction))]
public class CustomEditor_HintImageAction : Editor
{
    private HintImageAction _hintImageAction;

    private void OnEnable()
    {
        if (AssetDatabase.Contains(target))
        {
            //_hintImageAction = null;
        }
        else
        {
            _hintImageAction = (HintImageAction)target;
        }
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("*** 힌트이미지 정보 입력 ***");
        EditorGUILayout.Space();

        DefaultSetting();

        switch (_hintImageAction._type)
        {

            case HintImageAction.EType.MoveAction:
                {
                    _hintImageAction.MovePos = EditorGUILayout.Vector2Field("이동 위치", _hintImageAction.MovePos);
                }
                break;
            case HintImageAction.EType.JumpAction:
                {

                    _hintImageAction.JumpHeight = EditorGUILayout.FloatField("점프 높이", _hintImageAction.JumpHeight);
                }
                break;
            case HintImageAction.EType.VerticalFillMethodAction:
            case HintImageAction.EType.HorizontalFillMethodAction:
                {
                    _hintImageAction.IsFillOrigin = EditorGUILayout.IntField("FillAmount 좌/우 반전", _hintImageAction.IsFillOrigin);
                }
                break;
            default:
                break;
        }
    }

    void DefaultSetting()
    {
        _hintImageAction._type = (HintImageAction.EType)EditorGUILayout.EnumPopup("힌트 종류", _hintImageAction._type);
        _hintImageAction.ImageName = EditorGUILayout.TextField("이미지 이름", _hintImageAction.ImageName);
        _hintImageAction.Position = EditorGUILayout.Vector2Field("생성 위치", _hintImageAction.Position);
        _hintImageAction.Time = EditorGUILayout.FloatField("시간", _hintImageAction.Time);
        _hintImageAction.WaitTime = EditorGUILayout.FloatField("지연 시간", _hintImageAction.WaitTime);
        _hintImageAction.IsFade = EditorGUILayout.Toggle("힌트 이미지 페이드", _hintImageAction.IsFade);
    }
}

/// <summary>
/// 이미지 장면 전환 커스텀 에디터
/// </summary>
[CustomEditor(typeof(TransitionImageAction))]
public class CustomEditor_TransitionImageAction : Editor
{
    private TransitionImageAction _transitionImageAction;

    private void OnEnable()
    {
        if (AssetDatabase.Contains(target))
        {
            //_transitionImageAction = null;
        }
        else
        {
            _transitionImageAction = (TransitionImageAction)target;
        }
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("*** 이미지 장면 전환 정보 입력 ***");
        EditorGUILayout.Space();

        DefaultSetting();

        switch (_transitionImageAction._type)
        {
            case TransitionImageAction.EType.ZoomInOut:
            case TransitionImageAction.EType.BingGleInOut:
            case TransitionImageAction.EType.FadeInOut:
            case TransitionImageAction.EType.HorizontalInOut:
            case TransitionImageAction.EType.Radial360InOut:
                {
                    _transitionImageAction.WaitTime = EditorGUILayout.FloatField("지연 시간", _transitionImageAction.WaitTime);
                }
                break;
            default:
                break;
        }
    }

    void DefaultSetting()
    {
        _transitionImageAction._type = (TransitionImageAction.EType)EditorGUILayout.EnumPopup("전환 방식", _transitionImageAction._type);
        _transitionImageAction.ImageName = EditorGUILayout.TextField("이미지 이름", _transitionImageAction.ImageName);
        _transitionImageAction.Position = EditorGUILayout.Vector2Field("생성 위치", _transitionImageAction.Position);
        _transitionImageAction.Time = EditorGUILayout.FloatField("시간", _transitionImageAction.Time);
        _transitionImageAction.ColorIsBlack = EditorGUILayout.Toggle("이미지 검정변환", _transitionImageAction.ColorIsBlack);
    }
}

/// <summary>
/// 사용자 제스쳐 커스텀 에디터
/// </summary>
[CustomEditor(typeof(UserInputGestureAction))]
public class CustomEditor_UserInputGestureAction : Editor
{
    private UserInputGestureAction _userInputGestureAction;

    private void OnEnable()
    {
        if (AssetDatabase.Contains(target))
        {
            //_userInputGestureAction = null;
        }
        else
        {
            _userInputGestureAction = (UserInputGestureAction)target;
        }
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("*** 사용자 제스쳐 정보 입력 ***");
        EditorGUILayout.Space();

        DefaultSetting();

        if (_userInputGestureAction._type != UserInputGestureAction.EType.Tap &&
            _userInputGestureAction._type != UserInputGestureAction.EType.DoubleTap)
        {
            SerializedProperty progressingEvent = serializedObject.FindProperty("_progressingEvent");
            EditorGUILayout.PropertyField(progressingEvent);
        }

        if (_userInputGestureAction._type == UserInputGestureAction.EType.DragAndDropCenterPos ||
            _userInputGestureAction._type == UserInputGestureAction.EType.DragAndDropPickedPos ||
            _userInputGestureAction._type == UserInputGestureAction.EType.Press ||
            _userInputGestureAction._type == UserInputGestureAction.EType.Rotate ||
            _userInputGestureAction._type == UserInputGestureAction.EType.FlickLeftToRight ||
            _userInputGestureAction._type == UserInputGestureAction.EType.FlickRightToLeft ||
            _userInputGestureAction._type == UserInputGestureAction.EType.SwipeDown ||
            _userInputGestureAction._type == UserInputGestureAction.EType.SwipeUp ||
            _userInputGestureAction._type == UserInputGestureAction.EType.SwipeLeft ||
            _userInputGestureAction._type == UserInputGestureAction.EType.SwipeRight ||
            _userInputGestureAction._type == UserInputGestureAction.EType.TouchDownAndUp
            )
        {
            SerializedProperty failEvent = serializedObject.FindProperty("_failEvent");
            EditorGUILayout.PropertyField(failEvent);
        }

        if (_userInputGestureAction._type == UserInputGestureAction.EType.DragAndDropCenterPos ||
            _userInputGestureAction._type == UserInputGestureAction.EType.DragAndDropPickedPos)
        {
            SerializedProperty hintStartEvent = serializedObject.FindProperty("_hintStartEvent");
            EditorGUILayout.PropertyField(hintStartEvent);

            SerializedProperty hintStopEvent = serializedObject.FindProperty("_hintStopEvent");
            EditorGUILayout.PropertyField(hintStopEvent);

            _userInputGestureAction.SuccessNeedsPosition = EditorGUILayout.Vector2Field("성공 위치", _userInputGestureAction.SuccessNeedsPosition);
        }

        if (_userInputGestureAction._type == UserInputGestureAction.EType.Press)
        {
            _userInputGestureAction.SuccessNeedsTime = EditorGUILayout.FloatField("성공에 필요한 시간", _userInputGestureAction.SuccessNeedsTime);
        }

        if (_userInputGestureAction._type == UserInputGestureAction.EType.SwipeDown ||
            _userInputGestureAction._type == UserInputGestureAction.EType.SwipeLeft ||
            _userInputGestureAction._type == UserInputGestureAction.EType.SwipeRight ||
            _userInputGestureAction._type == UserInputGestureAction.EType.SwipeUp ||
            _userInputGestureAction._type == UserInputGestureAction.EType.DragAndDropCenterPos ||
            _userInputGestureAction._type == UserInputGestureAction.EType.DragAndDropPickedPos)
        {
            _userInputGestureAction.SuccessNeedsDistance = EditorGUILayout.FloatField("성공에 필요한 거리", _userInputGestureAction.SuccessNeedsDistance);
        }

        if (GUI.changed)
        {

            serializedObject.ApplyModifiedProperties();
            EditorUtility.SetDirty(_userInputGestureAction);
        }
    }

    void DefaultSetting()
    {
        _userInputGestureAction._type = (UserInputGestureAction.EType)EditorGUILayout.EnumPopup("종류", _userInputGestureAction._type);
        _userInputGestureAction._judgeWay = (UserInputGestureAction.EJudgeWay)EditorGUILayout.EnumPopup("판단 방법", _userInputGestureAction._judgeWay);
        _userInputGestureAction._isNotDestroy = EditorGUILayout.Toggle("해당 컴포넌트 삭제 안하심?", _userInputGestureAction._isNotDestroy);
        _userInputGestureAction._addTouchPos = EditorGUILayout.Vector3Field("오브젝트 좌표에 해당 값을 더해준다", _userInputGestureAction._addTouchPos);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("목표 대상 오브젝트");
        _userInputGestureAction.GoalGameObject = (GameObject)EditorGUILayout.ObjectField(_userInputGestureAction.GoalGameObject, typeof(GameObject), true);

        SerializedProperty completeEvent = serializedObject.FindProperty("_completeEvent");
        EditorGUILayout.PropertyField(completeEvent);
    }
}

#endif
