﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : MonoBehaviour
{
    //todo 오브젝트 단일이 아닌 종류를 다양하게 해야할 경우 비율을 추가해야하는데.. 흠.. 어ㄹ.
    [Header("오브젝트")]
    [SerializeField]
    private GameObject[] _poolingObjects;
    public GameObject[] PoolingObjects { get; }

    [Header("오브젝트 프리팹 이름")]
    [SerializeField]
    private string[] _prefabNames;
    public string[] PrefabNames{ get; }

    [Header("개수")]
    [SerializeField]
    private int _count;
    public int Count { get; }

    [Header("섞기")]
    [SerializeField]
    private bool _isShuffle;
    public bool IsShuffle { get; }

    private bool _isVisible = true;

    //오브젝트의 부모 트랜스폼
    private Transform _parentTransform;

    //오브젝트 담을 큐
    private Queue<GameObject> _poolingObjectQueue = new Queue<GameObject>();

    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine("CoObjectPoolingSystem");
    }

    /// <summary>
    /// 동적으로 필요한 정보를 넣습니다. (게임오브젝트)
    /// </summary>
    /// <param name="poolingObject">게임오브젝트</param>
    /// <param name="count">오브젝트 개수</param>
    /// <param name="isShuffle">오브젝트 섞기</param>
    public void CreateObjectPooling(GameObject[] poolingObjects,int count,bool isShuffle = false)
    {
        _poolingObjects = poolingObjects;
        _count = count;
        _isShuffle = isShuffle;
    }

    /// <summary>
    /// 동적으로 필요한 정보를 넣습니다. (이미지)
    /// </summary>
    /// <param name="imageName">이미지 이imageName</param>
    /// <param name="count">오브젝트 개수</param>
    /// <param name="isShuffle">오브젝트 섞기</param>
    public void CreateObjectPooling(string[] prefabNames, int count, bool isShuffle = false)
    {
        _prefabNames = prefabNames;
        _count = count;
        _isShuffle = isShuffle;
    }

    //큐에 오브젝트 넣음
    void DefaultSetting()
    {
        if (_prefabNames != null)
        {
            _poolingObjects = new GameObject[_prefabNames.Length];
        }

        if (_poolingObjects[0] == null)
        {
            for (int i = 0; i < _prefabNames.Length; i++)
            {
                CreateFactory createFactory = new CreateFactory();
                _poolingObjects[i] = createFactory.CreateGameObject(Constants.EObjectType.Prefab, Constants.EFolderRootType.Prefabs, Constants.EFolderLeafType.Game, _prefabNames[i], true, transform);
                _poolingObjects[i].SetActive(false);
            }
        }

        for (int i = 0; i < _count; i++)
        {
            for (int j = 0; j < _poolingObjects.Length; j++)
            {
                GameObject tempObject = Instantiate(_poolingObjects[j]);
                _poolingObjectQueue.Enqueue(tempObject);
            }
        }
        //큐에있는 오브젝트 섞기
        if (_isShuffle)
        {
            Util.GetInstance.Shuffle(_poolingObjectQueue);
        }
    }

    IEnumerator CoObjectPoolingSystem()
    {
        yield return new WaitUntil(()=> _count != 0);
        DefaultSetting();
    }

    /// <summary>
    /// 큐에 있는 오브젝트나 새로 오브젝트를 생성해 가져온다
    /// </summary>
    /// <param name="parentTransform">오브젝트를 붙일 부모 트랜스폼</param>
    /// <returns>반환 오브젝트</returns>
    /// 
    public GameObject GetObject(Transform parentTransform)
    {
        _parentTransform = parentTransform;
        GameObject tempObject = null;

        if (_poolingObjectQueue.Count > 0)
        {
            tempObject = _poolingObjectQueue.Dequeue();
        }
        //큐에 오브젝트가 없을 경우 새로 생성 한다
        else
        {
            if(_poolingObjects.Length != 0)
            {
                tempObject = Instantiate(_poolingObjects[Random.Range(0, _prefabNames.Length)]);
            }
            else
            {
                CreateFactory createFactory = new CreateFactory();
                tempObject = Instantiate(createFactory.CreateGameObject(Constants.EObjectType.Prefab, Constants.EFolderRootType.Prefabs, Constants.EFolderLeafType.Game, _prefabNames[Random.Range(0, _prefabNames.Length)], true, transform));
            }
        }

        tempObject.transform.SetParent(_parentTransform);
        //설정된 상태로 불러온다
        tempObject.SetActive(_isVisible);
        return tempObject;
    }

    /// <summary>
    /// 오브젝트를 큐에 다시 넣는다
    /// </summary>
    public void ReturnObject(GameObject gameObject)
    {
        gameObject.SetActive(false);
        gameObject.transform.SetParent(_parentTransform);
        _poolingObjectQueue.Enqueue(gameObject);
    }

    /// <summary>
    /// 오브젝트를 활성 / 비활성 상태로 가져온다
    /// </summary>
    /// <param name="isVisible"></param>
    public void SetVisible(bool isVisible)
    {
        _isVisible = isVisible;
    }
}
