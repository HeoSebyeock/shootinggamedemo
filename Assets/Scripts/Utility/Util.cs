﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Util : MonoBehaviour
{
    [SerializeField]
    private Transform _UITransformPanel;

    private static Util _instance = null;

    const float SCREEN_WIDTH = 1280;
    const float SCREEN_HEIGHT = 720;

    /// <summary>
    /// touch control
    /// </summary>
    private bool _isNotTouched = false;
    public bool IsNotTouched { get; set; }

    private void Awake()
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            //씬 전환 시 전환 된 씬에 util이 존재하면 전환된 씬의 util을 지워준다
            Destroy(this.gameObject);
        }
    }

    public static Util GetInstance
    {
        get
        {
            if(_instance == null)
            {
                return null;
            }
            return _instance;
        }
    }

    /// <summary>
    /// 터치한 위치의 로컬 좌표를 반환합니다.
    /// </summary>
    /// <param name="parent"></param>
    /// <returns></returns>
    public Vector3 GetTouchLocalPosition(Transform parent)
    {
        return parent.InverseTransformPoint(Camera.main.ScreenToWorldPoint(Input.mousePosition));
    }

    /// <summary>
    /// 터치한 위치의 월드 좌표를 반환합니다.
    /// </summary>
    public Vector2 GetTouchPosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public Vector2 GetTouchPosition(Camera camera)
    {
        return camera.ScreenToWorldPoint(Input.mousePosition);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public Vector2 GetDesignSize()
    {
        return new Vector2(SCREEN_WIDTH,SCREEN_HEIGHT);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public Vector3 GetDesignSize_Vector3()
    {
        return new Vector2(SCREEN_WIDTH, SCREEN_HEIGHT);
    }

    public Vector3 GetScreenScale()
    {
        float scaleX = ((Screen.width * GetDesignSize().y) / Screen.height) / GetDesignSize().x;
        return new Vector3(scaleX, 1.0f, 1.0f);
    }

    public Transform GetUITransform()
    {
        return _UITransformPanel;
    }

    /// <summary>
    /// List Shuffle
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    public void Shuffle<T>(IList<T> list)
    {
        for (var i = 0; i < list.Count - 1; ++i)
        {
            var r = UnityEngine.Random.Range(i, list.Count);
            var tmp = list[i];
            list[i] = list[r];
            list[r] = tmp;
        }
    }
    /// <summary>
    /// Queue Shuffle
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    public void Shuffle<T>(Queue<T> list)
    {
        List<T> _list = new List<T>();
        for (int i = 0; i < list.Count; i++)
        {
            _list.Add(list.Dequeue());
        }
        for (var i = 0; i < _list.Count - 1; ++i)
        {
            var r = UnityEngine.Random.Range(i, _list.Count);
            var tmp = _list[i];
            _list[i] = _list[r];
            _list[r] = tmp;
        }
        for (int i = 0; i < _list.Count; i++)
        {
            list.Enqueue(_list[i]);
        }
    }

    /// <summary>
    /// List Reverse
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    public void Reverse<T>(IList<T> list)
    {
        var left = 0;
        var right = list.Count - 1;
        for (var i = 0; i < list.Count / 2; i++)
        {
            var tmp = list[left];
            list[left] = list[right];
            list[right] = tmp;
            left++;
            right--;
        }
    }
}
