using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
public class TextureImportSetting : AssetPostprocessor
{
    void OnPreprocessTexture()
    {
        TextureImporter ti = (TextureImporter)assetImporter;
        ti.spritePixelsPerUnit = Constants.SPRITE_PIXELS_PER_UNIT;

        ti.androidETC2FallbackOverride = AndroidETC2FallbackOverride.Quality32BitDownscaled;
    }
}
#endif
