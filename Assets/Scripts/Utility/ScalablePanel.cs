﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ScalablePanel : MonoBehaviour
{
    private void Start()
    {
        RectTransform rect = gameObject.GetComponent<RectTransform>();
        rect.localScale = Util.GetInstance.GetScreenScale();
    }
}
