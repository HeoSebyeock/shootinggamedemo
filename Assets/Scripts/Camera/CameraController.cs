﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class CameraController : MonoBehaviour
{
    public enum ECameraAspectRatio
    {
        DeviceRatio = 0,
        Aspect_16_9
    }
    
    public float letterboxMoveSpeed = 5.0f;

    [Header("Gizmos")]
    public Color camBorderColor = Color.red;


    public static float DeviceRatio { get { return (float)Screen.width / (float)Screen.height; } }
    public static Vector2 DeviceCameraSize { get { return new Vector2(ORTHOGONAL_CAMERA_SIZE * 2 * DeviceRatio, ORTHOGONAL_CAMERA_SIZE * 2); } }
    public Vector2 CameraSize { get { return new Vector2(cam.orthographicSize * 2 * targetRatio, cam.orthographicSize * 2); } }

    private float targetRatio = (float)Screen.width / (float)Screen.height;
    private float ratio = (float)Screen.width / (float)Screen.height;
    private Camera cam;
    private Rect originViewPort;
    private Vector2 vectorAnchor = new Vector2(0.5f, 0.5f);

    private const float ORTHOGONAL_CAMERA_SIZE = 360.0f;

    private void Awake()
    {
        cam = GetComponent<Camera>();
        originViewPort = cam.rect;
    }

    private void Update()
    {
        if (Mathf.Abs(targetRatio - ratio) > Mathf.Epsilon)
        {
            ratio = Mathf.Lerp(ratio, targetRatio, letterboxMoveSpeed * Time.deltaTime);
            CalculateAndSetCameraRatio(ratio);
        }
    }

    public void SetAspectRatio(ECameraAspectRatio ratio)
    {
        switch (ratio)
        {
            case ECameraAspectRatio.DeviceRatio:
                this.targetRatio = DeviceRatio;
                break;
            case ECameraAspectRatio.Aspect_16_9:
                this.targetRatio = 16.0f / 9.0f;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Force camera to render at given ratio
    /// </summary>
    private void CalculateAndSetCameraRatio(float targetAspect)
    {
        float deviceAspect = DeviceRatio;

        float fullWidth = targetAspect / deviceAspect;
        float fullHeight = deviceAspect / targetAspect;
        bool horizontalLetterbox = deviceAspect <= targetAspect;

        Rect localViewPort = new Rect();
        if (horizontalLetterbox)
        {
            localViewPort.width = 1;
            localViewPort.height = fullHeight;
        }
        else
        {
            localViewPort.width = fullWidth;
            localViewPort.height = 1;
        }

        Rect screenViewPortHorizontal = new Rect();
        Rect screenViewPortVertical = new Rect();

        screenViewPortHorizontal.width = originViewPort.width;
        screenViewPortHorizontal.height = originViewPort.width * (localViewPort.height / localViewPort.width);
        screenViewPortHorizontal.x = originViewPort.x;
        screenViewPortHorizontal.y = Mathf.Lerp(originViewPort.y, originViewPort.y + (originViewPort.height - screenViewPortHorizontal.height), vectorAnchor.y);

        screenViewPortVertical.width = originViewPort.height * (localViewPort.width / localViewPort.height);
        screenViewPortVertical.height = originViewPort.height;
        screenViewPortVertical.x = Mathf.Lerp(originViewPort.x, originViewPort.x + (originViewPort.width - screenViewPortVertical.width), vectorAnchor.x);
        screenViewPortVertical.y = originViewPort.y;

        if (screenViewPortHorizontal.height >= screenViewPortVertical.height && screenViewPortHorizontal.width >= screenViewPortVertical.width)
        {
            if (screenViewPortHorizontal.height <= originViewPort.height && screenViewPortHorizontal.width <= originViewPort.width)
            {
                cam.rect = screenViewPortHorizontal;
            }
            else
            {
                cam.rect = screenViewPortVertical;
            }
        }
        else
        {
            if (screenViewPortVertical.height <= originViewPort.height && screenViewPortVertical.width <= originViewPort.width)
            {
                cam.rect = screenViewPortVertical;
            }
            else
            {
                cam.rect = screenViewPortHorizontal;
            }
        }
    }

    //#region Gizmos
    //private void OnDrawGizmos()
    //{
    //    //Camera tempCam = Camera.main;

    //    //Gizmos.color = Color.green;
    //    //Gizmos.DrawWireCube(Vector3.zero, new Vector3(tempCam.orthographicSize * 2 * 16.0f / 9.0f, tempCam.orthographicSize * 2, 1));
    //    //Gizmos.DrawWireCube(Vector3.zero, new Vector3(tempCam.orthographicSize * 2 * 19.5f / 9.0f, tempCam.orthographicSize * 2, 1));
    //    //Gizmos.DrawWireCube(Vector3.zero, new Vector3(tempCam.orthographicSize * 2 * 4.0f / 3.0f, tempCam.orthographicSize * 2, 1));

    //    //Gizmos.color = camBorderColor;
    //    //Gizmos.DrawWireCube(Vector3.zero, new Vector3(tempCam.orthographicSize * 2 * tempCam.aspect, tempCam.orthographicSize * 2, 1));
    //}
    //#endregion
}