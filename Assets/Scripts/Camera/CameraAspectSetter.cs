﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAspectSetter : MonoBehaviour
{
    public CameraController.ECameraAspectRatio aspectRatio;

    private void Start()
    {
        CameraController camController = Camera.main.GetComponent<CameraController>();
        Debug.Assert(camController != null);

        camController.SetAspectRatio(aspectRatio);
    }
}
