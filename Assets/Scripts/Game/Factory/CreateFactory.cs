﻿using UnityEngine;
using System.Collections;
using Spine;
using Spine.Unity;
using UnityEngine.UI;
/// <summary>
/// 생성 클래스
/// </summary>
public class CreateFactory : MonoBehaviour
{
    const string SKELETONDATA = "_SkeletonData";

    /// <summary>
    /// 게임 오브젝트 생성
    /// </summary>
    /// <param name="objectType">"오브젝트 종류"</param>
    /// <param name="rootType">상위 폴더 종류</param>
    /// <param name="leafType">하위 폴더 종류</param>
    /// <param name="ImageName">불러올 오브젝트 이름</param>
    /// <param name="attach">부모 트랜스폼에 붙을지 여부</param>
    /// <param name="parentTransform">붙을 부모 트랜스폼</param>
    /// <param name="sortingOrder">정렬순서</param>
    /// <param name="position">위치</param>
    /// <returns>완성된 오브젝트</returns>
    public GameObject CreateGameObject(Constants.EObjectType objectType,Constants.EFolderRootType rootType, Constants.EFolderLeafType leafType, string ImageName, bool attach = false, Transform parentTransform = null,int sortingOrder = 1, Vector2 position = default)
    {
        GameObject tempObject = new GameObject();
        tempObject.name = ImageName;
        ImageName = rootType.ToString() + "/" + leafType.ToString() + "/" + ImageName;

        switch (objectType)
        {
            case Constants.EObjectType.Prefab:
                {
                    tempObject = Instantiate(Resources.Load(ImageName) as GameObject);
                }
                break;
            case Constants.EObjectType.Spine:
                {
                    ImageName += SKELETONDATA;

                    SkeletonDataAsset skeletonDataAsset = Resources.Load<SkeletonDataAsset>(ImageName);
                    tempObject = SkeletonAnimation.NewSkeletonAnimationGameObject(skeletonDataAsset).gameObject;
                    tempObject.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;
                }
                break;
            case Constants.EObjectType.SpriteRenderer:
                {
                    tempObject.AddComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(ImageName);
                    tempObject.GetComponent<SpriteRenderer>().sortingOrder = sortingOrder;
                }
                break;

            case Constants.EObjectType.Image:
                {
                    tempObject.AddComponent<Image>().sprite = Resources.Load<Sprite>(ImageName);
                    tempObject.GetComponent<Image>().SetNativeSize();
                    tempObject.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(tempObject.GetComponent<Image>().rectTransform.sizeDelta.x * 0.01f * Constants.SPRITE_PIXELS_PER_UNIT, tempObject.GetComponent<Image>().rectTransform.sizeDelta.y * 0.01f * Constants.SPRITE_PIXELS_PER_UNIT);
                }
                break;

            case Constants.EObjectType.SpriteMask:
                {
                    tempObject.AddComponent<SpriteMask>().sprite = Resources.Load<Sprite>(ImageName);
                }
                break;
            default:
                break;
        }

        if (attach)
        {
            tempObject.transform.SetParent(parentTransform);
            tempObject.transform.localPosition = position;
            tempObject.transform.localScale = Vector2.one;
        }

        Debug.Log("GameObject Path == " + ImageName);

        return tempObject;
    }
    /// <summary>
    /// 스파인 생성
    /// </summary>
    /// <param name="rootType">상위 폴더 종류</param>
    /// <param name="leafType">하위 폴더 종류</param>
    /// <param name="ImageName">불러올 skeletonData 이름</param>
    /// <returns>완성된 스켈레톤애니메이션</returns>
    public SkeletonAnimation CreateSkeletonAnimation(Constants.EFolderRootType rootType,Constants.EFolderLeafType leafType, string ImageName)
    {
        ImageName = rootType.ToString() + "/" + leafType.ToString() + "/" + ImageName + SKELETONDATA;

        SkeletonDataAsset skeletonDataAsset = Resources.Load<SkeletonDataAsset>(ImageName);
        SkeletonAnimation skeletonAnimation = SkeletonAnimation.NewSkeletonAnimationGameObject(skeletonDataAsset);
        return skeletonAnimation;
    }
    /// <summary>
    /// 스프라이트 생성
    /// </summary>
    /// <param name="rootType">상위 폴더 종류</param>
    /// <param name="leafType">하위 폴더 종류</param>
    /// <param name="ImageName">불러올 스프라이트 이름</param>
    /// <returns>완성된 스프라이트 이미지</returns>
    public Sprite CreateSprite(Constants.EFolderRootType rootType, Constants.EFolderLeafType leafType, string ImageName)
    {
        ImageName = rootType.ToString() + "/" + leafType.ToString() + "/" + ImageName;
        Sprite sprite = Resources.Load<Sprite>(ImageName);
        return sprite;
    }
    /// <summary>
    /// 텍스쳐 생성 
    /// </summary>
    /// <param name="rootType">상위 폴더 종류</param>
    /// <param name="leafType">하위 폴더 종류</param>
    /// <param name="ImageName">불러올 텍스쳐 이름</param>
    /// <returns>완성된 텍스쳐</returns>
    public Texture2D CreateTexture(Constants.EFolderRootType rootType, Constants.EFolderLeafType leafType, string ImageName)
    {
        ImageName = rootType.ToString() + "/" + leafType.ToString() + "/" + ImageName;

        Texture2D texture = Resources.Load<Texture2D>(ImageName);
        return texture;
    }

    /// <summary>
    /// 스크립트오브젝트 생성
    /// </summary>
    /// <param name="rootType">상위 폴더 종류</param>
    /// <param name="leafType">하위 폴더 종류</param>
    /// <param name="dataName">불러올 데이터 이름</param>
    /// <returns></returns>
    public ScriptableObject CreateScriptableObject(Constants.EFolderRootType rootType,Constants.EFolderLeafType leafType , string dataName)
    {
        dataName = rootType.ToString() + "/" + leafType.ToString() + "/" + dataName;

        ScriptableObject scriptableObject = Resources.Load<ScriptableObject>(dataName);
        return scriptableObject;
    }
}
