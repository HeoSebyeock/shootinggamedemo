﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.Events;
using Spine.Unity;

public class IronPororo_Game2_Actor : MonoBehaviour
{
    public enum EAnimation
    {
        Idle = 1,
        Fly,
        PowerFly,
        Attack,
        Down,
        Happy,
    }

    [Header("적과 부딪혔을때 이벤트(메인)")]
    [SerializeField]
    private UnityEvent _enemyCrashEvent;

    [Header("적의 총알에 맞았을때 이벤트(메인)")]
    [SerializeField]
    private UnityEvent _enemyBulletCrashEvent;

    [Header("슈퍼 아이템")]
    [SerializeField]
    private UnityEvent _superItemGetEvent;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoMoveAction");
    }

    IEnumerator CoMoveAction()
    {
        yield return new WaitForSeconds(1.5f);
        transform.DOLocalMoveY(300,1).SetEase(Ease.OutBack).SetRelative();
        yield return new WaitForSeconds(1);
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Idle).ToString(), true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.Enemy.ToString()))
        {
            _enemyCrashEvent.Invoke();
            
        }
        else if (collision.tag.Equals(Constants.ETag.EnemyNormalLaiser.ToString()))
        {
            collision.gameObject.SetActive(false);
            _enemyBulletCrashEvent.Invoke();
        }
        else if (collision.tag.Equals(Constants.ETag.SuperItem.ToString()))
        {
            collision.gameObject.SetActive(false);
            _superItemGetEvent.Invoke();
        }
    }

}
