﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Spine.Unity;

public class IronPororo_Game3_Actor : MonoBehaviour
{
    enum ELaiserRocation
    {
        Left,
        Right,
        Size,
    }

    public enum EAnimation
    {
        Fly = 1,
        Attack,
        LaiserReady,
        LaiserPlay,
        Happy,
    }

    [Header("레이저 부모 트랜스폼")]
    [SerializeField]
    private Transform _laiserParentTransform;

    [Header("피격 당했을 경우 나오는 이펙트 부모 트랜스폼")]
    [SerializeField]
    private Transform _laiserEffectParentTransform;

    private Transform _powerLaiserObjectPoolingTransform;
    private float[] _successCollectAreaPos = new float[] { 450, 450, 100, 720 - 200 };
    
    private Vector3[] _laiserAddPos = new Vector3[] { new Vector3(50,-50),new Vector3(80,-15)};

    private const int SHIELD_CHILD = 1;
    private const int SUPER_ITEM_CHILD = 2;
    private const float COMPLETE_FEVER_COUNT = 100;
    private const int LAISER_COLLIDER = 3;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoGameLoop");
    }

    IEnumerator CoGameLoop()
    {
        Util.GetInstance.IsNotTouched = true;
        _powerLaiserObjectPoolingTransform = transform.GetChild(0);
        GetComponent<UserInputGestureAction>().SetSuccessCollectArea(_successCollectAreaPos);
        yield return new WaitForSeconds(1.5f);
        transform.DOLocalRotate(Vector3.zero, 1);
        yield return new WaitForSeconds(1.25f);
        GetComponent<SkeletonAnimation>().state.ClearTracks();
        GetComponent<SkeletonAnimation>().skeleton.SetToSetupPose();
        
        transform.DOLocalMoveY(-200, 1.5f).SetRelative();
        yield return new WaitForSeconds(1.5f);
        GetComponent<SkeletonAnimation>().state.SetAnimation(0,((int)EAnimation.Fly).ToString(),true);
        yield return new WaitForSeconds(1f);
        StartCoroutine("CoPororoAttackAction");
        Util.GetInstance.IsNotTouched = false;
    }

    IEnumerator CoPororoAttackAction()
    {
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Attack).ToString(), true);
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            for (int i = 0; i < (int)ELaiserRocation.Size; i++)
            {
                GameObject laizer = GetComponent<ObjectPooling>().GetObject(_laiserParentTransform);
                laizer.transform.localRotation = Quaternion.Euler(Vector3.zero);
                laizer.transform.localScale = Vector2.one * 0.4f;
                laizer.transform.localPosition = transform.localPosition + _laiserAddPos[i];
                laizer.transform.DOScale(0.7f, 0.75f);
                laizer.transform.DOLocalMoveX(Util.GetInstance.GetDesignSize().x + 100, 0.75f).SetRelative().OnComplete(() =>
                {
                    GetComponent<ObjectPooling>().ReturnObject(laizer);
                });
            }
            
            yield return new WaitForSeconds(0.577f);
        }
    }

    //IEnumerator CoPororoPowerAttackAction()
    //{
    //    GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.PowerAttack).ToString(), true);
    //    while (true)
    //    {
    //        yield return new WaitForSeconds(0.1f);
    //        for (int i = 0; i < (int)ELaiserRocation.Size; i++)
    //        {
    //            GameObject laizer = _powerLaiserObjectPoolingTransform.GetComponent<ObjectPooling>().GetObject(_laiserParentTransform);
    //            laizer.transform.localRotation = Quaternion.Euler(Vector3.zero);
    //            laizer.transform.localScale = Vector2.one * 0.5f;
    //            laizer.transform.localPosition = transform.localPosition + _laiserAddPos[i];
    //            laizer.transform.DOScale(0.7f, 0.75f);
    //            laizer.transform.DOLocalMoveX(Util.GetInstance.GetDesignSize().x + 100, 0.75f).SetRelative().OnComplete(() =>
    //            {
    //                _powerLaiserObjectPoolingTransform.GetComponent<ObjectPooling>().ReturnObject(laizer);
    //            });
    //        }

    //        yield return new WaitForSeconds(0.577f);
    //    }
    //}

    IEnumerator CoPororoLaiserAction()
    {
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.LaiserReady).ToString(), false);
        GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)EAnimation.LaiserPlay).ToString(), true, 0);
        yield return new WaitForSeconds(0.25f);
        transform.GetChild(LAISER_COLLIDER).DOScaleX(1, 0.25f);
        //yield return new WaitForSeconds(3.5f);
        //GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Attack).ToString(), true);
        //StartCoroutine("CoPettyAttackAction");
        //gameObject.tag = Constants.ETag.Untagged.ToString();
        //transform.GetChild(LAISER_COLLIDER).DOScaleX(0, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.Untagged.ToString()) && gameObject.tag.Equals(Constants.ETag.Untagged.ToString()))
        {
            IronPororo_Game3_Enemy.currentFeverHitCount += 4 / COMPLETE_FEVER_COUNT;
            IronPororo_Game3.feverGaugeBar.DOFillAmount(IronPororo_Game3_Enemy.currentFeverHitCount, 0.2f);

            if (collision.name.Contains("Normal"))
            {
                HitDefault();
                GameObject laiserEffect = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs.ToString() + "/" + Constants.EFolderLeafType.Game.ToString() + "/CaptainPetty_Laiser_Effect") as GameObject, _laiserEffectParentTransform);
                laiserEffect.transform.localPosition = collision.transform.localPosition + new Vector3(25,0);
                laiserEffect.transform.localScale = new Vector2(0.5f, 0.5f);
                laiserEffect.transform.GetChild(0).gameObject.SetActive(true);

                StartCoroutine(CoLaiserEffect(laiserEffect));
                collision.gameObject.SetActive(false);
            }
            else if(collision.name.Contains("Power"))
            {
                HitDefault();
                collision.gameObject.SetActive(false);
                GameObject powerLaiserObj = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs.ToString() + "/" + Constants.EFolderLeafType.Game.ToString() + "/IronPororo_Game3_PowerLaiserEffect") as GameObject,transform);
                powerLaiserObj.transform.localPosition = new Vector3(105, 0);
                powerLaiserObj.GetComponent<SkeletonAnimation>().state.ClearTracks();
                powerLaiserObj.GetComponent<SkeletonAnimation>().skeleton.SetToSetupPose();
                powerLaiserObj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", false);
            }

            StopCoroutine("CoHitAction");
            StartCoroutine("CoHitAction");
        }

        else if (collision.tag.Equals(Constants.ETag.SuperItem.ToString()))
        {
            collision.gameObject.SetActive(false);
            StartCoroutine("CoSuperItemAction");
        }
    }

    IEnumerator CoSuperItemAction()
    {
        IronPororo_Game3_Enemy.currentFeverHitCount += 8 / COMPLETE_FEVER_COUNT;
        IronPororo_Game3.feverGaugeBar.DOFillAmount(IronPororo_Game3_Enemy.currentFeverHitCount, 0.2f);
        transform.GetChild(SUPER_ITEM_CHILD).gameObject.SetActive(true);
        transform.GetChild(SUPER_ITEM_CHILD).GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", false);
        yield return new WaitForSeconds(0.667f);
        transform.GetChild(SUPER_ITEM_CHILD).gameObject.SetActive(false);
    }

    IEnumerator CoLaiserEffect(GameObject laiserEffect)
    {
        yield return new WaitForSeconds(0.1f);
        laiserEffect.transform.GetChild(0).gameObject.SetActive(false);
        laiserEffect.transform.GetChild(1).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        Destroy(laiserEffect);
    }

    IEnumerator CoHitAction()
    {
        //GetComponent<SkeletonAnimation>().state.ClearTracks();
        //GetComponent<SkeletonAnimation>().skeleton.SetToSetupPose();
        transform.GetChild(SHIELD_CHILD).gameObject.SetActive(true);
        transform.GetChild(SHIELD_CHILD).GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", false);
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Fly).ToString(), false);
        yield return new WaitForSeconds(0.667f);
        transform.GetChild(SHIELD_CHILD).gameObject.SetActive(false);
        
        //yield return new WaitForSeconds(1);
        //float waitTime = GetComponent<SpineAnimation>().BlinkAction(blinkCount);
        //yield return new WaitForSeconds(waitTime * 0.5f);
        
        //Util.GetInstance.IsNotTouched = false;
        //yield return new WaitForSeconds(waitTime * 0.5f);
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Attack).ToString(), true);
        //gameObject.tag = Constants.ETag.Untagged.ToString();
        //no fever mode
        
        StartCoroutine("CoPororoAttackAction");
    }

    public void HitDefault()
    {
        //Util.GetInstance.IsNotTouched = true;
        //GetComponent<UserInputGestureAction>().StopProgressEvent();
        //gameObject.tag = Constants.ETag.HitLaiser.ToString();
        StopCoroutine("CoPororoAttackAction");
    }

    public void FeverModeCallBack()
    {
        //fever
        gameObject.tag = Constants.ETag.FeverMode.ToString();
        StopCoroutine("CoPororoAttackAction");
        StopCoroutine("CoHitAction");
        StartCoroutine("CoPororoLaiserAction");
    }

    public void FeverModeFinishCallBack()
    {
        gameObject.tag = Constants.ETag.Untagged.ToString();
        transform.GetChild(LAISER_COLLIDER).DOScaleX(0, 0);
        StartCoroutine("CoPororoAttackAction");
        StopCoroutine("CoPororoLaiserAction");
    }

    public void EndGameCallBack()
    {
        transform.DOKill();
        GetComponent<BoxCollider2D>().enabled = false;
        StopCoroutine("CoPororoAttackAction");
        StopCoroutine("CoPororoLaiserAction");
        StopCoroutine("CoHitAction");
        GetComponent<UserInputGestureAction>().StopProgressEvent();
        GetComponent<UserInputGestureAction>().enabled = false;
    }
}
