﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using Spine.Unity;
using UnityEngine.Events;

public class IronPororo_Game3_Enemy : MonoBehaviour
{
    public enum EAnimation
    {
        Idle = 1,
        Attack,
        PowerAttacked,
        PowerAttack,
        Sturn,
    }

    [Header("피버모드 이벤트(메인)")]
    [SerializeField]
    private UnityEvent _feverModeEvent;

    [Header("피버모드 이벤트(주인공)")]
    [SerializeField]
    private UnityEvent _feverModeActorEvent;

    [Header("피격 당했을 경우 나오는 이펙트 부모 트랜스폼")]
    [SerializeField]
    private Transform _laiserEffectParentTransform;

    [Header("죽을때 폭발 이펙트")]
    [SerializeField]
    private SkeletonAnimation _explosionEffectSkeletonAnimation;

    [Header("레이저 부모 트랜스폼")]
    [SerializeField]
    private Transform _laiserParentTransform;

    [Header("게임 끝 이벤트(메인)")]
    [SerializeField]
    private UnityEvent _endGameEvent;

    [Header("게임 끝 이벤트(주인공)")]
    [SerializeField]
    private UnityEvent _endGameActorEvent;

    private Sequence _moveActionSequence;
    private Image _gaugeBar;
    private float _currentHitCount = 1;
    private bool _isFever = false;

    public static float currentFeverHitCount = 0;

    private const float ENEMY_START_POSX = 1112.0f;
    private const float ENEMY_BOUNDARY_HEIGHT = 150;
    private const float MOVE_DURING_TIME = 1.5f;
    private const float COMPLETE_HIT_COUNT = 100;
    private const float COMPLETE_FEVER_COUNT = 100;
    private const float LAISER_DELAY_TIME = 0.667f;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoGameLoop");
    }

    IEnumerator CoGameLoop()
    {
        _gaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
        _gaugeBar.fillAmount = 1;
        yield return new WaitForSeconds(4.5f);
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.PowerAttack).ToString(), false);
        GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)EAnimation.Attack).ToString(), true, 0);
        transform.DOLocalMoveX(ENEMY_START_POSX, 1).SetEase(Ease.OutCubic);
        yield return new WaitForSeconds(1.0f);
        MoveAction();
        StartCoroutine("CoAttackAction");
    }


    void MoveAction()
    {
        _moveActionSequence = DOTween.Sequence();
        _moveActionSequence.AppendCallback(() =>
        {
            transform.DOLocalMoveY(Util.GetInstance.GetDesignSize().y - ENEMY_BOUNDARY_HEIGHT, MOVE_DURING_TIME);
        });
        _moveActionSequence.AppendInterval(MOVE_DURING_TIME );
        _moveActionSequence.AppendCallback(() => {
            transform.DOLocalMoveY(ENEMY_BOUNDARY_HEIGHT - 100, MOVE_DURING_TIME);
        });
        _moveActionSequence.AppendInterval(MOVE_DURING_TIME);
        _moveActionSequence.SetLoops(-1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.tag.Equals(Constants.ETag.ActorAttack.ToString()))
        {
            if (collision.name.Contains("Normal"))
            {
                LaiserEffectVisible(collision.gameObject);
                collision.gameObject.SetActive(false);
                _currentHitCount -= 1 / COMPLETE_HIT_COUNT;
                currentFeverHitCount += 4 / COMPLETE_FEVER_COUNT;
                IronPororo_Game3.feverGaugeBar.DOFillAmount(currentFeverHitCount, 0.2f);
                _gaugeBar.DOFillAmount(_currentHitCount, 0.2f);
            }
            else
            {
                StartCoroutine("CoLaiserAttacked");
            }
            

            if(_gaugeBar.fillAmount <= 0f)
            {
                //end game
                StartCoroutine("CoCompleteGame");
                
            }
            
            if (currentFeverHitCount >= 1.0f && !_isFever)
            {
                //fever mode
                _isFever = true;
                StopCoroutine("CoAttackAction");
                StopCoroutine("CoMissileAttack");
                _moveActionSequence.Play();
                GetComponent<SkeletonAnimation>().timeScale = 1.0f;
                GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Sturn).ToString(), true);
                _feverModeEvent.Invoke();
                _feverModeActorEvent.Invoke();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.ActorAttack.ToString()) && collision.name.Contains("Laiser"))
        {
            StopCoroutine("CoLaiserEffect");
            StopCoroutine("CoLaiserAttacked");
        }
    }

    //petty laiser
    IEnumerator CoLaiserAttacked()
    {
        while (true)
        {
            //StopCoroutine("CoAttackAction");
            yield return new WaitForSeconds(0.1f);
            //GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Sturn).ToString(), true);
            _currentHitCount -= 1 / COMPLETE_HIT_COUNT;
            _gaugeBar.DOFillAmount(_currentHitCount, 0.2f);
            LaiserEffectVisible();
            if (_currentHitCount <= 0)
            {
                StopCoroutine("CoLaiserAttacked");
                StartCoroutine("CoCompleteGame");
            }
        }
    }

    IEnumerator CoCompleteGame()
    {
        foreach (Transform laiser in _laiserParentTransform)
        {
            laiser.gameObject.SetActive(false);
        }
        
        Time.timeScale = 0.2f;
        transform.DOKill();
        _explosionEffectSkeletonAnimation.transform.localPosition = transform.localPosition;
        _explosionEffectSkeletonAnimation.state.SetAnimation(0, "1", false);
        _moveActionSequence.Kill();
        GetComponent<BoxCollider2D>().enabled = false;
        StopCoroutine("CoAttackAction");
        StopCoroutine("CoMissileAttack");

        _endGameEvent.Invoke();
        _endGameActorEvent.Invoke();
        yield return new WaitForSeconds(0.5f);
        Time.timeScale = 1f;
        
    }

    private void LaiserEffectVisible(GameObject collisionObject = null)
    {
        GameObject laiserEffect = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs.ToString() + "/" + Constants.EFolderLeafType.Game.ToString() + "/CaptainPetty_Laiser_Effect") as GameObject, _laiserEffectParentTransform);
        if (collisionObject == null)
        {
            laiserEffect.transform.localPosition = transform.localPosition;
        }
        else
        {
            laiserEffect.transform.localPosition = collisionObject.transform.localPosition;
        }
        laiserEffect.transform.localScale = new Vector2(0.5f, 0.5f);
        laiserEffect.transform.GetChild(0).gameObject.SetActive(true);
        StartCoroutine(CoLaiserEffect(laiserEffect));
    }

    IEnumerator CoLaiserEffect(GameObject laiserEffect)
    {
        yield return new WaitForSeconds(0.1f);
        laiserEffect.transform.GetChild(0).gameObject.SetActive(false);
        laiserEffect.transform.GetChild(1).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        Destroy(laiserEffect);
    }

    IEnumerator CoAttackAction()
    {
        yield return new WaitForSeconds(0.5f);
        StopCoroutine("CoMissileAttack");
        StartCoroutine("CoMissileAttack");
        while (true)
        {
            GameObject laiser = GetComponent<ObjectPooling>().GetObject(_laiserParentTransform);
            laiser.transform.localPosition = transform.localPosition + new Vector3(-40,0);
            laiser.transform.localScale = Vector2.one * 0.4f;
            laiser.transform.DOLocalMoveX(-Util.GetInstance.GetDesignSize().x, 2f);
            laiser.transform.DOScale(0.6f, 2f);
            yield return new WaitForSeconds(LAISER_DELAY_TIME);
        }
    }

    IEnumerator CoMissileAttack()
    {
        while (true)
        {
            yield return new WaitForSeconds(3f);
            StopCoroutine("CoAttackAction");
            transform.DOPause();
            _moveActionSequence.Pause();
            GetComponent<SkeletonAnimation>().timeScale = 0.3f;
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.PowerAttack).ToString(), false);
            GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)EAnimation.Attack).ToString(), true, 0);
            yield return new WaitForSeconds(0.65f);
            GetComponent<SkeletonAnimation>().timeScale = 0.0f;
            yield return new WaitForSeconds(0.1f);
            GetComponent<SkeletonAnimation>().timeScale = 1.0f;
            yield return new WaitForSeconds(0.2f);
            transform.DOScaleY(transform.localScale.y * 1f, 0.25f);
            
            GameObject missile = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs.ToString() + "/" + Constants.EFolderLeafType.Game.ToString() + "/IronPororo_Game3_EnemyPowerLaiser") as GameObject, _laiserParentTransform);
            missile.transform.localPosition = transform.localPosition + new Vector3(-80,-10);
            missile.transform.localScale = Vector2.one;
            missile.SetActive(true);
            
            missile.transform.DOLocalMoveX(-Util.GetInstance.GetDesignSize().x, 2f);
            //yield return new WaitForSeconds(_laiserTime * 2f);
            StopCoroutine("CoAttackAction");
            StartCoroutine("CoAttackAction");
            _moveActionSequence.Play();
            transform.DOPlay();
        }
    }

    public void FeverModeFinishCallBack()
    {
        currentFeverHitCount = 0;
        _isFever = false;
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Attack).ToString(), true);
        StopCoroutine("CoLaiserAttacked");

        StartCoroutine("CoAttackAction");
    }
}
