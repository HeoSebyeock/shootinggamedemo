﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.UI;

public class IronPororo_Game2 : MonoBehaviour
{
    enum EButton
    {
        Up,
        Down,
    }

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("뽀로로")]
    [SerializeField]
    private SkeletonAnimation _pororoSkeletonAnimation;

    [Header("버튼 부모 트랜스폼")]
    [SerializeField]
    private Transform _buttonParentTransform;

    [Header("배경 트랜스폼")]
    [SerializeField]
    private Transform _backTransform;

    [Header("슈퍼 아이템")]
    [SerializeField]
    private Transform[] _superItemTransforms;

    [Header("포비")]
    [SerializeField]
    private SkeletonAnimation _pobySkeletonAnimation;

    [Header("패티")]
    [SerializeField]
    private SkeletonAnimation _pettySkeletonAnimation;

    [Header("3번 슈팅 게임")]
    [SerializeField]
    private GameObject _shootingGame;

    private int _currentFloor = 0;
    private Image _gaugeBar;
    private float _currentHitCount = 0;

    private const float FLOOR_HEIGHT = 240.0f;
    private const float COMPLETE_HIT_COUNT = 18;
    private const int EXPLOSION_CHILD = 0;
    private const int HIT_EFFECT_CHILD = 1;
    private const float SUPERITEM_MOVE_TIME = 4.0f;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoGameLoop");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            
            _clickParticle.StartAction(Input.mousePosition);
        }
    }

    IEnumerator CoGameLoop()
    {
        Util.GetInstance.IsNotTouched = true;
        _gaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
        for (int i = 0; i < _superItemTransforms.Length; i++)
        {
            Sequence moveSuperItem = DOTween.Sequence();
            moveSuperItem.Append(_superItemTransforms[i].transform.DOLocalMoveX(100, SUPERITEM_MOVE_TIME));
            moveSuperItem.Append(_superItemTransforms[i].transform.DOLocalMoveX(Util.GetInstance.GetDesignSize().x - 100, SUPERITEM_MOVE_TIME));
            moveSuperItem.SetLoops(-1);
        }
        yield return new WaitForSeconds(1.5f);
        //pororo animation wait time
        yield return new WaitForSeconds(1.25f);
        //action 1
        transform.DOLocalMoveY(-4320, 2).SetEase(Ease.InQuad);
        yield return new WaitForSeconds(2.75f);
        transform.DOLocalMoveY(0, 1);
        yield return new WaitForSeconds(1);

        //action 2
        //transform.DOScale(0.2f, 2);
        //transform.DOLocalMoveX(500, 2);
        //yield return new WaitForSeconds(2.25f);
        //transform.DOScale(1f, 2);
        //transform.DOLocalMoveX(0, 2);

        Util.GetInstance.IsNotTouched = false;
    }

    public void ButtonDown()
    {
        _buttonParentTransform.GetChild((int)EButton.Up).gameObject.SetActive(false);
        _buttonParentTransform.GetChild((int)EButton.Down).gameObject.SetActive(true);
    }
    public void ButtonUp()
    {
        if (Util.GetInstance.IsNotTouched)
        {
            return;
        }

        _currentHitCount += 1 / COMPLETE_HIT_COUNT;
        _gaugeBar.DOFillAmount(_currentHitCount, 0.2f);

        StartCoroutine("CoUpstairAction");

        _buttonParentTransform.GetChild((int)EButton.Up).gameObject.SetActive(true);
        _buttonParentTransform.GetChild((int)EButton.Down).gameObject.SetActive(false);
    }

    public void ButtonUpFail()
    {
        _buttonParentTransform.GetChild((int)EButton.Up).gameObject.SetActive(true);
        _buttonParentTransform.GetChild((int)EButton.Down).gameObject.SetActive(false);
    }

    IEnumerator CoUpstairAction()
    {
        Util.GetInstance.IsNotTouched = true;
        _currentFloor++;
        
        _pororoSkeletonAnimation.GetComponent<BoxCollider2D>().enabled = false;
        _pororoSkeletonAnimation.state.SetAnimation(0, ((int)IronPororo_Game2_Actor.EAnimation.Fly).ToString(), true);
        _backTransform.DOLocalMoveY(-FLOOR_HEIGHT, 0.2f).SetRelative();
        yield return new WaitForSeconds(0.2f);
        _pororoSkeletonAnimation.state.SetAnimation(0, ((int)IronPororo_Game2_Actor.EAnimation.Idle).ToString(), true);

        if (_currentFloor == COMPLETE_HIT_COUNT)
        {
            Util.GetInstance.IsNotTouched = true;
            StartCoroutine("CoCompleteGame");
        }
        else
        {
            Util.GetInstance.IsNotTouched = false;
            _pororoSkeletonAnimation.GetComponent<BoxCollider2D>().enabled = true;
        }
    }

    IEnumerator CoCompleteGame()
    {
        yield return new WaitForSeconds(0.5f);
        _pobySkeletonAnimation.transform.DOLocalMoveX(-300, 1f).SetRelative();
        yield return new WaitForSeconds(1f);
        _pobySkeletonAnimation.state.SetAnimation(0, "2", false);
        _pettySkeletonAnimation.state.SetAnimation(0, "3", true);
        yield return new WaitForSeconds(0.667f);
        _pobySkeletonAnimation.state.ClearTracks();
        _pobySkeletonAnimation.skeleton.SetToSetupPose();
        _pobySkeletonAnimation.transform.DOScaleY(0.7f, 0.15f).SetEase(Ease.Flash);
        _pobySkeletonAnimation.transform.DOLocalMoveY(-25, 0.15f).SetRelative();
        yield return new WaitForSeconds(0.15f);
        //_pororoAnimation.state.SetAnimation(0, ((int)IronPororo_Game1_Actor.EAnimation.Fly).ToString(), true);
        yield return new WaitForSeconds(0.55f);
        _pobySkeletonAnimation.transform.DOScaleY(1.0f, 0.2f);
        _pobySkeletonAnimation.transform.DOLocalMove(new Vector2(0, 500), 0.25f).SetRelative();
        yield return new WaitForSeconds(0.25f);
        

        _pororoSkeletonAnimation.state.ClearTracks();
        _pororoSkeletonAnimation.skeleton.SetToSetupPose();
        _pororoSkeletonAnimation.transform.DOScaleY(0.8f, 0.15f).SetEase(Ease.Flash);
        _pororoSkeletonAnimation.transform.DOLocalMoveY(-25, 0.15f).SetRelative();
        yield return new WaitForSeconds(0.15f);
        //_pororoAnimation.state.SetAnimation(0, ((int)IronPororo_Game1_Actor.EAnimation.Fly).ToString(), true);
        yield return new WaitForSeconds(0.55f);
        _pororoSkeletonAnimation.transform.DOScaleY(1.1f, 0.2f);
        _pororoSkeletonAnimation.transform.DOLocalMove(new Vector2(0, Util.GetInstance.GetDesignSize().y), 0.25f).SetRelative();
        yield return new WaitForSeconds(0.55f);
        gameObject.transform.parent.gameObject.AddComponent<TransitionImageAction>().CreateTransitionImage("circle", TransitionImageAction.EType.ZoomInOut, new Vector2(Util.GetInstance.GetDesignSize().x * 0.5f, Util.GetInstance.GetDesignSize().y * 0.5f), 1, 0);
        gameObject.transform.parent.gameObject.GetComponent<TransitionImageAction>().StartAction();
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);
        _shootingGame.SetActive(true);
    }

    public void ActorEnemyCrashCallBack()
    {
        if (Util.GetInstance.IsNotTouched)
        {
            return;
        }
        _currentHitCount -= (_currentFloor % 3) / COMPLETE_HIT_COUNT;
        _gaugeBar.DOFillAmount(_currentHitCount, 0.2f);
        StartCoroutine("CoActorEnemyCrashCallBack");
    }

    IEnumerator CoActorEnemyCrashCallBack()
    {
        Util.GetInstance.IsNotTouched = true;
        _pororoSkeletonAnimation.GetComponent<BoxCollider2D>().enabled = false;
        _pororoSkeletonAnimation.transform.GetChild(HIT_EFFECT_CHILD).GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", false);
        yield return new WaitForSeconds(0.15f);
        _pororoSkeletonAnimation.state.SetAnimation(0, ((int)IronPororo_Game2_Actor.EAnimation.Down).ToString(), true);
        
        _backTransform.DOLocalMoveY(FLOOR_HEIGHT * (_currentFloor % 3), 0.2f * (_currentFloor % 3)).SetRelative();
        _pororoSkeletonAnimation.transform.DOScaleY(0.65f, 0.2f * (_currentFloor % 3));
        yield return new WaitForSeconds(0.2f * (_currentFloor % 3));

        _pororoSkeletonAnimation.state.ClearTracks();
        _pororoSkeletonAnimation.skeleton.SetToSetupPose();

        _pororoSkeletonAnimation.transform.DOPunchPosition(new Vector2(0, 50), 0.2f, 0).SetEase(Ease.Flash);
        yield return new WaitForSeconds(0.2f);
        _pororoSkeletonAnimation.transform.DOPunchPosition(new Vector2(0, 20), 0.05f, 0).SetEase(Ease.Flash);
        yield return new WaitForSeconds(0.05f);

        _currentFloor = _currentFloor / 3 * 3;

        
        yield return new WaitForSeconds(0.5f);
        
        _pororoSkeletonAnimation.transform.DOScaleY(1,0.25f).SetEase(Ease.OutBounce);
        _pororoSkeletonAnimation.transform.DOPunchPosition(new Vector2(0,75), 0.35f, 0);
        _pororoSkeletonAnimation.state.SetAnimation(0, ((int)IronPororo_Game2_Actor.EAnimation.Idle).ToString(), true);
        _pororoSkeletonAnimation.timeScale = 1f;
        Util.GetInstance.IsNotTouched = false;
        _pororoSkeletonAnimation.GetComponent<BoxCollider2D>().enabled = true;
    }

    public void ActorEnemyBulletCrashCallBack()
    {
        if (Util.GetInstance.IsNotTouched)
        {
            return;
        }
        _currentHitCount -= (_currentFloor % 3) / COMPLETE_HIT_COUNT;
        _gaugeBar.DOFillAmount(_currentHitCount, 0.2f);
        StartCoroutine("CoActorEnemyBulletCrashCallBack");
    }

    IEnumerator CoActorEnemyBulletCrashCallBack()
    {
        Util.GetInstance.IsNotTouched = true;
        _pororoSkeletonAnimation.GetComponent<BoxCollider2D>().enabled = false;
        _pororoSkeletonAnimation.transform.GetChild(EXPLOSION_CHILD).GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", false);
        yield return new WaitForSeconds(0.1f);
        _pororoSkeletonAnimation.state.SetAnimation(0, ((int)IronPororo_Game2_Actor.EAnimation.Down).ToString(), true);
        _backTransform.DOLocalMoveY(FLOOR_HEIGHT * (_currentFloor % 3), 0.75f * (_currentFloor % 3)).SetRelative();
        yield return new WaitForSeconds(0.75f * (_currentFloor % 3));
        _currentFloor = _currentFloor / 3 * 3;
        _pororoSkeletonAnimation.state.SetAnimation(0, ((int)IronPororo_Game2_Actor.EAnimation.Idle).ToString(), true);
        Util.GetInstance.IsNotTouched = false;
        _pororoSkeletonAnimation.GetComponent<BoxCollider2D>().enabled = true;
    }

    public void SuperItemGetCallBack()
    {
        if (Util.GetInstance.IsNotTouched)
        {
            return;
        }
        _currentHitCount += 2 / COMPLETE_HIT_COUNT;
        _gaugeBar.DOFillAmount(_currentHitCount, 0.2f);
        StartCoroutine("CoSuperItemGetCallBack");
    }

    IEnumerator CoSuperItemGetCallBack()
    {
        Util.GetInstance.IsNotTouched = true;
        _currentFloor+= 2;

        _pororoSkeletonAnimation.GetComponent<BoxCollider2D>().enabled = false;
        _pororoSkeletonAnimation.state.SetAnimation(0, ((int)IronPororo_Game2_Actor.EAnimation.PowerFly).ToString(), true);
        _backTransform.DOLocalMoveY(-(FLOOR_HEIGHT * 2), 0.4f).SetRelative();
        yield return new WaitForSeconds(0.4f);
        _pororoSkeletonAnimation.state.SetAnimation(0, ((int)IronPororo_Game2_Actor.EAnimation.Idle).ToString(), true);

        if (_currentFloor == COMPLETE_HIT_COUNT)
        {
            StartCoroutine("CoCompleteGame");
        }
        else
        {
            Util.GetInstance.IsNotTouched = false;
            _pororoSkeletonAnimation.GetComponent<BoxCollider2D>().enabled = true;
        }
    }
}
