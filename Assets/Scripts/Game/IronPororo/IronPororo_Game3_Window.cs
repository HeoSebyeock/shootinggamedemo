﻿using UnityEngine;
using System.Collections;
using Spine.Unity;

public class IronPororo_Game3_Window : MonoBehaviour
{

    private int _currentWindowNum = 0;

    private const int WINDOW_MAX_COUNT = 3;

    // Use this for initialization
    void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.name.Contains("Enemy")){
            if(_currentWindowNum < WINDOW_MAX_COUNT)
            {
                collision.gameObject.SetActive(false);
                StartCoroutine("CoWindowEffect");
                transform.GetChild(_currentWindowNum).gameObject.SetActive(false);
                transform.GetChild(_currentWindowNum + 1).gameObject.SetActive(true);
                _currentWindowNum++;
            }
        }
    }

    IEnumerator CoWindowEffect()
    {
        GameObject windowEffect = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs.ToString() + "/" + Constants.EFolderLeafType.Game.ToString() + "/IronPororo_Game3_WindowEffect") as GameObject, transform);
        windowEffect.transform.localPosition = new Vector2(100,-50);
        windowEffect.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", false);
        windowEffect.transform.GetChild(0).localPosition = new Vector2(100, 50);
        windowEffect.transform.GetChild(0).GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", false);
        yield return new WaitForSeconds(0.667f);
        windowEffect.SetActive(false);
    }
}
