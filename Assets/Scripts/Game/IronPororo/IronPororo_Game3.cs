﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Spine.Unity;
using UnityEngine.Events;
using DG.Tweening;

public class IronPororo_Game3 : MonoBehaviour
{
    enum EPettyAnimation
    {
        Idle = 1,
        Surprised,
        Happy,
    }

    [Header("패티")]
    [SerializeField]
    private SkeletonAnimation _pettySkeletonAnimation;

    [Header("뽀로로")]
    [SerializeField]
    private SkeletonAnimation _pororoSkeletonAnimation;

    [Header("포비")]
    [SerializeField]
    private SkeletonAnimation _pobySkeletonAnimation;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("성공 파티클")]
    [SerializeField]
    private Particles _successParticle;

    [Header("FEVER_BG_PARENT_TRANSFORM")]
    [SerializeField]
    private Transform _feverLoopBgParentTransform;

    [Header("피버모드 이벤트 종료(주인공)")]
    [SerializeField]
    private UnityEvent _feverModeFinishActorEvent;

    [Header("피버모드 이벤트 종료(악당)")]
    [SerializeField]
    private UnityEvent _feverModeFinishEnemyEvent;

    [Header("슈퍼 아이템 부모 트랜스폼")]
    [SerializeField]
    private Transform _superItemParentTransform;

    public static Image feverGaugeBar;

    private const float SUPER_ITEM_PADDING = 100;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoGameLoop");
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }

            _clickParticle.StartAction(Input.mousePosition);
        }
    }

    IEnumerator CoGameLoop()
    {
        Util.GetInstance.IsNotTouched = true;
        feverGaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
        yield return new WaitForSeconds(1.5f);
        Util.GetInstance.IsNotTouched = false;

        StartCoroutine("CoSuperItemFactory");
    }

    public void FeverModeCallBack()
    {
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Surprised).ToString(), true);
        _feverLoopBgParentTransform.GetChild(0).gameObject.SetActive(true);
        _feverLoopBgParentTransform.GetComponent<LoopImageAction>().StartAction();
        StopCoroutine("CoSuperItemFactory");
        StartCoroutine("CoFeverTimer");
    }

    IEnumerator CoFeverTimer()
    {
        while (true)
        {
            feverGaugeBar.fillAmount -= Time.deltaTime * 0.2f;
            if(feverGaugeBar.fillAmount <= 0)
            {
                //fever finish
                _feverLoopBgParentTransform.GetComponent<LoopImageAction>().LoopStop();
                for (int i = 0; i < _feverLoopBgParentTransform.childCount; i++)
                {
                    _feverLoopBgParentTransform.GetChild(i).gameObject.SetActive(false);
                }
                _feverModeFinishActorEvent.Invoke();
                _feverModeFinishEnemyEvent.Invoke();
                StartCoroutine("CoSuperItemFactory");
                yield break;
            }
            yield return null;
        }
    }

    IEnumerator CoSuperItemFactory()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(7,9));
            
            GameObject superItem = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs + "/" + Constants.EFolderLeafType.Game + "/IronPororo_Game3_SuperItem") as GameObject,_superItemParentTransform);
            superItem.transform.localPosition = new Vector2(Util.GetInstance.GetDesignSize().x + SUPER_ITEM_PADDING, Random.Range(150,Util.GetInstance.GetDesignSize().y - SUPER_ITEM_PADDING));
            superItem.transform.DOLocalMoveX(-150, Random.Range(4, 6)).OnComplete(()=> {
                superItem.SetActive(false);
            });
        }
    }

    public void EndGameCallBack()
    {
        //end game
        Util.GetInstance.IsNotTouched = true;
        foreach (Transform superItem in _superItemParentTransform)
        {
            superItem.gameObject.SetActive(false);
        }
        StopCoroutine("CoSuperItemFactory");
        StopCoroutine("CoFeverTimer");
        //fever finish
        _feverLoopBgParentTransform.GetComponent<LoopImageAction>().LoopStop();
        for (int i = 0; i < _feverLoopBgParentTransform.childCount; i++)
        {
            _feverLoopBgParentTransform.GetChild(i).gameObject.SetActive(false);
        }
        GetComponent<GameGauge>().GaugeDestroy(true);
        StartCoroutine("CoCompleteGame");
    }

    IEnumerator CoCompleteGame()
    {
        _pobySkeletonAnimation.state.SetAnimation(0, ((int)IronPororo_Game3_Enemy.EAnimation.Sturn).ToString(), true);
        yield return new WaitForSeconds(0.5f);
        _pobySkeletonAnimation.transform.DOLocalRotate(new Vector3(0, 0, -90), 1);
        _pobySkeletonAnimation.transform.DOLocalJump(new Vector2(Util.GetInstance.GetDesignSize().x, -200), 50, 1, _pobySkeletonAnimation.transform.localPosition.y / 250);

        _pororoSkeletonAnimation.state.SetAnimation(0, ((int)IronPororo_Game3_Actor.EAnimation.Happy).ToString(), true);
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Happy).ToString(), true);
        yield return new WaitForSeconds(0.5f);
        
        
        yield return new WaitForSeconds(1);
        _pororoSkeletonAnimation.state.SetAnimation(0, ((int)IronPororo_Game3_Actor.EAnimation.Fly).ToString(), true);
        _pororoSkeletonAnimation.transform.DOLocalJump(new Vector2(120,633), 300, 1, 3).SetEase(Ease.Flash);
        _pororoSkeletonAnimation.transform.DOScale(0.8f, 3);
        yield return new WaitForSeconds(3);
        _successParticle.StartAction(_pettySkeletonAnimation.transform.localPosition + new Vector3(75,50));
        yield return new WaitForSeconds(1);
        //success action need add
        //transform.DOLocalMove(new Vector2(),)
        _pororoSkeletonAnimation.transform.DOLocalMove(new Vector2(25, -25),0.5f).SetRelative();
        _pororoSkeletonAnimation.state.SetAnimation(0, ((int)IronPororo_Game3_Actor.EAnimation.Attack).ToString(), false);
        yield return new WaitForSeconds(0.5f);
        _pororoSkeletonAnimation.timeScale = 0;
        //_pettySkeletonAnimation.timeScale = 0;
        yield return new WaitForSeconds(0.5f);
        _pororoSkeletonAnimation.transform.DOLocalMoveY(300, 0.25f).SetRelative();
        _pettySkeletonAnimation.transform.DOLocalMoveY(300, 0.25f).SetRelative();
        yield return new WaitForSeconds(0.25f);
        GameManager.GameEndCallBackEvent.Invoke();
    }
}
