﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;

public class IronPororo_Game1_Actor : MonoBehaviour
{
    public enum EAnimation
    {
        Fly = 1,
        Idle,
        LeftAttack,
        RightAttack,
        Happy,
    }

    [Header("레이저 부모 트랜스폼")]
    [SerializeField]
    private Transform _laiserParentTransform;

    private bool _isLeft = false;

    private Vector2 _leftLaiserCreatePos = new Vector2(623,150);
    private Vector2 _rightLaiserCreatePos = new Vector2(663,150);

    private const float LAISER_SHOT_DISTANCE = 500.0f;

    private float _laiserAngle;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            if(Input.mousePosition.y <= 300)
            {
                return;
            }
            GameObject Laiser = GetComponent<ObjectPooling>().GetObject(_laiserParentTransform);
            Laiser.transform.DOScaleY(1, 0).OnComplete(()=> { Laiser.transform.GetChild(0).GetComponent<SpriteRenderer>().DOFade(0, 0.25f).OnComplete(()=> {
                Laiser.SetActive(false);
            }); });
            //left
            if (Input.mousePosition.x < Util.GetInstance.GetDesignSize().x * 0.5f)
            {
                _laiserAngle = -Mathf.Atan2(_leftLaiserCreatePos.x - Input.mousePosition.x, _leftLaiserCreatePos.y - Input.mousePosition.y) * 180 / Mathf.PI + 180;
                Laiser.transform.localPosition = _leftLaiserCreatePos;
                GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.LeftAttack).ToString(), false);
                _isLeft = true;
            }
            else
            {
                _laiserAngle = -Mathf.Atan2(_rightLaiserCreatePos.x - Input.mousePosition.x, _rightLaiserCreatePos.y - Input.mousePosition.y) * 180 / Mathf.PI + 180;
                Laiser.transform.localPosition = _rightLaiserCreatePos;
                GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.RightAttack).ToString(), false);
                _isLeft = false;
            }
            Laiser.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, _laiserAngle));
            Laiser.transform.GetChild(0).DOLocalMoveY(LAISER_SHOT_DISTANCE, 0.25f).SetRelative();
            GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)EAnimation.Idle).ToString(), true, 0);
            StartCoroutine("CoAttackAction");
        }
    }

    IEnumerator CoAttackAction()
    {
        yield return new WaitForSeconds(0.2f);
    }
}
