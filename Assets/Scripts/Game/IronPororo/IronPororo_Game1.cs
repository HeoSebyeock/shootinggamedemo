﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.Events;

public class IronPororo_Game1 : MonoBehaviour
{
    

    enum EExtraCharacter
    {
        Eddy,
        Crong,
    }

    enum ERocation
    {
        Left,
        Right,
        Size,
    }

    private Vector3[] _extraCharacterFirstPos = new Vector3[] { new Vector3(291,240), new Vector3(1052,182) };

    private const int CHILD_SHADOW = 0;
    private const float OBSTACLE_LEFT_DOWN_POSX = 200f;
    private const float OBSTACLE_RIGHT_DOWN_POSX = 1052f;
    private const float OBSTACLE_DOWN_TIME = 2.5f;

    [Header("뽀로로")]
    [SerializeField]
    private SkeletonAnimation _pororoAnimation;

    [Header("엑스트라 캐릭터 부모 트랜스폼")]
    [SerializeField]
    private Transform _extraParentTransform;

    [Header("장애물 부모 트랜스폼")]
    [SerializeField]
    private Transform _obstacleParentTransform;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("전체 파티클")]
    [SerializeField]
    private Particles _fullParticle;

    [Header("전체 파티클2")]
    [SerializeField]
    private Particles _full2Particle;

    [Header("겁먹기 시작을 알리는 이벤트(에디)")]
    [SerializeField]
    private UnityEvent _veryScaredEventEddy;

    [Header("겁먹기 시작을 알리는 이벤트(크롱)")]
    [SerializeField]
    private UnityEvent _veryScaredEventCrong;

    public static Image gaugeBar;
    public static float currentHitGauge = 0;

    private bool _isCompleteGame = false;

    private float _obstacleCreateDelayTime = 2f;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoGameLoop");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }

            _clickParticle.StartAction(Input.mousePosition);
        }

        if(currentHitGauge >= 1.0f && !_isCompleteGame)
        {
            //end game;
            _isCompleteGame = true;
            Util.GetInstance.IsNotTouched = true;
            StartCoroutine("CoCompleteGame");
        }
    }

    IEnumerator CoCompleteGame()
    {
        StopCoroutine("CoObstacleDownAction");

        for (int i = 1; i < _extraParentTransform.GetChild(0).childCount; i++)
        {
            _extraParentTransform.GetChild(0).GetChild(i).transform.DOKill();
            _extraParentTransform.GetChild(0).GetChild(i).gameObject.GetComponent<SpineAnimation>().Fade(_extraParentTransform.GetChild(0).GetChild(i).gameObject.GetComponent<SkeletonAnimation>(), 0,0.5f);
        }
        for (int i = 1; i < _extraParentTransform.GetChild(1).childCount; i++)
        {
            _extraParentTransform.GetChild(1).GetChild(i).transform.DOKill();
            _extraParentTransform.GetChild(1).GetChild(i).gameObject.GetComponent<SpineAnimation>().Fade(_extraParentTransform.GetChild(1).GetChild(i).gameObject.GetComponent<SkeletonAnimation>(), 0, 0.5f);
        }

        _fullParticle.StartAction(new Vector2(Util.GetInstance.GetDesignSize().x * 0.5f, Util.GetInstance.GetDesignSize().y * 0.5f));
        _full2Particle.StartAction(new Vector2(Util.GetInstance.GetDesignSize().x * 0.5f, Util.GetInstance.GetDesignSize().y * 0.5f));
        GetComponent<GameGauge>().GaugeDestroy(true);
        for (int i = 0; i < _extraParentTransform.childCount; i++)
        {
            _extraParentTransform.GetChild(i).GetChild(0).GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)IronPororo_Game1_ExtraCharacter.EAnimation.Happy).ToString(), true);
        }

        _pororoAnimation.state.SetAnimation(0, ((int)IronPororo_Game1_Actor.EAnimation.Happy).ToString(), true);
        yield return new WaitForSeconds(4);
        _pororoAnimation.state.ClearTracks();
        _pororoAnimation.skeleton.SetToSetupPose();
        _pororoAnimation.transform.DOScaleY(0.9f, 0.15f).SetEase(Ease.Flash);
        _pororoAnimation.transform.DOLocalMoveY(-25, 0.15f).SetRelative();
        yield return new WaitForSeconds(0.15f);
        //_pororoAnimation.state.SetAnimation(0, ((int)IronPororo_Game1_Actor.EAnimation.Fly).ToString(), true);
        yield return new WaitForSeconds(0.55f);
        _pororoAnimation.transform.DOScaleY(1.1f, 0.2f);
        _pororoAnimation.transform.DOLocalMoveY(Util.GetInstance.GetDesignSize().y,0.2f).SetRelative();
        yield return new WaitForSeconds(0.3f);
        GameManager.GameEndCallBackEvent.Invoke();
    }

    IEnumerator CoGameLoop()
    {
        Util.GetInstance.IsNotTouched = true;
        yield return new WaitForSeconds(1.5f);
        gaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
        _pororoAnimation.transform.DOLocalMoveY(Util.GetInstance.GetDesignSize().y - 509, 3).SetEase(Ease.OutSine);
        yield return new WaitForSeconds(3);
        _pororoAnimation.transform.GetChild(CHILD_SHADOW).GetComponent<SpriteRenderer>().DOFade(1, 0.5f);
        _pororoAnimation.state.SetAnimation(0, ((int)IronPororo_Game1_Actor.EAnimation.Idle).ToString(), true);
        StartCoroutine("CoObstacleDownAction");
        Util.GetInstance.IsNotTouched = false;
    }

    IEnumerator CoObstacleDownAction()
    {
        while (true)
        {
            int randRocation = Random.Range((int)ERocation.Left, (int)ERocation.Size);

            GameObject obstacle = null;
            
            switch ((ERocation)randRocation)
            {
                case ERocation.Left:
                    {
                        _veryScaredEventEddy.Invoke();
                        obstacle = GetComponent<ObjectPooling>().GetObject(_extraParentTransform.GetChild((int)EExtraCharacter.Eddy).transform);
                        obstacle.transform.localPosition = new Vector2(OBSTACLE_LEFT_DOWN_POSX, Util.GetInstance.GetDesignSize().y + 200);
                        //_leftScaredActionCoroutine = StartCoroutine(CoScaredAction(_extraParentTransform.GetChild((int)EExtraCharacter.Eddy).GetChild(0).transform));
                        _extraParentTransform.GetChild((int)EExtraCharacter.Eddy).GetChild(0).GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)IronPororo_Game1_ExtraCharacter.EAnimation.VeryScared).ToString(), true);
                    }
                    break;
                case ERocation.Right:
                    {
                        _veryScaredEventCrong.Invoke();
                        obstacle = GetComponent<ObjectPooling>().GetObject(_extraParentTransform.GetChild((int)EExtraCharacter.Crong).transform);
                        obstacle.transform.localPosition = new Vector2(OBSTACLE_RIGHT_DOWN_POSX, Util.GetInstance.GetDesignSize().y + 200);
                        //_rightScaredActionCoroutine = StartCoroutine(CoScaredAction(_extraParentTransform.GetChild((int)EExtraCharacter.Crong).GetChild(0).transform));
                        _extraParentTransform.GetChild((int)EExtraCharacter.Crong).GetChild(0).GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)IronPororo_Game1_ExtraCharacter.EAnimation.VeryScared).ToString(), true);
                    }
                    break;
                default:
                    break;
            }

            obstacle.transform.DOLocalMoveY(-Util.GetInstance.GetDesignSize().y, OBSTACLE_DOWN_TIME).SetEase(Ease.InSine);

            yield return new WaitForSeconds(_obstacleCreateDelayTime - gaugeBar.fillAmount);
        }
    }
}
