﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Spine.Unity;

public class IronPororo_Game2_Enemy : MonoBehaviour
{
    enum EAnimation
    {
        Idle = 1,
        Cry,
        Attack,
    }

    [Header("움직이는데 걸리는 시간")]
    [SerializeField]
    private float _moveDuringTime;

    [Header("시작 위치 (좌/우)")]
    [SerializeField]
    private bool _isLeft;

    [Header("레이저 On/Off")]
    [SerializeField]
    private bool _isLaiser;

    [Header("레이저 부모 트랜스폼")]
    [SerializeField]
    private Transform _laiserParentTransform;

    [Header("레이저 번호")]
    [SerializeField]
    private int _laiserNum;

    private Sequence _moveActionSequence;

    private const float MOVE_PADDING_WIDTH = 200.0f;

    // Use this for initialization
    void Start()
    {
        MoveAction();
        if (_isLaiser)
        {
            StartCoroutine("CoLaiserFactory");
        }
    }

    void MoveAction()
    {
        int rocation = -1;
        if (_isLeft)
        {
            rocation = 1;
        }
        _moveActionSequence = DOTween.Sequence();
        _moveActionSequence.Append(transform.DOLocalMoveX(Util.GetInstance.GetDesignSize().x * rocation - MOVE_PADDING_WIDTH * rocation, _moveDuringTime).SetRelative());
        _moveActionSequence.AppendCallback(() => { transform.localScale = new Vector2(transform.localScale.x * -1, 1); });
        _moveActionSequence.Append(transform.DOLocalMoveX(-Util.GetInstance.GetDesignSize().x * rocation + MOVE_PADDING_WIDTH * rocation, _moveDuringTime).SetRelative());
        _moveActionSequence.AppendCallback(() => { transform.localScale = new Vector2(transform.localScale.x * -1, 1); });
        _moveActionSequence.SetLoops(-1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.Actor.ToString()))
        {
            _moveActionSequence.Pause();
            StopCoroutine("CoLaiserFactory");
            StartCoroutine("CoActorCrashAction");
        }
    }

    IEnumerator CoActorCrashAction()
    {
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Attack).ToString(), false);
        transform.DOPunchPosition(new Vector2(0,150), 0.35f, 0).SetEase(Ease.OutBounce);
        yield return new WaitForSeconds(0.667f);
        _moveActionSequence.Play();
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Idle).ToString(), true);
        if (_isLaiser)
        {
            StartCoroutine("CoLaiserFactory");
        }
    }

    IEnumerator CoLaiserFactory()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(3, 5));
            _moveActionSequence.Pause();
            transform.DOPause();
            float scaleX = transform.localScale.x;
            transform.DOScale(new Vector2(scaleX * 0.75f, 1.25f), 0.15f).SetEase(Ease.Flash);
            GetComponent<SkeletonAnimation>().timeScale = 0;
            yield return new WaitForSeconds(0.35f);
            transform.DOScale(new Vector2(scaleX * 1f, 1f), 0.15f).SetEase(Ease.Flash);
            GetComponent<SkeletonAnimation>().timeScale = 1;

            GameObject laiser = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs.ToString() + "/" + Constants.EFolderLeafType.Game.ToString() + "/CaptainPetty_EnemyLaiser" + _laiserNum.ToString()) as GameObject, _laiserParentTransform);
            laiser.SetActive(true);
            laiser.transform.localPosition = transform.localPosition + new Vector3(50 * -transform.localScale.x,-25);
            laiser.transform.DOLocalMoveX((Util.GetInstance.GetDesignSize().x + MOVE_PADDING_WIDTH) * -transform.localScale.x, _moveDuringTime * 0.75f).SetRelative().OnComplete(()=> { Destroy(laiser); });
            yield return new WaitForSeconds(_moveDuringTime * 0.1f);
            
            _moveActionSequence.Play();
            transform.DOPlay();
        }
    }
}
