﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.Events;

public class IronPororo_Game1_ExtraCharacter : MonoBehaviour
{
    public enum EAnimation
    {
        Scared = 1,
        VeryScared,
        Cry,
        Happy,
    }

    [Header("캐릭터 처음 위치")]
    [SerializeField]
    private Vector2 _firstPos;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.Untagged.ToString()))
        {
            collision.transform.DOKill();
            collision.GetComponent<BoxCollider2D>().enabled = false;
            StartCoroutine(CoObstacleHitAction(collision.GetComponent<SkeletonAnimation>()));
            StopCoroutine("CoScaredAction");
        }
    }

    IEnumerator CoObstacleHitAction(SkeletonAnimation obstacleSkeletonAnimation)
    {
        transform.DOKill();
        transform.localPosition = _firstPos;
        GetComponent<SpineAnimation>().BlinkAction(2);
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Cry).ToString(), false);
        GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)EAnimation.Scared).ToString(), true, 0);
        obstacleSkeletonAnimation.state.SetAnimation(0, "1", false);
        yield return new WaitForSeconds(0.5f);
        
        Destroy(obstacleSkeletonAnimation.gameObject);
        
    }

    public void ScaredCallBack()
    {
        StartCoroutine("CoScaredAction");
    }

    public void ScaredStopActionCallBack()
    {
        StopCoroutine("CoScaredAction");
        transform.DOKill();
        transform.localPosition = _firstPos;
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Scared).ToString(), true);
    }

    IEnumerator CoScaredAction()
    {
        while (true)
        {
            transform.DOLocalMoveX(-50, 0.175f).SetRelative();
            yield return new WaitForSeconds(0.175f);
            transform.DOLocalMoveX(50, 0.175f).SetRelative();
            yield return new WaitForSeconds(0.175f);
            transform.DOLocalMoveX(50, 0.175f).SetRelative();
            yield return new WaitForSeconds(0.175f);
            transform.DOLocalMoveX(-50, 0.175f).SetRelative();
            yield return new WaitForSeconds(0.175f);
        }
    }
}
