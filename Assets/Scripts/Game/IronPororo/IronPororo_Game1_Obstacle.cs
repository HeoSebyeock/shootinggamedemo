﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Spine.Unity;

public class IronPororo_Game1_Obstacle : MonoBehaviour
{
    const int LAISER_HIT_ANIMATION = 2;
    const float COMPLETE_HIT_COUNT = 15;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.Laiser.ToString()))
        {
            GetComponent<BoxCollider2D>().enabled = false;
            StartCoroutine("CoObstacleBrokenAction");
        }
    }

    IEnumerator CoObstacleBrokenAction()
    {
        IronPororo_Game1.currentHitGauge += 1 / COMPLETE_HIT_COUNT;
        IronPororo_Game1.gaugeBar.DOFillAmount(IronPororo_Game1.currentHitGauge, 0.15f);
        transform.DOKill();
        
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)LAISER_HIT_ANIMATION).ToString(), false);
        transform.parent.GetChild(0).GetComponent<IronPororo_Game1_ExtraCharacter>().ScaredStopActionCallBack();
        yield return new WaitForSeconds(0.5f);
        
        Destroy(gameObject);
    }
}
