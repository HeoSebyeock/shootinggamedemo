﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.UI;

public class ThorPoby_Game2 : MonoBehaviour
{
    enum EPobyOneStepAnimation {
        Idle = 1,
        Swing,
    }

    enum EPobyTwoStepAnimation
    {
        Idle = 1,
        One,
        Two,
        Three,
        HammerShot,
        Happy
    }

    public enum ELevel
    {
        Eazy,
        Normal,
        Hard,
        Final,
    }

    public enum ECloudLocation
    {
        Left,
        Right,
        Middle,
    }

    [Header("노말 포비")]
    [SerializeField]
    private SkeletonAnimation _pobySkeletonAnimation;

    [Header("플라이 포비")]
    [SerializeField]
    private SkeletonAnimation _flypobySkeletonAnimation;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("망치")]
    [SerializeField]
    private GameObject _hammerCollider;

    [Header("구름 부모 트랜스폼")]
    [SerializeField]
    private Transform _cloudParentTransform;

    [Header("성공 파티클")]
    [SerializeField]
    public static Particles successParticle;

    [Header("실패 파티클")]
    [SerializeField]
    public static Particles failParticle;

    [Header("생성 위치 부모 트랜스폼")]
    [SerializeField]
    private Transform _holeParentTransform;

    private Vector2 _pobyIdlePos = new Vector2(1028,351);
    //터치한 지점에 내려치도록 위치값을 더한다
    private Vector3 _addHamerPos = new Vector3(75,90);
    //구멍에 존재하는지 여부
    static public bool[] isExist = new bool[HOLE_COUNT];
    //현재 게임 단계
    public static ELevel _currentLevel = ELevel.Eazy;

    private EPobyTwoStepAnimation _currentFlyPobyAnimation = EPobyTwoStepAnimation.One;
    private int _cloudTouchCount = 0;

    public static float currentGauge;
    public static Image gaugeBar;
    
    private const int HOLE_COUNT = 7;
    private const int THUNDER = 3;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoGameLoop");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            _clickParticle.StartAction(Input.mousePosition);

            if (_currentLevel != ELevel.Final && Input.mousePosition.x > 100 && Util.GetInstance.GetDesignSize().x - 100 > Input.mousePosition.x
                && Input.mousePosition.y > 100 && Util.GetInstance.GetDesignSize().y - 100 > Input.mousePosition.y)
            {
                CollectAreaTap();
            }

        }

        if(gaugeBar == null)
        {
            return;
        }

        ////test
        //if (gaugeBar.fillAmount >= 0f && _currentLevel == ELevel.Hard)
        //{
        //    _currentLevel = ELevel.Final;
        //    Util.GetInstance.IsNotTouched = true;
        //    StopCoroutine("CoMonsterFactory");
        //    StartCoroutine("CoFinalStepInitailize");
        //}

        if (gaugeBar.fillAmount >= 0.2f && _currentLevel == ELevel.Eazy)
        {
            _currentLevel = ELevel.Normal;
        }
        else if (gaugeBar.fillAmount >= 0.5f && _currentLevel == ELevel.Normal)
        {
            _currentLevel = ELevel.Hard;
        }
        else if(gaugeBar.fillAmount >= 1f && _currentLevel == ELevel.Hard)
        {
            _currentLevel = ELevel.Final;
            Util.GetInstance.IsNotTouched = true;
            _cloudParentTransform.gameObject.SetActive(true);
            StartCoroutine("CoFinalStepInitailize");
        }
        
    }

    public void CollectAreaTap()
    {
        StartCoroutine("CoHammerShot");
    }

    IEnumerator CoFinalStepInitailize()
    {
        yield return new WaitForSeconds(0.25f);
        StopCoroutine("CoMonsterFactory");
        GetComponent<GameGauge>().GaugeDestroy(true);
        _pobySkeletonAnimation.state.ClearTracks();
        _pobySkeletonAnimation.skeleton.SetToSetupPose();
        _pobySkeletonAnimation.transform.DOScaleY(0.9f, 0.25f);
        _pobySkeletonAnimation.transform.DOLocalMoveY(-10, 0.25f).SetRelative();
        yield return new WaitForSeconds(0.75f);
        _pobySkeletonAnimation.transform.DOScaleY(1.1f, 0.25f);
        _pobySkeletonAnimation.transform.DOLocalMoveY(Util.GetInstance.GetDesignSize().y + 200, 0.5f).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < _holeParentTransform.childCount; i++)
        {
            GameObject monster = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs + "/" + Constants.EFolderLeafType.Game + "/" + "ThorHoleMonster") as GameObject);
            monster.transform.SetParent(_holeParentTransform.transform.GetChild(i));
            monster.transform.localPosition = Vector2.zero;
            StartCoroutine(CoMonsterScaredAction(monster.transform));
        }
        
        _flypobySkeletonAnimation.transform.DOLocalMoveY(Util.GetInstance.GetDesignSize().y - 378, 5);
        yield return new WaitForSeconds(2.5f);
        StartCoroutine("CoCloudMoveAction");
        yield return new WaitForSeconds(3.0f);
        Util.GetInstance.IsNotTouched = false;
    }
    IEnumerator CoMonsterScaredAction(Transform monsterTransform)
    {
        monsterTransform.DOLocalJump(new Vector2(0, 90), 10, 1, 0.25f).SetRelative();
        yield return new WaitForSeconds(0.25f);

        while (true)
        {
            monsterTransform.DOLocalMoveX(1.25f, 0.075f);
            yield return new WaitForSeconds(0.075f);
            monsterTransform.DOLocalMoveX(-1.25f, 0.075f);
            yield return new WaitForSeconds(0.075f);
        }

    }
    IEnumerator CoCloudMoveAction()
    {
        _cloudParentTransform.transform.GetChild((int)ECloudLocation.Left).DOLocalMoveX(522, 3);
        _cloudParentTransform.transform.GetChild((int)ECloudLocation.Right).DOLocalMoveX(756, 3);
        _cloudParentTransform.transform.GetChild((int)ECloudLocation.Middle).DOLocalMoveY(635, 3);
        yield return new WaitForSeconds(2);
    }

    IEnumerator CoHammerShot()
    {
        Util.GetInstance.IsNotTouched = true;
        _pobySkeletonAnimation.transform.localPosition = Input.mousePosition + _addHamerPos;
        _pobySkeletonAnimation.state.SetAnimation(0, ((int)EPobyOneStepAnimation.Swing).ToString(), false);
        yield return new WaitForSeconds(0.267f * 0.5f);
        _hammerCollider.SetActive(true);
        yield return new WaitForSeconds(0.267f * 0.5f);
        _pobySkeletonAnimation.state.SetAnimation(0, ((int)EPobyOneStepAnimation.Idle).ToString(), true);
        _pobySkeletonAnimation.transform.localPosition = _pobyIdlePos;
        _hammerCollider.SetActive(false);
        Util.GetInstance.IsNotTouched = false;
    }

    IEnumerator CoGameLoop()
    {
        GameObject successParticleObj = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs + "/" + Constants.EFolderLeafType.Game + "/" + "SuccessParticle") as GameObject);
        successParticle = successParticleObj.GetComponent<Particles>();

        GameObject failParticleObj = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs + "/" + Constants.EFolderLeafType.Game + "/" + "FailParticle") as GameObject);
        failParticle = failParticleObj.GetComponent<Particles>();

        Util.GetInstance.IsNotTouched = true;
        yield return new WaitForSeconds(2);
        _pobySkeletonAnimation.transform.DOLocalMoveY(_pobyIdlePos.y, 1).SetEase(Ease.OutBounce);
        yield return new WaitForSeconds(0.35f);
        transform.DOShakePosition(1f, new Vector2(30f,40f), 10, 90);
        yield return new WaitForSeconds(0.65f);
        _pobySkeletonAnimation.transform.GetChild(1).gameObject.SetActive(true);
        _pobySkeletonAnimation.transform.GetChild(1).GetComponent<SpineAnimation>().Fade(_pobySkeletonAnimation.transform.GetChild(1).GetComponent<SkeletonAnimation>(), 0, 1.25f);
        _pobySkeletonAnimation.transform.GetChild(1).transform.DOPunchScale(new Vector2(0.5f, 0.5f), 1.25f, 0);
        //_pobySkeletonAnimation.transform.GetChild(1).transform.DOScale(new Vector2(1.5f, 1.5f), 1.5f);
        yield return new WaitForSeconds(1f);
        _pobySkeletonAnimation.state.SetAnimation(0, ((int)EPobyOneStepAnimation.Idle).ToString(), true);
        yield return new WaitForSeconds(0.25f);
        gaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
        _pobySkeletonAnimation.transform.GetChild(1).gameObject.SetActive(false);
        Util.GetInstance.IsNotTouched = false;

        StartCoroutine("CoMonsterFactory");
    }

    IEnumerator CoMonsterFactory()
    {
        while (true)
        {
            for (int i = 0; i < Random.Range(1,2);)
            {
                int randNum = Random.Range(0, HOLE_COUNT);
                if (!isExist[randNum])
                {
                    GameObject monster = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs + "/" + Constants.EFolderLeafType.Game + "/" + "ThorHoleMonster") as GameObject);
                    monster.name = randNum.ToString();
                    monster.transform.SetParent(_holeParentTransform.transform.GetChild(randNum));
                    monster.transform.localPosition = Vector2.zero;
                    isExist[randNum] = true;
                    i++;
                }
                yield return null;
            }
            yield return new WaitForSeconds(0.65f - ((float)_currentLevel * 0.25f));
        }
    }

    public void CloudTapped()
    {
        _cloudTouchCount++;
        StartCoroutine("CoCloudAction");
    }
    IEnumerator CoCloudAction()
    {
        //test
        Util.GetInstance.IsNotTouched = true;

        if (_cloudTouchCount == 3)
        {
            _currentFlyPobyAnimation = EPobyTwoStepAnimation.Two;
        }
        else if (_cloudTouchCount == 6)
        {
            _currentFlyPobyAnimation = EPobyTwoStepAnimation.Three;
        }
        else if (_cloudTouchCount == 9)
        {
            _currentFlyPobyAnimation = EPobyTwoStepAnimation.HammerShot;
        }
        
        for (int i = 0; i < _cloudParentTransform.childCount - 1; i++)
        {
            _cloudParentTransform.GetChild(i).transform.DOScale(-0.025f, 0.25f).SetRelative();
            _cloudParentTransform.GetChild(i).GetComponent<SkeletonAnimation>().state.SetAnimation(0, "3", false);
            _cloudParentTransform.GetChild(i).GetComponent<SkeletonAnimation>().state.AddAnimation(0, "1", true, 0);
        }

        _cloudParentTransform.GetChild((int)THUNDER).GetComponent<SkeletonAnimation>().state.SetAnimation(0, "2", false);

        //complete game
        if(_currentFlyPobyAnimation == EPobyTwoStepAnimation.HammerShot)
        {
            _flypobySkeletonAnimation.state.SetAnimation(0, ((int)_currentFlyPobyAnimation - 1).ToString(), false);
            yield return new WaitForSeconds(1f);

            foreach (Transform item in _holeParentTransform)
            {
                item.GetChild(1).transform.DOLocalMoveY(40,0.25f).SetRelative();
            }

            for (int i = 0; i < _cloudParentTransform.childCount; i++)
            {
                _cloudParentTransform.GetChild(i).transform.DOScale(0, 0.5f).SetEase(Ease.InBack);
            }
            yield return new WaitForSeconds(0.5f);
            _flypobySkeletonAnimation.state.SetAnimation(0, ((int)_currentFlyPobyAnimation).ToString(), false);
            yield return new WaitForSeconds(0.15f);
            Time.timeScale = 0.15f;
            transform.DOScale(1.15f, 0.2f);
            yield return new WaitForSeconds(0.2f);
            foreach (Transform item in _holeParentTransform)
            {
                for (int i = 2; i < 9; i++)
                {
                    item.GetChild(1).GetChild(i).GetChild(3).gameObject.SetActive(false);
                    item.GetChild(1).GetChild(i).GetChild(1).gameObject.SetActive(true);
                }
            }
            yield return new WaitForSeconds(0.3f);

            foreach (Transform item in _holeParentTransform)
            {
                item.GetChild(1).transform.DOLocalMoveY(0, 1f);
            }
            transform.localScale = Vector2.one;
            transform.DOShakePosition(2.25f, new Vector2(30f, 30f), 10, 90);
            Time.timeScale = 1f;
            _flypobySkeletonAnimation.state.SetAnimation(0, ((int)_currentFlyPobyAnimation + 1).ToString(), true);
            yield return new WaitForSeconds(3);
            GameManager.GameEndCallBackEvent.Invoke();
        }
        else
        {
            _flypobySkeletonAnimation.state.SetAnimation(0, ((int)_currentFlyPobyAnimation).ToString(), false);
            yield return new WaitForSeconds(1f);
            _flypobySkeletonAnimation.state.SetAnimation(0, ((int)_currentFlyPobyAnimation - 1).ToString(), true);
            Util.GetInstance.IsNotTouched = false;
        }
    }
}
