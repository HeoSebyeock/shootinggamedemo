﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.UI;

public class ThorPoby_Game1 : MonoBehaviour
{
    enum ECloudDragPower
    {
        Idle,
        Weak,
        Strong,
    }

    enum EPobyAnimation
    {
        OneStep_Idle = 1,
        OneStep_Charge,
        OneStep_Happy,
        TwoStep_Idle,
        TwoStep_Charge,
        TwoStep_Happy,
    }

    [Header("포비")]
    [SerializeField]
    private SkeletonAnimation _pobySkeletonAnimation;

    [Header("구름 부모 트랜스폼")]
    [SerializeField]
    private Transform _cloudParent;

    [Header("번개")]
    [SerializeField]
    private SkeletonAnimation _thunderSkeletonAnimation;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("전체 파티클")]
    [SerializeField]
    private Particles _fullParticle;

    [Header("성공 파티클")]
    [SerializeField]
    private Particles _successParticle;

    //touch judge and timer
    private Vector2 _beginTouch;
    private float _cloudTouchTimer;

    private Image _gaugeBar;
    private float _currentGauge;

    private bool _isTwoStep = false;

    private const float LEAST_SWIPE_TIME = 0.25f;
    private const float TIMER_START_DISTANCE = 50f;
    private const float COMPLETE_THUNDER_COUNT = 15f;

    // Use this for initialization
    void Start()
    {
        _gaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            _clickParticle.StartAction(Input.mousePosition);

        }

        if (Input.GetMouseButton(0))
        {
            //swipe start timer
            if (_beginTouch != Vector2.zero && Vector2.Distance(_beginTouch, Input.mousePosition) > TIMER_START_DISTANCE)
            {
                _beginTouch = Vector2.zero;
                StartCoroutine("CoCloudTouchTime");
            }
        }
    }

    public void CloudSwipeComplete()
    {
        StopCoroutine("CoCloudTouchTime");
        StartCoroutine("CoThunderAction");
    }

    public void CloudSwipeStart()
    {
        _beginTouch = Input.mousePosition;
        _successParticle.StartAction(Input.mousePosition);
        for (int i = 0; i < _cloudParent.childCount; i++)
        {
            _cloudParent.GetChild(i).transform.DOKill();
            _cloudParent.GetChild(i).transform.DOScale(1.15f, 0.25f).SetEase(Ease.OutBack);
        }
    }

    public void CloudSwipeFail()
    {
        _beginTouch = Vector2.zero;
        StopCoroutine("CoCloudTouchTime");
        for (int i = 0; i < _cloudParent.childCount; i++)
        {
            _cloudParent.GetChild(i).transform.DOKill();
            _cloudParent.GetChild(i).transform.DOScale(1f, 0.25f).SetEase(Ease.OutBack);
        }
    }

    IEnumerator CoCloudTouchTime()
    {
        while (true)
        {
            _cloudTouchTimer += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator CoThunderAction()
    {
        Util.GetInstance.IsNotTouched = true;
        float actionTime = 0f;
        if (_cloudTouchTimer <= LEAST_SWIPE_TIME) {
            actionTime = LEAST_SWIPE_TIME * 2f;
            _currentGauge += 2 / COMPLETE_THUNDER_COUNT;
        }
        else {
            actionTime = _cloudTouchTimer + LEAST_SWIPE_TIME * 2f;
            _currentGauge += 1 / COMPLETE_THUNDER_COUNT;
        }

        _gaugeBar.DOFillAmount(_currentGauge, 0.25f);

        for (int i = 0; i < _cloudParent.childCount; i++)
        {
            _cloudParent.GetChild(i).transform.DOKill();
            _cloudParent.GetChild(i).transform.DOScale(1f, 0.15f).SetEase(Ease.OutBack);
            _cloudParent.GetChild(i).transform.DOLocalMoveX(380 + (i * -760), actionTime).SetRelative().SetEase(Ease.OutBounce);
        }
        StartCoroutine(CoCloudScaleAction(actionTime));
        yield return new WaitForSeconds(actionTime - actionTime * 0.175f);

        for (int i = 0; i < _cloudParent.childCount; i++)
        {
            if (_cloudTouchTimer <= LEAST_SWIPE_TIME)
            {
                _cloudParent.GetChild(i).GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)ECloudDragPower.Strong + 1).ToString(), false);
                _thunderSkeletonAnimation.state.SetAnimation(0, ((int)ECloudDragPower.Strong).ToString(), false);
            }
            else
            {
                _cloudParent.GetChild(i).GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)ECloudDragPower.Weak + 1).ToString(), false);
                _thunderSkeletonAnimation.state.SetAnimation(0, ((int)ECloudDragPower.Weak).ToString(), false);
            }
            _cloudParent.GetChild(i).GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)ECloudDragPower.Idle + 1).ToString(), true, 0);
        }
        _cloudTouchTimer = 0;

        //two step
        if (_currentGauge >= 0.5f)
        {
            if (!_isTwoStep)
            {
                _isTwoStep = true;
                _pobySkeletonAnimation.state.SetAnimation(0, ((int)EPobyAnimation.OneStep_Charge).ToString(), false);
                _pobySkeletonAnimation.state.AddAnimation(0, ((int)EPobyAnimation.OneStep_Happy).ToString(), false, 0);
                _pobySkeletonAnimation.state.AddAnimation(0, ((int)EPobyAnimation.TwoStep_Idle).ToString(), true, 0);
                yield return new WaitForSeconds(1f);
            }
            //complete game
            else if(_currentGauge >= 1f)
            {
                GetComponent<GameGauge>().GaugeDestroy(true);
                _pobySkeletonAnimation.state.SetAnimation(0, ((int)EPobyAnimation.TwoStep_Charge).ToString(), false);
                _pobySkeletonAnimation.state.AddAnimation(0, ((int)EPobyAnimation.TwoStep_Happy).ToString(), true, 0);
                yield return new WaitForSeconds(1);
                for (int i = 0; i < _cloudParent.childCount; i++)
                {
                    _cloudParent.GetChild(i).GetComponent<SpineAnimation>().Fade(_cloudParent.GetChild(i).GetComponent<SkeletonAnimation>(), 0, 0.5f);
                }
                yield return new WaitForSeconds(0.5f);
                _fullParticle.StartAction(Util.GetInstance.GetDesignSize() * 0.5f);
                yield return new WaitForSeconds(3.0f);
                GameManager.GameEndCallBackEvent.Invoke();
                yield break;
            }
            else
            {
                _pobySkeletonAnimation.state.SetAnimation(0, ((int)EPobyAnimation.TwoStep_Charge).ToString(), false);
                _pobySkeletonAnimation.state.AddAnimation(0, ((int)EPobyAnimation.TwoStep_Idle).ToString(), true, 0);
            }
        }
        //one step
        else
        {
            _pobySkeletonAnimation.state.SetAnimation(0, ((int)EPobyAnimation.OneStep_Charge).ToString(), false);
            _pobySkeletonAnimation.state.AddAnimation(0, ((int)EPobyAnimation.OneStep_Idle).ToString(), true, 0);
        }

        yield return new WaitForSeconds(1f);
        for (int i = 0; i < _cloudParent.childCount; i++)
        {
            _cloudParent.GetChild(i).transform.DOLocalMoveX(-380 + (i * 760), 0.5f).SetRelative();
        }
        yield return new WaitForSeconds(0.5f);
        Util.GetInstance.IsNotTouched = false;
    }

    IEnumerator CoCloudScaleAction(float actionTime)
    {
        yield return new WaitForSeconds(actionTime * 0.275f);
        //_crashParticle.StartAction(new Vector2(650,612));
        for (int i = 0; i < _cloudParent.childCount; i++)
        {
            _cloudParent.GetChild(i).DOPunchScale(new Vector2(-0.25f, 0.25f), 0.25f, 10);
        }
    }
}
