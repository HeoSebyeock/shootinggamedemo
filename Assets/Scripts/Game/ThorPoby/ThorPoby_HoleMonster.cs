﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public class ThorPoby_HoleMonster : MonoBehaviour
{
    enum EImage
    {
        idle,
        hit,
        collider,
        scared,
    }

    enum EState
    {
        YariGariAndUp,
        YariGariAndDown,
        JumpUp,
        Size,
    }

    private GameObject _monster;
    private GameObject _collider = null;

    private bool _isFinalStep = false;

    private const float COMPLETE_HIT_COUNT = 30;

    // Use this for initialization
    void Start()
    {
        if (ThorPoby_Game2._currentLevel != ThorPoby_Game2.ELevel.Final)
        {
            //monster choice
            _monster = transform.GetChild(UnityEngine.Random.Range(0, transform.childCount)).gameObject;
            _collider = _monster.transform.GetChild(2).gameObject;
            //monster action
            StartCoroutine("CoMonsterMoveAction");
        }
        else
        {
            //monster choice
            _monster = transform.GetChild(UnityEngine.Random.Range(2, transform.childCount)).gameObject;
            _monster.transform.GetChild((int)EImage.idle).gameObject.SetActive(false);
            _monster.transform.GetChild((int)EImage.scared).gameObject.SetActive(true);
        }
        _monster.SetActive(true);
    }

    private void Update()
    {
        if(ThorPoby_Game2._currentLevel == ThorPoby_Game2.ELevel.Final && !_isFinalStep && _collider != null)
        {
            _isFinalStep = true;
            StartCoroutine("CoFinalStepInitailize");
        }
    }

    IEnumerator CoFinalStepInitailize()
    {
        StopAllCoroutines();
        transform.DOKill();
        transform.DOLocalMoveY(0, 0.25f).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(0.25f);
        gameObject.SetActive(false);
    }

    IEnumerator CoMonsterMoveAction()
    {
        EState randState = (EState)UnityEngine.Random.Range(0, (int)EState.Size + 1);
        if(randState >= EState.Size)
        {
            randState = EState.JumpUp;
        }
        //EState randState = EState.JumpUp;
        switch (randState)
        {
            case EState.YariGariAndUp:
                {
                    transform.DOLocalJump(new Vector2(0, 65), 10, 1, 0.25f).SetRelative();
                    yield return new WaitForSeconds(0.25f);
                    transform.DOPunchPosition(new Vector2(0,10), 0.15f, 0).SetEase(Ease.OutBack).SetLoops(3);
                    yield return new WaitForSeconds(0.45f);
                    transform.DOLocalJump(new Vector2(0, 85), 10, 1, 0.25f).SetRelative();
                    yield return new WaitForSeconds(0.25f);
                    _collider.SetActive(true);
                    yield return new WaitForSeconds(1.75f - (float)ThorPoby_Game2._currentLevel * 0.35f);
                    _collider.SetActive(false);
                    transform.DOLocalMoveY(0, 0.25f).SetEase(Ease.OutBack);
                }
                break;
            case EState.YariGariAndDown:
                {
                    transform.DOLocalJump(new Vector2(0, 65), 10, 1, 0.25f).SetRelative();
                    yield return new WaitForSeconds(0.25f);
                    transform.DOPunchPosition(new Vector2(0, 10), 0.15f, 0).SetEase(Ease.OutBack).SetLoops(3);
                    yield return new WaitForSeconds(0.45f);
                    transform.DOLocalMoveY(0, 0.25f).SetEase(Ease.OutBack);
                    
                }
                break;
            case EState.JumpUp:
                {
                    transform.DOLocalJump(new Vector2(0, 140), 10, 1, 0.25f).SetRelative();
                    yield return new WaitForSeconds(0.25f);
                    _collider.SetActive(true);
                    yield return new WaitForSeconds(1.75f - (float)ThorPoby_Game2._currentLevel * 0.35f);
                    _collider.SetActive(false);
                    transform.DOLocalMoveY(0, 0.5f).SetEase(Ease.OutBack);
                }
                break;
            default:
                break;
        }
        yield return new WaitForSeconds(0.5f);
        ThorPoby_Game2.isExist[Int32.Parse(name)] = false;
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        StopCoroutine("CoMonsterMoveAction");
        StartCoroutine("CoMonsterDeadAction");
        
        if (_monster.tag.Equals(Constants.ETag.Collect.ToString()))
        {
            Debug.Log("Success action");
            ThorPoby_Game2.currentGauge += 1 / COMPLETE_HIT_COUNT;
            ThorPoby_Game2.successParticle.StartAction(transform.parent.localPosition - Util.GetInstance.GetDesignSize_Vector3() * 0.5f + new Vector3(0,125));
        }
        else if(_monster.tag.Equals(Constants.ETag.Wrong.ToString()))
        {
            ThorPoby_Game2.failParticle.StartAction(transform.parent.localPosition - Util.GetInstance.GetDesignSize_Vector3() * 0.5f + new Vector3(0, 125));
        }
        
        ThorPoby_Game2.gaugeBar.DOFillAmount(ThorPoby_Game2.currentGauge, 0.25f);
    }

    IEnumerator CoMonsterDeadAction()
    {
        //
        transform.parent.parent.parent.DOShakePosition(0.5f, new Vector2(7.5f, 12.5f), 10, 90);
        _collider.SetActive(false);
        if (_monster.tag.Equals(Constants.ETag.Collect.ToString()))
        {
            _monster.transform.DOScaleY(0.75f, 0.15f).SetEase(Ease.OutQuad);
            _monster.transform.DOLocalMoveY(-25, 0.15f).SetEase(Ease.OutQuad);
        }
        
        _monster.transform.GetChild((int)EImage.idle).gameObject.SetActive(false);
        _monster.transform.GetChild((int)EImage.hit).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        transform.DOLocalMoveY(0, 1f);
        yield return new WaitForSeconds(1.5f);
        ThorPoby_Game2.isExist[Int32.Parse(name)] = false;
        Destroy(gameObject);
    }
}
