﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;

public class SpiderLoopy_Game1 : MonoBehaviour
{
    enum EGameKind
    {
        SpiderNetShotGame,
        RopePullGame,
        CompleteGame,
    }

    enum ELoopyAnimation
    {
        Fly = 1,
        SitDown,
        Idle,
        SpiderShot,
        CatchRope,
        PullRope,
        Happy,
    }

    enum EPororoAnimation
    {
        Cry = 1,
        UnWind,
        Happy,
    }

    [Header("루피")]
    [SerializeField]
    private SkeletonAnimation _loopySkeletonAnimation;

    [Header("뽀로로")]
    [SerializeField]
    private SkeletonAnimation _pororoSkeletonAnimation;

    [Header("악당")]
    [SerializeField]
    private SkeletonAnimation[] _enemySkeletonAnimation;

    [Header("거미줄 부모 트랜스폼")]
    [SerializeField]
    private Transform _spiderParentTransform;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("성공 파티클")]
    [SerializeField]
    private Particles _successParticle;

    [Header("하트 파티클")]
    [SerializeField]
    private Particles _heartParticle;

    [Header("전체 파티클")]
    [SerializeField]
    private Particles _fullParticle;

    [Header("로프")]
    [SerializeField]
    private GameObject _rope;

    private Vector2 _spiderMakePos = new Vector2(640, 515);
    private EGameKind _currentGameKind = EGameKind.SpiderNetShotGame;
    public static int EnemyCatchedCnt;

    private const float LIMIT_ANGLE = 72f;
    private const int COMPLETE_CATCHED_COUNT = 30;
    private const float TOUCH_ADDFORCE = 30;
    private const float PORORO_HEIGHT_UP = 5f;
    private const float COMPLETE_PULL_HEIGHT = 485f;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoGameLoop");
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            if(_currentGameKind == EGameKind.SpiderNetShotGame)
            {
                float angle = Mathf.Atan2(_spiderMakePos.x - Input.mousePosition.x, _spiderMakePos.y - Input.mousePosition.y) * 180 / Mathf.PI;
                if (angle <= LIMIT_ANGLE && angle >= -LIMIT_ANGLE)
                {
                    SpiderShot(Input.mousePosition, angle);
                }
            }
            _clickParticle.StartAction(Input.mousePosition);
        }

        if(EnemyCatchedCnt == COMPLETE_CATCHED_COUNT)
        {
            EnemyCatchedCnt++;
            Util.GetInstance.IsNotTouched = true;
            StartCoroutine(CoCompleteCatchedGame());
        }
    }

    IEnumerator CoCompleteCatchedGame()
    {
        
        yield return new WaitForSpineAnimationComplete(_loopySkeletonAnimation.state.SetAnimation(0, ((int)ELoopyAnimation.Happy).ToString(), false));
        _loopySkeletonAnimation.state.SetAnimation(0, ((int)ELoopyAnimation.CatchRope).ToString(), false);
        _loopySkeletonAnimation.state.AddAnimation(0, ((int)ELoopyAnimation.PullRope).ToString(), true, 0);
        yield return new WaitForSeconds(0.6f);
        DOTween.To(() => _rope.GetComponent<HingeJoint2D>().connectedAnchor, x => _rope.GetComponent<HingeJoint2D>().connectedAnchor = x, _rope.GetComponent<HingeJoint2D>().connectedAnchor + new Vector2(0,15), 0.4f);
        yield return new WaitForSeconds(0.4f);
        _currentGameKind = EGameKind.RopePullGame;
        StartCoroutine("CoGameLoop");
    }

    void SpiderShot(Vector2 touchPos,float angle)
    {

        _loopySkeletonAnimation.state.SetAnimation(0, ((int)ELoopyAnimation.SpiderShot).ToString(), false);
        _loopySkeletonAnimation.state.AddAnimation(0, ((int)ELoopyAnimation.Idle).ToString(), true,0);
        GameObject spider = GetComponent<ObjectPooling>().GetObject(_spiderParentTransform);
        
        spider.transform.localPosition = _spiderMakePos;
        spider.transform.localRotation = Quaternion.Euler(new Vector3(0,0, -angle));
        spider.transform.DOLocalMove(touchPos, 0.25f).OnComplete(()=> {
            GetComponent<ObjectPooling>().ReturnObject(spider);
        });
    }


    IEnumerator CoGameLoop()
    {
        switch (_currentGameKind)
        {
            case EGameKind.SpiderNetShotGame:
                {
                    Util.GetInstance.IsNotTouched = true;
                    StartCoroutine("CoSpiderLoopyMoveAction");
                }
                break;

            case EGameKind.RopePullGame:
                {
                    Util.GetInstance.IsNotTouched = false;
                    _pororoSkeletonAnimation.gameObject.AddComponent<UserInputGestureAction>().CreateInputGesture(UserInputGestureAction.EType.Tap, UserInputGestureAction.EJudgeWay.Collider, _pororoSkeletonAnimation.gameObject, RopeAction);
                    _pororoSkeletonAnimation.gameObject.GetComponent<UserInputGestureAction>()._isNotDestroy = true;
                }
                break;

            case EGameKind.CompleteGame:
                {
                    Util.GetInstance.IsNotTouched = true;
                    _rope.SetActive(false);
                    Destroy(_pororoSkeletonAnimation.GetComponent<UserInputGestureAction>());
                    Destroy(_pororoSkeletonAnimation.GetComponent<HingeJoint2D>());
                    Destroy(_pororoSkeletonAnimation.GetComponent<Rigidbody2D>());
                    //_pororoSkeletonAnimation.transform.DOLocalMoveY(570, 0.25f);
                    _pororoSkeletonAnimation.transform.DORotate(new Vector3(0,0,-360), 0.667f, RotateMode.FastBeyond360);
                    _pororoSkeletonAnimation.transform.DOLocalJump(new Vector2(_pororoSkeletonAnimation.transform.localPosition.x + 100, 575f), 100, 1, 0.667f);
                    
                    _pororoSkeletonAnimation.state.SetAnimation(0, ((int)EPororoAnimation.UnWind).ToString(), false);
                    _pororoSkeletonAnimation.state.AddAnimation(0, ((int)EPororoAnimation.Happy).ToString(), true,0);
                    yield return new WaitForSeconds(0.667f);
                    _heartParticle.StartAction(_pororoSkeletonAnimation.transform.localPosition + new Vector3(110, 90));
                    _loopySkeletonAnimation.state.SetAnimation(0, ((int)ELoopyAnimation.Happy).ToString(), true);
                    yield return new WaitForSeconds(3);
                    GameManager.GameEndCallBackEvent.Invoke();
                }
                break;

            default:
                break;
        }
    }

    IEnumerator CoSpiderLoopyMoveAction()
    {
        yield return new WaitForSeconds(2);
        _loopySkeletonAnimation.gameObject.SetActive(true);
        _loopySkeletonAnimation.transform.DOLocalMoveY(_loopySkeletonAnimation.transform.localPosition.y + 10, 0.3f);
        yield return new WaitForSeconds(0.3f);
        _loopySkeletonAnimation.gameObject.SetActive(false);
        _loopySkeletonAnimation.transform.localPosition = new Vector2(1050, 585);
        _loopySkeletonAnimation.transform.localScale = new Vector2(0.6f, 0.6f);
        yield return new WaitForSeconds(0.5f);
        _loopySkeletonAnimation.gameObject.SetActive(true);
        _loopySkeletonAnimation.transform.DOLocalMoveY(_loopySkeletonAnimation.transform.localPosition.y + 10, 0.3f);
        yield return new WaitForSeconds(0.3f);
        _loopySkeletonAnimation.gameObject.SetActive(false);
        _loopySkeletonAnimation.transform.localPosition = new Vector2(639, 818);
        _loopySkeletonAnimation.transform.localScale = Vector2.one;

        yield return new WaitForSeconds(0.5f);
        _loopySkeletonAnimation.gameObject.SetActive(true);
        _loopySkeletonAnimation.state.SetAnimation(0, ((int)ELoopyAnimation.Fly).ToString(), false);
        _loopySkeletonAnimation.state.AddAnimation(0, ((int)ELoopyAnimation.SitDown).ToString(), false, 0);
        _loopySkeletonAnimation.state.AddAnimation(0, ((int)ELoopyAnimation.Idle).ToString(), true, 0);
        _loopySkeletonAnimation.transform.DOLocalMoveY(Util.GetInstance.GetDesignSize().y - 142, 1.333f);
        yield return new WaitForSeconds(2.333f);
        Util.GetInstance.IsNotTouched = false;
    }

    public void RopeAction()
    {
        _successParticle.StartAction(_pororoSkeletonAnimation.transform.localPosition + new Vector3(0,-25));
        _pororoSkeletonAnimation.GetComponent<HingeJoint2D>().connectedAnchor += new Vector2(0, PORORO_HEIGHT_UP);
        _rope.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(TOUCH_ADDFORCE * (_pororoSkeletonAnimation.transform.localPosition.x - Input.mousePosition.x) * 2f 
            , TOUCH_ADDFORCE * (Input.mousePosition.y - _pororoSkeletonAnimation.transform.localPosition.y) + TOUCH_ADDFORCE * 2), ForceMode2D.Force);

        //Complete Game
        if(_pororoSkeletonAnimation.transform.localPosition.y >= COMPLETE_PULL_HEIGHT)
        {
            _currentGameKind = EGameKind.CompleteGame;
            StartCoroutine("CoGameLoop");
        }
    }
}
