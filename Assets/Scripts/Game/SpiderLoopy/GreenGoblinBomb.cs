﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GreenGoblinBomb : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name.Contains(GreenGoblin.ESpiderKind.Spider.ToString())){
            Destroy(GetComponent<CircleCollider2D>());
            collision.name = GreenGoblin.ESpiderKind.Net.ToString();
            collision.transform.DOKill();
            collision.transform.GetChild((int)GreenGoblin.ESpiderKind.Spider).gameObject.SetActive(false);
            collision.transform.parent = transform;
            collision.transform.localPosition = new Vector2(10, 0);
            collision.transform.GetChild((int)GreenGoblin.ESpiderKind.Net).localScale = new Vector2(0.6f,0.6f);
            collision.transform.GetChild((int)GreenGoblin.ESpiderKind.Net).localRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 180)));
            collision.transform.GetChild((int)GreenGoblin.ESpiderKind.Net).gameObject.SetActive(true);

            transform.DOKill();
            transform.DOScale(0, 0.5f).SetEase(Ease.InBack).OnComplete(()=> {
                gameObject.SetActive(false);
            });
        }
    }
}
