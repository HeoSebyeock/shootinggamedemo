﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.UI;

public class SpiderLoopy_Game2 : MonoBehaviour
{
    enum EGameKind
    {
        GoToBuildingGame,
        GreenGoblinCatch,
    }
    enum EGameLevel
    {
        Eazy = 8,
        Normal = 6,
        Hard = 4,
    }
    enum EFloorStep
    {
        One,
        Two,
    }

    [Header("배경")]
    [SerializeField]
    private Transform _backGroundTransform;

    [Header("루프 배경")]
    [SerializeField]
    private GameObject[] _loopBack;

    [Header("루피")]
    [SerializeField]
    private SkeletonAnimation _loopySkeletonAnimation;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("성공 파티클")]
    [SerializeField]
    private Particles _successParticle;

    [Header("빌딩에 나오는 적 오브젝트 풀링")]
    [SerializeField]
    private ObjectPooling _enemyObjectPooling;

    [Header("인트로 악당")]
    [SerializeField]
    private SkeletonAnimation _introEnemy;

    [Header("고블린 잡기 게임 오브젝트")]
    [SerializeField]
    private GameObject _goblinCatchGame;

    private bool[] _isExist = new bool[WINDOW_SIZE];

    //pause game
    //public static bool isBackGroundLoopStop = false;
    private bool _isGamePause = false;
    private float _isWaitForPauseTime = 0f;

    //game gauge
    private Image _gaugeBar;

    //background intro move action
    private Tween _backGroundTween;

    //speed up
    private bool _isSpeedUp = false;
    private float _speedUpTime = 0;
    private float _currentTimeScale = 1;

    //back loop end
    private bool _isLoopEnd = false;

    private GotoBuildingCharacter gotoBuildingCharacter;

    private Vector2[] _enemyCreatePositions = new Vector2[] {
        new Vector2(-211,-248),new Vector2(211,-248),new Vector2(-211,-6),
        new Vector2(211,-6),new Vector2(-211,235),new Vector2(211,235),
        new Vector2(-211,-248),new Vector2(211,-248),new Vector2(-211,-6),
        new Vector2(211,-6),new Vector2(-211,235),new Vector2(211,235),
    };

    private EGameKind _currentGameKind = EGameKind.GoToBuildingGame;
    private EGameLevel _currentGameLevel = EGameLevel.Eazy;

    private LoopImageAction _backGroundLoopImageAction;

    private const float BACKGROUND_LOOPTIME = 2.75f;
    private const int WINDOW_SIZE = 12;
    private const float LOOPY_LEFT_XPOS = 442f;
    private const float LOOPY_RIGHT_XPOS = 863f;
    private const float GAUGE_FILL_POWER = 0.025f;
    private const int ENEMY_PARENT_TRANSFORM = 3;
    private const int SPEED_UP_PASS_ENEMY_COUNT = 3;
    private const float DEFAULT_TIMESCALE = 1f;
    private const float SPEED_UP_TIMESCALE = 1.35f;
    private const int WINDOW_BREAK_ANIMATION_CHILD_NUM = 4;

    // Use this for initialization
    void Start()
    {
        //Time.timeScale = 10;
        StartCoroutine("CoGameLoop");
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            _clickParticle.StartAction(Input.mousePosition);
        }
        if(gotoBuildingCharacter == null)
        {
            return;
        }

        if (gotoBuildingCharacter.IsEnemyCrashed)
        {
            gotoBuildingCharacter.IsEnemyCrashed = false;
            StartCoroutine("CoPauseAndRestart");
            _currentTimeScale = DEFAULT_TIMESCALE;
            if(_backGroundLoopImageAction != null)
            {
                _backGroundLoopImageAction.SetLoopSpeed(DEFAULT_TIMESCALE);
            }
        }

        if(gotoBuildingCharacter.EnemyPassCount == SPEED_UP_PASS_ENEMY_COUNT && _currentTimeScale != SPEED_UP_TIMESCALE)
        {
            _loopySkeletonAnimation.state.SetAnimation(0, ((int)GotoBuildingCharacter.ELoopyAnimation.FastMove).ToString(), true);
            _currentTimeScale = SPEED_UP_TIMESCALE;
            StartCoroutine("CoSpeedUpTime");
            _backGroundLoopImageAction.SetLoopSpeed(SPEED_UP_TIMESCALE);
        }

    }

    IEnumerator CoSpeedUpTime()
    {
        while (true)
        {
            _speedUpTime += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator CoPauseAndRestart()
    {
        StartCoroutine("CoWaitForPauseTime");
        _isGamePause = true;
        if (_backGroundLoopImageAction != null)
        {
            _backGroundLoopImageAction.LoopPause();
        }
        _backGroundTween.Pause();
        yield return new WaitForSeconds(2);
        StopCoroutine("CoWaitForPauseTime");
        if (_backGroundLoopImageAction != null)
        {
            _backGroundLoopImageAction.LoopReStart();
        }
        _backGroundTween.Play();
        _isGamePause = false;
        Util.GetInstance.IsNotTouched = false;
    }

    IEnumerator CoWaitForPauseTime()
    {
        while (true)
        {
            _isWaitForPauseTime += Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator CoGameLoop()
    {
        switch (_currentGameKind)
        {
            case EGameKind.GoToBuildingGame:
                {
                    yield return new WaitForSeconds(1);
                    gotoBuildingCharacter = _loopySkeletonAnimation.GetComponent<GotoBuildingCharacter>();
                    _gaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
                    _introEnemy.transform.DOLocalMoveX(-100, 1).SetRelative();
                    yield return new WaitForSeconds(1);
                    _introEnemy.state.SetAnimation(0, "2", true);
                    yield return new WaitForSeconds(1);
                    _introEnemy.transform.DOLocalMoveX(100, 1).SetRelative();
                    yield return new WaitForSeconds(0.5f);
                    Util.GetInstance.IsNotTouched = true;
                    _loopySkeletonAnimation.gameObject.SetActive(true);
                    StartCoroutine("CoUpStair");
                }
                break;

            case EGameKind.GreenGoblinCatch:
                {
                    gameObject.transform.parent.gameObject.AddComponent<TransitionImageAction>().CreateTransitionImage("circle", TransitionImageAction.EType.ZoomInOut,new Vector2(Util.GetInstance.GetDesignSize().x * 0.5f , Util.GetInstance.GetDesignSize().y * 0.5f), 1, 0);
                    gameObject.transform.parent.gameObject.GetComponent<TransitionImageAction>().StartAction();
                    yield return new WaitForSeconds(1);
                    gameObject.SetActive(false);
                    _goblinCatchGame.SetActive(true);
                }
                break;
            default:
                break;
        }

        yield return null;
    }

    IEnumerator CoUpStair()
    {
        yield return new WaitUntil(() => !Util.GetInstance.IsNotTouched);
        _backGroundTween = _backGroundTransform.DOLocalMoveY(_backGroundTransform.localPosition.y - Util.GetInstance.GetDesignSize().y, BACKGROUND_LOOPTIME);

        StartCoroutine("CoMakeEnemy");
        StartCoroutine("CoMoveGauge");
        yield return new WaitForSeconds(BACKGROUND_LOOPTIME);
        float endTime = 0;
        while (_isGamePause)
        {
            endTime += Time.deltaTime;
            yield return null;
        }
        //게임이 멈춘만큼 기다림
        yield return new WaitForSeconds(_isWaitForPauseTime - endTime);
        _backGroundLoopImageAction = _backGroundTransform.gameObject.AddComponent<LoopImageAction>();
        _backGroundLoopImageAction.CreateLoopImage(LoopImageAction.EType.UpToDownMove, BACKGROUND_LOOPTIME, _loopBack, Util.GetInstance.GetDesignSize() * 0.5f);
        _backGroundLoopImageAction.SetAddChangedYPos(Util.GetInstance.GetDesignSize().y);
        _backGroundLoopImageAction.StartAction();
    }

    IEnumerator CoMoveGauge()
    {
        while (true)
        {
            _gaugeBar.fillAmount += Time.deltaTime * GAUGE_FILL_POWER;

            if(_gaugeBar.fillAmount >= 0.25f && _currentGameLevel == EGameLevel.Eazy)
            {
                _currentGameLevel = EGameLevel.Normal;
            }

            else if (_gaugeBar.fillAmount >= 0.55f && _currentGameLevel == EGameLevel.Normal)
            {
                _currentGameLevel = EGameLevel.Hard;
            }
            else if(_gaugeBar.fillAmount >= 0.9f)
            {
                Time.timeScale = 1;
                StartCoroutine("CoCompleteGame");
                yield break;
            }
            yield return new WaitUntil(() => !_isGamePause);
        }
    }

    IEnumerator CoCompleteGame()
    {
        Util.GetInstance.IsNotTouched = true;
        StopCoroutine("CoMakeEnemy");
        GetComponent<GameGauge>().GaugeDestroy(true);
        _backGroundLoopImageAction.LoopStop();
        
        for (int i = 0; i < _loopBack.Length; i++)
        {
            foreach (Transform enemy in _loopBack[i].transform.GetChild(3))
            {
                Destroy(enemy.GetComponent<CircleCollider2D>());
                enemy.transform.GetChild(1).GetComponent<SpineAnimation>().Fade(enemy.transform.GetChild(1).GetComponent<SkeletonAnimation>(), 0, 0.25f);
            }
        }
        yield return new WaitUntil(() => _isLoopEnd);
        StopCoroutine("CoEnemyAdmin");
        _backGroundTransform.DOLocalMoveY(_backGroundTransform.localPosition.y - Util.GetInstance.GetDesignSize().y, BACKGROUND_LOOPTIME);
        yield return new WaitForSeconds(BACKGROUND_LOOPTIME);
        _loopySkeletonAnimation.state.SetAnimation(0, ((int)GotoBuildingCharacter.ELoopyAnimation.Surprise).ToString(),false);
        _loopySkeletonAnimation.state.AddAnimation(0, ((int)GotoBuildingCharacter.ELoopyAnimation.SlowMove).ToString(), true,0);
        yield return new WaitForSeconds(1);
        _loopySkeletonAnimation.timeScale = 2;
        _loopySkeletonAnimation.transform.DOLocalMoveY(Util.GetInstance.GetDesignSize().y - 250, 1.5f);
        yield return new WaitForSeconds(1.5f);
        _loopySkeletonAnimation.state.ClearTracks();
        _loopySkeletonAnimation.skeleton.SetToSetupPose();
        _loopySkeletonAnimation.transform.DOScale(0.85f, 0.5f);
        _loopySkeletonAnimation.transform.DOLocalMoveY(-50, 0.5f).SetRelative().SetEase(Ease.InBack);
        yield return new WaitForSeconds(0.5f);
        _loopySkeletonAnimation.transform.DOScale(1.2f, 0.75f).SetEase(Ease.OutBack);
        _loopySkeletonAnimation.transform.DOLocalMoveY(500, 1.5f).SetRelative().SetEase(Ease.OutBack);
        yield return new WaitForSeconds(0.85f);
        _currentGameKind++;
        StartCoroutine("CoGameLoop");
    }


    IEnumerator CoMakeEnemy()
    {
        StartCoroutine("CoEnemyAdmin");
        int num = 0;
        while (true)
        {
            _isWaitForPauseTime = 0;
            GameObject tempEnemy;

            if (num < WINDOW_SIZE * 0.5f)
            {
                tempEnemy = _enemyObjectPooling.GetObject(_loopBack[(int)EFloorStep.One].transform.GetChild(ENEMY_PARENT_TRANSFORM));
            }
            else
            {
                tempEnemy = _enemyObjectPooling.GetObject(_loopBack[(int)EFloorStep.Two].transform.GetChild(ENEMY_PARENT_TRANSFORM));
            }

            tempEnemy.transform.localPosition = _enemyCreatePositions[num + Random.Range(0, 2)];

            int randBreakWindow = Random.Range(0, 2);
            if(randBreakWindow == 0)
            {
                tempEnemy.transform.GetChild(WINDOW_BREAK_ANIMATION_CHILD_NUM).gameObject.SetActive(true);
                tempEnemy.transform.GetChild(WINDOW_BREAK_ANIMATION_CHILD_NUM).GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", false);
                //tempEnemy.transform.GetChild(WINDOW_BREAK_ANIMATION_CHILD_NUM).gameObject.SetActive(false);
            }

            num += (int)_currentGameLevel;

            if (num >= WINDOW_SIZE)
            {
                num -= WINDOW_SIZE;
            }

            //speed next deley time 
            float speedProcessTime = 0;
            if(_speedUpTime != 0)
            {
                speedProcessTime = _speedUpTime - (1 / _currentTimeScale * _speedUpTime);
                _speedUpTime = 0;
            }

            yield return new WaitForSeconds(BACKGROUND_LOOPTIME * ((int)_currentGameLevel / (WINDOW_SIZE * 0.5f)) * (1 / _currentTimeScale) - speedProcessTime);
            StopCoroutine("CoSpeedUpTime");
            float endTime = 0;
            while (_isGamePause)
            {
                endTime += Time.deltaTime;
                yield return null;
            }
            //게임이 멈춘만큼 기다림
            yield return new WaitForSeconds(_isWaitForPauseTime - endTime);

        }
    }

    /// <summary>
    /// 적이 지나가서 밑에 있을 경우 오브젝트 삭제
    /// </summary>
    /// <returns></returns>
    IEnumerator CoEnemyAdmin()
    {
        while (true)
        {
            _isLoopEnd = false;
            for (int i = 0; i < _loopBack.Length; i++)
            {
                if(_loopBack[i].transform.localPosition.y <= 360)
                {
                    _isLoopEnd = true;
                    foreach (Transform enemy in _loopBack[i].transform.GetChild(ENEMY_PARENT_TRANSFORM))
                    {
                        if (enemy.name.Contains("passEnemy"))
                        {
                            Destroy(enemy.gameObject);
                        }
                    } 
                }
            }
            yield return null;
        }
    }

    public void Tap_MoveLoopyTotheLeft()
    {
        StartCoroutine("CoColliderVisible");
        if (_loopySkeletonAnimation.transform.localPosition.x == LOOPY_RIGHT_XPOS)
        {
            _loopySkeletonAnimation.transform.DOLocalJump(new Vector2(LOOPY_LEFT_XPOS, _loopySkeletonAnimation.transform.localPosition.y), 100, 1, 0.2f);
            _loopySkeletonAnimation.transform.DOPunchScale(new Vector2(0.25f, 0.25f), 0.2f, 0, 0);
        }
    }

    public void Tap_MoveLoopyTotheRight()
    {
        StartCoroutine("CoColliderVisible");
        if (_loopySkeletonAnimation.transform.localPosition.x == LOOPY_LEFT_XPOS)
        {
            _loopySkeletonAnimation.transform.DOLocalJump(new Vector2(LOOPY_RIGHT_XPOS, _loopySkeletonAnimation.transform.localPosition.y), 100, 1, 0.2f);
            _loopySkeletonAnimation.transform.DOPunchScale(new Vector2(0.25f, 0.25f), 0.2f, 0, 0);
        }
    }

    public void Flick_MoveLoopyTotheLeft()
    {
        StartCoroutine("CoColliderVisible");
        if (_loopySkeletonAnimation.transform.localPosition.x == LOOPY_RIGHT_XPOS)
        {
            _loopySkeletonAnimation.transform.DOLocalMoveX(LOOPY_LEFT_XPOS, 0.2f);
        }
    }

    public void Flick_MoveLoopyTotheRight()
    {
        StartCoroutine("CoColliderVisible");
        if (_loopySkeletonAnimation.transform.localPosition.x == LOOPY_LEFT_XPOS)
        {
            _loopySkeletonAnimation.transform.DOLocalMoveX(LOOPY_RIGHT_XPOS, 0.2f);
        }
    }

    IEnumerator CoColliderVisible()
    {
        //_loopySkeletonAnimation.transform.GetChild(0) : Collider Component
        _loopySkeletonAnimation.transform.GetChild(0).gameObject.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        _loopySkeletonAnimation.transform.GetChild(0).gameObject.SetActive(true);
    }
}
