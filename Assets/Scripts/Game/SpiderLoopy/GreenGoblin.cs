﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Spine.Unity;
using Spine;
using UnityEngine.UI;

public class GreenGoblin : MonoBehaviour
{
    public enum ESpiderKind
    {
        Spider,
        Net,
    }

    public enum EGreenGoblinAnimation
    {
        Idle = 1,
        Hurt,
        BoomDown,
        BoomFire,
        Cry,
    }

    public enum EGameLevel
    {
        Eazy = 0,
        Normal = 30,
        Hard = 60,
    }

    public enum EBomb
    {
        No,
        Yes,
    }

    [Header("기본 스파인")]
    [SerializeField]
    private Material _defaultMaterial;

    [Header("외곽선 스파인")]
    [SerializeField]
    private Material _outLineMaterial;

    [Header("폭탄 부모 트랜스폼")]
    [SerializeField]
    private Transform _bombParentTransform;

    private Image _gaugeBar;

    private bool _isLeftToRight = false;
    private float _currentTimeScale = 1f;
    private Tween _currentTween;
    private float _currentHitCount = 1;
    private int totalHitCount = 0;

    private const float XPOS_PADDING = 150f;
    private const float MOVE_SPEED = 300;
    private const float MIN_TIMESCALE = 0.4f;
    private const float FILLAMOUNT_POWER = 100;
    
    private EGameLevel _currentGameLevel = EGameLevel.Eazy;

    private EGreenGoblinAnimation _currentAnimation = EGreenGoblinAnimation.Idle;
    
    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoGoblinWaitAndStart");
    }

    IEnumerator CoGoblinWaitAndStart()
    {
        Util.GetInstance.IsNotTouched = true;
        _gaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
        _gaugeBar.fillAmount = 1f;
        yield return new WaitForSeconds(5.5f);
        GoblinMoveAction();
        Util.GetInstance.IsNotTouched = false;
    }

    void GoblinMoveAction()
    {
        float moveTime = 0f;

        if (!_isLeftToRight)
        {
            moveTime = (transform.localPosition.x - XPOS_PADDING) / (MOVE_SPEED + (int)_currentGameLevel * 2);
            _currentTween = transform.DOLocalMoveX(XPOS_PADDING, moveTime).OnComplete(() => {
                _isLeftToRight = !_isLeftToRight;
                GoblinMoveAction();
            });
            _currentTween.timeScale = _currentTimeScale;
        }
        else
        {
            moveTime = (Util.GetInstance.GetDesignSize().x - XPOS_PADDING - transform.localPosition.x) / (MOVE_SPEED + (int)_currentGameLevel * 2);
            _currentTween = transform.DOLocalMoveX(Util.GetInstance.GetDesignSize().x - XPOS_PADDING, moveTime).OnComplete(() => {
                _isLeftToRight = !_isLeftToRight;
                GoblinMoveAction();
            });
            _currentTween.timeScale = _currentTimeScale;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.Obstacle.ToString()) && gameObject.tag.Equals(Constants.ETag.SuperGoblin.ToString()))
        {
            gameObject.tag = Constants.ETag.Untagged.ToString();
            GetComponent<SkeletonRenderer>().CustomMaterialOverride[_defaultMaterial] = _defaultMaterial;
            
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //거미줄에 맞을 경우
        if (collision.name.Contains(ESpiderKind.Spider.ToString()) && !gameObject.tag.Equals(Constants.ETag.SuperGoblin.ToString()))
        {
            totalHitCount++;
            if(totalHitCount == FILLAMOUNT_POWER && !SpiderLoopy_Game3.isCompleteGame)
            {
                SpiderLoopy_Game3.isCompleteGame = true;
                _currentTween.Kill();
                StopAllCoroutines();
                foreach (Transform item in _bombParentTransform)
                {
                    item.gameObject.SetActive(false);
                }
                foreach (Transform item in transform)
                {
                    item.gameObject.SetActive(false);
                }
                GetComponent<GameGauge>().GaugeDestroy(true);
                GetComponent<SkeletonAnimation>().state.SetAnimation(1, ((int)EGreenGoblinAnimation.Cry).ToString(), true);
            }

            if (totalHitCount >= (int)EGameLevel.Hard)
            {
                _currentGameLevel = EGameLevel.Hard;
            }

            else if (totalHitCount >= (int)EGameLevel.Normal)
            {
                _currentGameLevel = EGameLevel.Normal;
            }

            _currentHitCount -= 1 / FILLAMOUNT_POWER;
            
            _gaugeBar.DOFillAmount(_currentHitCount, 0.25f);

            StopCoroutine("CoHitAction");
            collision.name = ESpiderKind.Net.ToString();
            collision.transform.DOKill();
            collision.transform.GetChild((int)ESpiderKind.Spider).gameObject.SetActive(false);
            collision.transform.parent = transform;
            collision.transform.localPosition = new Vector3(Random.Range(-30, 30), Random.Range(-40, 20));
            collision.transform.GetChild((int)ESpiderKind.Net).localScale = transform.localScale * Random.Range(6, 10) * 0.1f;
            collision.transform.GetChild((int)ESpiderKind.Net).localRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 180)));
            collision.transform.GetChild((int)ESpiderKind.Net).gameObject.SetActive(true);

            if (_currentTimeScale > MIN_TIMESCALE)
            {
                _currentTimeScale -= 0.1f;
                GetComponent<SkeletonAnimation>().timeScale = _currentTimeScale;
                if (_currentTween != null)
                {
                    _currentTween.timeScale = _currentTimeScale;
                }
            }
            StartCoroutine("CoHitAction");
            StartCoroutine(CoComebackGoblin(collision.gameObject));
        }

        //무적 상태일 경우
        else if (gameObject.tag.Equals(Constants.ETag.SuperGoblin.ToString()) && !collision.tag.Equals(Constants.ETag.Obstacle.ToString()))
        {
            SpiderFail(collision.gameObject);
        }

        //폭탄 떨어뜨리는 영역에 도달한 경우
        else if (collision.tag.Equals(Constants.ETag.Obstacle.ToString()) && gameObject.tag.Equals(Constants.ETag.Untagged.ToString()))
        {
            StartCoroutine("CoBombMake");
        }
    }


    void SpiderFail(GameObject spider)
    {
        spider.name = "Fail";
        spider.transform.DOKill();
        spider.transform.DOLocalJump(new Vector3(Random.Range(-200, 200), Random.Range(-50, -125)), 100, 1, 0.5f).SetRelative().OnComplete(()=> {
            spider.SetActive(false);
        });
        spider.transform.DORotate(new Vector3(0, 0, -360), 0.15f, DG.Tweening.RotateMode.FastBeyond360).SetLoops(-1);
    }

    IEnumerator CoBombMake()
    {
        
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EGreenGoblinAnimation.BoomDown).ToString(), false);
        

        EBomb randBomb = (EBomb)Random.Range(0, 2);
        _currentTween.timeScale = 0;

        float delayTime = 0;

        if (randBomb >= EBomb.Yes)
        {
            GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)EGreenGoblinAnimation.Idle).ToString(), true, 0);
            gameObject.tag = Constants.ETag.SuperGoblin.ToString();
            GetComponent<SkeletonRenderer>().CustomMaterialOverride[_defaultMaterial] = _outLineMaterial;

            for (int i = 0; i < 1 + (int)_currentGameLevel / 30; i++)
            {
                StartCoroutine(CoBombDownAndFire());
                delayTime += 0.45f;
                yield return new WaitForSeconds(0.45f);
            }
        }
        yield return new WaitForSeconds(1.333f - delayTime);
        _currentTween.timeScale = 1;
        //gameObject.tag = Constants.ETag.Untagged.ToString();
    }

    IEnumerator CoBombDownAndFire()
    {
        GameObject bomb = GetComponent<ObjectPooling>().GetObject(_bombParentTransform);
        bomb.transform.localPosition = transform.localPosition + new Vector3(0, -125);
        bomb.transform.DOLocalJump(new Vector3(Random.Range(-90, 90), -235), 50, 1, 3 - ((float)_currentGameLevel / 30) * 0.75f).SetRelative();
        //bomb.transform.DOLocalMoveY(-235, 3 - ((float)_currentGameLevel / 30) * 0.5f).SetRelative();
        yield return new WaitForSeconds(3 - ((float)_currentGameLevel / 30) * 0.75f);

        bomb.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "2", false);
        yield return new WaitForSeconds(0.833f);
        //폭탄이 터질 경우
        if (bomb.transform.localScale.x == 1)
        {
            bomb.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "3", false);
            
            Destroy(bomb.GetComponent<CircleCollider2D>());
            if (bomb.transform.localPosition.x < Util.GetInstance.GetDesignSize().x * 0.5f)
            {
                SpiderLoopy_Game3.isBombFire = (int)SpiderLoopy_Game3.EFireLocation.Left;
            }
            else
            {
                SpiderLoopy_Game3.isBombFire = (int)SpiderLoopy_Game3.EFireLocation.Right;
            }

            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EGreenGoblinAnimation.BoomFire).ToString(), false);
            GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)EGreenGoblinAnimation.Idle).ToString(), true, 0);
            yield return new WaitForSeconds(1f);
            bomb.SetActive(false);
            yield return new WaitForSeconds(0.5f);
        }
    }

    IEnumerator CoHitAction()
    {
        if(_currentAnimation != EGreenGoblinAnimation.Hurt)
        {
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EGreenGoblinAnimation.Hurt).ToString(), true);
            _currentAnimation = EGreenGoblinAnimation.Hurt;
        }
        
        yield return new WaitForSeconds(1f);
        
        GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)EGreenGoblinAnimation.Idle).ToString(), true,0);
        _currentAnimation = EGreenGoblinAnimation.Idle;
    }

    IEnumerator CoComebackGoblin(GameObject spider)
    {
        yield return new WaitForSeconds(1f);

        spider.SetActive(false);
        if (_currentTimeScale < 1f)
        {
            _currentTimeScale += 0.1f;
            GetComponent<SkeletonAnimation>().timeScale = _currentTimeScale;
            if (_currentTween != null && _currentTween.timeScale != 0)
            {
                _currentTween.timeScale = _currentTimeScale;
            }
        }
    }
}
