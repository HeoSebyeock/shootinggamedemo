﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using UnityEngine.UI;
using DG.Tweening;

public class SpiderLoopy_Game3 : MonoBehaviour
{
    enum ELoopyAnimation
    {
        Fly = 1,
        SitDown,
        Idle,
        RightShot,
        LeftShot,
        RightSurprise,
        LeftSurprise,
        Happy
    }

    public enum EFireLocation
    {
        None = -1,
        Left,
        Right,
    }

    [Header("루피")]
    [SerializeField]
    private SkeletonAnimation _loopySkeletonAnimation;

    [Header("고블린")]
    [SerializeField]
    private SkeletonAnimation _goblinSkeletonAnimation;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("성공 파티클")]
    [SerializeField]
    private Particles _successParticle;

    [Header("거미줄 부모 트랜스폼")]
    [SerializeField]
    private Transform _spiderParentTransform;

    public static int isBombFire = (int)EFireLocation.None;
    public static bool isCompleteGame = false;

    private GameObject _bigSpider = null;

    private Vector3 LEFT_ADD_POS = new Vector3(-45,-62.5f);
    private Vector3 RIGHT_ADD_POS = new Vector3(35, -52.5f);
    private const float LIMIT_ANGLE = 80;

    //[Header("고블린")]
    //[SerializeField]
    //private SkeletonAniamtion
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("CoGameLoop");
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }

            Vector3 addPos = Vector3.zero;
            if (Input.mousePosition.x < Util.GetInstance.GetDesignSize().x * 0.5f)
            {
                addPos = LEFT_ADD_POS;
            }
            else
            {
                addPos = RIGHT_ADD_POS;
            }

            float angle = Mathf.Atan2(_loopySkeletonAnimation.transform.localPosition.x - Input.mousePosition.x + addPos.x, _loopySkeletonAnimation.transform.localPosition.y - Input.mousePosition.y) * 180 / Mathf.PI;

            if (angle > LIMIT_ANGLE || angle < -LIMIT_ANGLE)
            {
                if(addPos == LEFT_ADD_POS)
                {
                    _loopySkeletonAnimation.state.SetAnimation(0, ((int)ELoopyAnimation.LeftShot).ToString(), false);
                }
                else
                {
                    _loopySkeletonAnimation.state.SetAnimation(0, ((int)ELoopyAnimation.RightShot).ToString(), false);
                }
                SpiderShot(Input.mousePosition, angle , addPos);
            }

            _clickParticle.StartAction(Input.mousePosition);
        }

        if (isBombFire != (int)EFireLocation.None)
        {
            
            StartCoroutine(CoCryLoopyAction((EFireLocation)isBombFire));
            isBombFire = (int)EFireLocation.None;
        }

        if (isCompleteGame)
        {
            StartCoroutine("CoCompleteGame");
            isCompleteGame = false;
        }

        if(_bigSpider != null)
        {
            _bigSpider.transform.localPosition = _goblinSkeletonAnimation.skeleton.FindBone("cr_face_1_1").GetSkeletonSpacePosition();
        }
    }

    IEnumerator CoCompleteGame()
    {
        Util.GetInstance.IsNotTouched = true;
        //왕 거미줄
        if(_bigSpider == null)
        {
            _bigSpider = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs.ToString() + "/" + Constants.EFolderLeafType.Game.ToString() + "/" + GreenGoblin.ESpiderKind.Spider.ToString()) as GameObject);
            _bigSpider.transform.parent = _goblinSkeletonAnimation.transform;
            _bigSpider.transform.GetChild((int)GreenGoblin.ESpiderKind.Spider).gameObject.SetActive(false);
            _bigSpider.transform.GetChild((int)GreenGoblin.ESpiderKind.Net).gameObject.SetActive(true);
            _bigSpider.transform.localPosition = Vector2.zero;
            _bigSpider.transform.localScale = new Vector2(1.5f, 1.5f);
        }
        yield return new WaitForSeconds(1.333f);
        _goblinSkeletonAnimation.transform.DOScale(0, 1.5f);
        _loopySkeletonAnimation.state.SetAnimation(1, ((int)ELoopyAnimation.Happy).ToString(), true);
        yield return new WaitForSeconds(1.5f);
        _successParticle.StartAction(_goblinSkeletonAnimation.transform.localPosition);
        yield return new WaitForSeconds(3);
        GameManager.GameEndCallBackEvent.Invoke();
    }

    IEnumerator CoCryLoopyAction(EFireLocation isBombFire)
    {
        if (!Util.GetInstance.IsNotTouched)
        {
            if (isBombFire == EFireLocation.Left)
            {
                _loopySkeletonAnimation.state.SetAnimation(0, ((int)ELoopyAnimation.LeftSurprise).ToString(), false);
            }
            else if (isBombFire == EFireLocation.Right)
            {
                _loopySkeletonAnimation.state.SetAnimation(0, ((int)ELoopyAnimation.RightSurprise).ToString(), false);
            }
            _loopySkeletonAnimation.state.AddAnimation(0, ((int)ELoopyAnimation.Idle).ToString(), true, 0);
        }
        Util.GetInstance.IsNotTouched = true;
        yield return new WaitForSeconds(0.833f);
        Util.GetInstance.IsNotTouched = false;
    }

    IEnumerator CoGameLoop()
    {
        yield return new WaitForSeconds(0.75f);
        _loopySkeletonAnimation.GetComponent<ObjectAction>().StartAction();
        yield return new WaitForSeconds(0.5f);
        _loopySkeletonAnimation.state.SetAnimation(0, ((int)ELoopyAnimation.Fly).ToString(), false);
        _loopySkeletonAnimation.state.AddAnimation(0, ((int)ELoopyAnimation.SitDown).ToString(), false, 0);
        _loopySkeletonAnimation.state.AddAnimation(0, ((int)ELoopyAnimation.Idle).ToString(), true, 0);
        yield return new WaitForSeconds(1.5f);
        _goblinSkeletonAnimation.transform.DOLocalMoveY(Util.GetInstance.GetDesignSize().y - 196, 3.25f).SetEase(Ease.OutBack);
    }

    void SpiderShot(Vector2 touchPos, float angle, Vector3 addPos)
    {
        _loopySkeletonAnimation.state.AddAnimation(0, ((int)ELoopyAnimation.Idle).ToString(), true, 0);
        GameObject spider = GetComponent<ObjectPooling>().GetObject(_spiderParentTransform);

        spider.transform.localPosition = _loopySkeletonAnimation.transform.localPosition + addPos;
        spider.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, -angle));
        spider.transform.DOLocalMove(touchPos, 0.25f).OnComplete(() => {
            GetComponent<ObjectPooling>().ReturnObject(spider);
        });
    }
}
