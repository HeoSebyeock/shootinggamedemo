﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using DG.Tweening;

public class GotoBuildingCharacter : MonoBehaviour
{
    public enum ELoopyAnimation
    {
        SlowMove = 1,
        FastMove,
        Surprise,
    }

    private int _enemyPassCount;
    public int EnemyPassCount { get { return _enemyPassCount; } set { _enemyPassCount = value; } }

    private bool _isEnemyCrashed;
    public bool IsEnemyCrashed { get { return _isEnemyCrashed; } set { _isEnemyCrashed = value; } }

    private const float LEFT_XPOS = 442f;
    private const float RIGHT_XPOS = 863f;
    // Start is called before the first frame update
    private void OnEnable()
    {
        StartCoroutine("CoSpiderLoopyMoveAction");
    }

    IEnumerator CoSpiderLoopyMoveAction()
    {
        transform.DOLocalMoveY(Util.GetInstance.GetDesignSize().y * 0.25f, 2.75f);
        yield return new WaitForSeconds(2.75f);
        Util.GetInstance.IsNotTouched = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // enemy crash
        if (collision.tag.Equals(Constants.ETag.Enemy.ToString()))
        {
            Destroy(collision.GetComponent<CircleCollider2D>());
            collision.transform.GetChild(3).gameObject.SetActive(false);
            StartCoroutine(CoEnemyCrashAction(collision.gameObject));
        }

        // pass enemy
        if (collision.tag.Equals(Constants.ETag.PassCheck.ToString()))
        {
            _enemyPassCount++;
            collision.transform.parent.name = "passEnemy";
        }
    }

    IEnumerator CoEnemyCrashAction(GameObject enemy)
    {
        transform.DOKill();
        enemy.name = "passEnemy";
        Util.GetInstance.IsNotTouched = true;
        _enemyPassCount = 0;
        _isEnemyCrashed = true;
        enemy.transform.GetChild(1).GetComponent<SkeletonAnimation>().state.SetAnimation(0, "2", true);
        GetComponent<SkeletonAnimation>().state.ClearTracks();
        GetComponent<SkeletonAnimation>().skeleton.SetToSetupPose();
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)ELoopyAnimation.Surprise).ToString(), false);
        yield return new WaitForSeconds(1);
        enemy.transform.GetChild(1).DOLocalMoveY(-170, 0.5f).SetRelative();
        yield return new WaitForSeconds(GetComponent<SpineAnimation>().BlinkAction(3));
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)ELoopyAnimation.SlowMove).ToString(), true);
        //enemy.transform.GetChild(1).gameObject.SetActive(false);
    }
}
