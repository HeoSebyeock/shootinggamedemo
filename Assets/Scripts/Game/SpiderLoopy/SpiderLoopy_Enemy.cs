﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine.Unity;

public class SpiderLoopy_Enemy : MonoBehaviour
{
    enum ESpiderKind
    {
        Spider,
        Net,
    }

    enum EEnemyState
    {
        Catched,
        Revive,
    }

    enum EAction
    {
        Normal,
        Rotate,
        Jump,
    }

    enum ELocation
    {
        RightToLeft = -1,
        LeftToRight = 1,
    }
    [Header("생성 위치")]
    [SerializeField]
    private ELocation _location;

    [Header("움직이는데 걸리는 시간")]
    [SerializeField]
    private float _actionTime;

    [Header("거미줄 사라진 후 파티클")]
    [SerializeField]
    private Particles _successParticle;

    private Sequence _enemyMoveActionSeq;
    private float _currentScale = 1.1f;

    private const float MOVE_DISTANCE = 190f;
    private const float CREATE_WAIT_TIME_SCALE = 0.0125f;
    // 악당이 한번 움직일때 걸리는 시간 * 0.5f + 0.5f

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("CoFirstMoveAction");
    }

    IEnumerator CoFirstMoveAction()
    {
        yield return new WaitForSeconds(2f);
        if (_location == ELocation.LeftToRight)
        {
            transform.DOLocalMoveX(150, 1.75f).SetRelative();
        }
        else
        {
            transform.DOLocalMoveX(-135, 1.75f).SetRelative();
        }
        yield return new WaitForSeconds(1.75f);
        EnemyMoveAction();
    }

    void EnemyMoveAction()
    {
        _enemyMoveActionSeq = DOTween.Sequence();
        _enemyMoveActionSeq.AppendCallback(() => {
            if(_location == ELocation.LeftToRight)
            {
                EnemyChoiceAction(ELocation.LeftToRight);
            }
            else
            {
                EnemyChoiceAction(ELocation.RightToLeft);
            }
        });
        _enemyMoveActionSeq.AppendInterval(_actionTime * transform.localScale.x);
        _enemyMoveActionSeq.AppendCallback(() => {
            if (_location == ELocation.LeftToRight)
            {
                EnemyChoiceAction(ELocation.RightToLeft);
            }
            else
            {
                EnemyChoiceAction(ELocation.LeftToRight);
            }
        });
        _enemyMoveActionSeq.AppendInterval(_actionTime * transform.localScale.x);
        _enemyMoveActionSeq.SetLoops(-1);
    }

    void EnemyChoiceAction(ELocation location)
    {
        EAction randAction = (EAction)Random.Range(0, 3);
        switch (randAction)
        {
            case EAction.Normal:
                {
                    transform.DOLocalMoveX(transform.localPosition.x + MOVE_DISTANCE * (int)location, _actionTime * transform.localScale.x);
                }
                break;

            case EAction.Rotate:
                {
                    transform.DOLocalMoveX(transform.localPosition.x + MOVE_DISTANCE * (int)location, _actionTime * transform.localScale.x);
                    if (gameObject.tag.Equals(Constants.ETag.RotateEnemy.ToString()))
                    {
                        transform.DORotate(new Vector3(0, 0, -360), _actionTime * transform.localScale.x, RotateMode.FastBeyond360);
                    }
                }
                break;

            case EAction.Jump:
                {
                    transform.DOLocalJump(new Vector2(transform.localPosition.x + MOVE_DISTANCE * (int)location, transform.localPosition.y), 60, Random.Range(1,3), _actionTime * transform.localScale.x);
                }
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name.Contains(ESpiderKind.Spider.ToString()) && !gameObject.name.Equals(EEnemyState.Catched.ToString()))
        {
            gameObject.name = EEnemyState.Catched.ToString();
            _enemyMoveActionSeq.Pause();
            transform.DOPause();
            collision.name = ESpiderKind.Net.ToString();
            collision.transform.DOKill();
            collision.transform.GetChild((int)ESpiderKind.Spider).gameObject.SetActive(false);
            collision.transform.parent = transform;
            collision.transform.localPosition = Vector2.zero;
            collision.transform.GetChild((int)ESpiderKind.Net).localScale = transform.localScale;
            collision.transform.GetChild((int)ESpiderKind.Net).gameObject.SetActive(true);
            StartCoroutine(CoEnemyHitAction(collision));
        }
    }

    IEnumerator CoEnemyHitAction(Collider2D collision)
    {
        yield return new WaitForSpineAnimationComplete(GetComponent<SkeletonAnimation>().state.SetAnimation(0, "2", false));
        transform.DOScale(0, 0.25f);
        yield return new WaitForSeconds(0.25f);
        
        SpiderLoopy_Game1.EnemyCatchedCnt++;

        _successParticle.StartAction(transform.localPosition);
        Destroy(collision.gameObject);
        yield return new WaitForSpineAnimationComplete(GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", true));
        //yield return new WaitForSeconds(Random.Range(1,50) * CREATE_WAIT_TIME_SCALE);
        if(_currentScale > 0.8f)
        {
            transform.DOScale(_currentScale -= 0.075f, 0.25f).SetEase(Ease.OutBack);
            yield return new WaitForSeconds(0.25f);
            gameObject.name = EEnemyState.Revive.ToString();
            _enemyMoveActionSeq.Play();
            transform.DOPlay();
        }
    }
}
