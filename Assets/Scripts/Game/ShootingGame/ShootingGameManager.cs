﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class ShootingGameManager : MonoBehaviour
{
    enum EAppearKind
    {
        OneOnOne,
        OneOnMany
    }

    [Header("배경 Transform")]
    [SerializeField]
    private Transform _backGroundTransform;

    [Header("주인공")]
    [SerializeField]
    private GameObject _actor;

    [Header("악당")]
    [SerializeField]
    private GameObject[] _enemys;

    [Header("악당 등장 방식 [0. 1:1 / 1. 1:다수]")]
    [SerializeField]
    private int _appearKind;

    [Header("악당 등장 주기 (1:다수)")]
    [SerializeField]
    private float _appearDelay;

    [Header("슈퍼 아이템")]
    [SerializeField]
    private GameObject _superItem;

    [Header("슈퍼 아이템 이동 시간")]
    [SerializeField]
    private float _superItemMopeTime;

    [Header("슈퍼 아이템 생성 주기")]
    [SerializeField]
    private float _superItemMakeDelayTime;

    [Header("슈퍼모드 지속시간")]
    [SerializeField]
    private float _superModeTime;

    [Header("슈퍼모드 배경 Transform")]
    [SerializeField]
    private Transform _superModeBackgroundTransform;

    [Header("슈퍼 모드 끝 이벤트(=> 주인공)")]
    [SerializeField]
    private UnityEvent _superModeActorFinishEvent;

    [Header("슈퍼 모드 끝 이벤트(=> 악당)")]
    [SerializeField]
    private UnityEvent _superModeEnemyFinishEvent;

    private int _currentEnemyNum = 0;

    private const float SUPER_ITEM_YPOS_PADDING = 150f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("CGameLoop");
    }

    IEnumerator CGameLoop()
    {
        yield return null;
        Util.GetInstance.IsNotTouched = false;
        StartCoroutine("CEnemyAppearSetting");
        StartCoroutine("CSuperItemFactory");
        
    }

    /// <summary>
    /// 악당 출현 설정
    /// </summary>
    /// <returns></returns>
    IEnumerator CEnemyAppearSetting()
    {
        yield return new WaitForSeconds(1);
        switch ((EAppearKind)_appearKind)
        {
            case EAppearKind.OneOnOne:
                {
                    _enemys[_currentEnemyNum].SetActive(true);
                }
                break;

            case EAppearKind.OneOnMany:
                {
                    StartCoroutine("CEnemyMakeFactory");
                }
                break;
            default:
                break;
        }
        yield return null;
    }

    IEnumerator CEnemyMakeFactory()
    {
        while (true)
        {
            GameObject cloneEnemy = Instantiate(_enemys[Random.Range(0, _enemys.Length)], _enemys[0].transform.parent);
            cloneEnemy.SetActive(true);
            yield return new WaitForSeconds(_appearDelay);
        }
    }

    /// <summary>
    /// 슈퍼 아이템 생성하는 코루틴
    /// </summary>
    /// <returns></returns>
    IEnumerator CSuperItemFactory()
    {
        while (true)
        {
            yield return new WaitForSeconds(_superItemMakeDelayTime);
            GameObject superItemClone = Instantiate(_superItem, _backGroundTransform);
            superItemClone.transform.localPosition = new Vector2(Util.GetInstance.GetDesignSize().x * 1.25f, Random.Range(SUPER_ITEM_YPOS_PADDING, Util.GetInstance.GetDesignSize().y - SUPER_ITEM_YPOS_PADDING));
            superItemClone.SetActive(true);
            superItemClone.transform.DOLocalMoveX(-Util.GetInstance.GetDesignSize().x * 1.5f, _superItemMopeTime).SetRelative().OnComplete(() => {
                superItemClone.SetActive(false);
            });
        }
    }

    /// <summary>
    /// 슈퍼 모드 콜백 함수
    /// </summary>
    public void SuperModeCallBack()
    {
        //슈퍼 아이템 생성 중지
        StopCoroutine("CSuperItemFactory");
        //슈퍼모드에서 배경이 변할 경우
        if(_superModeBackgroundTransform != null)
        {
            _superModeBackgroundTransform.GetChild(0).gameObject.SetActive(true);
            _superModeBackgroundTransform.GetComponent<LoopImageAction>().StartAction();
        }
        StartCoroutine("CSuperModeTimer");
    }

    /// <summary>
    /// 슈퍼모드 지속 
    /// </summary>
    /// <returns></returns>
    IEnumerator CSuperModeTimer()
    {
        yield return new WaitForSeconds(_superModeTime);
        //슈퍼 아이템 생성 재시작
        StartCoroutine("CSuperItemFactory");
        //배경 변하는 상태일 경우 정지
        if (_superModeBackgroundTransform != null)
        {
            _superModeBackgroundTransform.GetComponent<LoopImageAction>().LoopStop();
            for (int i = 0; i < _superModeBackgroundTransform.childCount; i++)
            {
                _superModeBackgroundTransform.GetChild(i).gameObject.SetActive(false);
            }
        }
        _superModeActorFinishEvent.Invoke();
        _superModeEnemyFinishEvent.Invoke();
    }

    /// <summary>
    /// 적이 죽었을 경우 콜백 함수
    /// </summary>
    public void EnemyDeadCallBack()
    {
        if((EAppearKind)_appearKind == EAppearKind.OneOnOne)
        {
            _currentEnemyNum++;
            StartCoroutine("CEnemyAppearSetting");
        }
    }
}
