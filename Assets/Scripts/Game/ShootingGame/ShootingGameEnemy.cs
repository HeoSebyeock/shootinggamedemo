﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.Events;

/// <summary>
/// 슈팅게임 악당
/// </summary>

public class ShootingGameEnemy : ShootingGameObject
{
    private enum EMoveAction
    {
        UserSettingPath = -1,
        UpAndDown,
        Square,
        Spiral
    }

    private enum EObjectAction
    {
        StartMoveAction,
        PowerAttackAction,
        PowerBulletHitAction,
        DeadAction,
    }

    
    [Header("HP")]
    [Space(20)]
    [SerializeField]
    private int _hitTotalCount;

    [Header("이동 속도")]
    [SerializeField]
    private float _moveActionSpeed;

    [Header("움직이는 루프 액션 전 초기 좌표값")]
    [SerializeField]
    private Vector2 _initialPosition;

    [Header("움직이는 액션 [-1.Path(사용자 지정) 0.UpAndDown 1.Square]")]
    [SerializeField]
    private int _moveActionNum;

    [Header("파워 공격 주기")]
    [SerializeField]
    private float _powerAttackWaitForTime;

    [Header("파워 공격 시 멈출지 여부")]
    [SerializeField]
    private bool _isPowerAttackMovePause;

    [Header("파워 총알 쏘는 횟수")]
    [SerializeField]
    private int _powerAttackLoopCount;

    [Header("사망 이벤트(=> 슈팅게임 매니져)")]
    [SerializeField]
    private UnityEvent _deadEvent;

    //현재 공격 받은 횟수
    private int _currentHitCount;
    //레이저에 맞고 있는지에 대한 여부
    private bool _isHitLaiser;

    /// <summary>
    /// TODO 움직이는 로직 추가할 경우 변경해야함
    /// </summary>
    private Sequence[] moveSequence;
    private const int MOVEACTION_SEQUENCE_SIZE = 2;

    // Start is called before the first frame update
    protected override void Start()
    {
        GetComponent<ObjectAction>().StartAction((int)EObjectAction.StartMoveAction);
        StartCoroutine("CPowerBulletFactory");
        MoveActionInitialized();

        base.Start();
    }

    void MoveActionInitialized()
    {
        //Up and Down loop Action
        moveSequence = new Sequence[MOVEACTION_SEQUENCE_SIZE];

        moveSequence[(int)EMoveAction.UpAndDown] = DOTween.Sequence();
        moveSequence[(int)EMoveAction.UpAndDown].Pause();
        moveSequence[(int)EMoveAction.UpAndDown].Append(transform.DOLocalMoveY(_moveAreaPos[(int)EMovePermitArea.EndPosY], (_moveAreaPos[(int)EMovePermitArea.EndPosY] - _initialPosition.y) / _moveActionSpeed));
        moveSequence[(int)EMoveAction.UpAndDown].Append(transform.DOLocalMoveY(_moveAreaPos[(int)EMovePermitArea.StartPosY], (_moveAreaPos[(int)EMovePermitArea.EndPosY] - _moveAreaPos[(int)EMovePermitArea.StartPosY]) / _moveActionSpeed));
        moveSequence[(int)EMoveAction.UpAndDown].Append(transform.DOLocalMoveY(_initialPosition.y, _initialPosition.y / _moveActionSpeed));
        moveSequence[(int)EMoveAction.UpAndDown].SetLoops(-1);

        //Square loop Action
        moveSequence[(int)EMoveAction.Square] = DOTween.Sequence();
        moveSequence[(int)EMoveAction.Square].Pause();
        moveSequence[(int)EMoveAction.Square].Append(transform.DOLocalMoveY(_moveAreaPos[(int)EMovePermitArea.EndPosY], (_moveAreaPos[(int)EMovePermitArea.EndPosY] - _initialPosition.y) / _moveActionSpeed));
        moveSequence[(int)EMoveAction.Square].Append(transform.DOLocalMoveX(_moveAreaPos[(int)EMovePermitArea.StartPosX], (_moveAreaPos[(int)EMovePermitArea.EndPosX] - _moveAreaPos[(int)EMovePermitArea.StartPosX]) / _moveActionSpeed));
        moveSequence[(int)EMoveAction.Square].Append(transform.DOLocalMoveY(_moveAreaPos[(int)EMovePermitArea.StartPosY], (_moveAreaPos[(int)EMovePermitArea.EndPosY] - _moveAreaPos[(int)EMovePermitArea.StartPosY]) / _moveActionSpeed));
        moveSequence[(int)EMoveAction.Square].Append(transform.DOLocalMoveX(_moveAreaPos[(int)EMovePermitArea.EndPosX], (_moveAreaPos[(int)EMovePermitArea.EndPosX] - _moveAreaPos[(int)EMovePermitArea.StartPosX]) / _moveActionSpeed));
        moveSequence[(int)EMoveAction.Square].Append(transform.DOLocalMoveY(_initialPosition.y, _initialPosition.y / _moveActionSpeed));
        moveSequence[(int)EMoveAction.Square].SetLoops(-1);

        StartCoroutine("CMoveAction");
    }

    IEnumerator CMoveAction()
    {
        yield return new WaitForSeconds(1);

        GetComponent<Collider2D>().enabled = true;

        if((EMoveAction)_moveActionNum == EMoveAction.UserSettingPath)
        {
            //진입 액션으로 흐트러진 처음 위치값 조정
            Vector3 firstWps = GetComponent<DOTweenPath>().path.wps[0];
            GetComponent<DOTweenPath>().path.wps[0] = transform.localPosition;
            GetComponent<DOTweenPath>().GetTween().OnComplete(() => {
                GetComponent<DOTweenPath>().path.wps[0] = firstWps;
                GetComponent<DOTweenPath>().DORestart();
            });
            GetComponent<DOTweenPath>().DOPlay();
        }
        else
        {
            moveSequence[_moveActionNum].Play();
        }
    }

    /// <summary>
    /// 파워 총알을 만드는 코루틴 (악당)
    /// </summary>
    /// <param name="type">총알 종류</param>
    /// <returns></returns>
    IEnumerator CPowerBulletFactory()
    {
        yield return new WaitForSeconds(_powerAttackWaitForTime);

        if (_isPowerAttackMovePause)
        {
            PowerAttackMovePlayOrPause(false);
        }

        //기본 공격을 멈춘다
        for (int i = 0; i < _normalBullets.Length; i++)
        {
            StopCoroutine(_normalBulletFactoryCoroutines[i]);
        }

        if (_animations[(int)EAnimation.SuperAttack] != "-1")
        {
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, _animations[(int)EAnimation.SuperAttack], true);
        }

        for (int i = 0; i < _powerAttackLoopCount; i++)
        {
            //지정된 파워 공격 액션 연출
            GetComponent<ObjectAction>().StartAction((int)EObjectAction.PowerAttackAction);

            GameObject bullet = _powerBullet.GetComponent<ObjectPooling>().GetObject(transform);
            bullet.transform.SetParent(_powerBullet.transform);
            yield return new WaitForSeconds(bullet.GetComponent<ShootingGameBullet>().BulletMakeDelayTime);
        }

        if (_isPowerAttackMovePause)
        {
            PowerAttackMovePlayOrPause(true);
        }

        //기본 공격 재개 / 코루틴 재실행 
        NormalBulletFactoryActivate();
        StartCoroutine("CPowerBulletFactory");
    }

    /// <summary>
    /// 파워 공격 시 이동 멈춤/재개
    /// </summary>
    /// <param name="isPlay"></param>
    void PowerAttackMovePlayOrPause(bool isPlay)
    {
        if (isPlay)
        {
            //이동 액션 재개
            if ((EMoveAction)_moveActionNum == EMoveAction.UserSettingPath)
            {
                GetComponent<DOTweenPath>().DOPlay();
            }
            else
            {
                moveSequence[_moveActionNum].Play();
            }
        }
        else
        {
            //이동 액션 멈춤
            if ((EMoveAction)_moveActionNum == EMoveAction.UserSettingPath)
            {
                GetComponent<DOTweenPath>().DOPause();
            }
            else
            {
                moveSequence[_moveActionNum].Pause();
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //기본 총알에 피격 당했을 경우
        if (collision.tag.Equals(Constants.ETag.ActorBullet.ToString()))
        {
            collision.gameObject.SetActive(false);
            StartCoroutine(CHitBulletAction(collision.gameObject));
            HitCountCalculate();
        }

        else if (collision.tag.Equals(Constants.ETag.Laiser.ToString()))
        {
            HitLaiserAction(collision.gameObject,true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.Laiser.ToString()))
        {
            HitLaiserAction(collision.gameObject, false);
        }
    }

    private void HitLaiserAction(GameObject collision, bool isHit)
    {
        _isHitLaiser = isHit;
        if (_isHitLaiser)
        {
            StartCoroutine(CHitBulletEffectLoop(collision.gameObject));

            for (int i = 0; i < _normalBullets.Length; i++)
            {
                StopCoroutine(_normalBulletFactoryCoroutines[i]);
            }

            BulletHitAction();
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, _animations[(int)EAnimation.Cry], true);
        }

        else
        {
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, _animations[(int)EAnimation.Idle], true);
        }
    }

    private IEnumerator CHitBulletEffectLoop(GameObject collision)
    {
        while (true)
        {
            if (!_isHitLaiser)
            {
                yield break;
            }
            HitCountCalculate();
            StartCoroutine(CHitBulletEffect(collision));
            yield return new WaitForSeconds(0.2f);
            
        }
    }

    void HitCountCalculate()
    {
        _currentHitCount++;
        if (_currentHitCount == _hitTotalCount)
        {
            for (int i = 0; i < _normalBullets.Length; i++)
            {
                StopCoroutine(_normalBulletFactoryCoroutines[i]);
            }
            StopCoroutine("CPowerBulletFactory");
            GetComponent<Collider2D>().enabled = false;
            _deadEvent.Invoke();
            StartCoroutine("CDeadAction");
        }
    }

    IEnumerator CDeadAction()
    {
        PowerAttackMovePlayOrPause(false);
        GetComponent<ObjectAction>().StartAction((int)EObjectAction.DeadAction);
        yield return new WaitForSpineAnimationComplete(GetComponent<SkeletonAnimation>().state.SetAnimation(0, _animations[(int)EAnimation.Dead], false));
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);
    }

    protected override void BulletHitAction()
    {
        GetComponent<ObjectAction>().StartAction((int)EObjectAction.PowerBulletHitAction);
    }

    /// <summary>
    /// 슈퍼 모드 끝 (CallBack)
    /// </summary>
    public override void SuperModeFinishCallBack()
    {
        _isHitLaiser = false;
    }
}

