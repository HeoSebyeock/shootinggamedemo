﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using UnityEngine.Events;

public class ShootingGameObject : MonoBehaviour
{
    protected enum EAnimation
    {
        Idle,
        Attack,
        Cry,
        Happy,
        SuperAttack,
        LaiserReady,
        LaiserLoop,
        Dead,
    }

    protected enum EMovePermitArea
    {
        StartPosX,
        EndPosX,
        StartPosY,
        EndPosY,
    }

    enum EBulletHitEffect
    {
        Image,
        Spine,
        Animation,
    }

    [Header("idle/Attack/Cry/Happy/SuperAttack/LaiserReady/LaiserLoop/Dead")]
    [Header("애니메이션")]
    [SerializeField]
    protected string[] _animations;

    [Header("움직일수 있는 영역 (0:X좌표 시작 1:X좌표 끝 2:Y좌표 시작 3:Y좌표 끝)")]
    [SerializeField]
    protected float[] _moveAreaPos;

    [Header("기본 총알")]
    [SerializeField]
    protected GameObject[] _normalBullets;

    [Header("파워 총알")]
    [SerializeField]
    protected GameObject _powerBullet;

    [Header("slot[0.Normal/1.Power]")]
    [Header("피격 시 멈춤 여부")]
    [SerializeField]
    private bool[] isStopHitByBullet;

    [Header("설정된 주기마다 총알을 랜덤하게 쏘는 경우(단발)")]
    [SerializeField]
    protected bool _isSameTimeShootBullet;

    [Header("피격 이펙트 Parent Transform")]
    [SerializeField]
    private Transform _hitBulletEffectParentTransform;

    [Header("value[0.Image/1.Spine/2.Animation]")]
    [Header("slot[0.Normal/1.Power]")]
    [Header("피격 이펙트")]
    [SerializeField]
    private int[] _hitByBulletEffects;

    private const float HIT_IMAGE_ACTIVE_TIME = 0.2f;
    private const float WAIT_FOR_BULLET_SHOT = 1.0f;

    //보통 총알 생성 코루틴
    protected Coroutine[] _normalBulletFactoryCoroutines;

    // Use this for initialization
    protected virtual void Start()
    {
        StartCoroutine("CGameLoop");
    }

    IEnumerator CGameLoop()
    {
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, _animations[(int)EAnimation.Idle], true);

        _normalBulletFactoryCoroutines = new Coroutine[_normalBullets.Length];
        
        //yield return null;
        //TODO 주인공이 등장 액션 동안 총알 쏘지 않도록 딜레이
        yield return new WaitForSeconds(WAIT_FOR_BULLET_SHOT);
        NormalBulletFactoryActivate();
        
    }

    /// <summary>
    /// 기본 총알을 만드는 곳을 작동 시킴
    /// </summary>
    protected void NormalBulletFactoryActivate()
    {
        //공격 모션이 대기 상태랑 같을 경우 공격 모션 슬롯에 대기를 넣는다
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, _animations[(int)EAnimation.Attack], true);
        if (_isSameTimeShootBullet)
        {
            _normalBulletFactoryCoroutines[0] = StartCoroutine(CNormalBulletFactory(-1));
        }
        else
        {
            for (int i = 0; i < _normalBullets.Length; i++)
            {
                _normalBulletFactoryCoroutines[i] = StartCoroutine(CNormalBulletFactory(i));
            }
        }
    }

    /// <summary>
    /// 기본 총알을 만드는 코루틴
    /// </summary>
    /// <param name="type">총알 종류</param>
    /// <returns></returns>
    IEnumerator CNormalBulletFactory(int type)
    {
        while (true)
        {
            int bulletType = type;

            if (type == -1)
            {
                bulletType = Random.Range(0, _normalBullets.Length);
            }
            else
            {
                bulletType = type;
            }
            GameObject bullet = _normalBullets[bulletType].GetComponent<ObjectPooling>().GetObject(transform);
            bullet.transform.localRotation = transform.localRotation;
            bullet.transform.SetParent(_normalBullets[bulletType].transform);
            bullet.transform.localScale = Vector2.one;
            yield return new WaitForSeconds(bullet.GetComponent<ShootingGameBullet>().BulletMakeDelayTime);
        }
    }

    /// <summary>
    /// 총알에 대한 피격 액션
    /// </summary>
    /// <param name="collision">총알</param>
    /// <returns></returns>
    protected IEnumerator CHitBulletAction(GameObject collision)
    {
        StartCoroutine(CHitBulletEffect(collision.gameObject));

        //피격 시 멈춤 여부
        if (isStopHitByBullet[(int)collision.GetComponent<ShootingGameBullet>().Kind])
        {
            //빼야하는디..
            //주인공일 경우
            if (collision.tag.Contains("Enemy"))
            {
                Util.GetInstance.IsNotTouched = true;
                
                GetComponent<UserInputGestureAction>().StopProgressEvent();
            }
            
            BulletHitAction();
            
            for (int i = 0; i < _normalBullets.Length; i++)
            {
                StopCoroutine(_normalBulletFactoryCoroutines[i]);
            }

            
        }

        //주인공일 경우
        if (collision.tag.Contains("Enemy"))
        {
            yield return new WaitForSpineAnimationComplete(GetComponent<SkeletonAnimation>().state.SetAnimation(0, _animations[(int)EAnimation.Cry], false));
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, _animations[(int)EAnimation.Attack], true);
            Util.GetInstance.IsNotTouched = false;
            GetComponent<Collider2D>().enabled = true;
        }
        
        if (isStopHitByBullet[(int)collision.GetComponent<ShootingGameBullet>().Kind])
        {
            NormalBulletFactoryActivate();
        }
    }

    /// <summary>
    /// 총알에 대한 피격 이펙트
    /// </summary>
    /// <param name="collision">총알</param>
    /// <returns></returns>
    protected IEnumerator CHitBulletEffect(GameObject collision)
    {
        int hitEffectKindNum = -1;

        //주인공 레이저인 경우
        if (collision.GetComponent<ShootingGameBullet>() == null){
            //Slot[0.Normal / 1.Power]
            hitEffectKindNum = 1;
        }
        else
        {
            hitEffectKindNum = (int)collision.GetComponent<ShootingGameBullet>().Kind;
        }

        GameObject effectObj = Instantiate(_hitBulletEffectParentTransform.GetChild(hitEffectKindNum).gameObject, _hitBulletEffectParentTransform);
        effectObj.SetActive(true);

        switch ((EBulletHitEffect)_hitByBulletEffects[hitEffectKindNum])
        {

            case EBulletHitEffect.Image:
                {
                    effectObj.transform.localPosition = collision.transform.localPosition;
                    yield return new WaitForSeconds(HIT_IMAGE_ACTIVE_TIME);
                }
                break;

            case EBulletHitEffect.Spine:
                {
                    //TODO 피격 스파인이 보통 커서 주인공 위치로 좌표를 지정했으나 적의 총알 위치로 할 경우 변경해야함 transform->collision.transform
                    effectObj.transform.localPosition = transform.localPosition;
                    yield return new WaitForSpineAnimationComplete(effectObj.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", false));
                }
                break;

            case EBulletHitEffect.Animation:
                {
                    effectObj.transform.localPosition = collision.transform.localPosition;

                    if (collision.GetComponent<ShootingGameBullet>() == null)
                    {
                        effectObj.transform.localPosition = transform.localPosition;
                    }

                    effectObj.GetComponent<LoopImageAction>().StartAction();
                    //애니메이션 1회 루프만큼 기다린다.
                    yield return new WaitForSeconds(effectObj.GetComponent<LoopImageAction>().LoopTime * effectObj.GetComponent<LoopImageAction>().GameObjs.Length);
                    effectObj.GetComponent<LoopImageAction>().LoopStop();
                }
                break;

            default:
                break;
        }

        //기본 공격 재개
        Destroy(effectObj);
    }

    protected virtual void BulletHitAction() { }
    public virtual void SuperModeFinishCallBack() { }
}
