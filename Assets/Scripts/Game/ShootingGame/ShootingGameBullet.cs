﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ShootingGameBullet : MonoBehaviour
{
    public enum EKind
    {
        Normal,
        Power,
    }

    [Header("총알 종류")]
    [SerializeField]
    private EKind _kind;
    public EKind Kind { get { return _kind; } }

    [Header("총알 시작 지점 (주인공 위치를 기본으로 +되는 좌표값)")]
    [SerializeField]
    private Vector3 _bulletAddPos;

    [Header("총알이 이동하는데 걸리는 시간")]
    [SerializeField]
    private float _bulletMoveTime;
    public float BulletMoveTime { get { return _bulletMoveTime; } }

    [Header("총알 재사용 시간")]
    [SerializeField]
    private float _bulletMakeDelayTime;
    public float BulletMakeDelayTime { get { return _bulletMakeDelayTime; } }

    [Header("총알 방향")]
    [SerializeField]
    private bool isRightToLeft;

    private void OnEnable()
    {
        transform.localPosition = Vector2.zero;
        transform.localPosition += _bulletAddPos;
        StartCoroutine("CMoveBullet");
    }

    IEnumerator CMoveBullet()
    {
        float direction = 1;
        if (isRightToLeft)
        {
            direction = -1;
        }
        transform.DOLocalMoveX(Util.GetInstance.GetDesignSize().x * 1.25f * direction, _bulletMoveTime).SetRelative();
        yield return new WaitForSeconds(_bulletMoveTime);
        transform.parent.GetComponent<ObjectPooling>().ReturnObject(gameObject);
    }
}
