﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using Spine.Unity;
using Spine;

/// <summary>
/// 슈팅게임 주인공
/// TODO 주인공의 파워 공격이 레이저일 경우 레이저 길이만큼 콜라이더 길이 조정 필요하다.
/// </summary>

public class ShootingGameActor : ShootingGameObject
{
    enum EObjectAction
    {
        StartMoveAction,
        SuperItemGetAction,
        EnemyPowerBulletHitAction,
    }

    [Header("레이저 (주인공 스파인에 포함 필수!)")]
    [Space(20)]
    [SerializeField]
    private bool _isSuperModeBulletLaiser;

    [Header("슈퍼 모드 이벤트(=> 슈팅게임 매니져)")]
    [SerializeField]
    private UnityEvent _superModeEvent;

    private const int LAISER_COLLIDER_CHILD = 0;

    //슈퍼 모드
    private bool _isSuperMode;

    // Start is called before the first frame update
    protected override void Start()
    {
        GetComponent<ObjectAction>().StartAction((int)EObjectAction.StartMoveAction);
        GetComponent<UserInputGestureAction>().SetSuccessCollectArea(_moveAreaPos);
        StartCoroutine("CPowerBulletFactory");

        base.Start();
    }

    /// <summary>
    /// 파워 총알을 만드는 코루틴 (주인공)
    /// </summary>
    /// <param name="type">총알 종류</param>
    /// <returns></returns>
    IEnumerator CPowerBulletFactory()
    {
        yield return new WaitUntil(() => _isSuperMode);

        for (int i = 0; i < _normalBullets.Length; i++)
        {
            StopCoroutine(_normalBulletFactoryCoroutines[i]);
        }

        if (_isSuperModeBulletLaiser)
        {
            //TODO 레이저 길어지는 시간 동안 콜라이더 스케일도 늘어나야함 0.5초로 지정 했으나 달라질수 있음
            transform.GetChild(LAISER_COLLIDER_CHILD).transform.DOScaleX(1, 0.5f);
            yield return new WaitForSpineAnimationComplete(GetComponent<SkeletonAnimation>().state.SetAnimation(0, _animations[(int)EAnimation.LaiserReady], false));
            
            TrackEntry trackEntry = GetComponent<SkeletonAnimation>().state.SetAnimation(0, _animations[(int)EAnimation.LaiserLoop], true);
            
        }
        else
        {
            while (true)
            {
                if (_animations[(int)EAnimation.SuperAttack] != "-1")
                {
                    GetComponent<SkeletonAnimation>().state.SetAnimation(0, _animations[(int)EAnimation.SuperAttack], true);
                }

                GameObject bullet = _powerBullet.GetComponent<ObjectPooling>().GetObject(transform);
                bullet.transform.SetParent(_powerBullet.transform);
                yield return new WaitForSeconds(bullet.GetComponent<ShootingGameBullet>().BulletMakeDelayTime);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //TODO 슈퍼 모드일 경우 충돌 체크 무시하나 필요에 따라 변경
        if (_isSuperMode)
        {
            return;
        }

        //슈퍼 아이템을 먹었을 경우
        if (collision.tag.Equals(Constants.ETag.SuperItem.ToString()))
        {
            collision.gameObject.SetActive(false);
            GetComponent<ObjectAction>().StartAction((int)EObjectAction.SuperItemGetAction);
            _isSuperMode = true;
            _superModeEvent.Invoke();
        }

        //기본 총알에 피격 당했을 경우
        else if (collision.tag.Equals(Constants.ETag.EnemyBullet.ToString()))
        {
            collision.gameObject.SetActive(false);
            GetComponent<Collider2D>().enabled = false;
            StartCoroutine(CHitBulletAction(collision.gameObject));
        }
    }



    /// <summary>
    /// 슈퍼 모드 끝 (CallBack)
    /// </summary>
    public override void SuperModeFinishCallBack()
    {
        _isSuperMode = false;

        //레이저 콜라이더 스케일 초기화
        if (_isSuperModeBulletLaiser)
        {
            transform.GetChild(LAISER_COLLIDER_CHILD).transform.localScale = new Vector2(0,1);
        }

        StopCoroutine("CPowerBulletFactory");
        StartCoroutine("CPowerBulletFactory");
        NormalBulletFactoryActivate();
    }

    protected override void BulletHitAction()
    {
        GetComponent<ObjectAction>().StartAction((int)EObjectAction.EnemyPowerBulletHitAction);
        
    }
}
