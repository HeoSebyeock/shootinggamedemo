﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.Events;
using UnityEditor;

public class ObjectAction : MonoBehaviour
{
    [Header("오브젝트_데이터")]
    [SerializeField]
    private ObjectData _objectData;
    public ObjectData ObjectData { get { return _objectData; } }

    [Header("게임오브젝트")]
    [SerializeField]
    private GameObject _gameObject;
    public GameObject GameObject { get { return _gameObject; } }

    [Header("액션_데이터")]
    [SerializeField]
    private ActionData[] _actionData;
    public ActionData[] GetActionData { get { return _actionData; } }

    [Header("액션 끝나고 visible 여부")]
    [SerializeField]
    private bool _isEnable;
    public bool IsVisible { get; }

    private Tween _tween;
    private Sequence _sequence;
    private bool _isRelative = false;
    private float _totalTime;

    private int _selectActionNum;

    //리스트에 넣은 액션을 전부 재생 할 경우 값 = -1
    private const int MULTI_ACTION = -1;

    /// <summary>
    /// 오브 젝트 행동을 시작합니다.
    /// </summary>
    /// <param name="selectNum"></param>
    public void StartAction(int selectNum = MULTI_ACTION,bool isRelative = false)
    {
        _selectActionNum = selectNum;
        _isRelative = isRelative;
        StartCoroutine(CoObjectAction());
    }

    public void CreateObjectAction(ObjectData objectData,ActionData[] actionData,bool isEnable = false)
    {
        _objectData = objectData;
        _actionData = actionData;
        _isEnable = isEnable;
    }

    public void CreateObjectAction(GameObject gameObject, ActionData[] actionData, bool isEnable = false)
    {
        _gameObject = gameObject;
        _actionData = actionData;
        _isEnable = isEnable;
    }

    IEnumerator CoObjectAction()
    {
        yield return new WaitUntil(() => _actionData != null);
        DefaultSetting();

        //여러 액션 재생일 경우
        if(_selectActionNum == MULTI_ACTION)
        {
            for (int i = 0; i < _actionData.Length; i++)
            {
                _totalTime += _actionData[i].Time;
                JudgeAndExecute(i);
                _sequence.Insert(_actionData[i].ActionWaitTime, _tween);
            }
        }
        //원하는 단일 액션 재생일 경우
        else
        {
            _totalTime += _actionData[0].Time;
            JudgeAndExecute(_selectActionNum);
        }
        
        yield return null;
    }

    void DefaultAction(ActionData data)
    {
        switch (data.Type)
        {
            case ActionData.EType.Jump:
                {
                    if (_isRelative)
                    {
                        _tween = _gameObject.transform.DOLocalJump(data.MovePosition, data.JumpHeight, data.JumpCount, data.Time).SetRelative();
                    }
                    else
                    {
                        _tween = _gameObject.transform.DOLocalJump(data.MovePosition, data.JumpHeight, data.JumpCount, data.Time);
                    }
                }
                break;

            case ActionData.EType.Move:
                {
                    if (_isRelative)
                    {
                        _tween = _gameObject.transform.DOLocalMove(data.MovePosition, data.Time).SetRelative();
                    }
                    else
                    {
                        _tween = _gameObject.transform.DOLocalMove(data.MovePosition, data.Time);
                    }
                }
                break;

            case ActionData.EType.Rotate:
                {
                    if (_isRelative)
                    {
                        _tween = _gameObject.transform.DOLocalRotate(new Vector3(0, 0, data.RotateAngle), data.Time, data.RotateMode).SetRelative();
                    }
                    else
                    {
                        _tween = _gameObject.transform.DOLocalRotate(new Vector3(0, 0, data.RotateAngle), data.Time, data.RotateMode);
                    }
                }
                break;

            case ActionData.EType.Scale:
                {
                    if (_isRelative)
                    {
                        _tween = _gameObject.transform.DOScale(data.ChangeScale, data.Time).SetRelative();
                    }
                    else
                    {
                        _tween = _gameObject.transform.DOScale(data.ChangeScale, data.Time);
                    }
                }
                break;

            default:
                break;
        }
    }

    /// <summary>
    /// 행동 부사에 따른 액션 실행
    /// </summary>
    /// <param name="selectNum"></param>
    void JudgeAndExecute(int selectNum)
    {
        switch (_actionData[selectNum].Adverb)
        {
            case ActionData.EAdverb.Default:
                {
                    DefaultAction(_actionData[selectNum]);
                }
                break;

            case ActionData.EAdverb.Punch:
                {
                    PunchAction(_actionData[selectNum]);
                }
                break;

            case ActionData.EAdverb.Shake:
                {
                    ShakeAction(_actionData[selectNum]);
                }
                break;

            default:
                break;
        }
        _tween.SetEase(_actionData[selectNum].Ease).SetLoops(_actionData[selectNum].LoopCount, _actionData[selectNum].LoopType).OnComplete(() => {
            if (_isEnable)
            {
                KillTween();
                _gameObject.SetActive(false);
            }
        });
    }

    void PunchAction(ActionData data)
    {
        switch (data.Type)
        {
            case ActionData.EType.Move:
                {
                    _tween = _gameObject.transform.DOPunchPosition(data.MovePosition, data.Time, data.Vibrato, data.Elastic);
                }
                break;

            case ActionData.EType.Rotate:
                {
                    
                    _tween = _gameObject.transform.DOPunchRotation(new Vector3(0, 0, data.RotateAngle), data.Time, data.Vibrato, data.Elastic);
                }
                break;

            case ActionData.EType.Scale:
                {
                    _tween = _gameObject.transform.DOPunchScale(data.ChangeScale, data.Time, data.Vibrato, data.Elastic);
                }
                break;

            default:
                break;
        }
    }

    void ShakeAction(ActionData data)
    {
        switch (data.Type)
        {
            case ActionData.EType.Move:
                {
                    _tween = _gameObject.transform.DOShakePosition(data.Time, data.Strength, data.Vibrato, data.ShakeRandom);
                }
                break;

            case ActionData.EType.Rotate:
                {
                    _tween = _gameObject.transform.DOShakeRotation(data.Time, data.Strength, data.Vibrato, data.ShakeRandom);
                }
                break;

            case ActionData.EType.Scale:
                {
                    _tween = _gameObject.transform.DOShakeScale(data.Time, data.Strength, data.Vibrato, data.ShakeRandom);
                }
                break;

            default:
                break;
        }
    }

    

    void DefaultSetting()
    {
        _sequence = DOTween.Sequence();

        //게임 오브젝트 생성
        if(_gameObject == null && _objectData == null)
        {
            _gameObject = gameObject;
        }
        else if (_gameObject == null)
        {
            CreateFactory createFactory = new CreateFactory();
            _gameObject = createFactory.CreateGameObject(_objectData.ObjectType, Constants.EFolderRootType.Images, Constants.EFolderLeafType.Character, _objectData.ImageName, true, transform);
            _gameObject.transform.localPosition = _objectData.Position;
        }
    }

    /// <summary>
    /// 이동할 위치 변경할경우 단, 단일 액션일 경우에 사용가능하다.
    /// </summary>
    public void SetPosition(Vector2 pos,int actionIndex = 0)
    {
        _actionData[actionIndex].MovePosition = pos;
    }

    /// <summary>
    /// 액션 없애기
    /// </summary>
    public void KillTween()
    {
        _tween.Kill();
        _sequence.Kill();
    }

    /// <summary>
    /// 액션 일시 정지
    /// </summary>
    public void PauseTween()
    {
        _tween.Pause();
        _sequence.Pause();
    }

    /// <summary>
    /// 액션 다시 시작
    /// </summary>
    public void RestartTween()
    {
        _tween.Play();
        _sequence.Play();
    }

    /// <summary>
    /// 액션에 걸리는 시간
    /// </summary>
    /// <returns></returns>
    public float GetActionTotalTime()
    {
        return _totalTime;
    }
    /// <summary>
    /// 액션 데이터 길이
    /// </summary>
    /// <returns></returns>
    public int GetActionDataLength()
    {
        return _actionData.Length;
    }
}



