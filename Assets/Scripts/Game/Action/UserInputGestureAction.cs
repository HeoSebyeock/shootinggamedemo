﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// 사용자의 제스쳐에 따른 행동 클래스
/// </summary>
public class UserInputGestureAction : MonoBehaviour
{
    public enum EType
    {
        Tap,
        DoubleTap,

        TouchDownAndUp,

        Press,

        DragAndDropCenterPos,
        DragAndDropPickedPos,

        FlickLeftToRight,
        FlickRightToLeft,

        SwipeUp,
        SwipeDown,
        SwipeLeft,
        SwipeRight,

        Rotate,
    }

    public enum EJudgeWay
    {
        Distance,
        Collider,
    }

    public enum ESuccessAreaPos
    {
        StartPosX,
        EndPosX,
        StartPosY,
        EndPosY,
    }

    //커스텀 에디터에서 변수 설명
    public EType _type;
    public EJudgeWay _judgeWay;
    public bool _isNotDestroy = false;
    public Vector3 _addTouchPos;
    public Vector3 _addMovePos;

    [Header("대상이 되는 오브젝트")]
    [SerializeField]
    private GameObject _goalGameObject;
    public GameObject GoalGameObject { get { return _goalGameObject; } set { _goalGameObject = value; } }

    [Header("조건_충족_완료_이벤트")]
    [SerializeField]
    public UnityEvent _completeEvent;

    [Header("조건에_대한_진행_이벤트")]
    [SerializeField]
    public UnityEvent _progressingEvent;

    [Header("조건_진행_중_실패_이벤트")]
    [SerializeField]
    public UnityEvent _failEvent;

    [Header("조건_진행_중_힌트_시작_이벤트")]
    [SerializeField]
    public UnityEvent _hintStartEvent;

    [Header("조건_진행_중_힌트_멈춤_이벤트")]
    [SerializeField]
    public UnityEvent _hintStopEvent;

    //[Header("유저 인풋 제스처 컴포넌트 삭제 여부")]
    //[SerializeField]
    //public bool _isNotDestroy = false;
    //public bool IsNotDestroy { get; set; }

    //커스텀 에디터에서 변수 설명
    public float _successNeedsTime = DEFAULT_SUCCESS_NEEDS_TIME;
    public float SuccessNeedsTime { get { return _successNeedsTime; } set { _successNeedsTime = value; } }

    public float _successNeedsDistance = DEFAULT_SUCCESS_NEEDS_DISTANCE;
    public float SuccessNeedsDistance { get { return _successNeedsDistance; } set { _successNeedsDistance = value; } }

    public Vector2 _successNeedsPosition; 
    public Vector2 SuccessNeedsPosition { get { return _successNeedsPosition; } set { _successNeedsPosition = value; } }

    //판단 방법에 대한 조건 설정
    private Collider2D _collider2D;
    private float _collectDistance;

    //터치에 대한 유무 및 조건 제약
    private bool _isConditionStart = false;
    //힌트에 대한 조건 제약 설정
    private bool _isConditionHintStart = false;

    //터치 완료 조건이 콜라이더 판정인 경우
    private Collider2D _completeCollider;

    /// <summary>
    /// 터치가 성공 한 최초 위치
    /// </summary>
    private Vector3 _inputFirstPos;

    /// <summary>
    /// 드래그일 경우 드래그 범위 설정
    /// </summary>
    private float[] _successAreaPos;

    const float DOUBLE_TAP_SUCCESS_TIME = 0.25f;
    const float FLICK_SUCCESS_TIME = 0.35f;
    const float FLICK_SUCCESS_DISTANCE = 150f;

    public const float DEFAULT_SUCCESS_NEEDS_TIME = 5f;
    public const float DEFAULT_SUCCESS_NEEDS_DISTANCE = 150f;

    //
    private void OnEnable()
    {
        StartCoroutine(CoDefaultSetting());
    }

    /// <summary>
    /// 동적으로 제스쳐 인풋에 따른 행동을 생성 및 정의합니다.
    /// </summary>
    /// <param name="type">종류</param>
    /// <param name="judgeWay">판단 방법</param>
    /// <param name="goalGameObject">주체 오브젝트</param>
    /// <param name="completeAction">성공 시 행동</param>
    /// <param name="progressingAction">진행 중 행동</param>
    /// <param name="failAction">실패 시 행동</param>
    /// <param name="hintStartAction">힌트 시작 시 행동</param>
    /// <param name="hintStopAction">힌트 정지 시 행동</param>
    /// <param name="successNeedsTime">성공에 필요한 시간</param>
    /// <param name="successNeedsDistance">성공에 필요한 거리</param>
    /// <param name="successNeedsPosition">성공 지점</param>
    public void CreateInputGesture(EType type, EJudgeWay judgeWay, GameObject goalGameObject, UnityAction completeAction = null, UnityAction progressingAction = null, UnityAction failAction = null, UnityAction hintStartAction = null, UnityAction hintStopAction = null,
        float successNeedsTime = DEFAULT_SUCCESS_NEEDS_TIME, float successNeedsDistance = DEFAULT_SUCCESS_NEEDS_DISTANCE, Vector2 successNeedsPosition = default)
    {
        _type = type;
        _judgeWay = judgeWay;
        _goalGameObject = goalGameObject;

        if(completeAction != null)
        {
            _completeEvent = new UnityEvent();
            _completeEvent.AddListener(completeAction);
        }
        if (progressingAction != null)
        {
            _progressingEvent = new UnityEvent();
            _progressingEvent.AddListener(progressingAction);
        }
        if (failAction != null)
        {
            _failEvent = new UnityEvent();
            _failEvent.AddListener(failAction);
        }

        if(hintStartAction != null)
        {
            _hintStartEvent = new UnityEvent();
            _hintStartEvent.AddListener(hintStartAction);
        }

        if (hintStopAction != null)
        {
            _hintStopEvent = new UnityEvent();
            _hintStopEvent.AddListener(hintStopAction);
        }

        _successNeedsTime = successNeedsTime;
        _successNeedsDistance = successNeedsDistance;
        _successNeedsPosition = successNeedsPosition;
    }

    //판단 방법 설정
    IEnumerator CoDefaultSetting()
    {
        yield return new WaitUntil(() => _goalGameObject != null);

        if (_successAreaPos == null)
        {
            Debug.Log("create area");
            _successAreaPos = new float[] { 0, Util.GetInstance.GetDesignSize().x, 0, Util.GetInstance.GetDesignSize().y };
        }

        if (_judgeWay == EJudgeWay.Collider)
        {
            _collider2D = _goalGameObject.GetComponent<Collider2D>();

            if (_collider2D == null)
            {
                Debug.Log("ERROR!!!! 오브젝트에 콜라이더를 추가 해주세요.");
                yield break;
            }
        }

        else if(_judgeWay == EJudgeWay.Distance)
        {
            _collectDistance = Mathf.Sqrt(Mathf.Pow(_goalGameObject.transform.GetComponent<SpriteRenderer>().sprite.rect.size.x, 2) + Mathf.Pow(_goalGameObject.transform.GetComponent<SpriteRenderer>().sprite.rect.size.y, 2)) * 0.4f;
        }
        StartCoroutine(CoConditionCheck());
    }

    //Game Loop (Input check)
    IEnumerator CoConditionCheck()
    {
        while (true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                switch (_type)
                {
                    case EType.Tap:
                    case EType.DoubleTap:
                    case EType.TouchDownAndUp:
                    case EType.DragAndDropCenterPos:
                    case EType.DragAndDropPickedPos:
                    case EType.FlickLeftToRight:
                    case EType.FlickRightToLeft:
                    case EType.SwipeUp:
                    case EType.SwipeDown:
                    case EType.SwipeLeft:
                    case EType.SwipeRight:
                        {
                            TouchAction();
                        }
                        break;
                    default:
                        break;
                }
            }

            if (Input.GetMouseButton(0))
            {
                switch (_type)
                {
                    case EType.Press:
                    case EType.Rotate:
                        {
                            TouchAction();
                        }
                        break;
                    default:
                        break;
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                switch (_type)
                {
                    case EType.Press:
                    case EType.Rotate:
                    case EType.FlickLeftToRight:
                    case EType.FlickRightToLeft:
                    case EType.SwipeDown:
                    case EType.SwipeUp:
                    case EType.SwipeLeft:
                    case EType.SwipeRight:
                        {
                            if (_isConditionStart)
                            {
                                ProgressStopAndFailCallBack();
                            }
                        }
                        break;

                    case EType.DragAndDropCenterPos:
                    case EType.DragAndDropPickedPos:
                        {
                            if (_isConditionHintStart && _isConditionStart)
                            {
                                Debug.Log("success touch" + _goalGameObject.name);
                                ConditionComplete();
                            }
                            else if(_isConditionStart)
                            {
                                Debug.Log("fail touch" + _goalGameObject.name);
                                ProgressStopAndFailCallBack();
                            }
                        }
                        break;

                    case EType.TouchDownAndUp:
                        {
                            if (_isConditionStart)
                            {
                                if (_judgeWay == EJudgeWay.Collider)
                                {
                                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                                    RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);
                                    if (hit.collider != null && hit.collider.gameObject.Equals(_goalGameObject))
                                    {
                                        ConditionComplete();
                                    }
                                    else
                                    {
                                        ProgressStopAndFailCallBack();
                                    }
                                }
                                else
                                {
                                    ConditionComplete();
                                }
                            }
                        }
                        break;
                }
            }
            yield return null;
        }
        
    }

    //터치 성공 / 실패
    void TouchAction()
    {
        if (Util.GetInstance.IsNotTouched)
        {
            Debug.Log("Touch NO !!!!!!");
            return;
        }
        switch (_judgeWay)
        {
            case EJudgeWay.Collider:
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);
                    if (hit.collider != null && hit.collider.gameObject.Equals(_goalGameObject))
                    {
                        TouchedCollect();
                    }

                    else
                    {
                        TouchedFail();
                    }
                }
                break;
            case EJudgeWay.Distance:
                {
                    if (Vector2.Distance(Input.mousePosition,_goalGameObject.transform.localPosition + _addTouchPos) < _collectDistance)
                    {
                        TouchedCollect();
                    }
                    else
                    {
                        TouchedFail();
                    }
                }
                break;
            default:
                break;
        }
        
    }

    //터치가 성공일 경우
    void TouchedCollect()
    {
        switch (_type)
        {
            case EType.Tap:
                {
                    ConditionComplete();
                }
                break;

            case EType.DoubleTap:
            case EType.TouchDownAndUp:
            case EType.Press:
            case EType.DragAndDropCenterPos:
            case EType.DragAndDropPickedPos:
            case EType.FlickLeftToRight:
            case EType.FlickRightToLeft:
            case EType.SwipeUp:
            case EType.SwipeDown:
            case EType.SwipeLeft:
            case EType.SwipeRight:
            case EType.Rotate:
                {
                    if (!_isConditionStart)
                    {
                        if(_type == EType.DragAndDropPickedPos)
                        {
                            _inputFirstPos = _goalGameObject.transform.localPosition - Input.mousePosition;
                        }
                        else
                        {
                            _inputFirstPos = Input.mousePosition;
                        }

                        StartCoroutine("CoProgressing");
                    }

                    else if (_type == EType.DoubleTap)
                    {
                        ConditionComplete();
                    }
                }
                break;
        }
    }

    //터치가 실패 했을 경우
    void TouchedFail()
    {
        switch (_type)
        {
            case EType.Press:
                {
                    if (_isConditionStart)
                    {
                        ProgressStopAndFailCallBack();
                    }
                }
                break;
            default:
                break;
        }
    }

    //터치가 성공 영역에 도달 하여 진행 중일 경우
    IEnumerator CoProgressing()
    {
        _isConditionStart = true;
        if(_progressingEvent != null)
        {
            _progressingEvent.Invoke();
        }
        
        switch (_type)
        {
            case EType.DoubleTap:
                {
                    yield return new WaitForSeconds(DOUBLE_TAP_SUCCESS_TIME);
                    _isConditionStart = false;
                }
                break;
            case EType.Press:
                {
                    yield return new WaitForSeconds(_successNeedsTime);
                    ConditionComplete();
                }
                break;
            case EType.DragAndDropCenterPos:
            case EType.DragAndDropPickedPos:
                {
                    while (true)
                    {
                        if (_successAreaPos[(int)ESuccessAreaPos.StartPosX] <= (Input.mousePosition.x + _addMovePos.x) && _successAreaPos[(int)ESuccessAreaPos.EndPosX] > (Input.mousePosition.x + _addMovePos.x)
                            && _successAreaPos[(int)ESuccessAreaPos.StartPosY] <= (Input.mousePosition.y + _addMovePos.y) && _successAreaPos[(int)ESuccessAreaPos.EndPosY] > (Input.mousePosition.y + _addMovePos.y))
                        {
                            if (_type == EType.DragAndDropCenterPos)
                            {
                                _goalGameObject.transform.localPosition = Input.mousePosition + _addMovePos;
                            }
                            else if (_type == EType.DragAndDropPickedPos)
                            {
                                _goalGameObject.transform.localPosition = Input.mousePosition + _inputFirstPos + _addMovePos;
                            }
                        }

                        DragAreaEscape();

                        HintCheck();
                        yield return null;
                    }
                }
            case EType.FlickLeftToRight:
            case EType.FlickRightToLeft:
            {
                    StartCoroutine("CoFlickConditionTimeCheck");
                    while (true)
                    {
                        if (_inputFirstPos.x + FLICK_SUCCESS_DISTANCE < Input.mousePosition.x && _type == EType.FlickLeftToRight)
                        {
                            StopCoroutine("CoFlickConditionTimeCheck");
                            ConditionComplete();
                        }
                        else if (_inputFirstPos.x - FLICK_SUCCESS_DISTANCE > Input.mousePosition.x && _type == EType.FlickRightToLeft)
                        {
                            StopCoroutine("CoFlickConditionTimeCheck");
                            ConditionComplete();
                        }
                        yield return null;
                    }
                }

            case EType.SwipeDown:
            case EType.SwipeUp:
            case EType.SwipeLeft:
            case EType.SwipeRight:
                {
                    while (true)
                    {
                        if (_inputFirstPos.y - _successNeedsDistance > Input.mousePosition.y && _type == EType.SwipeDown
                            || _inputFirstPos.y + _successNeedsDistance < Input.mousePosition.y && _type == EType.SwipeUp
                            || _inputFirstPos.x - _successNeedsDistance > Input.mousePosition.x && _type == EType.SwipeLeft
                            || _inputFirstPos.x + _successNeedsDistance < Input.mousePosition.x && _type == EType.SwipeRight)
                        {
                            ConditionComplete();
                        }
                        yield return null;
                    }
                }

            case EType.Rotate:
                {
                    float firstAngle = 0;
                    while (true)
                    {
                        Vector2 t = new Vector2(Input.mousePosition.x - _goalGameObject.transform.localPosition.x, Input.mousePosition.y - _goalGameObject.transform.localPosition.y);
                        if (firstAngle == 0)
                        {
                            firstAngle = Mathf.Atan2(t.y, t.x) * Mathf.Rad2Deg + 180 + firstAngle - _goalGameObject.transform.eulerAngles.z;
                        }

                        float angle = Mathf.Atan2(t.y, t.x) * Mathf.Rad2Deg + 180 - firstAngle;
                        _goalGameObject.transform.localRotation = Quaternion.Euler(0, 0, angle);
                        
                        yield return null;
                    }
                }
            default:
                break;
        }
    }

    void DragAreaEscape()
    {
        // 설정 범위를 벗어날 경우
        if (_successAreaPos[(int)ESuccessAreaPos.StartPosX] > (Input.mousePosition.x + _addMovePos.x) && _successAreaPos[(int)ESuccessAreaPos.StartPosY] > (Input.mousePosition.y + _addMovePos.y))
        {
            if (_type == EType.DragAndDropCenterPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(_successAreaPos[(int)ESuccessAreaPos.StartPosX], _successAreaPos[(int)ESuccessAreaPos.StartPosY]) + _addMovePos;
            }
            else if (_type == EType.DragAndDropPickedPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(_successAreaPos[(int)ESuccessAreaPos.StartPosX], _successAreaPos[(int)ESuccessAreaPos.StartPosY]) + _inputFirstPos + _addMovePos;
            }
        }

        else if (_successAreaPos[(int)ESuccessAreaPos.EndPosX] <= (Input.mousePosition.x + _addMovePos.x) && _successAreaPos[(int)ESuccessAreaPos.StartPosY] > (Input.mousePosition.y + _addMovePos.y))
        {
            if (_type == EType.DragAndDropCenterPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(_successAreaPos[(int)ESuccessAreaPos.EndPosX], _successAreaPos[(int)ESuccessAreaPos.StartPosY]) + _addMovePos;
            }
            else if (_type == EType.DragAndDropPickedPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(_successAreaPos[(int)ESuccessAreaPos.EndPosX], _successAreaPos[(int)ESuccessAreaPos.StartPosY]) + _inputFirstPos + _addMovePos;
            }
        }

        else if (_successAreaPos[(int)ESuccessAreaPos.StartPosX] > (Input.mousePosition.x + _addMovePos.x) && _successAreaPos[(int)ESuccessAreaPos.EndPosY] <= (Input.mousePosition.y + _addMovePos.y))
        {
            if (_type == EType.DragAndDropCenterPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(_successAreaPos[(int)ESuccessAreaPos.StartPosX], _successAreaPos[(int)ESuccessAreaPos.EndPosY]) + _addMovePos;
            }
            else if (_type == EType.DragAndDropPickedPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(_successAreaPos[(int)ESuccessAreaPos.StartPosX], _successAreaPos[(int)ESuccessAreaPos.EndPosY]) + _inputFirstPos + _addMovePos;
            }
        }

        else if (_successAreaPos[(int)ESuccessAreaPos.EndPosX] <= (Input.mousePosition.x + _addMovePos.x) && _successAreaPos[(int)ESuccessAreaPos.EndPosY] <= (Input.mousePosition.y + _addMovePos.y))
        {
            if (_type == EType.DragAndDropCenterPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(_successAreaPos[(int)ESuccessAreaPos.EndPosX], _successAreaPos[(int)ESuccessAreaPos.EndPosY]) + _addMovePos;
            }
            else if (_type == EType.DragAndDropPickedPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(_successAreaPos[(int)ESuccessAreaPos.EndPosX], _successAreaPos[(int)ESuccessAreaPos.EndPosY]) + _inputFirstPos + _addMovePos;
            }
        }

        /////////

        else if (_successAreaPos[(int)ESuccessAreaPos.StartPosX] > (Input.mousePosition.x + _addMovePos.x))
        {
            if (_type == EType.DragAndDropCenterPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(_successAreaPos[(int)ESuccessAreaPos.StartPosX], Input.mousePosition.y) + _addMovePos;
            }
            else if (_type == EType.DragAndDropPickedPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(_successAreaPos[(int)ESuccessAreaPos.StartPosX], Input.mousePosition.y) + _inputFirstPos + _addMovePos;
            }
        }

        else if (_successAreaPos[(int)ESuccessAreaPos.EndPosX] <= (Input.mousePosition.x + _addMovePos.x))
        {
            if (_type == EType.DragAndDropCenterPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(_successAreaPos[(int)ESuccessAreaPos.EndPosX], Input.mousePosition.y) + _addMovePos;
            }
            else if (_type == EType.DragAndDropPickedPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(_successAreaPos[(int)ESuccessAreaPos.EndPosX], Input.mousePosition.y) + _inputFirstPos + _addMovePos;
            }
        }

        else if (_successAreaPos[(int)ESuccessAreaPos.StartPosY] > (Input.mousePosition.y + _addMovePos.y))
        {
            if (_type == EType.DragAndDropCenterPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(Input.mousePosition.x, _successAreaPos[(int)ESuccessAreaPos.StartPosY]) + _addMovePos;
            }
            else if (_type == EType.DragAndDropPickedPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(Input.mousePosition.x, _successAreaPos[(int)ESuccessAreaPos.StartPosY]) + _inputFirstPos + _addMovePos;
            }
        }

        else if (_successAreaPos[(int)ESuccessAreaPos.EndPosY] <= (Input.mousePosition.y + _addMovePos.y))
        {
            if (_type == EType.DragAndDropCenterPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(Input.mousePosition.x, _successAreaPos[(int)ESuccessAreaPos.EndPosY]) + _addMovePos;
            }
            else if (_type == EType.DragAndDropPickedPos)
            {
                _goalGameObject.transform.localPosition = new Vector3(Input.mousePosition.x, _successAreaPos[(int)ESuccessAreaPos.EndPosY]) + _inputFirstPos + _addMovePos;
            }
        }
    }

    //조건 충족 및 성공 액션 호출
    void ConditionComplete()
    {
        StopCoroutine("CoProgressing");

        if (_hintStopEvent != null)
        {
            _hintStopEvent.Invoke();
        }
        if(_completeEvent != null)
        {
            _completeEvent.Invoke();
        }
        if (!_isNotDestroy)
        {
            Destroy(this);
        }
        else
        {
            StopCoroutine("CoProgressing");

            _isConditionStart = false;
        }
    }

    //조건 실패 및 실패 액션 호출
    void ProgressStopAndFailCallBack()
    {
        StopCoroutine("CoProgressing");

        _isConditionStart = false;

        if (_failEvent != null)
        {
            _failEvent.Invoke();
        }
    }

    //긋기 액션이 실패할 경우 
    IEnumerator CoFlickConditionTimeCheck()
    {
        yield return new WaitForSeconds(FLICK_SUCCESS_TIME);
        ProgressStopAndFailCallBack();
    }

    //힌트 상태 액션 호출
    void HintCheck()
    {
        if(_completeCollider != null)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);
            if (hit.collider != null && hit.collider.gameObject.Equals(_completeCollider.gameObject) && !_isConditionHintStart)
            {
                _isConditionHintStart = true;
                _hintStartEvent.Invoke();
            }
            else if(hit.collider == null && _isConditionHintStart)
            {
                _isConditionHintStart = false;
                _hintStopEvent.Invoke();
            }
        }
        else
        {
            if (Vector2.Distance(Input.mousePosition, _successNeedsPosition) < _successNeedsDistance && !_isConditionHintStart)
            {
                _isConditionHintStart = true;
                _hintStartEvent.Invoke();
            }

            else if (Vector2.Distance(Input.mousePosition, _successNeedsPosition) > _successNeedsDistance && _isConditionHintStart)
            {
                _isConditionHintStart = false;
                _hintStopEvent.Invoke();
            }
        }
    }
    /// <summary>
    /// 부모 트랜스폼 하위에 있는 오브젝트라서 좌표가 제대로 터치 판정이 안될 경우 현재 오브젝트 좌표에 임의의 좌표를 더해준다.
    /// </summary>
    public void SetTouchDistanceAddPosition(Vector3 addPos)
    {
        _addTouchPos = addPos;
    }

    /// <summary>
    /// 부모 트랜스폼 하위에 있는 오브젝트라서 좌표가 제대로 안움직이는 경우 현재 오브젝트 좌표에 임의의 좌표를 더해준다.
    /// </summary>
    public void SetMoveAddPosition(Vector3 addPos)
    {
        _addMovePos = addPos;
    }

    /// <summary>
    /// 터치 완료 조건 판단을 콜라이더 영역으로 할 경우 세팅해준다.
    /// </summary>
    /// <param name="gameObject"></param>
    public void SetCompleteCollider(GameObject gameObject)
    {
        _completeCollider = gameObject.GetComponent<Collider2D>();
    }

    /// <summary>
    /// 행위를 할 오브젝트를 설정한다
    /// </summary>
    /// <param name="gameObject"></param>
    public void SetGoalGameObject(GameObject gameObject)
    {
        _goalGameObject = gameObject;
    }

    /// <summary>
    /// 드래그 영역 지정
    /// </summary>
    /// <param name="pos"></param>
    public void SetSuccessCollectArea(float[] pos)
    {
        _successAreaPos = pos;
    }

    /// <summary>
    /// 진행 중에 행해지는 이벤트 정지
    /// </summary>
    public void StopProgressEvent()
    { 
        StopCoroutine("CoProgressing");
    }
}
