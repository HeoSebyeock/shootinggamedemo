﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using Spine.Unity;
/// <summary>
/// 장면을 전환하는 이미지를 나타내는 클래스
/// </summary>
public class TransitionImageAction : MonoBehaviour
{
    public enum EType
    {
        FadeIn,
        FadeOut,
        FadeInOut,

        Radial360In,
        Radial360Out,
        Radial360InOut,

        HorizontalIn,
        HorizontalOut,
        HorizontalInOut,

        ZoomIn,
        ZoomOut,
        ZoomInOut,

        /// <summary>
        /// TODO 아직 안함 해야함 ㅎ
        /// </summary>
        BingGleIn,
        BingGleOut,
        BingGleInOut,
    }

    //마스킹 방식
    private enum EMask
    {
        None,
        VisibleInSide,
    }

    //커스텀 에디터에서 변수 설명
    public EType _type;
    
    public string _imageName = null;
    public string ImageName { get { return _imageName; } set { _imageName = value; } }

    public Vector2 _position;
    public Vector2 Position { get { return _position; } set { _position = value; } }

    public float _time = DEFAULT_TIME;
    public float Time { get { return _time; } set { _time = value; } }

    public float _waitTime;
    public float WaitTime { get { return _waitTime; } set { _waitTime = value; } }

    public bool _colorIsBlack;
    public bool ColorIsBlack { get { return _colorIsBlack; } set { _colorIsBlack = value; } }

    private GameObject _transitionObject;
    private Image _image;
    private GameObject _maskObj;

    const int MASK_SORTING_ORDER = 100;

    public const float DEFAULT_TIME = 1f;

    /// <summary>
    /// 장면 전환 이미지 동작을 시작합니다.
    /// </summary>
    public void StartAction()
    {
        StartCoroutine(CoTransitionAction());
    }

    /// <summary>
    /// 동적으로 필요한 정보를 넣습니다.
    /// </summary>
    /// <param name="imageName">생성할 이미지 이름</param>
    /// <param name="type">이미지 전환 종류</param>
    /// <param name="position">생성할 위치</param>
    /// <param name="actionTime">전환에 걸리는 시간</param>
    /// <param name="waitTime">전환 후 기다리는 시간 인아웃 액션에서 인 -> wait -> 아웃 </param>
    /// <param name="colorIsBlack">이미지를 검정색으로 바꿈</param>
    public TransitionImageAction CreateTransitionImage(string imageName, EType type, Vector2 position = default, float time = 1.0f, float waitTime = 1.0f, bool colorIsBlack = false)
    {
        _colorIsBlack = colorIsBlack;
        _position = position;
        _type = type;
        _time = time;
        _waitTime = waitTime;
        _imageName = imageName;
        return this;
    }

    //Game Loop
    IEnumerator CoTransitionAction()
    {
        DefaultSetting();

        switch (_type)
        {
            case EType.FadeIn:
                {
                    _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 0);
                    _image.DOFade(1, _time);
                }
                break;
            case EType.FadeOut:
                {
                    _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 1);
                    _image.DOFade(0, _time);
                }
                break;
            case EType.FadeInOut:
                {
                    _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 0);
                    _image.DOFade(1, _time);
                    yield return new WaitForSeconds(_time + _waitTime);
                    _image.DOFade(0, _time);
                }
                break;

            case EType.Radial360In:
                {
                    _image.fillAmount = 0;
                    _image.DOFillAmount(1, _time);
                }
                break;

            case EType.Radial360Out:
                {
                    _image.fillAmount = 1;
                    _image.fillClockwise = false;
                    _image.DOFillAmount(0, _time);
                }
                break;

            case EType.Radial360InOut:
                {
                    _image.fillAmount = 0;
                    _image.DOFillAmount(1, _time);
                    yield return new WaitForSeconds(_time);
                    yield return new WaitForSeconds(_waitTime);
                    _image.fillClockwise = false;
                    _image.DOFillAmount(0, _time);
                }
                break;

            case EType.HorizontalIn:
                {
                    _image.fillAmount = 0;
                    
                    _image.DOFillAmount(1, _time);
                }
                break;

            case EType.HorizontalOut:
                {
                    _image.fillAmount = 1;
                    //Right->Left change
                    _image.fillOrigin = 1;
                    _image.DOFillAmount(0, _time);
                }
                break;

            case EType.HorizontalInOut:
                {
                    _image.fillAmount = 0;
                    _image.DOFillAmount(1, _time);
                    yield return new WaitForSeconds(_time);
                    yield return new WaitForSeconds(_waitTime);
                    //Right->Left change
                    _image.fillOrigin = 1;
                    _image.DOFillAmount(0, _time);
                }
                break;

            case EType.ZoomIn:
                {
                    _maskObj.transform.DOScale(0, _time).SetEase(Ease.OutCubic);
                    yield return new WaitForSeconds(_time);
                    MaskObjSetting(EMask.None);
                }
                break;

            case EType.ZoomOut:
                {
                    _maskObj.transform.DOScale(1, _time).SetEase(Ease.InCubic);
                    yield return new WaitForSeconds(_time);
                    MaskObjSetting(EMask.None);
                }
                break;

            case EType.ZoomInOut:
                {
                    _maskObj.transform.DOScale(0, _time).SetEase(Ease.OutCubic);
                    yield return new WaitForSeconds(_time);
                    _transitionObject.transform.parent = Util.GetInstance.GetUITransform();
                    yield return new WaitForSeconds(_waitTime);
                    _maskObj.transform.DOScale(1, _time).SetEase(Ease.InCubic);
                    yield return new WaitForSeconds(_time);
                    MaskObjSetting(EMask.None);
                }
                break;

            default:
                break;
        }

        yield return new WaitForSeconds(_time + 0.25f);
        ObjectDestroy();
    }

    void DefaultSetting()
    {
        //게임 오브젝트 생성(Component : Image(Type = Filled))
        CreateFactory createFactory = new CreateFactory();
        _transitionObject = createFactory.CreateGameObject(Constants.EObjectType.Image, Constants.EFolderRootType.Images, Constants.EFolderLeafType.Common, _imageName, true, transform);
        _transitionObject.transform.SetAsLastSibling();
        _transitionObject.transform.localPosition = _position;

        _image = _transitionObject.GetComponent<Image>();
        _image.type = Image.Type.Filled;
        _image.rectTransform.sizeDelta = Util.GetInstance.GetDesignSize();

        if (_colorIsBlack)
        {
            _image.color = Color.black;
        }

        switch (_type)
        {
            case EType.Radial360In:
            case EType.Radial360Out:
            case EType.Radial360InOut:
                {
                    _image.fillMethod = Image.FillMethod.Radial360;
                }
                break;

            case EType.HorizontalIn:
            case EType.HorizontalOut:
            case EType.HorizontalInOut:
                {
                    _image.fillMethod = Image.FillMethod.Horizontal;
                }
                break;

            case EType.ZoomIn:
            case EType.ZoomOut:
            case EType.ZoomInOut:
                {
                    _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 0);
                    _transitionObject.AddComponent<SpriteRenderer>().sprite = createFactory.CreateSprite(Constants.EFolderRootType.Images, Constants.EFolderLeafType.Common, _imageName);
                    _transitionObject.tag = Constants.ETag.NotMasking.ToString();

                    _maskObj = createFactory.CreateGameObject(Constants.EObjectType.SpriteMask, Constants.EFolderRootType.Images, Constants.EFolderLeafType.Common, _imageName, true, _transitionObject.transform);

                    MaskObjSetting(EMask.VisibleInSide);
                }
                break;
        }
    }

    /// <summary>
    /// 장면 전환 액션 이후 제거
    /// </summary>
    void ObjectDestroy()
    {
        Destroy(_transitionObject);
        Destroy(this);
    }

    /// <summary>
    /// 마스킹 액션에 따른 설정
    /// </summary>
    /// <param name="mask">SpriteMaskInteraction 설정</param>
    void MaskObjSetting(EMask mask)
    {
        SpriteRenderer[] spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
        SkeletonAnimation[] skeletonAnimations = GetComponentsInChildren<SkeletonAnimation>();
        Image[] images = GetComponentsInChildren<Image>();

        switch (mask)
        {
            case EMask.None:
                {
                    for (int i = 0; i < spriteRenderers.Length; i++)
                    {
                        if (!spriteRenderers[i].CompareTag(Constants.ETag.NotMasking.ToString()))
                        {
                            spriteRenderers[i].maskInteraction = SpriteMaskInteraction.None;
                        }
                    }
                    for (int i = 0; i < skeletonAnimations.Length; i++)
                    {
                        skeletonAnimations[i].maskInteraction = SpriteMaskInteraction.None;
                    }
                }
                break;

            case EMask.VisibleInSide:
                {
                    for (int i = 0; i < spriteRenderers.Length; i++)
                    {
                        spriteRenderers[i].maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                        if (spriteRenderers[i].CompareTag(Constants.ETag.NotMasking.ToString()))
                        {
                            spriteRenderers[i].sortingOrder = MASK_SORTING_ORDER;
                            spriteRenderers[i].maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
                        }
                    }
                    for (int i = 0; i < skeletonAnimations.Length; i++)
                    {
                        skeletonAnimations[i].maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                    }
                    for (int i = 0; i < images.Length; i++)
                    {
                        if (images[i].GetComponent<SpriteRenderer>() == null)
                        {
                            images[i].gameObject.AddComponent<SpriteRenderer>().sprite = images[i].sprite;
                            images[i].gameObject.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                            images[i].gameObject.GetComponent<SpriteRenderer>().sortingOrder = MASK_SORTING_ORDER - 1;
                            images[i].gameObject.SetActive(false);
                        }
                    }
                }
                break;

            default:
                break;
        }

    }
}
