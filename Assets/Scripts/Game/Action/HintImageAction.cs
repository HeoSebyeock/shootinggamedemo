﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

/// <summary>
/// 게임 내에 유도 효과를 주는 힌트 클래스
/// </summary>
public class HintImageAction : MonoBehaviour
{
    public enum EType
    {
        JumpAction,
        MoveAction,
        RotateAction,
        VerticalFillMethodAction,
        HorizontalFillMethodAction,
    }

    //커스텀 에디터에서 변수 설명
    public EType _type;
    
    public string _imageName;
    public string ImageName { get { return _imageName; } set { _imageName = value; } }

    public Vector2 _position;
    public Vector2 Position { get { return _position; } set { _position = value; } }

    public float _time = DEFAULT_TIME;
    public float Time { get { return _time; } set { _time = value; } }

    public float _waitTime;
    public float WaitTime { get { return _waitTime; } set { _waitTime = value; } }

    public Vector2 _movePos;
    public Vector2 MovePos { get { return _movePos; } set { _movePos = value; } }

    public float _jumpHeight = DEFAULT_JUMP_HEIGHT;
    public float JumpHeight { get { return _jumpHeight; } set { _jumpHeight = value; } }

    public int _isFillOrigin;
    public int IsFillOrigin { get { return _isFillOrigin; } set { _isFillOrigin = value; } }

    public bool _isFade;
    public bool IsFade { get { return _isFade; } set { _isFade = value; } }

    public GameObject _gameObject;
    private Image _image;

    public const float DEFAULT_TIME = 1f;
    public const float DEFAULT_JUMP_HEIGHT = 75f;

    /// <summary>
    /// 힌트 동작을 시작 합니다.
    /// </summary>
    public void StartAction()
    {
        StartCoroutine(CoHintImageAction());
    }

    /// <summary>
    /// 동적으로 필요한 정보를 넣습니다.
    /// </summary>
    /// <param name="imageName">이미지 이름</param>
    /// <param name="type">힌트의 종류</param>
    /// <param name="position">힌트가 시작 될 위치</param>
    /// <param name="actionTime">힌트 액션에 걸리는 시간</param>
    /// <param name="loopWaitTime">2번 이상의 액션일 경우 1번 액션 후 기다리는 시간 </param>
    /// <param name="isFade">힌트 이미지를 페이드 인아웃으로 할 경우</param>
    /// <param name="jumpHeight">힌트가 점프 액션일 경우 높이</param>
    /// <param name="movePos">이동 액션일 경우 이동 거리</param>
    /// <param name="isFillOrigin">left->right fillamount 차는 위치 반전</param>
    public void CreateHint(string imageName,EType type,Vector2 position,float time, float waitTime = 0, bool isFade = false, float jumpHeight = 75,Vector2 movePos = default, int isFillOrigin = 0)
    {
        _imageName = imageName;
        _type = type;
        _position = position;
        _time = time;
        _waitTime = waitTime;
        _jumpHeight = jumpHeight;
        _movePos = movePos;
        _isFade = isFade;
        _isFillOrigin = isFillOrigin;
    }

    //Game Loop
    public IEnumerator CoHintImageAction()
    {
        DefaultSetting();

        switch (_type)
        {
            case EType.JumpAction:
                {
                    while (true)
                    {
                        _gameObject.transform.DOLocalJump(_position, _jumpHeight, 1, _time);
                        yield return new WaitForSeconds(_time + _waitTime);
                    }
                }

            case EType.MoveAction:
                {
                    while (true)
                    {
                        _gameObject.transform.DOLocalMove(_movePos, _time);
                        yield return new WaitForSeconds(_time);
                        _gameObject.transform.DOLocalMove(_position, _time);
                        yield return new WaitForSeconds(_time + _waitTime);
                    }
                }

            case EType.RotateAction:
                {
                    while (true)
                    {
                        _gameObject.transform.DORotate(new Vector3(0, 0, 360), _time, RotateMode.FastBeyond360);
                        yield return new WaitForSeconds(_time + _waitTime);
                    }
                }

            case EType.VerticalFillMethodAction:
                {
                    while (true)
                    {
                        _image.fillAmount = 0;
                        _image.fillMethod = Image.FillMethod.Vertical;
                        _image.fillOrigin = _isFillOrigin;
                        _image.DOFillAmount(1, _time);
                        yield return new WaitForSeconds(_time + _waitTime);
                    }
                }

            case EType.HorizontalFillMethodAction:
                {
                    while (true)
                    {
                        _image.fillAmount = 0;
                        _image.fillMethod = Image.FillMethod.Horizontal;
                        _image.fillOrigin = _isFillOrigin;
                        _image.DOFillAmount(1, _time);
                        yield return new WaitForSeconds(_time + _waitTime);
                    }
                }
        }


    }
    
    /// <summary>
    /// 힌트를 멈춥니다.
    /// </summary>
    public void StopAction()
    {
        OnOff(false);
        _gameObject.transform.DOKill();
        StopAllCoroutines();
    }

    void DefaultSetting()
    {
        //게임 오브젝트 생성(Component : Image(Type = Filled))
        CreateFactory createFactory = new CreateFactory();
        if(_gameObject == null)
        {
            _gameObject = createFactory.CreateGameObject(Constants.EObjectType.Image, Constants.EFolderRootType.Images, Constants.EFolderLeafType.Common, _imageName, true, transform);
            _image = _gameObject.GetComponent<Image>();
            _image.type = Image.Type.Filled;
        }

        Debug.Log("_position == y" + _position.y);
        _gameObject.transform.localPosition = _position;
        OnOff(true);
    }

    /// <summary>
    /// 힌트 오브젝트 켜고 끄기
    /// </summary>
    /// <param name="On">true = 켜기 false = 끄기</param>
    private void OnOff(bool On)
    {
        switch (On)
        {
            //isFade = 힌트 이미지 페이드 여부
            case true:
                {
                    if (_isFade)
                    {
                        _image.color = new Color(1, 1, 1, 0);
                        _image.DOFade(1, 0.5f);
                    }
                    else
                    {
                        _gameObject.SetActive(true);
                    }
                }
                break;
            case false:
                {
                    if (_isFade)
                    {
                        _image.DOFade(0, 0.5f);
                    }
                    else
                    {
                        _gameObject.SetActive(false);
                    }
                }
                break;
        }
        
    }
}
