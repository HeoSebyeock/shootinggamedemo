﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

/// <summary>
/// 이미지 반복 재생 시 사용되는 클래스 (BackGround Loop / Image Animation)
/// </summary>
public class LoopImageAction : MonoBehaviour
{
    public enum EType
    {
        Default,

        LeftToRightMove,
        RightToLeftMove,
        UpToDownMove,
        DownToUpMove,
    }


    [Header("종류")]
    [SerializeField]
    public EType _type;
    public EType Type { get; }

    [Header("루프 시간")]
    [SerializeField]
    private float _loopTime = DEFAULT_TIME;
    public float LoopTime { get { return _loopTime; } set { _loopTime = value; } }

    [Header("루프 시킬 오브젝트")]
    [SerializeField]
    private GameObject[] _gameObjs;
    public GameObject[] GameObjs { get { return _gameObjs; } }

    [Header("루프 시킬 오브젝트 이미지 이름")]
    [SerializeField]
    private string[] _imageNames;
    public string[] ImageNames { get { return _imageNames; } }

    [Header("위치")]
    [SerializeField]
    private Vector2 _position;
    public Vector2 Position { get { return _position; } }

    [Header("정렬 순서")]
    [SerializeField]
    private int _sortingOrder;
    public int SortingOrder { get { return _sortingOrder; } }

    //현재 화면 내 루프 이미지 번호
    private int _changedValue = 0;
    //_moveDistance = 이미지 크기를 얻어온다 (이미지 이동에 사용)
    private float _moveDistance;
    private Tween[] _currentTween;
    private float _loopTimeScale = 1f;

    //맵의 순서 변경이 이루어지는 좌표 조맵
    private float _addChangedYPos = 0;

    public const float DEFAULT_TIME = 1f;

    /// <summary>
    /// 이미지 반복 재생을 시작합니다.
    /// </summary>
    public void StartAction()
    {
        StartCoroutine("CoLoopImageAction");
    }

    /// <summary>
    /// 동적으로 필요한 정보를 넣습니다. (게임오브젝트)
    /// </summary>
    /// <param name="type">종류</param>
    /// <param name="loopTime">반복 재생 시간</param>
    /// <param name="gameObjs">반복 재생될 게임 오브젝트</param>
    public void CreateLoopImage(EType type,float loopTime,GameObject[] gameObjs,Vector2 position,int sortingOrder = 1)
    {
        _type = type;
        _loopTime = loopTime;
        _gameObjs = gameObjs;
        _sortingOrder = sortingOrder;
        _position = position;
    }

    /// <summary>
    /// 동적으로 필요한 정보를 넣습니다. (이미지 이름)
    /// </summary>
    /// <param name="type">종류</param>
    /// <param name="loopTime">반복 재생 시간</param>
    /// <param name="imageNames">반복 재생될 게임 이미지 이름</param>
    public void CreateLoopImage(EType type, float loopTime, string[] imageNames, Vector2 position, int sortingOrder = 1)
    {
        _type = type;
        _loopTime = loopTime;
        _imageNames = imageNames;
        _sortingOrder = sortingOrder;
        _position = position;
    }

    /// <summary>
    /// 맵의 순서 변경을 할때 ypos가 안맞을 경우 +y값을 해줌으로써 변경 가능 
    /// </summary>
    public void SetAddChangedYPos(float addYPos)
    {
        _addChangedYPos = addYPos;
    }

    //Game Loop
    IEnumerator CoLoopImageAction()
    {
        yield return new WaitUntil(() => _loopTime != 0);
        DefaultSetting();
        switch (_type)
        {
            case EType.Default:
                {
                    while (true)
                    {
                        yield return new WaitForSeconds(_loopTime);
                        _gameObjs[_changedValue % _gameObjs.Length].gameObject.SetActive(false);
                        _changedValue++;
                        _gameObjs[_changedValue % _gameObjs.Length].gameObject.SetActive(true);
                    }
                }

            case EType.UpToDownMove:
                {
                    while (true)
                    {
                        
                        for (int i = 0; i < _gameObjs.Length; i++)
                        {
                            _currentTween[i] = _gameObjs[i].transform.DOLocalMoveY(-_moveDistance, _loopTime).SetRelative();
                            _currentTween[i].timeScale = _loopTimeScale;
                        }
                        yield return new WaitUntil(() => _gameObjs[_changedValue % _gameObjs.Length].transform.localPosition.y == -_moveDistance * 0.5f + _addChangedYPos);
                        _gameObjs[_changedValue % _gameObjs.Length].transform.localPosition
                            = new Vector2(_gameObjs[_changedValue % _gameObjs.Length].transform.localPosition.x, _moveDistance * (_gameObjs.Length - 1) + _moveDistance * 0.5f + _addChangedYPos);
                        _changedValue++;
                    }
                }

            case EType.DownToUpMove:
                {
                    while (true)
                    {
                        for (int i = 0; i < _gameObjs.Length; i++)
                        {
                            _currentTween[i] = _gameObjs[i].transform.DOLocalMoveY(_moveDistance, _loopTime).SetRelative();
                            _currentTween[i].timeScale = _loopTimeScale;
                        }

                        yield return new WaitUntil(() => _gameObjs[_changedValue % _gameObjs.Length].transform.localPosition.y == _moveDistance * 0.5f + _addChangedYPos);

                        _gameObjs[_changedValue % _gameObjs.Length].transform.localPosition
                            = new Vector2(_gameObjs[_changedValue % _gameObjs.Length].transform.localPosition.x, -(_moveDistance * (_gameObjs.Length - 1) + _moveDistance * 0.5f + _addChangedYPos));
                        _changedValue++;
                    }
                }

            case EType.LeftToRightMove:
                {
                    while (true)
                    {
                        for (int i = 0; i < _gameObjs.Length; i++)
                        {
                            _currentTween[i] = _gameObjs[i].transform.DOLocalMoveX(_moveDistance, _loopTime).SetRelative();
                            _currentTween[i].timeScale = _loopTimeScale;
                        }

                        yield return new WaitUntil(() => _gameObjs[_changedValue % _gameObjs.Length].transform.localPosition.x == _moveDistance * 0.5f + _addChangedYPos);

                        _gameObjs[_changedValue % _gameObjs.Length].transform.localPosition
                            = new Vector2(-(_moveDistance * (_gameObjs.Length - 1) - _moveDistance * 0.5f + _addChangedYPos), _gameObjs[_changedValue % _gameObjs.Length].transform.localPosition.y);
                        _changedValue++;
                    }
                }

            case EType.RightToLeftMove:
                {
                    while (true)
                    {
                        for (int i = 0; i < _gameObjs.Length; i++)
                        {
                            _currentTween[i] = _gameObjs[i].transform.DOLocalMoveX(-_moveDistance, _loopTime).SetRelative();
                            _currentTween[i].timeScale = _loopTimeScale;
                        }

                        yield return new WaitUntil(() => _gameObjs[_changedValue % _gameObjs.Length].transform.localPosition.x == -_moveDistance * 0.5f + _addChangedYPos);

                        _gameObjs[_changedValue % _gameObjs.Length].transform.localPosition
                            = new Vector2(_moveDistance * (_gameObjs.Length - 1) + _moveDistance * 0.5f + _addChangedYPos, _gameObjs[_changedValue % _gameObjs.Length].transform.localPosition.y);
                        _changedValue++;
                    }
                }
            default:
                break;
        }
        yield return null;
    }

    void DefaultSetting()
    {
        if (_imageNames != null && _imageNames.Length != 0)
        {
            _gameObjs = new GameObject[_imageNames.Length];
        }

        if (_gameObjs[0] == null)
        {
            //게임 오브젝트 생성(Component : SpriteRenderer)
            CreateFactory createFactory = new CreateFactory();

            for (int i = 0; i < _imageNames.Length; i++)
            {
                _gameObjs[i] = createFactory.CreateGameObject(Constants.EObjectType.SpriteRenderer, Constants.EFolderRootType.Images, Constants.EFolderLeafType.Common, _imageNames[i], true, transform, _sortingOrder, _position);
            }

            GetSpriteRectSize();

            //초기 위치 값 설정
            switch (_type)
            {
                case EType.UpToDownMove:
                    {
                        for (int i = 0; i < _gameObjs.Length; i++)
                        {
                            _gameObjs[i].transform.localPosition += new Vector3(0, i * _moveDistance);
                        }
                    }
                    break;

                case EType.DownToUpMove:
                    {
                        for (int i = 0; i < _gameObjs.Length; i++)
                        {
                            _gameObjs[i].transform.localPosition += new Vector3(0, i * -_moveDistance);
                        }
                    }
                    break;

                case EType.LeftToRightMove:
                    {
                        for (int i = 0; i < _gameObjs.Length; i++)
                        {
                            _gameObjs[i].transform.localPosition += new Vector3(i * -_moveDistance, 0);
                        }
                    }
                    break;

                case EType.RightToLeftMove:
                    {
                        for (int i = 0; i < _gameObjs.Length; i++)
                        {
                            _gameObjs[i].transform.localPosition += new Vector3(i * _moveDistance, 0);
                        }
                    }
                    break;
            }
        }
        else
        {
            GetSpriteRectSize();
        }

        _currentTween = new Tween[_gameObjs.Length];
    }

    /// <summary>
    /// 이미지 사이즈를 얻어 루프 거리 값 가져옴
    /// </summary>
    void GetSpriteRectSize()
    {
        switch (_type)
        {
            case EType.UpToDownMove:
            case EType.DownToUpMove:
                {
                    _moveDistance = _gameObjs[0].GetComponent<SpriteRenderer>().sprite.rect.size.y;
                }
                break;

            case EType.LeftToRightMove:
            case EType.RightToLeftMove:
                {
                    _moveDistance = _gameObjs[0].GetComponent<SpriteRenderer>().sprite.rect.size.x;
                }
                break;
            default:
                break;
        }
    }

    public void LoopPause()
    {
        for (int i = 0; i < _gameObjs.Length; i++)
        {
            _currentTween[i].Pause();
        }
    }

    public void LoopReStart()
    {
        for (int i = 0; i < _gameObjs.Length; i++)
        {
            _currentTween[i].Play();
        }
    }

    public void SetLoopSpeed(float time, bool isDoTween = true, float duration = 1)
    {
        _loopTimeScale = time;
        if (isDoTween)
        {
            for (int i = 0; i < _gameObjs.Length; i++)
            {
                _currentTween[i].DOTimeScale(_loopTimeScale, duration);
            }
        }
        else
        {
            for (int i = 0; i < _gameObjs.Length; i++)
            {
                _currentTween[i].timeScale = _loopTimeScale;
            }
        }
    }

    public void LoopStop()
    {
        StopAllCoroutines();
    }

    /// <summary>
    /// 현재 이미지의 인덱스를 넘겨준다
    /// </summary>
    /// <returns></returns>
    public int CurrentImageIndex()
    {
        return _changedValue % _gameObjs.Length;
    }

    /// <summary>
    /// 현재 화면 내에 있는 루프 오브젝트를 반환
    /// </summary>
    /// <returns></returns>
    //public GameObject CurrentViewObject()
    //{
    //    GameObject tempObject = null;
    //    switch (_type)
    //    {
    //        case EType.UpToDownMove:
    //        case EType.DownToUpMove:
    //            {
    //                for (int i = 0; i < _gameObjs.Length; i++)
    //                {
    //                    if (_gameObjs[i].transform.localPosition.y < Util.GetInstance.GetDesignSize().y)
    //                    {
    //                        tempObject = _gameObjs[i];
    //                    }
    //                }
    //            }
    //            break;

    //        case EType.LeftToRightMove:
    //        case EType.RightToLeftMove:
    //            {
    //                for (int i = 0; i < _gameObjs.Length; i++)
    //                {
    //                    if (_gameObjs[i].transform.localPosition.x < Util.GetInstance.GetDesignSize().x)
    //                    {
    //                        tempObject = _gameObjs[i];
    //                    }
    //                }
    //            }
    //            break;

    //        default:
    //            break;
    //    }
    //    return tempObject;
    //}

}
