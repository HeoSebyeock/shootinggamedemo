﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// 파티클 효과를 주는 클래스
/// </summary>
public class Particles : MonoBehaviour
{
    public enum EType
    {
        SlowlyDown,
        Twinkle,
        BlingBling,
        Animation,
        YoYo,
        Spread,
        ShakeBounce,
        ShakeScale,
    }

    //파티클 오더
    const int SORTING_ORDER = 100;

    public EType _type;
    public EType Type { get; }

    public string _imageName;
    public string ImageName { get; }

    public float _moveDistance = DEFAULT_MOVE_DISTANCE;
    public float MoveDistance { get; }

    public float _scale = DEFAULT_SCALE;
    public float Scale { get; }

    public int _count = DEFAULT_COUNT;
    public int Count { get; }

    public float _fadeTime = DEFAULT_FADE_TIME;
    public float FadeTime { get; }

    public bool _isRandom;
    public bool IsRandom { get; }

    private GameObject _gameObject;
    private Vector2 _touchPosition;

    private CreateFactory _createFactory;

    //퍼졌다 되돌아오는 효과가 고정일 경우 이미지 개수는 5개로 고정
    const int YOYO_FIXED_COUNT = 5;
    //기본 값 설정
    public const float DEFAULT_MOVE_DISTANCE = 30f;
    public const float DEFAULT_SCALE = 1f;
    public const int DEFAULT_COUNT = 5;
    public const float DEFAULT_FADE_TIME = 1f;

    private void Start()
    {
        //_moveDistance = DEFAULT_MOVE_DISTANCE;
        //_scale = DEFAULT_SCALE;
        //_count = DEFAULT_COUNT;
        //_fadeTime = DEFAULT_FADE_TIME;
    }

    /// <summary>
    /// 동적으로 파티클을 생성 및 정의합니다.
    /// </summary>
    /// <param name="type">파티클 종류</param>
    /// <param name="imageName">쓰일 이미지 이름 / 단, 애니메이션 파티클일 경우에는 이미지 이름에 번호를 붙여주세요. ex) effect0,effect1,...</param>
    /// <param name="moveDistance">생성 위치에서의 이동거리</param>
    /// <param name="scale">이미지 크기</param>
    /// <param name="count">이미지 개수 / 단, 요요 파티클이고 랜덤이 아닐 경우는 5장으로 고정이 됩니다. 랜덤은 개수 조정 가능</param>
    /// <param name="fadeTime">이미지 페이드 인아웃 시간</param>
    /// <param name="random">특정 파티클의 경우 비랜덤 / 랜덤 두가지가 쓰일 경우에 해당 변수로 조정</param>
    public void CreateParticle(EType type,string imageName, float moveDistance = 30f, float scale = 1.0f, int count = 5 , float fadeTime = 1.0f,bool random = false)
    {
        _type = type;
        _imageName = imageName;
        _moveDistance = moveDistance;
        _scale = scale;
        _count = count;
        _fadeTime = fadeTime;
        _isRandom = random;
    }

    //Game Loop
    IEnumerator CoPlayEffect()
    {
        DefaultSetting();

        switch (_type)
        {
            case EType.SlowlyDown:
                {
                    for (int i = 0; i < _count; i++)
                    {
                        GameObject tempObj = Instantiate(_gameObject,_gameObject.transform.parent);
                        tempObj.GetComponent<SpriteRenderer>().sortingOrder = SORTING_ORDER + i;
                        SpriteRenderer spriteRenderer = tempObj.GetComponent<SpriteRenderer>();

                        tempObj.transform.localPosition += new Vector3(Random.Range(-_moveDistance * 0.25f, _moveDistance * 0.25f) * i * 0.5f, Random.Range(-_moveDistance * 0.5f, 0));
                        tempObj.transform.localScale *= _scale;

                        spriteRenderer.DOFade(1, _fadeTime * 0.5f);
                        tempObj.transform.DOLocalMove(new Vector3(_moveDistance + Random.Range(-_moveDistance, _moveDistance), -_moveDistance * 2f) + tempObj.transform.localPosition, 1.0f);
                        tempObj.transform.DORotate(new Vector3(0, 0, 360), 1.5f, RotateMode.FastBeyond360);

                        Sequence sequence = DOTween.Sequence();
                        sequence.AppendInterval(0.75f);
                        sequence.AppendCallback(() => {
                            spriteRenderer.DOFade(0, _fadeTime * 0.5f).OnComplete(() => {
                                Destroy(tempObj);
                            });
                        });

                        yield return new WaitForSeconds(0.15f);
                    }
                    yield return new WaitForSeconds(1.0f);
                }
                break;

            case EType.Twinkle:
                {
                    for (int i = 0; i < _count; i++)
                    {
                        GameObject tempObj = Instantiate(_gameObject, _gameObject.transform.parent);
                        tempObj.GetComponent<SpriteRenderer>().sortingOrder = SORTING_ORDER + i;
                        SpriteRenderer spriteRenderer = tempObj.GetComponent<SpriteRenderer>();
                        tempObj.transform.localPosition += new Vector3(Random.Range(-_moveDistance * 1.25f, _moveDistance * 1.25f), Random.Range(-_moveDistance * 1.25f, _moveDistance * 1.25f));

                        float randScale = Random.Range(0.5f * _scale, 1.0f * _scale);
                        tempObj.transform.localScale = new Vector2(randScale, randScale);
                        

                        Sequence sequence = DOTween.Sequence();
                        spriteRenderer.DOFade(1, _fadeTime * 0.095f);
                        sequence.AppendInterval(_fadeTime * 0.15f);
                        sequence.AppendCallback(() => {
                            spriteRenderer.DOFade(0, _fadeTime * 0.095f);
                        });
                        sequence.AppendInterval(_fadeTime * 0.15f);
                        sequence.AppendCallback(() => {
                            spriteRenderer.DOFade(1, _fadeTime * 0.095f);
                        });
                        sequence.AppendInterval(_fadeTime * 0.15f);
                        sequence.AppendCallback(() => {
                            spriteRenderer.DOFade(0, _fadeTime * 0.095f);
                        });
                        sequence.AppendInterval(_fadeTime * 0.15f);
                        sequence.AppendCallback(() => {
                            Destroy(tempObj);
                        });

                        yield return new WaitForSeconds(0.05f);
                    }
                    yield return new WaitForSeconds(1.0f);
                }
                break;

            case EType.BlingBling:
                {
                    for (int i = 0; i < _count; i++)
                    {
                        GameObject tempObj = Instantiate(_gameObject, _gameObject.transform.parent);
                        tempObj.GetComponent<SpriteRenderer>().sortingOrder = SORTING_ORDER + i;
                        SpriteRenderer spriteRenderer = tempObj.GetComponent<SpriteRenderer>();
                        tempObj.transform.localPosition += new Vector3(Random.Range(-_moveDistance * 1.25f, _moveDistance * 1.25f), Random.Range(-_moveDistance * 1.25f, _moveDistance * 1.25f));

                        float randScale = Random.Range(0.5f * _scale, 1.0f * _scale);
                        tempObj.transform.localScale = new Vector2(randScale, randScale);
                        

                        Sequence sequence = DOTween.Sequence();
                        spriteRenderer.DOFade(1, _fadeTime * 0.5f);
                        sequence.AppendInterval(_fadeTime * 0.75f);
                        sequence.AppendCallback(() => {
                            spriteRenderer.DOFade(0, _fadeTime * 0.5f);
                        });
                        sequence.AppendInterval(_fadeTime * 0.75f);
                        sequence.AppendCallback(() => {
                            Destroy(tempObj);
                        });

                        yield return new WaitForSeconds(0.05f);
                    }
                    yield return new WaitForSeconds(1.0f);
                }
                break;

            case EType.Animation:
                {
                    SpriteRenderer spriteRenderer = _gameObject.GetComponent<SpriteRenderer>();
                    for (int i = 0; i < _count; i++)
                    {
                        spriteRenderer.color = new Color(1, 1, 1, 1);
                        
                        spriteRenderer.sprite = _createFactory.CreateSprite(Constants.EFolderRootType.Images, Constants.EFolderLeafType.Effect, _imageName + i.ToString());
                        yield return new WaitForSeconds(0.025f);
                    }
                    spriteRenderer.color = new Color(1, 1, 1, 0);
                    yield return new WaitForSeconds(1.0f);
                }
                break;

            case EType.YoYo:
                {
                    for (int i = 0; i < _count; i++)
                    {
                        GameObject tempObj = Instantiate(_gameObject, _gameObject.transform.parent);
                        SpriteRenderer spriteRenderer = tempObj.GetComponent<SpriteRenderer>();
                        tempObj.transform.localScale *= _scale;

                        Sequence sequence = DOTween.Sequence();
                        spriteRenderer.DOFade(1, _fadeTime * 0.25f);
                        sequence.AppendInterval(_fadeTime * 0.35f);
                        sequence.AppendCallback(() => {
                            spriteRenderer.DOFade(0, _fadeTime * 0.25f);
                        });
                        sequence.AppendInterval(_fadeTime * 0.25f);
                        sequence.AppendCallback(() => {
                            Destroy(tempObj);
                        });

                        if (!_isRandom)
                        {
                            float essence = 0;
                            float height = 0;
                            if (i % 2 == 0)
                            {
                                essence = 1;
                            }
                            else
                            {
                                essence = -2;
                            }
                            if (i < YOYO_FIXED_COUNT * 0.5f)
                            {
                                height = _moveDistance * i;
                            }
                            else
                            {
                                height = (YOYO_FIXED_COUNT - 1 - i) * _moveDistance;
                            }

                            tempObj.transform.DOLocalMove(_touchPosition + new Vector2(-_moveDistance * YOYO_FIXED_COUNT * 0.5f + i * _moveDistance * YOYO_FIXED_COUNT * 0.25f, height * essence), 0.25f).SetLoops(2, LoopType.Yoyo);
                            if(i == YOYO_FIXED_COUNT - 1)
                            {
                                yield break;
                            }
                        }
                        else
                        {
                            tempObj.transform.DOShakePosition(0.5f, new Vector2(_moveDistance * 1.75f, _moveDistance * 1.75f), 3, 0).SetLoops(1,LoopType.Yoyo);
                        }
                    }
                    yield return new WaitForSeconds(1.0f);
                }
                break;

            case EType.Spread:
                {
                    for (int i = 0; i < _count; i++)
                    {
                        GameObject tempObj = Instantiate(_gameObject, _gameObject.transform.parent);
                        SpriteRenderer spriteRenderer = tempObj.GetComponent<SpriteRenderer>();
                        tempObj.transform.localScale *= _scale;

                        Sequence sequence = DOTween.Sequence();
                        spriteRenderer.DOFade(1, _fadeTime * 0.25f);
                        tempObj.transform.DOScale(0.25f, _fadeTime * 0.75f);
                        sequence.AppendInterval(_fadeTime * 0.35f);
                        sequence.AppendCallback(() => {
                            spriteRenderer.DOFade(0, _fadeTime * 0.25f);
                        });
                        sequence.AppendInterval(_fadeTime * 0.25f);
                        sequence.AppendCallback(() => {
                            Destroy(tempObj);
                        });

                        if (!_isRandom)
                        {
                            float essence = 0;
                            float height = 0;
                            if (i % 2 == 0)
                            {
                                essence = 1;
                            }
                            else
                            {
                                essence = -2;
                            }
                            if (i < YOYO_FIXED_COUNT * 0.5f)
                            {
                                height = _moveDistance * i;
                            }
                            else
                            {
                                height = (YOYO_FIXED_COUNT - 1 - i) * _moveDistance;
                            }

                            tempObj.transform.DOLocalMove(_touchPosition + new Vector2(-_moveDistance * YOYO_FIXED_COUNT * 0.5f + i * _moveDistance * YOYO_FIXED_COUNT * 0.25f, height * essence), 0.25f);
                            if (i == YOYO_FIXED_COUNT - 1)
                            {
                                yield break;
                            }
                        }
                        else
                        {
                            tempObj.transform.DOShakePosition(2.0f, new Vector2(_moveDistance * 1.75f, _moveDistance * 1.75f), 0, 0);
                            yield return new WaitForSeconds(0.125f);
                        }
                        
                    }
                    yield return new WaitForSeconds(1.0f);
                }
                break;

            case EType.ShakeBounce:
                {
                    for (int i = 0; i < _count; i++)
                    {
                        GameObject tempObj = Instantiate(_gameObject, _gameObject.transform.parent);
                        tempObj.GetComponent<SpriteRenderer>().sortingOrder = SORTING_ORDER + i;
                        SpriteRenderer spriteRenderer = tempObj.GetComponent<SpriteRenderer>();
                        tempObj.transform.localScale *= _scale;

                        Sequence sequence = DOTween.Sequence();
                        spriteRenderer.DOFade(1, _fadeTime * 0.25f);
                        tempObj.transform.DOScale(0.25f, _fadeTime * 0.75f);
                        sequence.AppendInterval(_fadeTime * 0.35f);
                        sequence.AppendCallback(() => {
                            spriteRenderer.DOFade(0, _fadeTime * 0.25f);
                        });
                        sequence.AppendInterval(_fadeTime * 0.25f);
                        sequence.AppendCallback(() => {
                            Destroy(tempObj);
                        });

                        tempObj.transform.DOShakePosition(5.0f, new Vector2(_moveDistance * 1.75f, _moveDistance * 1.75f), 2, 60);
                        yield return new WaitForSeconds(0.125f);

                    }
                    yield return new WaitForSeconds(1.0f);
                }
                break;

            case EType.ShakeScale:
                {
                    for (int i = 0; i < _count; i++)
                    {
                        GameObject tempObj = Instantiate(_gameObject, _gameObject.transform.parent);
                        tempObj.GetComponent<SpriteRenderer>().sortingOrder = SORTING_ORDER + i;
                        SpriteRenderer spriteRenderer = tempObj.GetComponent<SpriteRenderer>();
                        tempObj.transform.localPosition += new Vector3(Random.Range(-_moveDistance * 1.25f, _moveDistance * 1.25f), Random.Range(-_moveDistance * 1.25f, _moveDistance * 1.25f));
                        float randScale = Random.Range(0.5f * _scale, 1.0f * _scale);
                        tempObj.transform.localScale = new Vector2(randScale, randScale);
                        
                        Sequence sequence = DOTween.Sequence();
                        spriteRenderer.DOFade(1, _fadeTime * 0.25f);
                        sequence.AppendInterval(_fadeTime * 0.35f);
                        sequence.AppendCallback(() => {
                            spriteRenderer.DOFade(0, _fadeTime * 0.25f);
                        });
                        sequence.AppendInterval(_fadeTime * 0.25f);
                        sequence.AppendCallback(() => {
                            Destroy(tempObj);
                        });

                        tempObj.transform.DOShakeScale(2.0f, new Vector2(-_scale * 0.1f, -_scale * 0.1f), 2, 0);
                        yield return new WaitForSeconds(0.05f);

                    }
                    yield return new WaitForSeconds(1.0f);
                }
                break;

            default:
                break;
        }

        yield return null;
    }

    /// <summary>
    /// 파티클을 동작합니다.
    /// </summary>
    /// <param name="touchPos">파티클의 위치를 정의합니다.</param>
    public void StartAction(Vector2 touchPos)
    {
        _touchPosition = touchPos;
        StartCoroutine(CoPlayEffect());
    }

    void DefaultSetting()
    {
        if (_gameObject == null)
        {
            _createFactory = new CreateFactory();
            _gameObject = _createFactory.CreateGameObject(Constants.EObjectType.SpriteRenderer, Constants.EFolderRootType.Images, Constants.EFolderLeafType.Effect, _imageName, true, transform);
            _gameObject.GetComponent<SpriteRenderer>().sortingOrder = SORTING_ORDER;
            _gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        }
        _gameObject.transform.localScale = Vector2.one;
        _gameObject.transform.localPosition = _touchPosition;
        
    }

}
