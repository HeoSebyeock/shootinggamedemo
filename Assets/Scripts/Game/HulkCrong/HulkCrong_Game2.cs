﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.UI;

public class HulkCrong_Game2 : MonoBehaviour
{
    public enum EGameLevel
    {
        Start,
        Run,
        FinishLoadMap,
        PobyAndLoopyMap,
        Fight,
    }

    /// <summary>
    /// 게임 상태 (주인공이 미는 상태와 밀리는 상태)
    /// </summary>
    public enum EGameState
    {
        None,
        Back,
        Push,
    }

    [Header("크롱")]
    [SerializeField]
    private SkeletonAnimation _crongSkeletonAnimation;

    [Header("포비")]
    [SerializeField]
    private SkeletonAnimation _pobySkeletonAnimation;

    [Header("루피")]
    [SerializeField]
    private SkeletonAnimation _loopySkeletonAnimation;

    [Header("적")]
    [SerializeField]
    private Transform _enemyAndObstacleTransform;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("성공 파티클")]
    [SerializeField]
    private Particles _successParticle;

    [Header("배경")]
    [SerializeField]
    private Transform _backGroundTransform;

    [Header("길")]
    [SerializeField]
    private Transform _loadTransform;

    [Header("크롱 아이콘")]
    [SerializeField]
    private Transform _crongIcon;

    private Vector2 _firstCreatePos;
    //현재 맵이 이동한 거리
    private float _moveDistance;

    //포비 vs 크롱 게임 게이지
    private Image _gaugeBar;

    //포비 터치 성공 했을 경우
    private bool _isInputSuccess;

    private EGameLevel _currentGameLevel = EGameLevel.Start;
    private EGameState _currentGameState = EGameState.None;

    private float _addSpeed = 1;

    private const float FIRST_CRONG_XPOS = 490;
    private const float MAP_MOVE_POWER = -350;
    private const float ICON_MOVE_POWER = 0.03f;
    private const float MAKE_OBJECT_DELAY = 2.5f;
    // 캐릭터 서로 미는 힘
    private const float POWER = 125f;
    // 캐릭터 밀릴수 있는 최대 거리
    private const float LIMIT_DISTANCE = 170f;

    // Use this for initialization
    void Start()
    {
        //Time.timeScale = 10;
        StartCoroutine("CoGameLoop");
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            _clickParticle.StartAction(Input.mousePosition);
        }
    }
    IEnumerator CoGameLoop()
    {
        if(_currentGameLevel == EGameLevel.Start)
        {
            _firstCreatePos = new Vector2(Util.GetInstance.GetDesignSize().x + 200, 500);
            _crongSkeletonAnimation.transform.DOLocalMoveX(FIRST_CRONG_XPOS, 2f);
            StartCoroutine("CoMakeFactory");
            yield return new WaitForSeconds(2f);
            Util.GetInstance.IsNotTouched = false;
            _currentGameLevel = EGameLevel.Run;
            StartCoroutine("CoMoveCrong");
        }
        else
        {
            Destroy(_crongSkeletonAnimation.GetComponent<HulkCrong>());
            _gaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
            _pobySkeletonAnimation.state.SetAnimation(0, "1", true);
            _currentGameLevel = EGameLevel.Fight;
            Judge();
            Util.GetInstance.IsNotTouched = false;
        }
    }
    
    #region 영역 내 터치가 들어오는지 확인 
    public void TapCallBack()
    {
        if (Util.GetInstance.IsNotTouched)
        {
            return;
        }

        StopCoroutine("CoTapCallBack");
        StartCoroutine("CoTapCallBack");
    }

    IEnumerator CoTapCallBack()
    {
        _successParticle.StartAction(Input.mousePosition);
        _isInputSuccess = true;
        Judge();
        yield return new WaitForSeconds(0.2f);
        _isInputSuccess = false;
        Judge();
    }
    #endregion 

    /// <summary>
    /// 터치에 대한 판단
    /// </summary>
    void Judge()
    {
        //캐릭터가 밀때
        if (_isInputSuccess)
        {
            StopCoroutine("CoBackAction");
            StartCoroutine("CoPushAction");
        }
        //캐릭터가 밀릴때
        else
        {
            StopCoroutine("CoPushAction");
            StartCoroutine("CoBackAction");
        }
    }

    /// <summary>
    /// 캐릭터가 밀릴때 하는 행동 
    /// </summary>
    /// <returns></returns>
    IEnumerator CoBackAction()
    {
        //현재 상태가 밀리고 있는 상태가 아닌경우
        if (_currentGameState != EGameState.Back)
        {
            _currentGameState = EGameState.Back;
            _crongSkeletonAnimation.GetComponent<SpineAnimation>().SetToSetupPoseAndAnimation(_crongSkeletonAnimation.GetComponent<SkeletonAnimation>(), "4", 0);
            _pobySkeletonAnimation.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", true);
            _loopySkeletonAnimation.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", true);

            while (true)
            {
                if (_crongSkeletonAnimation.transform.localPosition.x <= Util.GetInstance.GetDesignSize().x * 0.5f - LIMIT_DISTANCE * 1.5f)
                {
                   
                }
                else
                {
                    _crongSkeletonAnimation.transform.localPosition -= new Vector3(Time.deltaTime * POWER, 0);
                    _pobySkeletonAnimation.transform.localPosition -= new Vector3(Time.deltaTime * POWER, 0);
                }
                yield return null;
            }
        }
    }

    /// <summary>
    /// 캐릭터가 밀때 하는 행동
    /// </summary>
    /// <returns></returns>
    IEnumerator CoPushAction()
    {
        //현재 상태가 밀고있는 상태가 아닌경우
        if (_currentGameState != EGameState.Push)
        {
            _currentGameState = EGameState.Push;
            _crongSkeletonAnimation.GetComponent<SpineAnimation>().SetToSetupPoseAndAnimation(_crongSkeletonAnimation.GetComponent<SkeletonAnimation>(), "3", 0);
            _pobySkeletonAnimation.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "2", true);
            _loopySkeletonAnimation.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "2", true);

            while (true)
            {
                _gaugeBar.fillAmount += Time.deltaTime * 0.15f;

                //clear and next stage
                if (_gaugeBar.fillAmount >= 1f)
                {
                    StopCoroutine("CoTapCallBack");
                    Util.GetInstance.IsNotTouched = true;
                    GetComponent<GameGauge>().GaugeDestroy(true);

                    _crongSkeletonAnimation.GetComponent<SpineAnimation>().SetToSetupPoseAndAnimation(_crongSkeletonAnimation.GetComponent<SkeletonAnimation>(), "5", 5);
                    _pobySkeletonAnimation.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "3", true);
                    _loopySkeletonAnimation.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "2", true);
                    yield return new WaitForSeconds(0.25f);
                    _pobySkeletonAnimation.transform.DOScaleX(-1, 0.2f);
                    yield return new WaitForSeconds(0.75f);
                    _pobySkeletonAnimation.transform.DOLocalMoveX(Util.GetInstance.GetDesignSize().x + 300, 1.5f);
                    yield return new WaitForSeconds(3.5f);
                    GameManager.GameEndCallBackEvent.Invoke();
                    StopCoroutine("CoPushAction");
                    yield break;
                }

                
                if (_crongSkeletonAnimation.transform.localPosition.x >= Util.GetInstance.GetDesignSize().x * 0.5f + LIMIT_DISTANCE)
                {
                    
                }
                else
                {
                    _crongSkeletonAnimation.transform.localPosition += new Vector3(Time.deltaTime * POWER, 0);
                    _pobySkeletonAnimation.transform.localPosition += new Vector3(Time.deltaTime * POWER, 0);
                }

                yield return null;
            }
        }
    }

    IEnumerator CoMoveCrong()
    {
        
        while (true)
        {
            if (_backGroundTransform.localPosition.x <= -Util.GetInstance.GetDesignSize().x * 2.5f && _currentGameLevel == EGameLevel.PobyAndLoopyMap)
            {
                Destroy(_crongIcon.parent.gameObject);
                StopCoroutine("CoMoveCrong");
                StartCoroutine("CoGameLoop");
                yield break;
            }
            else if (_backGroundTransform.localPosition.x <= -Util.GetInstance.GetDesignSize().x * 2.25f && _currentGameLevel == EGameLevel.FinishLoadMap)
            {
                //Time.timeScale = 1;
                _currentGameLevel = EGameLevel.PobyAndLoopyMap;
                Util.GetInstance.IsNotTouched = true;
            }
            else if (_backGroundTransform.localPosition.x <= -Util.GetInstance.GetDesignSize().x * 2f && _currentGameLevel == EGameLevel.Run)
            {
                StopCoroutine("CoMakeFactory");
                _currentGameLevel = EGameLevel.FinishLoadMap;
            }
            else
            {
                _addSpeed = (_crongIcon.transform.localPosition.x - 427) * 0.001f + 1;
                _moveDistance += Time.deltaTime * MAP_MOVE_POWER;
                _backGroundTransform.localPosition += new Vector3(Time.deltaTime * MAP_MOVE_POWER * 0.25f * _addSpeed, 0);
                _loadTransform.localPosition += new Vector3(Time.deltaTime * MAP_MOVE_POWER * _addSpeed, 0);
                _crongIcon.transform.localPosition -= new Vector3(Time.deltaTime * MAP_MOVE_POWER * ICON_MOVE_POWER * _addSpeed, 0);
                
                yield return null;
            }
        }
    }
    /// <summary>
    /// 적 / 장애물 생성
    /// </summary>
    /// <returns></returns>
    IEnumerator CoMakeFactory()
    {
        int makeCnt = 0;

        GameObject firstEnemy = Instantiate(_enemyAndObstacleTransform.GetChild(makeCnt % _enemyAndObstacleTransform.transform.childCount).gameObject);
        firstEnemy.transform.SetParent(_loadTransform);
        firstEnemy.transform.localPosition = new Vector2(_moveDistance + 600, 0);
        firstEnemy.SetActive(true);
        StartCoroutine(CoEnemyAction(firstEnemy));
        makeCnt++;

        yield return new WaitForSeconds(MAKE_OBJECT_DELAY - 0.5f);

        while (true)
        {
            GameObject enemyAndObstacleObj = Instantiate(_enemyAndObstacleTransform.GetChild(makeCnt % _enemyAndObstacleTransform.transform.childCount).gameObject);
            enemyAndObstacleObj.transform.SetParent(_loadTransform);
            Debug.Log("(_addSpeed * 100) == " + (_addSpeed * 100));
            enemyAndObstacleObj.transform.localPosition = _firstCreatePos + new Vector2(-_moveDistance + (_addSpeed * 1000 - 100), 0);
            enemyAndObstacleObj.SetActive(true);
            StartCoroutine(CoEnemyAction(enemyAndObstacleObj));
            makeCnt++;
            yield return new WaitForSeconds(MAKE_OBJECT_DELAY - _addSpeed + 0.45f);
        }
    }

    
    IEnumerator CoEnemyAction(GameObject enemyAndObstacleObj)
    {
        yield return new WaitForSeconds(0.175f);
        enemyAndObstacleObj.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        
        yield return new WaitForSeconds(0.4f);
        enemyAndObstacleObj.GetComponent<Rigidbody2D>().gravityScale = 0;
        if (enemyAndObstacleObj.name.Contains("fly"))
        {
            enemyAndObstacleObj.GetComponent<BoxCollider2D>().offset = new Vector2(0, 200);
        }
    }
}
