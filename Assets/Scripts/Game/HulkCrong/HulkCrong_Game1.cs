﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using Spine.Unity;

public class HulkCrong_Game1 : MonoBehaviour
{
    enum EGameState
    {
        None,
        GameLoop,
    }

    enum EBalloonFirstLocation
    {
        Left,
        Right,
        Size,
    }
    /// <summary>
    /// Full : 풍선 원본 이미지, Pang : 터질때 풍선 이미지 , Bubble : 터지고 남은 풍선 잔재
    /// </summary>
    enum EBalloonLevel
    {
        Full,
        Pang,
        Bubble,
    }
    [Header("크롱")]
    [SerializeField]
    private SkeletonAnimation _crongSkeletonAnimation;

    [Header("에디")]
    [SerializeField]
    private SkeletonAnimation _eddySkeletonAnimation;

    [Header("뽀로로")]
    [SerializeField]
    private SkeletonAnimation _pororoSkeletonAnimation;

    [Header("풍선 오브젝트 풀링")]
    [SerializeField]
    private ObjectPooling _balloonPooling;

    [Header("풍선 만드는 기계")]
    [SerializeField]
    private SkeletonAnimation _balloonMachine;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("성공 파티클")]
    [SerializeField]
    private Particles _successParticle;

    private Image _gaugeBar;
    private EGameState _currentGameState = EGameState.None;

    private Vector2[] _balloonFirstLocation = new Vector2[] {
        new Vector2(349.2f,306.8f),new Vector2(932,306.8f),
    };

    private float _currentFillAmount;

    //풍선이 커짐에 따라 포지션을 살짝 올려주는 y값
    private const float BALLOON_FIRST_LOCATION_YPOS = 23f;
    private const float COMPLETE_BALLOON_COUNT = 20f;

    private void Start()
    {
        StartCoroutine("CoNextStep");
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            _clickParticle.StartAction(Input.mousePosition);
        }
    }

    IEnumerator CoNextStep()
    {
        yield return null;
        switch (_currentGameState)
        {
            case EGameState.None:
                {
                    _currentGameState = EGameState.GameLoop;
                    StartCoroutine("CoMakeBalloon");
                }
                break;
            case EGameState.GameLoop:
                {

                }
                break;
            default:
                break;
        }

        _gaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
    }

    IEnumerator CoMakeBalloon()
    {
        while (true)
        {
            GameObject[] balloons = new GameObject[(int)EBalloonFirstLocation.Size];
            for (int i = 0; i < (int)EBalloonFirstLocation.Size; i++)
            {
                balloons[i] = _balloonPooling.GetObject(_balloonPooling.transform);
                balloons[i].transform.localPosition = _balloonFirstLocation[i] + new Vector2(0, - BALLOON_FIRST_LOCATION_YPOS);
                //풍선 터치 생성 이후부터 가능하게 바뀌면서 알파값 연한거 지움
                //balloons[i].transform.GetChild((int)EBalloonLevel.Full).GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);
                balloons[i].transform.GetChild((int)EBalloonLevel.Full).DOScale(0.35f, 0.35f);
                balloons[i].transform.GetChild((int)EBalloonLevel.Full).DOLocalMoveY(BALLOON_FIRST_LOCATION_YPOS, 0.35f);
            }
            yield return new WaitForSeconds(1.167f * 0.5f);
            BalloonChangeScale(balloons);
            yield return new WaitForSeconds(1.167f * 0.5f);
            BalloonChangeScale(balloons);
            yield return new WaitForSeconds(1.167f * 0.5f);
            StartCoroutine(CoBalloonMoveAction(balloons[(int)EBalloonFirstLocation.Left], EBalloonFirstLocation.Left));
            StartCoroutine(CoBalloonMoveAction(balloons[(int)EBalloonFirstLocation.Right], EBalloonFirstLocation.Right));
        }
    }

    IEnumerator CoBalloonMoveAction(GameObject balloon,EBalloonFirstLocation eBalloonFirstLocation)
    {
        balloon.transform.GetChild((int)EBalloonLevel.Full).gameObject.AddComponent<CircleCollider2D>().radius = 65;
        balloon.AddComponent<UserInputGestureAction>().CreateInputGesture(UserInputGestureAction.EType.Tap, UserInputGestureAction.EJudgeWay.Collider, balloon.transform.GetChild((int)EBalloonLevel.Full).gameObject, () => { BalloonTapComplete(balloon); });

        float waitTime = 0;
        waitTime = Random.Range(5f, 8f);
        waitTime -= _currentFillAmount * 1.25f;
        if (EBalloonFirstLocation.Left == eBalloonFirstLocation)
        {
            //balloon.transform.DOLocalMove(balloon.transform.localPosition + new Vector3(Random.Range(150, 50), 225 + Random.Range(0, 75)), waitTime).SetEase(Ease.OutBack);
            balloon.transform.DOLocalJump(new Vector2(balloon.transform.localPosition.x + Random.Range(150, 350f), Random.Range(-200, -100)), Random.Range(400,500), 1,waitTime).SetEase(Ease.OutQuad).OnComplete(()=> {
                BalloonObjPool(balloon);
            });
        }
        else
        {
            //balloon.transform.DOLocalMove(balloon.transform.localPosition + new Vector3(Random.Range(-200, -150), 225 + Random.Range(0, 75)), waitTime).SetEase(Ease.OutBack);
            balloon.transform.DOLocalJump(new Vector2(balloon.transform.localPosition.x + Random.Range(-150, -350f), Random.Range(-200, -100)), Random.Range(400, 500), 1, waitTime).SetEase(Ease.OutQuad).OnComplete(()=> {
                BalloonObjPool(balloon);
            });
        }

        balloon.transform.DOShakeRotation(waitTime * 2, 15, 2, 90);
        balloon.transform.DOShakeScale(waitTime, 0.1f, 3, 90);
        yield return new WaitForSeconds(waitTime * 0.3f);
        if(balloon == null)
        {
            yield break;
        }
        //가로 이동 
        //waitTime = Random.Range(0.5f, 1.25f);
        //waitTime -= _currentFillAmount * 1.25f;
        //if (EBalloonFirstLocation.Left == eBalloonFirstLocation)
        //{
        //    balloon.transform.DOLocalMove(new Vector2(511 + Random.Range(-15, 15), balloon.transform.localPosition.y + Random.Range(-75, 0)), waitTime);
        //}
        //else
        //{
        //    balloon.transform.DOLocalMove(new Vector2(778 + Random.Range(-15, 15), balloon.transform.localPosition.y + Random.Range(-75, 0)), waitTime);
        //}
        //balloon.transform.DOShakeRotation(waitTime, 10, 2, 20);
        //yield return new WaitForSeconds(waitTime * 0.95f);

        //내려오는 이동
        //balloon.transform.DOKill();

        //waitTime += 3.5f;
        //waitTime -= _currentFillAmount * 1.25f;
        //balloon.transform.DOLocalMove(new Vector2(balloon.transform.localPosition.x + Random.Range(-100f, 100f), -200), waitTime);
        //balloon.transform.DOShakeRotation(waitTime, 10, 2, 20);

        //yield return new WaitForSeconds(0.25f);


        //yield return new WaitForSeconds(waitTime - 0.25f);
        
    }

    void BalloonObjPool(GameObject balloon)
    {
        if (balloon != null)
        {
            Destroy(balloon.transform.GetChild((int)EBalloonLevel.Full).gameObject.GetComponent<CircleCollider2D>());
            //초기화
            balloon.transform.GetChild((int)EBalloonLevel.Full).localScale = Vector2.zero;
            balloon.transform.GetChild((int)EBalloonLevel.Full).localRotation = Quaternion.Euler(Vector2.zero);
            balloon.transform.GetChild((int)EBalloonLevel.Full).localPosition = Vector2.zero;
            if (_balloonPooling != null)
            {
                _balloonPooling.ReturnObject(balloon);
            }
        }
    }

    void BalloonTapComplete(GameObject balloon)
    {
        _crongSkeletonAnimation.state.SetAnimation(0, "2", false);
        _crongSkeletonAnimation.state.AddAnimation(0, "1", true,0);

        _successParticle.StartAction(balloon.transform.localPosition + balloon.transform.GetChild((int)EBalloonLevel.Full).localPosition);
        _currentFillAmount += 1 / COMPLETE_BALLOON_COUNT;
        _gaugeBar.DOFillAmount(_currentFillAmount, 0.25f);
        if(_currentFillAmount >= 1)
        {
            Util.GetInstance.IsNotTouched = true;
            StartCoroutine(CoCompleteGame());
        }
        StartCoroutine(CoPangBalloon(balloon));
    }

    IEnumerator CoCompleteGame()
    {
        foreach (Transform item in _balloonPooling.transform)
        {
            item.GetChild((int)EBalloonLevel.Full).GetComponent<SpriteRenderer>().DOFade(0, 0.5f).OnComplete(()=> {
                item.gameObject.SetActive(false);
            });
        }
        Destroy(_balloonPooling);
        GetComponent<GameGauge>().GaugeDestroy(true);
        _balloonMachine.state.ClearTracks();
        _balloonMachine.skeleton.SetToSetupPose();
        yield return new WaitForSeconds(0.5f);
        _pororoSkeletonAnimation.state.SetAnimation(0, "2", false);
        _pororoSkeletonAnimation.state.AddAnimation(0, "3", true,0);

        _eddySkeletonAnimation.state.SetAnimation(0, "2", false);
        _eddySkeletonAnimation.state.AddAnimation(0, "3", true, 0);

        _crongSkeletonAnimation.state.SetAnimation(0, "3", false);
        _crongSkeletonAnimation.state.AddAnimation(0, "4", true, 0);

        yield return new WaitForSeconds(3);
        GameManager.GameEndCallBackEvent.Invoke();
    }

    IEnumerator CoPangBalloon(GameObject balloon)
    {
        balloon.transform.DOKill();
        balloon.transform.GetChild((int)EBalloonLevel.Full).gameObject.SetActive(false);
        balloon.transform.GetChild((int)EBalloonLevel.Pang).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.15f);
        balloon.transform.GetChild((int)EBalloonLevel.Pang).gameObject.SetActive(false);
        balloon.transform.GetChild((int)EBalloonLevel.Bubble).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.15f);
        Destroy(balloon);
    }

    void BalloonChangeScale(GameObject[] balloons)
    {
        for (int i = 0; i < (int)EBalloonFirstLocation.Size; i++)
        {
            if (balloons[(int)EBalloonFirstLocation.Left].transform.GetChild((int)EBalloonLevel.Full).localScale.x == 0.35f)
            {
                balloons[i].transform.GetChild((int)EBalloonLevel.Full).DOLocalMoveY(BALLOON_FIRST_LOCATION_YPOS * 2, 0.35f);
                balloons[i].transform.GetChild((int)EBalloonLevel.Full).DOScale(0.65f, 0.35f);
                
            }
            else
            {
                balloons[i].transform.GetChild((int)EBalloonLevel.Full).DOLocalMoveY(BALLOON_FIRST_LOCATION_YPOS * 3, 0.35f);
                balloons[i].transform.GetChild((int)EBalloonLevel.Full).DOScale(1f, 0.35f);
            }
        }
    }
}
