﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine.Unity;

/// <summary>
/// 크롱과 충돌하는 물체 체크
/// </summary>
public class HulkCrong : MonoBehaviour
{
    [Header("최상단 Transform 땅 흔들기 효과에 사용")]
    [SerializeField]
    private Transform _parentTransform;
    public Transform ParentTransform { get; }

    [Header("성공 파티클")]
    [SerializeField]
    private Particles _successParticle;

    private const float JUMP_HEIGHT = 300;
    private GameObject _crashedObstacleObj = null;
    private GameObject _crashedEnemyObj = null;

    //점프 중인가?
    private bool _isJumped = true;
    //점프 중에 내려오는 중인가?
    private bool _isDown = false;
    

    private void Start()
    {
        StartCoroutine(CoWaitAndJumpPossible());
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            if (!_isJumped)
            {
                StartCoroutine("CoJumpAction");
            }
            
        }
    }

    IEnumerator CoWaitAndJumpPossible()
    {
        yield return new WaitForSeconds(2);
        _isJumped = false;
    }

    IEnumerator CoJumpAction()
    {
        GetComponent<SpineAnimation>().SetToSetupPoseAndAnimation(GetComponent<SkeletonAnimation>(), "1", 0);
        _isJumped = true;
        _parentTransform.DOKill();
        _parentTransform.DOLocalMove(Vector2.zero, 0.25f);
        //그림자
        transform.GetChild(0).gameObject.SetActive(false);
        transform.DOLocalJump(transform.localPosition + new Vector3(0, 0), JUMP_HEIGHT, 1, 0.5f);
        yield return new WaitForSeconds(0.25f);
        _isDown = true;
        yield return new WaitForSeconds(0.25f);
        transform.GetChild(0).gameObject.SetActive(true);
        _parentTransform.DOShakePosition(1.75f, 12, 10, 90);
        _isDown = false;
        _isJumped = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals(Constants.ETag.Enemy.ToString()))
        {
            DestroyColliderAndPunchAnimation(collision);
            _crashedEnemyObj = collision.gameObject;
            StartCoroutine("CoActionAndDestroyEnemy");
            
        }

        else if (collision.gameObject.tag.Equals(Constants.ETag.Obstacle.ToString()))
        {
            DestroyColliderAndPunchAnimation(collision);
            _crashedObstacleObj = collision.gameObject;
            
            StartCoroutine("CoAnimationAndDestroyObstacle");
        }
    }

    void DestroyColliderAndPunchAnimation(Collision2D collision)
    {
        Destroy(collision.gameObject.GetComponent<BoxCollider2D>());
        Destroy(collision.gameObject.GetComponent<Rigidbody2D>());

        if (!_isJumped)
        {
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, "2", false);
            GetComponent<SkeletonAnimation>().state.AddAnimation(0, "1", true, 0);
        }
    }

    IEnumerator CoAnimationAndDestroyObstacle()
    {
        yield return new WaitForSpineAnimationComplete(_crashedObstacleObj.gameObject.GetComponent<SkeletonAnimation>().state.SetAnimation(1, "1", false));
        _crashedObstacleObj.SetActive(false);
    }

    IEnumerator CoActionAndDestroyEnemy()
    {
        _crashedEnemyObj.gameObject.GetComponent<SkeletonAnimation>().state.SetAnimation(2, "2", true);
        if (!_isJumped)
        {
            yield return new WaitForSeconds(0.15f);
        }
        

        _crashedEnemyObj.transform.GetChild(0).gameObject.SetActive(false);
        
        if (_isDown && !_crashedEnemyObj.name.Contains("fly"))
        {
            _crashedEnemyObj.transform.DOLocalMoveY(_crashedEnemyObj.transform.localPosition.y - 50, 0.35f).SetEase(Ease.OutQuart);
            _crashedEnemyObj.transform.DOScaleY(0.25f, 0.35f).SetEase(Ease.OutQuart);
            _crashedEnemyObj.GetComponent<Particles>().StartAction(Vector2.zero);
            yield return new WaitForSeconds(0.5f);
            _crashedEnemyObj.SetActive(false);
        }
        else
        {
            //_crashedEnemyObj.gameObject.GetComponent<ObjectAction>().SetPosition(_crashedEnemyObj.gameObject.transform.localPosition + new Vector3(Util.GetInstance.GetDesignSize().x, 500), 1);
            _crashedEnemyObj.gameObject.GetComponent<ObjectAction>().StartAction(-1,true);
            
        }
    }
}
