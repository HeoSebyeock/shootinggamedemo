﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using Spine.Unity;
using System.Collections.Generic;
using UnityEngine.Events;

public class JokerCrong : MonoBehaviour
{
    enum Eapear
    {
        NoApear,
        Apear,
        Size,
    }

    enum EAnimation
    {
        LeftIdle = 1,
        RightIdle,
        LeftMove,
        RightMove,
        LeftAttack,
        RightAttack,
        LeftCry,
        RightCry,
    }

    enum ELocation
    {
        None,
        Left,
        Right,
        Size,
    }

    enum EBuildingAndLocationPath
    {
        BuildingOneAndLeft,
        BuildingOneAndRight,
        BuildingTwoAndLeft,
        BuildingTwoAndRight,
        BuildingThreeAndLeft,
        BuildingThreeAndRight,
    }

    enum ECollisionName
    {
        Shuriken,
    }

    [Header("에디 스파인")]
    [SerializeField]
    private SkeletonAnimation _eddyAnimation;

    [Header("에디 맞았을때 이벤트")]
    [SerializeField]
    private UnityEvent _eddyBlinkEvent;

    [Header("게임 끝났을때 이벤트")]
    [SerializeField]
    private UnityEvent _completeGameEvent;

    private Image _gaugeBar;
    private ELocation _currentLocation = ELocation.None;

    //private Dictionary<int, Vector3[]> _moveWayPointDictionary = new Dictionary<int, Vector3[]>();

    //크롱이 에디에게 다가갈때 필요한 변수들 
    private Vector3 _arrivePos;
    private float _speed;
    private float _currentHitCount = 1;

    const int JOKERCRONG_SLOT_NUM = 3;
    private const float FILLAMOUNT_POWER = 10;
    private const float BALLANCE_TIMESCALE = 0.5f;
    
    // Use this for initialization
    void Start()
    {
        _gaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
        _gaugeBar.fillAmount = 1f;
    }

    private void OnEnable()
    {
        //GetComponent<GameGauge>().GaugeVisible(false);
        StartCoroutine("CoEnemyApearAction");
    }

    IEnumerator CoEnemyApearAction()
    {
        yield return new WaitForSeconds(Random.Range(0.25f, 0.75f));
        Eapear randApear = (Eapear)UnityEngine.Random.Range((int)Eapear.NoApear, (int)Eapear.Size);
        if (randApear == Eapear.NoApear)
        {
            transform.DOLocalMoveY(-15, 0.1f).OnComplete(() => {
                StartCoroutine("CoReCreateEnemy");
            });
        }
        else
        {
            transform.DOLocalJump(new Vector2(0, 100), 10, 1, 0.5f).SetRelative();
            yield return new WaitForSeconds(0.5f);
            GetComponent<GameGauge>().GaugeisDisable(false);
            GetComponent<BoxCollider2D>().enabled = true;
            StartCoroutine("CoLocationAction");
        }
    }

    IEnumerator CoReCreateEnemy()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(0.5f, 1.5f));
        BatMan_Game3.isBuildingExist[int.Parse(gameObject.name)] = false;
        BatMan_Game3.isEnemyExist[JOKERCRONG_SLOT_NUM] = false;
        gameObject.SetActive(false);
    }

    IEnumerator CoLocationAction()
    {
        _currentLocation = (ELocation)Random.Range((int)ELocation.Left,(int)ELocation.Size);

        if(_currentLocation == ELocation.Left)
        {
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.LeftIdle).ToString(), true);
            
        }
        else
        {
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.RightIdle).ToString(), true);
        }
        yield return new WaitForSeconds(1);
        StartCoroutine("CoMoveAction");
    }

    IEnumerator CoMoveAction()
    {
        float waitTime = 0f;
        if (_currentLocation == ELocation.Left)
        {
            if (gameObject.name != "2")
            {
                waitTime = Random.Range(1.25f, 2f);
                GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.LeftMove).ToString(), true);
                transform.DOLocalMoveX(-180, waitTime).SetRelative();
            }
        }
        else
        {
            if(gameObject.name != "1")
            {
                waitTime = Random.Range(0.375f, 1f);
                GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.RightMove).ToString(), true);
                transform.DOLocalMoveX(90, waitTime).SetRelative();
            }
        }
        yield return new WaitForSeconds(waitTime);
        
        //Eapear randApear = (Eapear)UnityEngine.Random.Range((int)Eapear.NoApear, (int)Eapear.Size);
        StartCoroutine("CoDoPathAction");
        yield return new WaitForSeconds(1);
        //BatMan_Game3.isBuildingExist[int.Parse(gameObject.name)] = false;
    }

    IEnumerator CoDoPathAction()
    {
        //transform.DOLocalPath(_moveWayPointDictionary[0], 5, PathType.CatmullRom);
        //GetComponent<DOTweenPath>().path.wps[0] = new Vector3(transform.parent.localPosition.x + transform.localPosition.x - Util.GetInstance.GetDesignSize().x * 0.5f,
        //    transform.parent.localPosition.y + transform.localPosition.y - Util.GetInstance.GetDesignSize().y * 0.5f);

        switch (gameObject.name)
        {
            //건물 번호
            case "0":
                {
                    if(_currentLocation == ELocation.Left)
                    {
                        float waitTime = Random.Range(1.5f,2.5f) *BALLANCE_TIMESCALE;
                        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.RightMove).ToString(), true);
                        transform.DOLocalMoveY(-300, waitTime).SetRelative();
                        yield return new WaitForSeconds(waitTime * 0.5f);
                        GetComponent<MeshRenderer>().sortingOrder += 2;
                        yield return new WaitForSeconds(waitTime * 0.5f);
                        _speed = waitTime;
                        _arrivePos = new Vector2(325, -73);
                        _currentLocation = ELocation.Right;
                        StartCoroutine("CoMoveToEddy");
                    }
                    else
                    {
                        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.RightMove).ToString(), true);
                        _speed = Random.Range(1f, 1.5f);
                        _arrivePos = new Vector2(315, 0);
                        StartCoroutine("CoMoveToEddy");
                    }
                }
                break;
            case "1":
                {
                    if (_currentLocation == ELocation.Left)
                    {
                        float waitTime = Random.Range(1.25f, 2f) * BALLANCE_TIMESCALE;
                        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.RightMove).ToString(), true);
                        transform.DOLocalMoveY(-150, waitTime).SetRelative();
                        yield return new WaitForSeconds(waitTime * 0.5f);
                        GetComponent<MeshRenderer>().sortingOrder += 2;
                        yield return new WaitForSeconds(waitTime * 0.5f);
                        
                        transform.DOLocalMoveX(275, waitTime).SetRelative();
                        yield return new WaitForSeconds(waitTime);
                        _speed = 3.5f - waitTime;
                        _arrivePos = new Vector2(131, 170);
                        _currentLocation = ELocation.Right;
                        StartCoroutine("CoMoveToEddy");
                    }
                    else
                    {
                        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.RightMove).ToString(), true);
                        _speed = Random.Range(0.75f, 1.25f);
                        _arrivePos = new Vector2(131, 170);
                        _currentLocation = ELocation.Right;
                        StartCoroutine("CoMoveToEddy");
                    }
                }
                break;
            case "2":
                {
                    
                    if (_currentLocation == ELocation.Left)
                    {
                        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.LeftMove).ToString(), true);
                        _speed = Random.Range(1.25f, 2f);
                        _arrivePos = new Vector2(-325, 94);
                        StartCoroutine("CoMoveToEddy");
                    }
                    else
                    {
                        float waitTime = Random.Range(0.25f, 0.5f) * BALLANCE_TIMESCALE;
                        transform.DOLocalMoveX(50, waitTime).SetRelative();
                        yield return new WaitForSeconds(waitTime);
                        waitTime = Random.Range(1.75f, 2.75f);
                        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.LeftMove).ToString(), true);
                        transform.DOLocalMoveY(-325, waitTime).SetRelative();
                        yield return new WaitForSeconds(waitTime * 0.15f);
                        GetComponent<MeshRenderer>().sortingOrder += 2;
                        yield return new WaitForSeconds(waitTime * 0.85f);

                        transform.DOLocalMoveX(-325, waitTime).SetRelative();
                        yield return new WaitForSeconds(waitTime);
                        _speed = 3.5f - waitTime;
                        _arrivePos = new Vector2(-325, 70);
                        _currentLocation = ELocation.Left;
                        StartCoroutine("CoMoveToEddy");
                    }
                }
                break;
            default:
                break;
        }
        yield return null;
    }

    IEnumerator CoMoveToEddy()
    {
        while (true)
        {
            transform.localPosition = Vector2.MoveTowards(transform.localPosition, _arrivePos, _speed);
            if(transform.localPosition == _arrivePos)
            {
                StartCoroutine("CoAttackToEddy");
                yield break;
            }
            yield return null;
        }
    }

    IEnumerator CoAttackToEddy()
    {
        if (_currentLocation == ELocation.Left)
        {
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.LeftAttack).ToString(), false);
            yield return new WaitForSeconds(0.667f * 0.5f);
            _eddyAnimation.state.SetAnimation(0, ((int)BatMan_Game3.EEddyAnimation.RightHit).ToString(),false);
            _eddyAnimation.state.AddAnimation(0, ((int)BatMan_Game3.EEddyAnimation.RightIdle).ToString(), true,0);
        }
        else
        {
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.RightAttack).ToString(), false);
            yield return new WaitForSeconds(0.667f * 0.5f);
            _eddyAnimation.state.SetAnimation(0, ((int)BatMan_Game3.EEddyAnimation.LeftHit).ToString(), false);
            _eddyAnimation.state.AddAnimation(0, ((int)BatMan_Game3.EEddyAnimation.LeftIdle).ToString(), true, 0);
        }
        Util.GetInstance.IsNotTouched = true;
        yield return new WaitForSeconds(0.667f * 0.5f);
        _eddyBlinkEvent.Invoke();
        BatMan_Game3.isBuildingExist[int.Parse(gameObject.name)] = false;
        gameObject.SetActive(false);
        GetComponent<GameGauge>().GaugeisDisable(true);
        GetComponent<SkeletonAnimation>().state.ClearTracks();
        GetComponent<SkeletonAnimation>().skeleton.SetToSetupPose();
    }

    private void OnDisable()
    {
        GetComponent<BoxCollider2D>().enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name.Contains(ECollisionName.Shuriken.ToString()))
        {
            Debug.Log("hit shuriken");
            _currentHitCount -= 1 / FILLAMOUNT_POWER;
            _gaugeBar.DOFillAmount(_currentHitCount, 0.2f);
            StopAllCoroutines();

            transform.DOKill();

            GetComponent<BoxCollider2D>().enabled = false;

            if (_currentHitCount <= 0f)
            {
                Util.GetInstance.IsNotTouched = true;
                GetComponent<GameGauge>().GaugeDestroy(false);
                StartCoroutine("CoCompleteGame");
                return;
            }

            if (transform.parent.localPosition.x > Util.GetInstance.GetDesignSize().x * 0.5f)
            {
                _eddyAnimation.state.SetAnimation(0, ((int)BatMan_Game3.EEddyAnimation.RightHappy).ToString(), false);
                _eddyAnimation.state.AddAnimation(0, ((int)BatMan_Game3.EEddyAnimation.RightIdle).ToString(), true, 0);

                GetComponent<SkeletonAnimation>().state.SetAnimation(0,((int)EAnimation.LeftCry).ToString(), false);
                transform.DOLocalJump(new Vector2(30, 0), 30, 1, 0.25f).SetRelative().OnComplete(() =>
                {
                    transform.DOLocalJump(new Vector2(15, 0), 10, 1, 0.15f).SetRelative().OnComplete(() => {
                        StartCoroutine("CoWaitTimeAndInitialize");
                    });
                });
            }
            else
            {
                _eddyAnimation.state.SetAnimation(0, ((int)BatMan_Game3.EEddyAnimation.LeftHappy).ToString(), false);
                _eddyAnimation.state.AddAnimation(0, ((int)BatMan_Game3.EEddyAnimation.LeftIdle).ToString(), true, 0);

                GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.RightCry).ToString(), false);
                transform.DOLocalJump(new Vector2(-30, 0), 30, 1, 0.25f).SetRelative().OnComplete(() => {
                    transform.DOLocalJump(new Vector2(-15, 0), 10, 1, 0.15f).SetRelative().OnComplete(() => {
                        StartCoroutine("CoWaitTimeAndInitialize");
                    });
                });
            }
        }
    }

    IEnumerator CoWaitTimeAndInitialize()
    {
        yield return new WaitForSeconds(0.25f);
        BatMan_Game3.isBuildingExist[int.Parse(gameObject.name)] = false;
        BatMan_Game3.isEnemyExist[JOKERCRONG_SLOT_NUM] = false;
        gameObject.SetActive(false);
        GetComponent<GameGauge>().GaugeisDisable(true);
        GetComponent<SkeletonAnimation>().state.ClearTracks();
        GetComponent<SkeletonAnimation>().skeleton.SetToSetupPose();
    }

    IEnumerator CoCompleteGame()
    {
        _completeGameEvent.Invoke();
        Time.timeScale = 0.2f;
        if (transform.parent.localPosition.x < Util.GetInstance.GetDesignSize().x * 0.5f)
        {
            
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.RightCry).ToString(), false);
            transform.DOLocalRotate(new Vector3(0, 0, 90), 0.667f);
            transform.DOLocalJump(new Vector2(-100, -50), 50, 1, 0.667f * 0.5f).SetRelative().SetEase(Ease.Flash);
            yield return new WaitForSeconds(0.667f * 0.5f);
            transform.DOLocalJump(new Vector2(-50, -20), 25, 1, 0.667f * 0.25f).SetRelative().SetEase(Ease.Flash);
            yield return new WaitForSeconds(0.667f * 0.25f);
            _eddyAnimation.state.SetAnimation(0, ((int)BatMan_Game3.EEddyAnimation.RightHappy).ToString(), true);
        }
        else
        {
            
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.LeftCry).ToString(), false);
            transform.DOLocalRotate(new Vector3(0, 0, -90), 0.667f);
            transform.DOLocalJump(new Vector2(100, -50), 50, 1, 0.667f * 0.5f).SetRelative().SetEase(Ease.Flash);
            yield return new WaitForSeconds(0.667f * 0.5f);
            transform.DOLocalJump(new Vector2(50, -20), 25, 1, 0.667f * 0.25f).SetRelative().SetEase(Ease.Flash);
            yield return new WaitForSeconds(0.667f * 0.25f);
            _eddyAnimation.state.SetAnimation(0, ((int)BatMan_Game3.EEddyAnimation.LeftHappy).ToString(), true);
        }
        Time.timeScale = 1;
        GetComponent<SpineAnimation>().Fade(GetComponent<SkeletonAnimation>(), 0, 0.25f);
        
    }
}
