﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Spine;

public class BatManCar : MonoBehaviour
{
    public enum EEddyCarAnimation
    {
        Run = 1,
        Cry,
        Boost,
    }

    [Header("장애물 이벤트")]
    [SerializeField]
    private UnityEvent _obstacleEvent;

    [Header("부스트 아이템 이벤트")]
    [SerializeField]
    private UnityEvent _boostItemEvent;

    [Header("성공 파티클")]
    [SerializeField]
    private Particles _successParticle;

    [Header("성공 파티클 부스트")]
    [SerializeField]
    private Particles _successBoostParticle;

    [Header("스티커 부모 트랜스폼")]
    [SerializeField]
    private Transform _stickerParentTransform;

    private GameObject _crashedObstacleObj = null;

    //점프 중인가?
    private bool _isJumped = true;
    //점프 중에 내려오는 중인가?
    private bool _isDown = false;

    private Vector2 _firstCarPos = new Vector2(304,260);

    private const float JUMP_HEIGHT = 350;
    private const float CARSPINE_SCALE = 0.85f;
    
    private void Start()
    {
        StartCoroutine(CoWaitAndJumpPossible());
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            if (!_isJumped)
            {
                StartCoroutine("CoJumpAction");
            }

        }
    }

    IEnumerator CoWaitAndJumpPossible()
    {
        yield return new WaitForSeconds(2);
        _isJumped = false;
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        DestroyColliderAndPunchAnimation(collider);

        //boost item get
        if (collider.gameObject.tag.Equals(Constants.ETag.BoostItem.ToString()))
        {
            gameObject.transform.DOPunchScale(new Vector2(0.15f,0.15f), 0.5f,0,0).SetEase(Ease.Flash);
            collider.gameObject.SetActive(false);
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EEddyCarAnimation.Boost).ToString(), true);
            gameObject.tag = Constants.ETag.BoostItem.ToString();
            _boostItemEvent.Invoke();

        }
        else
        {
            _crashedObstacleObj = collider.gameObject;
        }
        StartCoroutine("CoAnimationAndDestroyObstacle");

    }

    void DestroyColliderAndPunchAnimation(Collider2D collider)
    {
        if(collider.gameObject.GetComponent<BoxCollider2D>() == null)
        {
            return;
        }
        Destroy(collider.gameObject.GetComponent<BoxCollider2D>());

        if (!_isJumped && collider.gameObject.tag.Equals(Constants.ETag.Obstacle.ToString()) && gameObject.tag.Equals(Constants.ETag.Untagged.ToString()))
        {
            transform.DOLocalJump(_firstCarPos + new Vector2(-60, 0), 45, 1, 0.175f);
            //transform.DOLocalMoveX(_firstCarPos.x + -60, 0.035f).SetEase(Ease.OutBack);
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EEddyCarAnimation.Cry).ToString(), false);
            GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)EEddyCarAnimation.Run).ToString(), true, 0);
        }
        
    }

    IEnumerator CoAnimationAndDestroyObstacle()
    {
        if (gameObject.tag.Equals(Constants.ETag.BoostItem.ToString()))
        {
            _successBoostParticle.StartAction(transform.localPosition + new Vector3(200,0));
            TrackEntry trackEntry = _crashedObstacleObj.gameObject.GetComponent<SkeletonAnimation>().state.SetAnimation(1, "1", false);
            trackEntry.TimeScale = 1.5f;
            yield return new WaitForSpineAnimationComplete(trackEntry);
            if(_crashedObstacleObj != null)
            {
                _crashedObstacleObj.SetActive(false);
            }
        }
        else
        {
            if (!_isJumped)
            {
                _obstacleEvent.Invoke();
                _crashedObstacleObj.GetComponent<SpineAnimation>().Fade(_crashedObstacleObj.GetComponent<SkeletonAnimation>(), 0, 0.5f);
                yield return new WaitForSeconds(0.75f);
                GetComponent<SkeletonAnimation>().state.ClearTracks();
                GetComponent<SkeletonAnimation>().skeleton.SetToSetupPose();
                //transform.DOShakePosition(0.085f, new Vector2(0, 7.5f), 10, 0).SetLoops(8);

                //yield return new WaitForSeconds(0.68f);
                float time = GetComponent<SpineAnimation>().BlinkAction(3,0.1f);
                StartCoroutine("CoStickerBlinkAction");
                yield return new WaitForSeconds(time);
                transform.DOLocalMoveX(_firstCarPos.x, 0.25f);
                GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)EEddyCarAnimation.Run).ToString(), true, 0);
            }
            else
            {
                _successParticle.StartAction(transform.localPosition + new Vector3(0,-100));
                StartCoroutine("CoPowerJumpAction");
                yield return new WaitForSpineAnimationComplete(_crashedObstacleObj.gameObject.GetComponent<SkeletonAnimation>().state.SetAnimation(1, "1", false));
                _crashedObstacleObj.SetActive(false);
            }
        }
    }

    IEnumerator CoStickerBlinkAction()
    {
        for (int i = 0; i < 3; i++)
        {
            foreach (Transform sticker in _stickerParentTransform)
            {
                sticker.gameObject.SetActive(false);
            }
            yield return new WaitForSeconds(0.1f);
            foreach (Transform sticker in _stickerParentTransform)
            {
                sticker.gameObject.SetActive(true);
            }
            yield return new WaitForSeconds(0.2f);
        }
    }

    IEnumerator CoJumpAction()
    {
        transform.DOKill();
        transform.DOScale(new Vector2(CARSPINE_SCALE, CARSPINE_SCALE), 0.5f);
        transform.GetChild(0).gameObject.SetActive(false);

        //Normal Mode
        if (gameObject.tag.Equals(Constants.ETag.Untagged.ToString()))
        {
            GetComponent<SpineAnimation>().SetToSetupPoseAndAnimation(GetComponent<SkeletonAnimation>(), ((int)EEddyCarAnimation.Run).ToString(), 0);
            _isJumped = true;
            transform.DORotate(new Vector3(0, 0, 30), 0.075f, 0);
            transform.DOLocalJump(new Vector2(300 * 0.25f, 0), JUMP_HEIGHT * 0.75f, 1, 0.45f).SetRelative();
            yield return new WaitForSeconds(0.225f);
            transform.GetChild(0).gameObject.SetActive(true);
            _isDown = true;
            yield return new WaitForSeconds(0.15f);
            transform.DORotate(new Vector3(0, 0, 0), 0.075f, 0);
            yield return new WaitForSeconds(0.075f);

            _isDown = false;

            transform.DOLocalJump(new Vector2(-200 * 0.25f, 0), 85 * 0.75f, 1, 0.35f).SetRelative();
            yield return new WaitForSeconds(0.35f);
            transform.DOLocalJump(new Vector2(-75f * 0.25f, 0), 35 * 0.75f, 1, 0.21f).SetRelative();
            yield return new WaitForSeconds(0.21f);
            transform.DOLocalJump(new Vector2(-25f * 0.25f, 0), 25 * 0.75f, 1, 0.15f).SetRelative();
            yield return new WaitForSeconds(0.15f);
            _isJumped = false;
        }
        //Boost Mode
        else
        {
            _isJumped = true;
            transform.GetChild(0).gameObject.SetActive(true);
            transform.DOScale(new Vector2(CARSPINE_SCALE * 0.88f, CARSPINE_SCALE * 1.12f), 0.1f);
            yield return new WaitForSeconds(0.1f);
            transform.DOScale(new Vector2(CARSPINE_SCALE * 1.12f, CARSPINE_SCALE * 0.88f), 0.1f);
            transform.DOLocalMoveX(_firstCarPos.x + 350, 0.4f).SetEase(Ease.OutQuart);
            yield return new WaitForSeconds(0.4f);
            transform.DOKill();
            transform.DOScale(new Vector2(CARSPINE_SCALE, CARSPINE_SCALE), 0.8f);
            transform.DOLocalMoveX(_firstCarPos.x, 0.8f);
            yield return new WaitForSeconds(0.8f);
            _isJumped = false;
        }
    }

    IEnumerator CoPowerJumpAction()
    {
        StopCoroutine("CoJumpAction");
        transform.DOKill();
        transform.localRotation = Quaternion.Euler(Vector3.zero);
        transform.DOLocalJump(_firstCarPos + new Vector2(400 * 0.25f, 0), JUMP_HEIGHT * 1.5f * 0.75f, 1, 0.8f);
        transform.DORotate(new Vector3(0, 0, 30), 0.075f, 0);
        yield return new WaitForSeconds(0.4f);
        _isDown = true;
        yield return new WaitForSeconds(0.25f);
        transform.DORotate(new Vector3(0, 0, 0), 0.075f, 0);
        yield return new WaitForSeconds(0.15f);

        _isDown = false;

        transform.DOLocalJump(_firstCarPos + new Vector2(200 * 0.25f, 0), 100 * 0.75f, 1, 0.4f);
        yield return new WaitForSeconds(0.4f);
        transform.DOLocalJump(_firstCarPos + new Vector2(75 * 0.25f, 0), 50 * 0.75f, 1, 0.25f);
        yield return new WaitForSeconds(0.25f);
        transform.DOLocalJump(_firstCarPos + new Vector2(0, 0), 25 * 0.75f, 1, 0.2f);
        yield return new WaitForSeconds(0.1f);
        _isJumped = false;
    }
}
