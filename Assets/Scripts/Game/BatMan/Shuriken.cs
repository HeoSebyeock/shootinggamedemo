﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Shuriken : MonoBehaviour
{
    private Vector2 _createPos = new Vector2(639,372);

    // Start is called before the first frame update
    void Start()
    {
        transform.DOLocalMove(Input.mousePosition, Vector2.Distance(Input.mousePosition, _createPos) / 1000).OnComplete(() =>
        {
            BoundAction();
        });
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GetComponent<BoxCollider2D>().enabled = false;
        transform.DOKill();
        BoundAction();
    }

    private void BoundAction()
    {
        transform.DOLocalJump(new Vector2(Random.Range(-30, 30), Random.Range(-30, 30)), 30, 1, 0.35f).SetRelative();
        transform.DOLocalRotate(new Vector3(0, 0, Random.Range(-90, 90)), 0.35f).SetEase(Ease.Flash).OnComplete(() => {
            GetComponent<SpriteRenderer>().DOFade(0, 0.2f).OnComplete(()=> {
                Destroy(gameObject);
            });
        });
    }
}
