﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Spine.Unity;
using UnityEngine.Events;

public class BatMan_Game3 : MonoBehaviour
{
    public enum EEddyAnimation
    {
        RightIdle = 1,
        LeftIdle,
        RightThrow,
        LeftThrow,
        RightHit,
        LeftHit,
        RightHappy,
        LeftHappy,
    }

    enum EEnemy
    {
        Pororo,
        Petty,
        Poby,
        Joker,
        Size = 6,
    }

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("에디")]
    [SerializeField]
    private SkeletonAnimation _eddyAnimation;

    [Header("에디 자동차")]
    [SerializeField]
    private SkeletonAnimation _carAnimation;

    [Header("배트맨 표창 부모 트랜스폼")]
    [SerializeField]
    private Transform _shurikenParentTransform;

    [Header("이전 스티커 부모 본")]
    [SerializeField]
    private Transform _stickerBeforeParentBoneTransform;

    [Header("현재 스티커 부모 본")]
    [SerializeField]
    private Transform _stickerCurrentParentBoneTransform;

    [Header("악당")]
    [SerializeField]
    private GameObject[] _enemys;

    [Header("건물 부모 트랜스폼")]
    [SerializeField]
    private Transform _buildingParentTransform;

    [Header("전체 파티클")]
    [SerializeField]
    private Particles _fullParticle;

    [Header("전체 파티클2")]
    [SerializeField]
    private Particles _fullParticle2;

    public static bool[] isBuildingExist = new bool[BUILDING_COUNT];
    public static bool[] isEnemyExist = new bool[ENEMY_COUNT];

    private float[] _enemyCreateHeight = new float[] { 65,15,115};

    //const float LEFT_SHURIKEN_CREATE_POS = 608f;
    //const float RIGHT_SHURIKEN_CREATE_POS = 670f;
    const float HEIGHT_SHURIKEN_CREATE_POS = 340f;
    const int STICKER_COUNT = 8;
    const int BUILDING_COUNT = 3;
    const int ENEMY_COUNT = 4;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoGameLoop");
    }

    IEnumerator CoGameLoop()
    {
        Util.GetInstance.IsNotTouched = true;
        _stickerBeforeParentBoneTransform.SetParent(_stickerCurrentParentBoneTransform);
        _stickerBeforeParentBoneTransform.localPosition = Vector2.zero;
        _stickerBeforeParentBoneTransform.localScale = new Vector2(0.3f, 0.3f);
        yield return new WaitForSeconds(1.5f);
        _carAnimation.state.SetAnimation(0, "1", false);
        _carAnimation.state.AddAnimation(0, "2", false,0);
        _carAnimation.state.AddAnimation(0, "4", false, 0);
        yield return new WaitForSeconds(0.833f + 0.667f);
        _eddyAnimation.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.833f);
        StartCoroutine("CoMakeEnemey");
        Util.GetInstance.IsNotTouched = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            
            _clickParticle.StartAction(Input.mousePosition);

            StopCoroutine("CoShurikenThrowAction");
            StartCoroutine("CoShurikenThrowAction");
        }
    }

    IEnumerator CoShurikenThrowAction()
    {
        GameObject shuriken = _shurikenParentTransform.GetComponent<ObjectPooling>().GetObject(_shurikenParentTransform);
        _eddyAnimation.state.ClearTracks();
        if (Input.mousePosition.x > _eddyAnimation.transform.localPosition.x)
        {
            shuriken.transform.localPosition = new Vector2(_eddyAnimation.transform.localPosition.x, HEIGHT_SHURIKEN_CREATE_POS);
            _eddyAnimation.state.SetAnimation(0, ((int)EEddyAnimation.RightThrow).ToString(), false);
            _eddyAnimation.state.AddAnimation(0, ((int)EEddyAnimation.RightIdle).ToString(), true,0);
        }
        else
        {
            shuriken.transform.localPosition = new Vector2(_eddyAnimation.transform.localPosition.x, HEIGHT_SHURIKEN_CREATE_POS);
            _eddyAnimation.state.SetAnimation(0, ((int)EEddyAnimation.LeftThrow).ToString(), false);
            _eddyAnimation.state.AddAnimation(0, ((int)EEddyAnimation.LeftIdle).ToString(), true,0);
        }
        yield return null;
    }

    IEnumerator CoMakeEnemey()
    {
        while (true)
        {
            
            for (int i = 0; i < 1;)
            {
                int randBuildingNum = Random.Range(0, 3);
                if (!isBuildingExist[randBuildingNum])
                {
                    for (int j = 0; j < 1;)
                    {
                        int randEnemyNum = Random.Range(0, (int)EEnemy.Size);
                        if(randEnemyNum >= (int)EEnemy.Joker)
                        {
                            randEnemyNum = (int)EEnemy.Joker;
                        }
                        if (!isEnemyExist[randEnemyNum]) 
                        {
                            if(randEnemyNum >= (int)EEnemy.Joker)
                            {
                                _enemys[(int)EEnemy.Joker].name = EEnemy.Joker.ToString();
                                _enemys[(int)EEnemy.Joker].SetActive(true);
                                _enemys[(int)EEnemy.Joker].transform.SetParent(_buildingParentTransform.GetChild(randBuildingNum));
                                _enemys[(int)EEnemy.Joker].GetComponent<MeshRenderer>().sortingOrder = _buildingParentTransform.GetChild(randBuildingNum).GetComponent<SpriteRenderer>().sortingOrder - 1;
                                _enemys[(int)EEnemy.Joker].transform.localPosition = new Vector2(0, _enemyCreateHeight[randBuildingNum]);
                                isEnemyExist[randEnemyNum] = true;
                                j++;
                                yield return null;
                                //생성된 빌딩 위치를 알고 있어야해
                                _enemys[(int)EEnemy.Joker].name = randBuildingNum.ToString();
                            }
                            else
                            {
                                GameObject enemy = Instantiate(_enemys[randEnemyNum]);
                                enemy.SetActive(true);
                                enemy.transform.SetParent(_buildingParentTransform.GetChild(randBuildingNum));
                                enemy.GetComponent<MeshRenderer>().sortingOrder = _buildingParentTransform.GetChild(randBuildingNum).GetComponent<SpriteRenderer>().sortingOrder - 1;
                                enemy.transform.localPosition = new Vector2(0, _enemyCreateHeight[randBuildingNum]);
                                isEnemyExist[randEnemyNum] = true;
                                j++;
                                yield return null;
                                //생성된 빌딩 위치를 알고 있어야해
                                enemy.name = randBuildingNum.ToString();
                            }
                        }
                        yield return null;
                    }
                    isBuildingExist[randBuildingNum] = true;
                    i++;
                }
                yield return null;
            }
            yield return new WaitForSeconds(Random.Range(0.25f,1.25f));
            
        }
    }

    public void EddyBlinkAnimation()
    {
        StartCoroutine("CoEddyBlinkAnimation");
    }

    IEnumerator CoEddyBlinkAnimation()
    {
        float blinkTime = _eddyAnimation.GetComponent<SpineAnimation>().BlinkAction(6);
        yield return new WaitForSeconds(blinkTime);
        isEnemyExist[(int)EEnemy.Joker] = false;

        Util.GetInstance.IsNotTouched = false;
    }

    public void CompleteGame()
    {
        StopAllCoroutines();
        for (int i = 0; i < BUILDING_COUNT; i++)
        {
            foreach (Transform enemy in _buildingParentTransform.GetChild(i))
            {
                if (enemy.tag.Equals(Constants.ETag.Enemy.ToString()))
                {
                    enemy.DOKill();
                    enemy.GetComponent<SpineAnimation>().Fade(enemy.GetComponent<SkeletonAnimation>(), 0, 0.5f);
                    //enemy.gameObject.SetActive(false);
                    //enemy.DOKill();
                }
            } 
        }
        StartCoroutine("CoCompleteGame");
    }

    IEnumerator CoCompleteGame()
    {
        yield return new WaitForSeconds(2f);
        _fullParticle.StartAction(Util.GetInstance.GetDesignSize() * 0.5f);
        _fullParticle2.StartAction(Util.GetInstance.GetDesignSize() * 0.5f);
        yield return new WaitForSeconds(3f);
        GameManager.GameEndCallBackEvent.Invoke();
    }
}
