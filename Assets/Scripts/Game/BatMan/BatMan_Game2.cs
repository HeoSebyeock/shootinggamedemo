﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Spine.Unity;

public class BatMan_Game2 : MonoBehaviour
{
    [Header("자동차 부모 오브젝트")]
    [SerializeField]
    private GameObject _carParentObject;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("배경")]
    [SerializeField]
    private Transform _backGroundTransform;

    [Header("자동차 아이콘")]
    [SerializeField]
    private Transform _carIcon;

    [Header("장애물 부모 트랜스폼")]
    [SerializeField]
    private Transform _obstacleTransform;

    [Header("조커 스파인")]
    [SerializeField]
    private SkeletonAnimation _jokerSkeletonAnimation;

    [Header("게임 게이지")]
    [SerializeField]
    private SpriteRenderer[] _gameGauges;

    [Header("조커 잡기 게임")]
    [SerializeField]
    private GameObject _jokerCatchGame;

    //현재 맵이 이동한 거리
    private float _moveDistance;

    private float _currentSpeed = 1;

    private bool _isCompleteGame = false;

    //시퀀스
    private Sequence _mapSequence;
    private Sequence _makeObjSequence;


    private const float FIRST_CAR_XPOS = 490;
    private const float MAP_MOVE_POWER = -1000;
    private const float ICON_MOVE_POWER = 0.004f;
    private const float MAKE_OBJECT_DELAY = 0.85f;
    private const float OBSTACLE_HEIGHT = -150f;
    private const float MAP_WIDTH = 28160f - 840f;
    private const float BOOST_SPEED = 1.5f;
    private const int CARSPINE_CHILD_NUM = 1;
    private const int BOOST_ITEM_CREATE_CYCLE = 5;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoGameLoop");
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            _clickParticle.StartAction(Input.mousePosition);
        }

        if (_backGroundTransform.localPosition.x + Util.GetInstance.GetDesignSize().x * 0.65f <= -MAP_WIDTH && !_isCompleteGame)
        {
            _isCompleteGame = true;
            StartCoroutine("CoCompleteGame");
            
        }
        else if (_backGroundTransform.localPosition.x <= -MAP_WIDTH && !Util.GetInstance.IsNotTouched)
        {
            Util.GetInstance.IsNotTouched = true;
            _makeObjSequence.Kill();
            foreach (Transform item in _backGroundTransform)
            {
                if (item.name.Contains("Clone"))
                {
                    item.tag = Constants.ETag.Untagged.ToString();
                    item.GetComponent<SpineAnimation>().Fade(item.GetComponent<SkeletonAnimation>(), 0, 0.5f);
                }
            }
        }
    }

    IEnumerator CoCompleteGame()
    {
        for (int i = 0; i < _gameGauges.Length; i++)
        {
            _gameGauges[i].DOFade(0, 0.5f);
        }

        _mapSequence.Kill();
        _carParentObject.transform.DOScale(new Vector2(0.925f,1.075f), 0.25f).SetEase(Ease.InCirc);
        _carParentObject.transform.DORotate(new Vector3(0,0,7.5f), 0.25f).SetEase(Ease.InCirc);
        yield return new WaitForSeconds(0.5f);
        _carParentObject.transform.GetChild(CARSPINE_CHILD_NUM).GetComponent<SkeletonAnimation>().state.ClearTracks();
        _carParentObject.transform.GetChild(CARSPINE_CHILD_NUM).GetComponent<SkeletonAnimation>().skeleton.SetToSetupPose();
        _carParentObject.transform.DOScale(new Vector2(1, 1), 0.15f).SetEase(Ease.OutBack);
        _carParentObject.transform.DORotate(new Vector3(0, 0, 0), 0.15f).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(1.15f);
        _jokerSkeletonAnimation.state.SetAnimation(0, "1", true);
        yield return new WaitForSeconds(1f);
        _jokerSkeletonAnimation.state.SetAnimation(0, "4", true);
        _jokerSkeletonAnimation.transform.DOLocalMoveX(500, 1).SetRelative();
        yield return new WaitForSeconds(1.5f);


        _carParentObject.transform.GetChild(CARSPINE_CHILD_NUM).DOScale(new Vector2(0.85f * 0.9f, 0.85f * 1.15f), 0.1f);
        _carParentObject.transform.DORotate(new Vector3(0, 0, 10f), 0.25f).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(0.5f);
        _carParentObject.transform.GetChild(CARSPINE_CHILD_NUM).GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)BatManCar.EEddyCarAnimation.Run).ToString(), true);
        _carParentObject.transform.DORotate(new Vector3(0, 0, 0), 0.15f).SetEase(Ease.OutBack);
        _carParentObject.transform.GetChild(CARSPINE_CHILD_NUM).DOScale(new Vector2(0.85f * 1.15f, 0.85f * 0.85f), 0.1f);
        _carParentObject.transform.GetChild(CARSPINE_CHILD_NUM).DOLocalMoveX(Util.GetInstance.GetDesignSize().x, 0.8f).SetEase(Ease.OutQuart).SetRelative();
        yield return new WaitForSeconds(0.8f);
        _carParentObject.transform.GetChild(CARSPINE_CHILD_NUM).DOKill();
        gameObject.transform.parent.gameObject.AddComponent<TransitionImageAction>().CreateTransitionImage("circle", TransitionImageAction.EType.ZoomInOut, new Vector2(Util.GetInstance.GetDesignSize().x * 0.5f, Util.GetInstance.GetDesignSize().y * 0.5f), 1, 0);
        gameObject.transform.parent.gameObject.GetComponent<TransitionImageAction>().StartAction();
        yield return new WaitForSeconds(1);
        _carParentObject.SetActive(false);
        gameObject.SetActive(false);
        _jokerCatchGame.SetActive(true);
    }

    IEnumerator CoGameLoop()
    {
        yield return new WaitForSeconds(1.5f);
        //_carParentObject.transform.localPosition = new Vector2(-800 - Util.GetInstance.GetDesignSize().x, 25);
        _carParentObject.transform.GetChild(1).localPosition = new Vector2(304,260);
        _carParentObject.transform.localPosition = new Vector2(-600, 50);
        _carParentObject.transform.DOLocalMoveX(Util.GetInstance.GetDesignSize().x * 0.5f, 1f).SetRelative();
        _carParentObject.transform.GetChild(CARSPINE_CHILD_NUM).localScale = new Vector2(0.85f,0.85f);
        _carParentObject.SetActive(true);
        
        yield return new WaitForSeconds(1f);
        Util.GetInstance.IsNotTouched = false;
        MoveBatManCar();
        MakeFactory();
    }

    void MoveBatManCar()
    {
        _mapSequence = DOTween.Sequence();
        _mapSequence.AppendCallback(() =>
        {
            _moveDistance += Time.deltaTime * MAP_MOVE_POWER * _currentSpeed;
            _backGroundTransform.localPosition += new Vector3(Time.deltaTime * MAP_MOVE_POWER * 0.25f * _currentSpeed, 0);
            _carIcon.transform.localPosition -= new Vector3(Time.deltaTime * MAP_MOVE_POWER * ICON_MOVE_POWER * _currentSpeed, 0);
        });
        _mapSequence.SetLoops(-1);
    }

    void MakeFactory()
    {
        int makeCnt = 1;
        _makeObjSequence = DOTween.Sequence();
        _makeObjSequence.AppendCallback(() =>
        {
            GameObject SuperItemOrObstacleObj;
            if (makeCnt % BOOST_ITEM_CREATE_CYCLE == 0)
            {
                SuperItemOrObstacleObj = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs + "/" + Constants.EFolderLeafType.Game + "/" + "BoostItem") as GameObject);
            }
            else
            {
                SuperItemOrObstacleObj = Instantiate(_obstacleTransform.GetChild(Random.Range(0, _obstacleTransform.childCount)).gameObject);
            }

            SuperItemOrObstacleObj.transform.SetParent(_backGroundTransform);
            SuperItemOrObstacleObj.transform.localPosition = new Vector2(Util.GetInstance.GetDesignSize().x * 0.65f + -_moveDistance * 0.85f, OBSTACLE_HEIGHT);
            SuperItemOrObstacleObj.SetActive(true);
            makeCnt++;
        });
        _makeObjSequence.AppendInterval(MAKE_OBJECT_DELAY - (_currentSpeed - 1));
        _makeObjSequence.SetLoops(-1);
    }

    public void ObstacleAction()
    {
        StartCoroutine("CoObstacleAction");
    }

    IEnumerator CoObstacleAction()
    {
        Util.GetInstance.IsNotTouched = true;
        _makeObjSequence.timeScale = 0;
        _mapSequence.timeScale = 0;
        yield return new WaitForSeconds(1.75f);
        Util.GetInstance.IsNotTouched = false;
        _makeObjSequence.timeScale = 1;
        _mapSequence.timeScale = 1;
    }

    public void BoostItemAction()
    {
        StartCoroutine("CoBoostItemAction");
    }

    IEnumerator CoBoostItemAction()
    {
        _currentSpeed = BOOST_SPEED;
        yield return new WaitForSeconds(4);
        _carParentObject.transform.GetChild(CARSPINE_CHILD_NUM).GetComponent<SkeletonAnimation>().state.SetAnimation(0,((int)BatManCar.EEddyCarAnimation.Run).ToString(),true);
        _carParentObject.transform.GetChild(CARSPINE_CHILD_NUM).tag = Constants.ETag.Untagged.ToString();
        _currentSpeed = 1;
    }
}
