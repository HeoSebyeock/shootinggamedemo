﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Spine.Unity;
using UnityEngine.Events;

public class NormalEnemy : MonoBehaviour
{
    enum EName
    {
        Pororo,
        Petty,
        Poby,
    }

    enum EAnimation
    {
        RightMove = 1,
        LeftMove,
        RightCry,
        LeftCry,
    }

    enum Eapear
    {
        NoApear,
        Apear,
        Size,
    }

    enum EMove
    {
        None,
        Left,
        LeftJump,

        Right,
        RightJump,
        Size,
    }

    enum ECollisionName
    {
        Shuriken,
    }

    [Header("에디 스파인")]
    [SerializeField]
    private SkeletonAnimation _eddyAnimation;

    private string _enemyName;
    private EAnimation _currentAnimation;

    private EMove _currentMoveState = EMove.None;
    private float _moveDistance = 60f;
    

    // Use this for initialization
    void Start()
    {
        _enemyName = gameObject.name;
        StartCoroutine("CoEnemyApearAction");
    }

    IEnumerator CoEnemyApearAction()
    {
        yield return new WaitForSeconds(Random.Range(0.25f,1f));
        Eapear randApear = (Eapear)UnityEngine.Random.Range((int)Eapear.NoApear, (int)Eapear.Size);
        if(randApear == Eapear.NoApear)
        {
            transform.DOLocalMoveY(-15, 0.1f).OnComplete(()=> {
                Debug.Log("여기에 안빠지냐?");
                StartCoroutine("CoReCreateEnemy");
            });
        }
        else
        {
            transform.DOLocalJump(new Vector2(0,100), 10, 1, 0.5f).SetRelative();
            yield return new WaitForSeconds(0.5f);
            GetComponent<BoxCollider2D>().enabled = true;
            StartCoroutine("CoMoveAction");
        }
    }
    IEnumerator CoMoveAction()
    {
        while (true)
        {
            if (_currentMoveState == EMove.None)
            {
                _currentMoveState = (EMove)UnityEngine.Random.Range((int)EMove.Left, (int)EMove.Size);
            }
            else
            {
                _moveDistance = 120f;

                if (_currentMoveState <= EMove.LeftJump)
                {
                    _currentMoveState = (EMove)UnityEngine.Random.Range((int)EMove.Right, (int)EMove.Size);
                }
                else
                {
                    _currentMoveState = (EMove)UnityEngine.Random.Range((int)EMove.Left, (int)EMove.Right);
                }
            }

            float waitTime = UnityEngine.Random.Range(0.5f, 1f);

            switch (_currentMoveState)
            {

                case EMove.Left:
                    {
                        _currentAnimation = EAnimation.LeftMove;
                        transform.DOLocalMoveX(-_moveDistance, waitTime).SetRelative();
                    }
                    break;
                case EMove.LeftJump:
                    {
                        _currentAnimation = EAnimation.LeftMove;
                        transform.DOLocalJump(new Vector2(-_moveDistance, 0), UnityEngine.Random.Range(15, 40), UnityEngine.Random.Range(1,3), waitTime).SetRelative().SetEase(Ease.Flash);
                    }
                    break;
                case EMove.Right:
                    {
                        _currentAnimation = EAnimation.RightMove;
                        transform.DOLocalMoveX(_moveDistance, waitTime).SetRelative();
                    }
                    break;
                case EMove.RightJump:
                    {
                        _currentAnimation = EAnimation.RightMove;
                        transform.DOLocalJump(new Vector2(_moveDistance, 0), UnityEngine.Random.Range(15, 40), UnityEngine.Random.Range(1, 3), waitTime).SetRelative().SetEase(Ease.Flash);
                    }
                    break;
                default:
                    {
                        Debug.Log("여기에 빠진다");
                    }
                    break;
            }

            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)_currentAnimation).ToString(), true);

            yield return new WaitForSeconds(waitTime);
            int isStopRand = UnityEngine.Random.Range(0, 2);
            if (isStopRand == 0)
            {
                yield return new WaitForSeconds(UnityEngine.Random.Range(0.5f, 1.5f));
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.name.Contains(ECollisionName.Shuriken.ToString()))
        {
            StopCoroutine("CoMoveAction");
            transform.DOKill();

            GetComponent<BoxCollider2D>().enabled = false;
            
            if (transform.parent.localPosition.x < Util.GetInstance.GetDesignSize().x * 0.5f)
            {
                _eddyAnimation.state.SetAnimation(0, ((int)BatMan_Game3.EEddyAnimation.LeftHappy).ToString(), false);
                _eddyAnimation.state.AddAnimation(0, ((int)BatMan_Game3.EEddyAnimation.LeftIdle).ToString(), true,0);

                transform.DOLocalJump(new Vector2(-30, 0), 30, 1, 0.25f).SetRelative().OnComplete(() =>
                {
                    transform.DOLocalJump(new Vector2(-15, 0), 10, 1, 0.15f).SetRelative().OnComplete(()=> {
                        
                        GetComponent<SpineAnimation>().Fade(GetComponent<SkeletonAnimation>(), 0, 0.25f);
                        StartCoroutine("CoReCreateEnemy");
                    });
                });
            }
            else
            {
                _eddyAnimation.state.SetAnimation(0, ((int)BatMan_Game3.EEddyAnimation.RightHappy).ToString(), false);
                _eddyAnimation.state.AddAnimation(0, ((int)BatMan_Game3.EEddyAnimation.RightIdle).ToString(), true,0);

                transform.DOLocalJump(new Vector2(30, 0), 30, 1, 0.25f).SetRelative().OnComplete(() => {
                    transform.DOLocalJump(new Vector2(15, 0), 10, 1, 0.15f).SetRelative().OnComplete(() => {
                        
                        GetComponent<SpineAnimation>().Fade(GetComponent<SkeletonAnimation>(), 0, 0.25f);
                        StartCoroutine("CoReCreateEnemy");
                    });
                });
            }
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)_currentAnimation + 2).ToString(), false);
            
        }
    }

    IEnumerator CoReCreateEnemy()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(0.5f,1.5f));
        Debug.Log("????" + int.Parse(gameObject.name));
        BatMan_Game3.isBuildingExist[int.Parse(gameObject.name)] = false;
        if (_enemyName.Contains(EName.Pororo.ToString()))
        {
            Debug.Log("Pororo");
            BatMan_Game3.isEnemyExist[(int)EName.Pororo] = false;
        }
        else if (_enemyName.Contains(EName.Petty.ToString()))
        {
            Debug.Log("Petty");
            BatMan_Game3.isEnemyExist[(int)EName.Petty] = false;
        }
        else
        {
            Debug.Log("Poby");
            BatMan_Game3.isEnemyExist[(int)EName.Poby] = false;
        }
        Destroy(gameObject);
    }
}
