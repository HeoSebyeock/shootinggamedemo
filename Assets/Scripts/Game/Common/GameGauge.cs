﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

/// <summary>
/// 게이지를 생성 하는 경우 쓰이는 클래스
/// </summary>
public class GameGauge : MonoBehaviour
{
    public enum EType
    {
        Horizontal,
        Vertical,
    }

    public enum EGauge
    {
        BackGauge,
        InGauge,
        Icon,
        Size,
    }

    [Header("종류")]
    [SerializeField]
    private EType _type;
    public EType Type { get { return _type; } }

    [Header("이미지 오브젝트 이름")]
    [SerializeField]
    private string _imageName;
    public string ImageName { get { return _imageName; } }

    [Header("위치")]
    [SerializeField]
    private Vector2 _position;
    public Vector2 Position{ get { return _position; } }

    [Header("게이지 보이게 할지말지")]
    [SerializeField]
    private bool _isDisable = false;
    public bool IsVisible { get { return _isDisable; } }

    //게이지 오브젝트들 (순서 게이지 배경 1, 게이지 2, 아이콘 3) 
    private GameObject[] _gauges;
    public GameObject[] GameObjs { get { return _gauges; } }

    /// <summary>
    /// 게이지를 만들고 게이지 이미지를 반환합니다.
    /// </summary>
    /// <returns>게이지 이미지</returns>
    public Image MakeAndInGaugeImageReturn()
    {
        return Setting();
    }

    /// <summary>
    /// 동적으로 필요한 정보를 넣습니다.
    /// </summary>
    /// <param name="type">종류</param>
    /// <param name="imageName">이미지 이름</param>
    public void CreateGameGauge(EType type, string imageName,Vector2 position)
    {
        _type = type;
        _imageName = imageName;
        _position = position;
    }

    Image Setting()
    {
        _gauges = new GameObject[(int)EGauge.Size];
        //게임 오브젝트 생성(Component : Image(Type = Filled))
        CreateFactory createFactory = new CreateFactory();
        for (int i = 0; i < _gauges.Length; i++)
        {
            _gauges[i] = createFactory.CreateGameObject(Constants.EObjectType.Image, Constants.EFolderRootType.Images, Constants.EFolderLeafType.Common, _imageName + (i + 1).ToString(), true, transform, default, _position);
            if(_gauges[i] == null)
            {
                Debug.Log("null game Object");
            }
        }

        _gauges[(int)EGauge.InGauge].GetComponent<Image>().type = Image.Type.Filled;

        switch (_type)
        {
            case EType.Horizontal:
                {
                    _gauges[(int)EGauge.Icon].transform.localPosition -= new Vector3(_gauges[(int)EGauge.BackGauge].GetComponent<Image>().sprite.rect.size.x * 0.5f + _gauges[(int)EGauge.Icon].GetComponent<Image>().sprite.rect.size.x * 0.25f, 0);
                    _gauges[(int)EGauge.InGauge].GetComponent<Image>().fillMethod = Image.FillMethod.Horizontal;
                }
                break;

            case EType.Vertical:
                {
                    _gauges[(int)EGauge.Icon].transform.localPosition += new Vector3(0, _gauges[(int)EGauge.BackGauge].GetComponent<Image>().sprite.rect.size.y * 0.5f);
                    _gauges[(int)EGauge.InGauge].GetComponent<Image>().fillMethod = Image.FillMethod.Vertical;
                }
                break;

            default:
                break;
        }

        _gauges[(int)EGauge.InGauge].GetComponent<Image>().fillAmount = 0;
        GaugeisDisable(_isDisable);
        return _gauges[(int)EGauge.InGauge].GetComponent<Image>();
    }

    /// <summary>
    /// 게이지를 지웁니다
    /// </summary>
    /// <param name="isFade">isFade = true (Fade action) / isFade = false (object Disable)</param>
    public void GaugeDestroy(bool isFade)
    {
        for (int i = 0; i < _gauges.Length; i++)
        {
            if (isFade)
            {
                _gauges[i].GetComponent<Image>().DOFade(0, 0.5f);
            }
            else
            {
                _gauges[i].gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// 게이지 아이콘 오브젝트를 가져옵니다
    /// </summary>
    /// <returns></returns>
    public GameObject GetGaugeIcon()
    {
        return _gauges[(int)EGauge.Icon];
    }

    /// <summary>
    /// 게이지의 부모 트랜스폼을 변경합니다
    /// </summary>
    /// <param name="transform"></param>
    public void Setparent(Transform transform)
    {
        for (int i = 0; i < _gauges.Length; i++)
        {
            _gauges[i].transform.parent = transform;
        }
    }

    /// <summary>
    /// 게이지 VISIBLE
    /// </summary>
    public void GaugeisDisable(bool isDisable)
    {
        if (isDisable)
        {
            for (int i = 0; i < _gauges.Length; i++)
            {
                _gauges[i].gameObject.SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < _gauges.Length; i++)
            {
                _gauges[i].gameObject.SetActive(true);
            }
        }
        
    }
}
