﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class Guide : MonoBehaviour
{
    enum EGuideType
    {
        Press,
        Tap,
    }

    [Header("이미지 오브젝트")]
    [SerializeField]
    private GameObject _imageObject;

    [Header("가이드 타입")]
    [SerializeField]
    private EGuideType _type;

    [Header("힌트 위치")]
    [SerializeField]
    private Vector2 _imagePos;

    [Header("완료_이벤트")]
    [SerializeField]
    public UnityEvent _completeEvent;

    [Header("진행 이벤트")]
    [SerializeField]
    public UnityEvent _progressingEvent;

    private void OnEnable()
    {
        StartCoroutine("CoActionStart");
    }

    IEnumerator CoActionStart()
    {
        _imageObject.transform.localPosition = _imagePos;

        switch (_type)
        {
            case EGuideType.Press:
                {
                    Util.GetInstance.IsNotTouched = true;
                    _progressingEvent.Invoke();
                    yield return new WaitForSeconds(1);
                    _imageObject.transform.DOScale(0.85f, 0.25f);
                    yield return new WaitForSeconds(3);
                    _completeEvent.Invoke();
                    _imageObject.transform.localScale = Vector2.one;
                    Util.GetInstance.IsNotTouched = false;
                }
                break;

            case EGuideType.Tap:
                {
                    Util.GetInstance.IsNotTouched = true;
                    _progressingEvent.Invoke();
                    yield return new WaitForSeconds(0.15f);
                    Util.GetInstance.IsNotTouched = false;
                    while (true)
                    {
                        _imageObject.transform.DOScale(0.85f, 0.175f);
                        yield return new WaitForSeconds(0.175f);
                        //_imageObject.transform.localScale = Vector2.one;
                        _imageObject.transform.DOScale(1f, 0.175f);
                        yield return new WaitForSeconds(0.175f);
                    }
                }
                break;
            default:
                {

                }
                break;
        }
        
    }

    public void SetGuidePosition(Vector2 pos)
    {
        _imageObject.transform.localPosition = pos;
    }
}
