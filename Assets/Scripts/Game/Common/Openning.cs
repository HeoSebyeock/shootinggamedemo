﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using DG.Tweening;
using Spine.Unity;
using Spine;
using UnityEngine.UI;

public class Openning : MonoBehaviour
{
    enum EButton
    {
        Pressed,
        Up,
    }

    enum EGameStep
    {
        EmergencyAndPressButton,
        CharacterTransform,
    }

    [Header("게임")]
    [SerializeField]
    private GameObject[] _gameList;
    public GameObject[] GameList { get { return _gameList; } }
    
    [Header("게임 단계")]
    [SerializeField]
    private int _gameStep;
    public int GameStep { get { return _gameStep; } }

    #region 게임스탭 1
    [Header("캐릭터")]
    [Header("*** Game Step 1 ***")]
    [Space(20)]
    [SerializeField]
    private SkeletonAnimation _gameStep1_characterAnimation;
    public SkeletonAnimation GameStep1_CharacterAnimation { get { return _gameStep1_characterAnimation; } }
    
    [Header("버튼")]
    [SerializeField]
    private GameObject _gameStep1_buttonUpAndDown;
    
    #endregion

    #region 게임스탭 2
    [Header("배경")]
    [Header("*** Game Step 2 ***")]
    [Space(20)]
    [SerializeField]
    private Transform _gameStep2_BackGround;
    public Transform GameStep2_BackGround { get { return _gameStep2_BackGround; } }

    [Header("배경효과")]
    [SerializeField]
    private Transform _gameStep2_BackGroundEffect;
    public Transform GameStep2_BackGroundEffect { get { return _gameStep2_BackGroundEffect; } }

    [Header("배경빛")]
    [SerializeField]
    private SpriteRenderer _gameStep2_BackGroundLight;
    public SpriteRenderer GameStep2_BackGroundLight { get { return _gameStep2_BackGroundLight; } }

    [Header("캐릭터")]
    [SerializeField]
    private SkeletonAnimation _gameStep2_characterAnimation;
    public SkeletonAnimation GameStep2_characterAnimation { get { return _gameStep2_characterAnimation; } }

    [Header("버튼")]
    [SerializeField]
    private GameObject _gameStep2_buttonUpAndDown;

    //캐릭터 track entry
    private TrackEntry _trackEntry;

    //게이지
    private Image _gauge;

    //게이지 액션 트윈
    private Tween _gaugeAction;


    const float BACKGROUND_LOOP_TIME = 3f;
    const float GAME_COMPLETE_WAIT_TIME = 3f;
    //const float CHARACTER_ANIMATION_LOOP_TIME = 3.667f;
    #endregion

    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(CoNextStep());
    }

    #region 버튼 누르는 액션

    #region 게임 1,2 공통
   
    public void ButtonUpComplete()
    {
        StartCoroutine(CoButtonUpComplete());
    }

    IEnumerator CoButtonUpComplete()
    {
        switch ((EGameStep)_gameStep)
        {
            case EGameStep.EmergencyAndPressButton:
                {
                    _gameList[(int)EGameStep.EmergencyAndPressButton].GetComponent<ObjectAction>().StartAction();
                    _gameStep1_buttonUpAndDown.transform.GetChild((int)EButton.Pressed).gameObject.SetActive(false);
                    _gameStep1_buttonUpAndDown.transform.GetChild((int)EButton.Up).gameObject.SetActive(true);
                    yield return new WaitForSeconds(0.25f);
                    if(GameManager.currentGameKind == GameManager.EGameKind.BatMan)
                    {
                        //감옥 틀
                        _gameList[(int)EGameStep.EmergencyAndPressButton].transform.GetChild(2).GetChild(6).transform.DOMoveX(1390, 2);
                        yield return new WaitForSeconds(1f);
                        _gameList[(int)EGameStep.EmergencyAndPressButton].transform.GetChild(2).GetChild(4).gameObject.SetActive(false);
                        _gameList[(int)EGameStep.EmergencyAndPressButton].transform.GetChild(5).GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", true);
                        _gameList[(int)EGameStep.EmergencyAndPressButton].transform.GetChild(5).DOScale(1.2f, 6);
                    }
                    _gameStep1_characterAnimation.state.SetAnimation(0, "2", true);
                    yield return new WaitForSeconds(4.0f);
                    StartCoroutine(CoNextStep());
                    yield return new WaitForSeconds(2.0f);
                }
                break;

            case EGameStep.CharacterTransform:
                {
                    _gameStep2_buttonUpAndDown.transform.GetChild((int)EButton.Up).gameObject.SetActive(true);
                    _gameStep2_buttonUpAndDown.transform.GetChild((int)EButton.Pressed).gameObject.SetActive(false);
                    _gameStep2_characterAnimation.state.SetAnimation(0, "4", true);
                    yield return new WaitForSeconds(0.5f);
                    _gameStep2_buttonUpAndDown.transform.GetChild((int)EButton.Up).GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
                    _gameList[_gameStep].GetComponent<GameGauge>().GaugeDestroy(true);
                    yield return new WaitForSeconds(GAME_COMPLETE_WAIT_TIME);
                    GameManager.GameEndCallBackEvent.Invoke();
                }
                break;

            default:
                break;
        }
    }

    public void ButtonPressed()
    {
        switch ((EGameStep)_gameStep)
        {
            case EGameStep.EmergencyAndPressButton:
                {
                    _gameList[_gameStep].GetComponent<HintImageAction>().StopAction();
                    _gameStep1_buttonUpAndDown.transform.GetChild((int)EButton.Up).gameObject.SetActive(false);
                    _gameStep1_buttonUpAndDown.transform.GetChild((int)EButton.Pressed).gameObject.SetActive(true);
                }
                break;

            case EGameStep.CharacterTransform:
                {
                    _gameStep2_buttonUpAndDown.transform.GetChild((int)EButton.Up).gameObject.SetActive(false);
                    _gameStep2_buttonUpAndDown.transform.GetChild((int)EButton.Pressed).gameObject.SetActive(true);
                    TrackEntry trackEntry = _gameStep2_characterAnimation.GetComponent<SpineAnimation>().UnReverseAnimation("2");
                    if(_gaugeAction == null)
                    {
                        _gaugeAction = _gauge.DOFillAmount(1, trackEntry.Animation.Duration).OnComplete(()=> {
                            _gameStep2_buttonUpAndDown.GetComponent<UserInputGestureAction>().StopAllCoroutines();
                            Destroy(_gameStep2_buttonUpAndDown.GetComponent<UserInputGestureAction>());
                            ButtonUpComplete();
                        });
                    }
                    else
                    {
                        _gaugeAction.PlayForward();
                    }
                }
                break;

            default:
                break;
        }

    }
    #endregion

    #region 게임 2에만 사용
    public void ButtonPressFail()
    {
        _gameStep2_buttonUpAndDown.transform.GetChild((int)EButton.Up).gameObject.SetActive(true);
        _gameStep2_buttonUpAndDown.transform.GetChild((int)EButton.Pressed).gameObject.SetActive(false);
        _gameStep2_characterAnimation.GetComponent<SpineAnimation>().ReverseAnimation();
        _gaugeAction.PlayBackwards();
    }
    #endregion

    #endregion

    IEnumerator CoNextStep()
    {
        _gameStep++;
        switch ((EGameStep)_gameStep)
        {
            case EGameStep.EmergencyAndPressButton:
                {
                    _gameList[_gameStep].AddComponent<HintImageAction>().CreateHint("arrow_2", HintImageAction.EType.JumpAction, _gameStep1_buttonUpAndDown.transform.GetChild((int)EButton.Pressed).transform.localPosition + new Vector3(0, 110), 1, 0, false, 60);
                    _gameList[_gameStep].GetComponent<HintImageAction>().StartAction();
                }
                break;

            case EGameStep.CharacterTransform:
                {
                    
                    gameObject.AddComponent<TransitionImageAction>().CreateTransitionImage("circle", TransitionImageAction.EType.ZoomInOut, Util.GetInstance.GetDesignSize() * 0.5f, 0.75f, 0);
                    gameObject.GetComponent<TransitionImageAction>().StartAction();
                    yield return new WaitForSeconds(0.75f);
                    _gameList[(int)EGameStep.EmergencyAndPressButton].SetActive(false);
                    _gameList[(int)EGameStep.CharacterTransform].SetActive(true);

                    _gauge = _gameList[_gameStep].GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
                    yield return new WaitForSeconds(0.75f);
                    _gameStep2_BackGround.GetComponent<LoopImageAction>().StartAction();
                    _gameStep2_BackGroundEffect.GetComponent<LoopImageAction>().StartAction();
                    
                }
                break;
            default:
                break;
        }
        yield return null;
    }
}
