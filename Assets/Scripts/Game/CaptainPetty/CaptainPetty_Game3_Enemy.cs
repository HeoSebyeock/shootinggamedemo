﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.Events;
using Spine.Unity;

public class CaptainPetty_Game3_Enemy : MonoBehaviour
{
    enum EAttackArm
    {
        Left,
        Right,
        All,
        Size,
    }

    enum EEnemyAnimation
    {
        Idle = 1,
        Attacked,
        Happy,
        Dead,
    }

    private Sequence _moveActionSequence;

    private const float ENEMY_START_POSX = 1062.0f;
    private const float ENEMY_BOUNDARY_HEIGHT = 150;

    [Header("움직이는데 걸리는 시간")]
    [SerializeField]
    private float _moveDuringTime;

    [Header("레이저 쏘기까지 걸리는 시간")]
    [SerializeField]
    private float _laiserDelayTime;

    [Header("레이저 움직이는 시간")]
    [SerializeField]
    private float _laiserTime;

    [Header("게임 완료에 필요한 맞은 횟수")]
    [SerializeField]
    private float _completeHitCount;

    [Header("게임에 등장하는 훈장 개수")]
    [SerializeField]
    private int _superItemCount;

    [Header("피격 당했을 경우 나오는 이펙트 부모 트랜스폼")]
    [SerializeField]
    private Transform _laiserEffectParentTransform;

    [Header("폭발 이펙트")]
    [SerializeField]
    private SkeletonAnimation _explosionEffect;

    [Header("악당 레이저 부모 트랜스폼")]
    [SerializeField]
    private Transform _laiserParentTransform;

    [Header("악당이 죽으면 호출할 함수")]
    [SerializeField]
    private UnityEvent _enemyDeadCallBackEvent;

    private Image _gaugeBar;
    private float _currentHitCount = 1;

    private bool _isPettySuperItem = false;

    private void OnEnable()
    {
        _gaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
        _gaugeBar.fillAmount = 1;
        transform.DOLocalMoveX(ENEMY_START_POSX, 1).SetEase(Ease.OutCubic).OnComplete(()=> {
            if(_superItemCount == 2)
            {
                GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EEnemyAnimation.Idle).ToString(), true);
            }
        });
        //StartCoroutine("CoMoveAction");
        MoveAction();
        StartCoroutine("CoAttackAction");
    }

    void MoveAction()
    {
        _moveActionSequence = DOTween.Sequence();
        _moveActionSequence.AppendCallback(() =>
        {
            transform.DOLocalMoveY(Util.GetInstance.GetDesignSize().y - ENEMY_BOUNDARY_HEIGHT, _moveDuringTime);
        });
        _moveActionSequence.AppendInterval(_moveDuringTime);
        _moveActionSequence.AppendCallback(() => {
            transform.DOLocalMoveY(ENEMY_BOUNDARY_HEIGHT, _moveDuringTime);
        });
        _moveActionSequence.AppendInterval(_moveDuringTime);
        _moveActionSequence.SetLoops(-1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.ActorAttack.ToString()))
        {
            //normal attack
            if (collision.name.Contains("Normal"))
            {
                _currentHitCount -= 1 / _completeHitCount;
                _gaugeBar.DOFillAmount(_currentHitCount, 0.2f);

                if (_gaugeBar.fillAmount <= 0.65f && _superItemCount != 0 && !_isPettySuperItem)
                {
                    _isPettySuperItem = true;
                    _superItemCount--;
                    GameObject superItem = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs.ToString() + "/" + Constants.EFolderLeafType.Game.ToString() + "/CaptainPetty3_SuperItem") as GameObject, _laiserParentTransform);
                    superItem.transform.localPosition = new Vector2(Util.GetInstance.GetDesignSize().x + 200, Random.Range(150, Util.GetInstance.GetDesignSize().y - 150));
                    superItem.transform.DOLocalMoveX(-200, 3).OnComplete(() => { superItem.SetActive(false); _isPettySuperItem = false; });
                }

                LaiserEffectVisible(collision.gameObject);
                collision.gameObject.SetActive(false);

            }
            //laiser attack
            else if (collision.name.Contains("Laiser"))
            {
                //StopCoroutine("CoLaiserAttacked");
                StartCoroutine("CoLaiserAttacked");
            }

            if (_currentHitCount <= 0)
            {
                EnemyDead();
            }
        }
    }

    private void LaiserEffectVisible(GameObject collisionObject = null)
    {
        GameObject laiserEffect = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs.ToString() + "/" + Constants.EFolderLeafType.Game.ToString() + "/CaptainPetty_Laiser_Effect") as GameObject, _laiserEffectParentTransform);
        if(collisionObject == null)
        {
            laiserEffect.transform.localPosition = transform.localPosition;
        }
        else
        {
            laiserEffect.transform.localPosition = collisionObject.transform.localPosition;
        }
        laiserEffect.transform.localScale = new Vector2(0.5f, 0.5f);
        laiserEffect.transform.GetChild(0).gameObject.SetActive(true);
        StartCoroutine(CoLaiserEffect(laiserEffect));
    }

    private void EnemyDead()
    {
        foreach (Transform laiser in _laiserParentTransform)
        {
            Destroy(laiser.gameObject);
        }
        //complete game
        GetComponent<BoxCollider2D>().enabled = false;
        transform.DOKill();
        _moveActionSequence.Kill();
        GetComponent<GameGauge>().GaugeDestroy(true);
        StopCoroutine("CoAttackAction");
        StopCoroutine("CoMissileAttack");
        StartCoroutine("CoDeadAction");
        _enemyDeadCallBackEvent.Invoke();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.ActorAttack.ToString()) && collision.name.Contains("Laiser"))
        {
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EEnemyAnimation.Idle).ToString(), true);
            StartCoroutine("CoAttackAction");
            StopCoroutine("CoLaiserEffect");
            StopCoroutine("CoLaiserAttacked");
        }
    }

    //petty laiser
    IEnumerator CoLaiserAttacked()
    {
        while (true)
        {
            StopCoroutine("CoAttackAction");
            yield return new WaitForSeconds(0.1f);
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EEnemyAnimation.Attacked).ToString(), true);
            _currentHitCount -= 1 / _completeHitCount;
            _gaugeBar.DOFillAmount(_currentHitCount, 0.2f);
            LaiserEffectVisible();
            if (_currentHitCount <= 0)
            {
                StopCoroutine("CoLaiserAttacked");
                EnemyDead();
            }
        }
    }

    IEnumerator CoAttackAction()
    {
        yield return new WaitForSeconds(0.5f);
        StopCoroutine("CoMissileAttack");
        StartCoroutine("CoMissileAttack");
        while (true)
        {
            yield return new WaitForSeconds(_laiserDelayTime);
            GameObject laiser = GetComponent<ObjectPooling>().GetObject(_laiserParentTransform);
            int rand = Random.Range(0, (int)EAttackArm.Size);
            switch (rand)
            {
                case (int)EAttackArm.Left:
                    {
                        laiser.transform.localPosition = transform.localPosition + new Vector3(-100, -60);
                    }
                    break;
                case (int)EAttackArm.Right:
                    {
                        laiser.transform.localPosition = transform.localPosition + new Vector3(10, -60);
                    }
                    break;
                case (int)EAttackArm.All:
                    {
                        laiser.transform.localPosition = transform.localPosition + new Vector3(-100, -60);
                        GameObject laiser2 = GetComponent<ObjectPooling>().GetObject(_laiserParentTransform);
                        laiser2.transform.localPosition = transform.localPosition + new Vector3(10, -60);
                        laiser2.transform.DOLocalMoveX(-Util.GetInstance.GetDesignSize().x + 110, _laiserTime);
                    }
                    break;
                default:
                    break;
            }
            
            laiser.transform.DOLocalMoveX(-Util.GetInstance.GetDesignSize().x, _laiserTime);
        }
    }

    IEnumerator CoMissileAttack()
    {
        while (true)
        {
            yield return new WaitForSeconds(3f);
            StopCoroutine("CoAttackAction");
            transform.DOPause();
            _moveActionSequence.Pause();
            GetComponent<SkeletonAnimation>().state.ClearTracks();
            GetComponent<SkeletonAnimation>().skeleton.SetToSetupPose();
            transform.DOScaleY(0.8f, 0.25f).SetEase(Ease.Flash);
            yield return new WaitForSeconds(0.5f);
            transform.DOScaleY(1.2f, 0.2f).SetEase(Ease.InCubic);
            yield return new WaitForSeconds(0.2f);
            GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EEnemyAnimation.Idle).ToString(), true);
            transform.DOScaleY(1f, 0.25f);
            GameObject missile = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs.ToString() + "/" + Constants.EFolderLeafType.Game.ToString() + "/CaptainPetty_EnemyMissile" + gameObject.name.ToString()) as GameObject, _laiserParentTransform);
            missile.transform.localPosition = transform.localPosition + new Vector3(-100, -60);
            missile.SetActive(true);
            missile.transform.DOLocalMoveX(-Util.GetInstance.GetDesignSize().x, _laiserTime);
            //yield return new WaitForSeconds(_laiserTime * 2f);
            StopCoroutine("CoAttackAction");
            StartCoroutine("CoAttackAction");
            _moveActionSequence.Play();
            transform.DOPlay();
        }
    }

    IEnumerator CoLaiserEffect(GameObject laiserEffect)
    {
        yield return new WaitForSeconds(0.1f);
        laiserEffect.transform.GetChild(0).gameObject.SetActive(false);
        laiserEffect.transform.GetChild(1).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        Destroy(laiserEffect);
    }

    IEnumerator CoDeadAction()
    {
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EEnemyAnimation.Dead).ToString(), true);
        yield return new WaitForSeconds(1);
        _explosionEffect.transform.localPosition = transform.localPosition;
        _explosionEffect.state.ClearTracks();
        _explosionEffect.skeleton.SetToSetupPose();
        _explosionEffect.state.SetAnimation(0, "1", false);
        gameObject.SetActive(false);
    }
}
