﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;

public class CaptainPetty_Game2 : MonoBehaviour
{
    enum EPettyAnimation
    {
        Idle = 1,
        Fly,
        Attack,
        Happy,
        Scared,
    }

    enum EPlanetStep
    {
        Normal,
        Broken,
        Enemy,
    }

    [Header("패티")]
    [SerializeField]
    private SkeletonAnimation _pettySkeletonAnimation;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("패티 이동 위치")]
    [SerializeField]
    private Vector2[] _pettyMoveNextPlanetPositions;

    [Header("부서지는 이펙트 위치")]
    [SerializeField]
    private Vector2[] _brokenEffectPositions;

    [Header("폭발 이펙트 위치")]
    [SerializeField]
    private Vector2[] _explosionEffectPositions;

    [Header("스케일 오브젝트")]
    [SerializeField]
    private Transform _scaleLoopParentTransform;

    [Header("행성 부모 트랜스폼")]
    [SerializeField]
    private Transform _planetParentTransform;

    [Header("반짝이는 오브젝트 부모 트랜스폼")]
    [SerializeField]
    private Transform _starParentTransform;

    [Header("폭발 이펙트")]
    [SerializeField]
    private GameObject _explosionEffect;

    [Header("부서지는 이펙트")]
    [SerializeField]
    private GameObject _brokenEffect;

    [Header("다리 부모 트랜스폼")]
    [SerializeField]
    private Transform _bridgeParentTransform;

    [Header("빛 파티클")]
    [SerializeField]
    private GameObject _lightObj;

    [Header("슈팅 게임")]
    [SerializeField]
    private GameObject _shootingGame;

    private int _currentGameStep = 0;
    private int _currentTapCount = 0;

    private int[] _transPlanetCount = new int[]{
        1,2,4,5,7,8
    };

    private int[] _completePlanetCount = new int[]{
        3,5,7,9,12,15
    };

    private float[] _transPlanetScale = new float[] {
        0.5f,0.55f,0.65f,0.7f,0.85f,1f
    };

    private const int END_GAME_STEP = 6;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoGameLoop");
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            _clickParticle.StartAction(Input.mousePosition);

        }
    }

    IEnumerator CoGameLoop()
    {
        Util.GetInstance.IsNotTouched = true;
        StartCoroutine("CoStarBlingAction");
        StartCoroutine("CoScaleLoopObject");
        yield return new WaitForSeconds(1.5f);
        _pettySkeletonAnimation.transform.DOLocalMove(new Vector2(142, 219), 1.5f).SetEase(Ease.OutBack);
        _bridgeParentTransform.transform.GetChild(_currentGameStep).GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
        yield return new WaitForSeconds(1.5f);
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Idle).ToString(), true);
        //yield return new WaitForSeconds(0.5f);
        StartCoroutine("CoMoveNextPlanet");
    }

    IEnumerator CoScaleLoopObject()
    {
        while (true)
        {
            _scaleLoopParentTransform.DOLocalMoveY(15, 2.5f).SetRelative();
            yield return new WaitForSeconds(2.5f);
            _scaleLoopParentTransform.DOLocalMoveY(-15, 2.5f).SetRelative();
            yield return new WaitForSeconds(2.5f);
        }
    }

    IEnumerator CoMoveNextPlanet()
    {
        //yield return new WaitForSeconds(0.5f);
        _currentTapCount = 0;
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Fly).ToString(), true);
        _pettySkeletonAnimation.transform.DOLocalMove(_pettyMoveNextPlanetPositions[_currentGameStep], 1);
        
        yield return new WaitForSeconds(1);
        StartCoroutine("CoPlanetScaleAction");
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Idle).ToString(), true);
        _planetParentTransform.GetChild(_currentGameStep).GetComponent<CircleCollider2D>().enabled = true;
        StartCoroutine("CoPlanetScaleAction");
        //_planetParentTransform.GetChild(_currentGameStep).GetComponent<HintImageAction>().StartAction();
        Util.GetInstance.IsNotTouched = false;
    }

    IEnumerator CoPlanetScaleAction()
    {
        while (true)
        {
            _planetParentTransform.GetChild(_currentGameStep).transform.DOScale(1.075f, 0.35f);
            yield return new WaitForSeconds(0.35f);
            _planetParentTransform.GetChild(_currentGameStep).transform.DOScale(1f, 0.35f);
            yield return new WaitForSeconds(0.35f);
        }
    }

    IEnumerator CoAttackAction()
    {
        Util.GetInstance.IsNotTouched = true;
        _pettySkeletonAnimation.state.ClearTracks();
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Attack).ToString(), false);
        _pettySkeletonAnimation.state.AddAnimation(0, ((int)EPettyAnimation.Idle).ToString(), true, 0);
        yield return new WaitForSeconds(0.167f);
        Util.GetInstance.IsNotTouched = false;
    }

    IEnumerator CoStarBlingAction()
    {
        foreach (Transform star in _starParentTransform)
        {
            star.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", true);
            yield return new WaitForSeconds(0.2f);
        }
    }

    public void PlanetTap()
    {
        StopCoroutine("CoPlanetTap");
        StopCoroutine("CoAttackAction");
        StartCoroutine("CoPlanetTap");
        StartCoroutine("CoAttackAction");
    }

    IEnumerator CoPlanetTap()
    {
        //_planetParentTransform.GetChild(_currentGameStep).GetComponent<HintImageAction>().StopAction();
        StopCoroutine("CoPlanetScaleAction");
        yield return new WaitForSeconds(0.05f);
        _currentTapCount++;

        _planetParentTransform.GetChild(_currentGameStep).transform.DOKill();
        _planetParentTransform.GetChild(_currentGameStep).transform.DOShakePosition(0.75f, 20f, 10, 90);


        if (_currentTapCount == _completePlanetCount[_currentGameStep])
        {
            Util.GetInstance.IsNotTouched = true;
            StopCoroutine("CoAttackAction");
            StopCoroutine("CoPlanetScaleAction");
            StartCoroutine("CoExplosionPlanet");
            yield break;
        }
        else if (_currentTapCount == _transPlanetCount[_currentGameStep])
        {
            //행성 변함
            _planetParentTransform.GetChild(_currentGameStep).GetChild((int)EPlanetStep.Normal).gameObject.SetActive(false);
            _planetParentTransform.GetChild(_currentGameStep).GetChild((int)EPlanetStep.Broken).gameObject.SetActive(true);
            _brokenEffect.transform.localPosition = _brokenEffectPositions[_currentGameStep];
            _brokenEffect.transform.localScale = new Vector2(_transPlanetScale[_currentGameStep], _transPlanetScale[_currentGameStep]);
            _brokenEffect.GetComponent<SkeletonAnimation>().state.SetAnimation(0, (_currentGameStep + 1).ToString(), false);
        }
        yield return new WaitForSeconds(2);
        StartCoroutine("CoPlanetScaleAction");
        //_planetParentTransform.GetChild(_currentGameStep).GetComponent<HintImageAction>().StartAction();
    }

    IEnumerator CoExplosionPlanet()
    {
        
        _explosionEffect.transform.localPosition = _explosionEffectPositions[_currentGameStep];
        _explosionEffect.transform.localScale = new Vector2(_transPlanetScale[_currentGameStep], _transPlanetScale[_currentGameStep]);
        _currentGameStep++;
        if (_currentGameStep != END_GAME_STEP)
        {
            _bridgeParentTransform.transform.GetChild(_currentGameStep).GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
        }
        _planetParentTransform.GetChild(_currentGameStep - 1).GetChild((int)EPlanetStep.Enemy).gameObject.SetActive(true);
        _explosionEffect.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", false);

        _planetParentTransform.GetChild(_currentGameStep - 1).GetChild((int)EPlanetStep.Broken).gameObject.SetActive(false);
        
        yield return new WaitForSeconds(1f);

        if(_currentGameStep == END_GAME_STEP)
        {
            StartCoroutine("CoCompleteGame");
        }
        else
        {
            _planetParentTransform.GetChild(_currentGameStep - 1).GetChild((int)EPlanetStep.Enemy).gameObject.GetComponent<DOTweenPath>().DOPlay();
            //yield return new WaitForSeconds(1f);
            //yield return new WaitForSeconds(0.5f);

            StartCoroutine("CoMoveNextPlanet");
        }
    }

    IEnumerator CoCompleteGame()
    {
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Happy).ToString(), true);
        //_planetParentTransform.GetChild(_currentGameStep - 1).GetChild((int)EPlanetStep.Enemy).GetComponent<SkeletonAnimation>().state.ClearTracks();
        //_planetParentTransform.GetChild(_currentGameStep - 1).GetChild((int)EPlanetStep.Enemy).GetComponent<SkeletonAnimation>().skeleton.SetToSetupPose();
        _planetParentTransform.GetChild(_currentGameStep - 1).GetChild((int)EPlanetStep.Enemy).GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", true);
        yield return new WaitForSeconds(1);
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Scared).ToString(), true);
        //complete action
        //_lightObj.SetActive(true);
        //_lightObj.GetComponent<SkeletonAnimation>().skeleton.A = 0f;
        //_lightObj.GetComponent<SpineAnimation>().Fade(_lightObj.GetComponent<SkeletonAnimation>(), 1, 0.5f);
        //yield return new WaitForSeconds(1);
        //_lightObj.GetComponent<SpineAnimation>().Fade(_lightObj.GetComponent<SkeletonAnimation>(), 0, 0.5f);

        //yield return new WaitForSeconds(1);
        _planetParentTransform.GetChild(_currentGameStep - 1).DOLocalMove(new Vector2(Util.GetInstance.GetDesignSize().x * 0.5f, Util.GetInstance.GetDesignSize().y * 0.5f) + new Vector2(175, 0), 1.5f);
        _pettySkeletonAnimation.transform.DOLocalMove(new Vector2(Util.GetInstance.GetDesignSize().x * 0.5f, Util.GetInstance.GetDesignSize().y * 0.5f), 1.5f);
        yield return new WaitForSeconds(1.5f);
        _planetParentTransform.GetChild(_currentGameStep - 1).GetChild((int)EPlanetStep.Enemy).GetComponent<SkeletonAnimation>().state.SetAnimation(0, "2", false);
        yield return new WaitForSeconds(1f);
        _planetParentTransform.GetChild(_currentGameStep - 1).DOScaleY(0.9f, 0.25f).SetEase(Ease.Flash);
        _planetParentTransform.GetChild(_currentGameStep - 1).DOLocalMoveY(-25, 0.25f).SetRelative();
        yield return new WaitForSeconds(0.65f);
        _planetParentTransform.GetChild(_currentGameStep - 1).DOScaleY(1.1f, 0.2f);
        _planetParentTransform.GetChild(_currentGameStep - 1).DOLocalMove(new Vector2(200f,750f), 0.35f).SetRelative();
        yield return new WaitForSeconds(0.75f);

        _pettySkeletonAnimation.state.ClearTracks();
        _pettySkeletonAnimation.skeleton.SetToSetupPose();
        _pettySkeletonAnimation.transform.DOScaleY(0.9f, 0.25f).SetEase(Ease.Flash);
        _pettySkeletonAnimation.transform.DOLocalMoveY(-25, 0.25f).SetRelative();
        yield return new WaitForSeconds(0.25f);
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Fly).ToString(), true);
        yield return new WaitForSeconds(0.55f);
        _pettySkeletonAnimation.transform.DOScaleY(1.1f, 0.2f);
        _pettySkeletonAnimation.transform.DOLocalMove(new Vector2(200, 750f), 0.35f).SetRelative();
        yield return new WaitForSeconds(0.2f);
        gameObject.transform.parent.gameObject.AddComponent<TransitionImageAction>().CreateTransitionImage("circle", TransitionImageAction.EType.ZoomInOut, new Vector2(Util.GetInstance.GetDesignSize().x * 0.5f, Util.GetInstance.GetDesignSize().y * 0.5f), 1, 0);
        gameObject.transform.parent.gameObject.GetComponent<TransitionImageAction>().StartAction();
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);
        _shootingGame.SetActive(true);
    }
}
