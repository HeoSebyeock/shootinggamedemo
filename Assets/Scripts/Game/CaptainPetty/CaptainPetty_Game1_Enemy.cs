﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using DG.Tweening;

public class CaptainPetty_Game1_Enemy : MonoBehaviour
{
    public enum EAnimation
    {
        Idle = 1,
        Happy,
        Dead,
    }
    
    private Transform _touchCollectAreaItemTransform;

    const float FILLAMOUNT_POWER = 30;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.Untagged.ToString()))
        {
            CaptainPetty_TouchCollectArea.touchCollectAreaTransform.DOPunchScale(new Vector2(0.25f,0.25f), 0.1f,0);
            GetComponent<BoxCollider2D>().enabled = false;
            if(CaptainPetty_Game1.isHintGuide == -1)
            {
                CaptainPetty_Game1.isHintGuide = 0;
            }
            CaptainPetty_Game1.currentHitCount += 1 / FILLAMOUNT_POWER;
            CaptainPetty_Game1.gaugeBar.DOFillAmount(CaptainPetty_Game1.currentHitCount, 0.25f);

            StartCoroutine("CoDeadAction");
        }
    }

    

    IEnumerator CoDeadAction()
    {
        transform.DOKill();
        transform.DOLocalJump(new Vector2(425, -350), 300, 1, 0.75f).SetEase(Ease.Flash).SetRelative().OnComplete(() => { transform.DOKill(); gameObject.SetActive(false); gameObject.GetComponent<MeshRenderer>().sortingOrder = 3; });
        transform.DORotate(new Vector3(0, 0, -360), 0.35f, DG.Tweening.RotateMode.WorldAxisAdd).SetLoops(-1);
        transform.GetComponent<MeshRenderer>().sortingOrder = 10;
        CaptainPetty_TouchCollectArea.touchCollectAreaTransform.gameObject.AddComponent<Particles>().CreateParticle(Particles.EType.Spread, "sharpBling", 25, 1.5f, 5, 0.75f);
        CaptainPetty_TouchCollectArea.touchCollectAreaTransform.gameObject.GetComponent<Particles>().StartAction(Vector2.zero);
        gameObject.GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EAnimation.Dead).ToString(), false);
        yield return new WaitForSeconds(0.5f);
        //gameObject.SetActive(false);
        CaptainPetty_Game1.isExist[int.Parse(gameObject.name)] = false;
    }
}
