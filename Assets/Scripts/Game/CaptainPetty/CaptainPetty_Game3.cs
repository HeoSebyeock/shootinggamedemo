﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;

public class CaptainPetty_Game3 : MonoBehaviour
{
    [Header("배경 부모 트랜스폼")]
    [SerializeField]
    private Transform _backGroundParentTransform;

    [Header("패티")]
    [SerializeField]
    private SkeletonAnimation _pettySkeletonAnimation;

    [Header("악당 부모 트랜스폼")]
    [SerializeField]
    private Transform _enemyParentTransform;

    [Header("배경 기타 오브젝트")]
    [SerializeField]
    private Transform[] _backGroundEtcObjs;

    [Header("반짝이는 오브젝트 부모 트랜스폼")]
    [SerializeField]
    private Transform _starParentTransform;

    private float[] _successCollectAreaPos = new float[] {100,750, 100, 720-100};
    private int _currentGameStep = 0;

    private const int ENEMY_COMPLETE_COUNT = 5;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoGameLoop");
    }

    IEnumerator CoGameLoop()
    {
        StartCoroutine("CoStarBlingAction");
        for (int i = 0; i < _backGroundEtcObjs.Length; i++)
        {
            StartCoroutine(CoScaleLoopObject(_backGroundEtcObjs[i]));
        }
        Util.GetInstance.IsNotTouched = true;
        _pettySkeletonAnimation.GetComponent<UserInputGestureAction>().SetSuccessCollectArea(_successCollectAreaPos);
        StartCoroutine("CoNextStep");
        yield return new WaitForSeconds(3);
        Util.GetInstance.IsNotTouched = false;
    }

    IEnumerator CoStarBlingAction()
    {
        foreach (Transform star in _starParentTransform)
        {
            star.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", true);
            yield return new WaitForSeconds(0.2f);
        }
    }

    IEnumerator CoScaleLoopObject(Transform ObjTransform)
    {
        while (true)
        {
            ObjTransform.DOLocalMoveY(25, 2.5f).SetRelative();
            yield return new WaitForSeconds(2.5f);
            ObjTransform.DOLocalMoveY(-25, 2.5f).SetRelative();
            yield return new WaitForSeconds(2.5f);
        }
    }


    IEnumerator CoNextStep()
    {
        yield return new WaitForSeconds(1.5f);
        _enemyParentTransform.GetChild(_currentGameStep).gameObject.SetActive(true);
        yield return new WaitForSeconds(0);
    }

    public void EnemyDeadCallBack()
    {
        StartCoroutine("CoEnemyDeadCallBack");
        
    }

    IEnumerator CoEnemyDeadCallBack()
    {
        
        _pettySkeletonAnimation.transform.GetChild(0).DOScaleX(0, 0);
        _currentGameStep++;
        if (_currentGameStep == ENEMY_COMPLETE_COUNT)
        {
            Util.GetInstance.IsNotTouched = true;
            _pettySkeletonAnimation.GetComponent<CaptainPetty_Game3_Actor>().EndGame();
            Time.timeScale = 0.25f;
            yield return new WaitForSeconds(0.55f);
            Time.timeScale = 1f;
            _pettySkeletonAnimation.state.SetAnimation(0, ((int)CaptainPetty_Game3_Actor.EPettyAnimation.Happy).ToString(),true);
            yield return new WaitForSeconds(3);
            GameManager.GameEndCallBackEvent.Invoke();
        }
        else
        {
            _pettySkeletonAnimation.GetComponent<CaptainPetty_Game3_Actor>().EnemyDeadAction();
            yield return new WaitForSeconds(1);
            StartCoroutine("CoNextStep");
        }
    }
}
