﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.UI;

public class CaptainPetty_Game1 : MonoBehaviour
{
    enum EPettyAnimation
    {
        Idle = 1,
        NormalAttack,
        Angry,
        Laiser,
        Happy,
    }

    enum EPlanetAnimation
    {
        Scared = 1,
        Happy,
    }

    enum EStage
    {
        Eazy,
        Normal,
        Hard,
        Complete,
    }

    [Header("패티")]
    [SerializeField]
    private SkeletonAnimation _pettySkeletonAnimation;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("행성 띠 트랜스폼")]
    [SerializeField]
    private Transform _ringTransform;

    [Header("행성")]
    [SerializeField]
    private SkeletonAnimation _planetSkeletonAnimation;

    [Header("빛 파티클")]
    [SerializeField]
    private SkeletonAnimation _lightObj;

    [Header("힌트 가이드")]
    [SerializeField]
    private GameObject _hintGuide;

    [Header("정답 영역 버튼")]
    [SerializeField]
    private GameObject _collectAreaButton;

    [Header("반짝이는 오브젝트 부모 트랜스폼")]
    [SerializeField]
    private Transform _starParentTransform;

    public static Image gaugeBar;
    private GameObject _attackCheckCollider;
    private EStage _currentStage = EStage.Eazy;
    private float _currentTimeScale = 1f;

    private Sequence _makeEnemySequence;

    public static bool isSuperItemGet = false;
    public static Transform superItemTransform;
    public static int isHintGuide = -1;

    public int currentObjectCount = 0;
    public static bool[] isExist = new bool[] { false,false,false,false,false,false};
    public static float currentHitCount;
    private bool[] isSuperItem = new bool[] { false,false};

    private const float ENEMY_MAKE_WAIT_FOR_SECOND = 0.8285f * VALANCE_SPEED;
    private const int MAX_OBJECT_COUNT = 6;
    private const float VALANCE_SPEED = 2.5f;
    private const int TOP_SORTING_ORDER = 10;
    private const float FILLAMOUNT_POWER = 30;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("CoGameLoop");
        //_currentStage = EStage.Hard;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            _clickParticle.StartAction(Input.mousePosition);

            StopCoroutine("CoPettyAttackAction");
            StartCoroutine("CoPettyAttackAction");
        }

        if (isSuperItemGet)
        {
            isSuperItemGet = false;
            StartCoroutine("CoLaiserAction");
        }

        if (isHintGuide == 0)
        {
            isHintGuide = 1;
            GuideComplete();
        }
        if(currentHitCount >= 1f && _currentStage == EStage.Hard)
        {
            _currentStage = EStage.Complete;
            StartCoroutine("CoCompleteGame");
        }

        //speed up
        else if (currentHitCount >= 0.5f && _currentStage == EStage.Normal)
        {
            _currentStage = EStage.Hard;
            _currentTimeScale = 1.7f;
            SequenceSetTimeScale(1.7f);
        }

        else if (currentHitCount >= 0.2f && _currentStage == EStage.Eazy)
        {
            _currentStage = EStage.Normal;
            _currentTimeScale = 1.35f;
            SequenceSetTimeScale(1.35f);
        }
    }

    IEnumerator CoCompleteGame()
    {
        Util.GetInstance.IsNotTouched = true;
        GetComponent<GameGauge>().GaugeDestroy(true);
        _makeEnemySequence.Kill();
        foreach (Transform enemyTransform in _ringTransform)
        {
            enemyTransform.GetComponent<SpineAnimation>().Fade(enemyTransform.GetComponent<SkeletonAnimation>(),0,0.5f);
        }
        yield return new WaitForSeconds(0.5f);
        foreach (Transform enemyTransform in _ringTransform)
        {
            Destroy(enemyTransform.gameObject);
        }

        _planetSkeletonAnimation.state.SetAnimation(0, ((int)EPlanetAnimation.Happy).ToString(), true);
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Happy).ToString(), true);
        
        _lightObj.gameObject.SetActive(true);
        _lightObj.skeleton.A = 0f;
        _lightObj.GetComponent<SpineAnimation>().Fade(_lightObj, 1, 0.5f);
        yield return new WaitForSeconds(3);
        _lightObj.GetComponent<SpineAnimation>().Fade(_lightObj, 0, 0.5f);
        //ending action
        _pettySkeletonAnimation.state.ClearTracks();
        _pettySkeletonAnimation.skeleton.SetToSetupPose();
        _pettySkeletonAnimation.transform.DOScaleY(0.85f, 0.25f).SetEase(Ease.Flash);
        _pettySkeletonAnimation.transform.DOLocalMoveY(-50, 0.25f).SetRelative();
        yield return new WaitForSeconds(0.5f);
        _pettySkeletonAnimation.transform.DOScaleY(1.1f, 0.2f);
        _pettySkeletonAnimation.transform.DOLocalMoveY(600, 0.2f).SetRelative();
        yield return new WaitForSeconds(0.2f);
        GameManager.GameEndCallBackEvent.Invoke();
    }

    IEnumerator CoLaiserAction()
    {
        Util.GetInstance.IsNotTouched = true;
        superItemTransform.gameObject.AddComponent<Particles>().CreateParticle(Particles.EType.YoYo, "06_petty", 40, 1.5f, 5, 0.75f);
        superItemTransform.gameObject.GetComponent<Particles>().StartAction(Vector2.zero);

        SequenceSetTimeScale(0);
        superItemTransform.DOKill();
        superItemTransform.GetComponent<MeshRenderer>().sortingOrder = TOP_SORTING_ORDER;
        superItemTransform.transform.DOLocalMove(new Vector2(-586,7), 1);
        superItemTransform.transform.DOScale(0.5f, 1);
        yield return new WaitForSeconds(1);
        superItemTransform.transform.DOScale(0.25f, 0.5f);
        superItemTransform.GetComponent<SpineAnimation>().Fade(superItemTransform.GetComponent<SkeletonAnimation>(), 0, 0.5f);
        yield return new WaitForSeconds(0.5f);
        Destroy(superItemTransform.gameObject);
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Laiser).ToString(), true);
        _ringTransform.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "2", true);
        _planetSkeletonAnimation.state.SetAnimation(0, ((int)EPlanetAnimation.Happy).ToString(), true);
        foreach (Transform enemy in _ringTransform)
        {
            if (!enemy.tag.Equals(Constants.ETag.SuperItem.ToString()))
            {
                enemy.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "3", true);
            }
        }
        yield return new WaitForSeconds(1f);

        currentHitCount += 4 / FILLAMOUNT_POWER;
        gaugeBar.DOFillAmount(CaptainPetty_Game1.currentHitCount, 0.25f * 5);

        foreach (Transform enemy in _ringTransform)
        {
            enemy.transform.DOKill();
            enemy.transform.DOLocalJump(new Vector2(125, -350), 300, 1, 1.15f).SetEase(Ease.Flash).SetRelative().OnComplete(() => { enemy.transform.DOKill(); enemy.gameObject.SetActive(false); });
            enemy.transform.DORotate(new Vector3(0, 0, -360), 0.35f, DG.Tweening.RotateMode.WorldAxisAdd).SetLoops(-1);

            CaptainPetty_TouchCollectArea.touchCollectAreaTransform.gameObject.AddComponent<Particles>().CreateParticle(Particles.EType.Spread, "1_a_2_1", 25, 1.5f, 5, 0.75f);
            CaptainPetty_TouchCollectArea.touchCollectAreaTransform.gameObject.GetComponent<Particles>().StartAction(Vector2.zero);
            
            enemy.gameObject.GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)CaptainPetty_Game1_Enemy.EAnimation.Dead).ToString(), false);
            //yield return new WaitForSeconds(0.5f);
            //gameObject.SetActive(false);
            //CaptainPetty_Game1.isExist[int.Parse(gameObject.name)] = false;
            //enemy.GetComponent<SpineAnimation>().Fade(enemy.GetComponent<SkeletonAnimation>(), 0, 0.5f);
        }
        
        yield return new WaitForSeconds(1.15f);

        //foreach (Transform enemy in _ringTransform)
        //{
        //    Destroy(enemy.gameObject);
        //}
        for (int i = 0; i < MAX_OBJECT_COUNT; i++)
        {
            isExist[i] = false;
        }
        _planetSkeletonAnimation.state.SetAnimation(0, ((int)EPlanetAnimation.Scared).ToString(), true);
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Idle).ToString(), true);
        _ringTransform.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", true);

        Util.GetInstance.IsNotTouched = false;
        SequenceSetTimeScale(_currentTimeScale);
    }
    //}

    IEnumerator CoGameLoop()
    {
        //yield return new WaitForSeconds(1.5f);
        Util.GetInstance.IsNotTouched = true;
        _attackCheckCollider = _pettySkeletonAnimation.transform.GetChild(0).gameObject;
        StartCoroutine("CoStarBlingAction");
        gaugeBar = GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
        yield return new WaitForSeconds(0.1f);
        MakeEnemy();
        yield return new WaitForSeconds(1.4f);
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.Angry).ToString(), true);
        yield return new WaitForSeconds(1f);
        _pettySkeletonAnimation.state.AddAnimation(0, ((int)EPettyAnimation.Idle).ToString(), true,0);
        //_pettySkeletonAnimation.transform.DOLocalMove(new Vector2(182, 355), 1);
        yield return new WaitForSeconds(1.32f * VALANCE_SPEED);
        yield return new WaitForSeconds(3.5f);
        _hintGuide.SetActive(true);
    }

    IEnumerator CoStarBlingAction()
    {
        foreach (Transform star in _starParentTransform)
        {
            star.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", true);
            yield return new WaitForSeconds(0.2f);
        }
    }

    void SequenceSetTimeScale(float timeScale)
    {
        foreach (Transform enemy in _ringTransform)
        {
            if (enemy.GetComponent<DOTweenPath>().GetTween() != null)
            {
                enemy.GetComponent<DOTweenPath>().GetTween().timeScale = timeScale;
                
            }
        }
        _makeEnemySequence.timeScale = timeScale;
    }

    public void GuideProgress()
    {
        SequenceSetTimeScale(0);
    }

    public void GuideComplete()
    {
        _hintGuide.SetActive(false);
        SequenceSetTimeScale(1);
        Util.GetInstance.IsNotTouched = false;
    }

    void MakeEnemy()
    {
        _makeEnemySequence = DOTween.Sequence();
        _makeEnemySequence.AppendCallback(() => {
            if (currentObjectCount >= MAX_OBJECT_COUNT)
            {
                currentObjectCount = 0;
            }
            
            if (!isExist[currentObjectCount])
            {
                isExist[currentObjectCount] = true;

                if ((_currentStage == EStage.Normal && !isSuperItem[(int)EStage.Normal - 1]) || (_currentStage == EStage.Hard && !isSuperItem[(int)EStage.Hard - 1]))
                {
                    isSuperItem[(int)_currentStage - 1] = true;
                    MakeSuperItem();
                }
                else
                {
                    GameObject enemy = GetComponent<ObjectPooling>().GetObject(_ringTransform);
                    enemy.name = currentObjectCount.ToString();
                    enemy.GetComponent<DOTweenPath>().GetTween().timeScale = _currentTimeScale;
                    enemy.GetComponent<DOTweenPath>().DOPlay();
                }
            }
            currentObjectCount++;
            _makeEnemySequence.timeScale = _currentTimeScale;
        });
        _makeEnemySequence.AppendInterval(ENEMY_MAKE_WAIT_FOR_SECOND);
        _makeEnemySequence.SetLoops(-1);
        
    }

    void MakeSuperItem()
    {
        GameObject superItem = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs.ToString() + "/" + Constants.EFolderLeafType.Game.ToString() + "/CaptainPetty_SuperItem") as GameObject, _ringTransform);
        superItem.name = currentObjectCount.ToString();
        superItem.GetComponent<DOTweenPath>().GetTween().timeScale = _currentTimeScale;
        superItem.GetComponent<DOTweenPath>().DOPlay();
    }

    IEnumerator CoPettyAttackAction()
    {
        //Util.GetInstance.IsNotTouched = true;
        _attackCheckCollider.SetActive(false);
        _pettySkeletonAnimation.state.ClearTracks();
        _pettySkeletonAnimation.skeleton.SetToSetupPose();
        _pettySkeletonAnimation.state.SetAnimation(0, ((int)EPettyAnimation.NormalAttack).ToString(), false);
        _pettySkeletonAnimation.state.AddAnimation(0, ((int)EPettyAnimation.Idle).ToString(), true,0);
        yield return new WaitForSeconds(0.333f * 0.75f);
        _attackCheckCollider.SetActive(true);
        yield return new WaitForSeconds(0.333f * 0.25f);
        //Util.GetInstance.IsNotTouched = false;
        _attackCheckCollider.SetActive(false);
    }

}
