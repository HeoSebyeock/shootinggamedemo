﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class CaptainPetty_SuperItem : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.Untagged.ToString()))
        {
            GetComponent<BoxCollider2D>().enabled = false;
            CaptainPetty_Game1.superItemTransform = transform;
            CaptainPetty_Game1.isSuperItemGet = true;
            
            
        }
    }
}
