﻿using UnityEngine;
using System.Collections;
using Spine.Unity;
using DG.Tweening;

public class CaptainPetty_Game3_Actor : MonoBehaviour
{
    public enum EPettyAnimation
    {
        Idle = 1,
        Hit,
        Happy,
        LaiserReady,
        LaiserPlay,
    }

    [Header("피격 당했을 경우 나오는 이펙트 부모 트랜스폼")]
    [SerializeField]
    private Transform _laiserEffectParentTransform;

    [Header("패티 레이저 부모 트랜스폼")]
    [SerializeField]
    private Transform _laiserParentTransform;

    [Header("적 미사일 맞을경우 이펙트")]
    [SerializeField]
    private SkeletonAnimation _missileEffect;

    private Vector3 _addLaizerPos = new Vector3(25, -50);

    private const int LAISER_COLLIDER = 0;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("CoStartAction");
    }

    IEnumerator CoStartAction()
    {
        yield return new WaitForSeconds(1.5f);
        transform.DOLocalMove(new Vector2(142, 360), 1.5f).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(1.5f);
        StartCoroutine("CoPettyAttackAction");
    }

    IEnumerator CoPettyAttackAction()
    {
        while (true)
        {
            GameObject laizer = GetComponent<ObjectPooling>().GetObject(_laiserParentTransform);
            laizer.transform.localPosition = transform.localPosition + _addLaizerPos;
            laizer.transform.DOLocalMoveX(Util.GetInstance.GetDesignSize().x + 100, 0.75f).SetRelative().OnComplete(() => {
                GetComponent<ObjectPooling>().ReturnObject(laizer);
            });
            yield return new WaitForSeconds(0.2f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.tag.Equals(Constants.ETag.Untagged.ToString()))
        {
            if (collision.name.Contains("SuperItem"))
            {
                //update 
                gameObject.tag = Constants.ETag.SuperItem.ToString();
                StopCoroutine("CoPettyAttackAction");
                StartCoroutine("CoPettyLaiserAction");
                collision.gameObject.SetActive(false);
                GetComponent<Particles>().StartAction(Vector2.zero);
                
            }

            else if (collision.tag.Equals(Constants.ETag.EnemyNormalLaiser.ToString()))
            {
                HitDefault();
                GameObject laiserEffect = Instantiate(Resources.Load(Constants.EFolderRootType.Prefabs.ToString() + "/" + Constants.EFolderLeafType.Game.ToString() + "/CaptainPetty_Laiser_Effect") as GameObject, _laiserEffectParentTransform);
                laiserEffect.transform.localPosition = collision.transform.localPosition + new Vector3(-50, 0);
                laiserEffect.transform.localScale = new Vector2(0.5f, 0.5f);
                laiserEffect.transform.GetChild(0).gameObject.SetActive(true);

                StartCoroutine(CoLaiserEffect(laiserEffect));
                collision.gameObject.SetActive(false);
                StartCoroutine(CoHitAction(3));
            }
            else if (collision.tag.Equals(Constants.ETag.EnemyMissile.ToString()))
            {
                HitDefault();
                collision.gameObject.SetActive(false);
                _missileEffect.transform.localPosition = transform.localPosition;
                _missileEffect.state.ClearTracks();
                _missileEffect.skeleton.SetToSetupPose();
                _missileEffect.state.SetAnimation(0, "1", false);
                StartCoroutine(CoHitAction(6));
            }
        }

        else if (gameObject.tag.Equals(Constants.ETag.SuperItem.ToString()) && (collision.tag.Equals(Constants.ETag.EnemyNormalLaiser.ToString()) || collision.tag.Equals(Constants.ETag.EnemyMissile.ToString())))
        {
            collision.gameObject.SetActive(false);
        }
    }

    IEnumerator CoPettyLaiserAction()
    {
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EPettyAnimation.LaiserReady).ToString(),false);
        GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)EPettyAnimation.LaiserPlay).ToString(), false,0);
        yield return new WaitForSeconds(1.15f);
        transform.GetChild(LAISER_COLLIDER).DOScaleX(1, 0.25f);
        yield return new WaitForSeconds(3.5f);
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EPettyAnimation.Idle).ToString(), true);
        StartCoroutine("CoPettyAttackAction");
        gameObject.tag = Constants.ETag.Untagged.ToString();
        transform.GetChild(LAISER_COLLIDER).DOScaleX(0, 0);
    }

    public void HitDefault()
    {
        //Util.GetInstance.IsNotTouched = true;
        gameObject.tag = Constants.ETag.HitLaiser.ToString();
        //GetComponent<UserInputGestureAction>().StopProgressEvent();
        StopCoroutine("CoPettyAttackAction");
    }

    IEnumerator CoLaiserEffect(GameObject laiserEffect)
    {
        yield return new WaitForSeconds(0.1f);
        laiserEffect.transform.GetChild(0).gameObject.SetActive(false);
        laiserEffect.transform.GetChild(1).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        Destroy(laiserEffect);
    }

    IEnumerator CoHitAction(int blinkCount)
    {
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EPettyAnimation.Hit).ToString(), false);
        yield return new WaitForSeconds(1);
        float waitTime = GetComponent<SpineAnimation>().BlinkAction(blinkCount);
        yield return new WaitForSeconds(waitTime * 0.5f);
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EPettyAnimation.Idle).ToString(), true);
        //Util.GetInstance.IsNotTouched = false;
        yield return new WaitForSeconds(waitTime * 0.5f);
        
        gameObject.tag = Constants.ETag.Untagged.ToString();
        StartCoroutine("CoPettyAttackAction");
        
    }

    public void EnemyDeadAction()
    {
        StopCoroutine("CoPettyLaiserAction");
        StopCoroutine("CoPettyAttackAction");
        StartCoroutine("CoPettyHappyAction");
    }

    public void EndGame()
    {
        foreach (Transform laiser in _laiserParentTransform)
        {
            laiser.gameObject.SetActive(false);
        }
        StopCoroutine("CoPettyLaiserAction");
        StopCoroutine("CoPettyAttackAction");
        GetComponent<UserInputGestureAction>().StopProgressEvent();
        
    }

    IEnumerator CoPettyHappyAction()
    {
        Util.GetInstance.IsNotTouched = true;
        gameObject.tag = Constants.ETag.HitLaiser.ToString();
        GetComponent<UserInputGestureAction>().StopProgressEvent();
        GetComponent<SkeletonAnimation>().state.SetAnimation(0, ((int)EPettyAnimation.Happy).ToString(), false);
        GetComponent<SkeletonAnimation>().state.AddAnimation(0, ((int)EPettyAnimation.Idle).ToString(), true,0);
        yield return new WaitForSeconds(1);
        Util.GetInstance.IsNotTouched = false;
        yield return new WaitForSeconds(1.5f);
        StopCoroutine("CoPettyAttackAction");
        StartCoroutine("CoPettyAttackAction");
        gameObject.tag = Constants.ETag.Untagged.ToString();
    }
}
