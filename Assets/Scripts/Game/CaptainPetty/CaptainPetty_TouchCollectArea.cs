﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CaptainPetty_TouchCollectArea : MonoBehaviour
{
    enum ECollectButton
    {
        Normal,
        CollectArea,
    }

    public static Transform touchCollectAreaTransform;

    // Start is called before the first frame update
    void Start()
    {
        touchCollectAreaTransform = transform;
        StartCoroutine("CoCollectAreaBounceAction");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.Enemy.ToString()) || collision.tag.Equals(Constants.ETag.SuperItem.ToString()))
        {
            collision.transform.DOScale(1.15f, 0.15f);
            //transform.DOKill();
            //transform.DORotate(new Vector3(0, 0, -360), 0.25f, DG.Tweening.RotateMode.WorldAxisAdd).SetLoops(-1);
            transform.GetChild((int)ECollectButton.Normal).gameObject.SetActive(false);
            transform.GetChild((int)ECollectButton.CollectArea).gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals(Constants.ETag.Enemy.ToString()) || collision.tag.Equals(Constants.ETag.SuperItem.ToString()))
        {
            collision.transform.DOScale(1f, 0.15f);
            transform.GetChild((int)ECollectButton.Normal).gameObject.SetActive(true);
            transform.GetChild((int)ECollectButton.CollectArea).gameObject.SetActive(false);
        }
    }

    IEnumerator CoCollectAreaBounceAction()
    {
        yield return new WaitForSeconds(3.5f);
        transform.DOScale(1f, 0.35f).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(0.525f);
        while (true)
        {
            transform.DOScale(new Vector2(1.1f, 1.1f), 0.375f);
            yield return new WaitForSeconds(0.375f);
            transform.DOScale(new Vector2(1f, 1f), 0.625f);
            yield return new WaitForSeconds(0.625f);
        }
    }
}
