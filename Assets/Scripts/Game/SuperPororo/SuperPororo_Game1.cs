﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.UI;

public class SuperPororo_Game1 : MonoBehaviour
{
    /// <summary>
    /// 각 단계별 충족 해야하는 태양 개수
    /// </summary>
    enum EGameStep
    {
        One,
        Two = 7,
        Three = 16,
        End = 27,
    }

    /// <summary>
    /// 태양의 종류
    /// </summary>
    enum ESunKind
    {
        SuccessSun,
        FailSun,
        BigSuccessSun,
    }

    /// <summary>
    /// 태양의 생성 x축 위치 
    /// </summary>
    enum ESunLocation
    {
        LeftToRight,
        RightToLeft,
        Size,
    }

    /// <summary>
    /// 태양의 생성 y축 위치
    /// </summary>
    enum ESunHeight
    {
        Down,
        Middle,
        Up,
        Size,
    }

    /// <summary>
    /// 태양 액션
    /// </summary>
    enum ESunActionData
    {
        SunScaleAction,
        SunTouchedScaleAction,
    }

    [Header("게임 단계")]
    [SerializeField]
    private int _gameStep;
    public int GameStep { get { return _gameStep; } }

    [Header("배경")]
    [SerializeField]
    private Transform _backGround;
    public Transform BackGround { get; }

    [Header("루프 배경")]
    [SerializeField]
    private GameObject[] _loopBackOne;
    [SerializeField]
    private GameObject[] _loopBackTwo;
    [SerializeField]
    private GameObject[] _loopBackThree;

    [Header("캐릭터")]
    [SerializeField]
    private GameObject _superPororoCharacter;
    public GameObject SuperPororoCharacter { get; }

    [Header("태양 오브젝트 풀링")]
    [SerializeField]
    private ObjectPooling _sunObjectPooling;


    [Header("태양 부모 트랜스폼")]
    [SerializeField]
    private Transform _sunParentTransform;

    [Header("큰 태양 움직임 포지션1")]
    [SerializeField]
    private Vector3[] _wavePositions;


    [Header("큰 태양 움직임 포지션2")]
    [SerializeField]
    private Vector3[] _tonadoPositions;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("성공 파티클")]
    [SerializeField]
    private Particles _successParticle;

    [Header("성공 파티클2")]
    [SerializeField]
    private Particles _successParticleBigSun;

    [Header("실패 파티클")]
    [SerializeField]
    private Particles _failParticle;

    [Header("전체 파티클")]
    [SerializeField]
    private Particles _fullParticle;

    [Header("캐릭터 엔딩 효과")]
    [SerializeField]
    private Particles _characterEndEffect;

    private LoopImageAction _backGroundLoopImageAction;
    private float[] _sunCreateYPos = new float[] {25f,155f,282f};
    //해당 y좌표에 sun이 존재하는가 안하는가 판단
    private bool[] _sunCreateYPosExist = new bool[] { false,false,false};
    private Image _characterGauge;
    private float _currentFillAmount;
    private int _sunCount;
    private int _bigSunTapCount;
    //다음 floor에 진입 할때 움직이는 액_floorTween
    private Tween _floorTween;

    // 큰 태양의 path 좌표
    private Dictionary<int, Vector3[]> _wayPointDictionary = new Dictionary<int, Vector3[]>();
    
    private const float BACKGROUND_LOOPTIME = 2.5f;
    private const float SUN_CREATE_LEFT_XPOS = -100f -640f;
    private const float SUN_CREATE_RIGHT_XPOS = 100f + 640f;
    private const float SUN_COMPLETE_COUNT = 27f;
    private const int BLINK_COUNT = 3;
    private const int BIG_SUN_TAP_COMPLETE_COUNT = 8;

    private void Start()
    {
        StartCoroutine("CoNextStep");
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0)){
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            _clickParticle.StartAction(Input.mousePosition);
        }
    }

    IEnumerator CoNextStep()
    {
        _superPororoCharacter.transform.DORotate(new Vector3(0, 0, 0), 1.0f);
        yield return new WaitForSeconds(3.5f);
        _characterGauge = _superPororoCharacter.GetComponent<GameGauge>().MakeAndInGaugeImageReturn();
        foreach (Transform item in _superPororoCharacter.transform)
        {
            item.localScale = new Vector2(0.8f,0.9f);
        }
        _backGroundLoopImageAction = _backGround.gameObject.AddComponent<LoopImageAction>();
        _backGroundLoopImageAction.CreateLoopImage(LoopImageAction.EType.UpToDownMove, BACKGROUND_LOOPTIME, _loopBackOne, Util.GetInstance.GetDesignSize() * 0.5f);
        _backGroundLoopImageAction.StartAction();
        _wayPointDictionary.Add(0, _wavePositions);
        _wayPointDictionary.Add(1, _tonadoPositions);

        StartCoroutine("CoMakeSun");
    }

    //태양 생성 및 액션
    IEnumerator CoMakeSun()
    {
        while (true)
        {
            ESunLocation sunLocation = (ESunLocation)Random.Range(0, (int)ESunLocation.Size);
            ESunHeight height = ESunHeight.Down;

            //ypos에 해당 오브젝트가 없으면 생성
            for (int i = 0; i < 1;)
            {
                height = (ESunHeight)Random.Range(0, (int)ESunHeight.Size);
                if (!_sunCreateYPosExist[(int)height])
                {
                    _sunCreateYPosExist[(int)height] = true;
                    i++;
                }
                yield return new WaitForSeconds(0.5f);
            }
            // pooling system
            GameObject sun = _sunObjectPooling.GetObject(_sunParentTransform);
            sun.name = height.ToString();

            //태양에 UserInputGestureAction Component 추가 (Tap,Collider)
            if (sun.GetComponent<UserInputGestureAction>() == null)
            {
                sun.AddComponent<UserInputGestureAction>().CreateInputGesture(UserInputGestureAction.EType.Tap, UserInputGestureAction.EJudgeWay.Collider, sun, (()=> { SunInitialize(sun, true); }));
            }

            switch (sunLocation)
            {
                case ESunLocation.LeftToRight:
                    {
                        sun.transform.localPosition = new Vector2(SUN_CREATE_LEFT_XPOS, _sunCreateYPos[(int)height]);
                        sun.transform.DOMoveX(SUN_CREATE_RIGHT_XPOS, Random.Range(BACKGROUND_LOOPTIME * 1.5f, BACKGROUND_LOOPTIME * 2f) * (1 - _sunCount * 0.02f)).OnComplete(()=> {
                            SunInitialize(sun);
                        });
                    }
                    break;
                case ESunLocation.RightToLeft:
                    {
                        sun.transform.localPosition = new Vector2(SUN_CREATE_RIGHT_XPOS, _sunCreateYPos[(int)height]);
                        sun.transform.DOMoveX(SUN_CREATE_LEFT_XPOS, Random.Range(BACKGROUND_LOOPTIME * 1.5f, BACKGROUND_LOOPTIME * 2.5f) * (1 - _sunCount * 0.02f)).OnComplete(()=> {
                            SunInitialize(sun);
                        });
                    }
                    break;
                default:
                    break;
            }
            sun.GetComponent<ObjectAction>().StartAction((int)ESunActionData.SunScaleAction);
            yield return null;
        }
    }

    void SunInitialize(GameObject sun,bool touched = false)
    {
        sun.transform.DOKill();
        sun.GetComponent<ObjectAction>().KillTween();
        sun.transform.localScale = Vector2.one;

        ESunHeight height = ESunHeight.Down;

        if(sun.name == ESunHeight.Middle.ToString())
        {
            height = ESunHeight.Middle;
        }
        else if(sun.name == ESunHeight.Up.ToString())
        {
            height = ESunHeight.Up;
        }
            _sunCreateYPosExist[(int)height] = false;

        if (touched)
        {
            StartCoroutine(CoSunTapAction(sun));
        }
        else
        {
            _sunObjectPooling.ReturnObject(sun);
        }
    }

    /// <summary>
    /// 태양 터치시 행동 함수
    /// </summary>
    /// <param name="sun"></param>
    /// <returns></returns>
    IEnumerator CoSunTapAction(GameObject sun)
    {
        sun.GetComponent<ObjectAction>().StartAction((int)ESunActionData.SunTouchedScaleAction);
        StartCoroutine(CoSunJudge(sun));
        yield return new WaitForSeconds(0.5f);
        sun.GetComponent<ObjectAction>().PauseTween();
        sun.transform.DOLocalJump(_superPororoCharacter.transform.localPosition - Util.GetInstance.GetDesignSize_Vector3() * 0.5f + new Vector3(0,50), 100, 1, 0.25f);
        yield return new WaitForSeconds(0.25f);
        sun.GetComponent<SpriteRenderer>().DOFade(0, 0.15f).OnComplete(()=> {
            //Destroy(sun);
            sun.GetComponent<ObjectAction>().RestartTween();
            sun.GetComponent<SpriteRenderer>().DOFade(1, 0);
            _sunObjectPooling.ReturnObject(sun);
        });
        
        
    }

    /// <summary>
    /// 터치한 태양이 어떤 태양인지 판단
    /// </summary>
    /// <param name="sun">터치한 태양 오브젝트</param>
    /// <returns></returns>
    IEnumerator CoSunJudge(GameObject sun)
    {
        if (sun.tag.Equals("SuccessSun"))
        {
            _successParticle.StartAction(sun.transform.localPosition + Util.GetInstance.GetDesignSize_Vector3() * 0.5f);
            yield return new WaitForSeconds(0.75f);

            StartCoroutine(CoFloorJudge());
            _currentFillAmount += 1 / SUN_COMPLETE_COUNT;
            _sunCount++;
            _superPororoCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "2", false);
            _superPororoCharacter.GetComponent<SkeletonAnimation>().state.AddAnimation(0, "1", true, 0);
            _characterGauge.DOFillAmount(_currentFillAmount, 0.25f);
        }
        else
        {
            _failParticle.StartAction(sun.transform.localPosition + Util.GetInstance.GetDesignSize_Vector3() * 0.5f);
            Util.GetInstance.IsNotTouched = true;
            yield return new WaitForSeconds(0.75f);
            _superPororoCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "4", false);
            _superPororoCharacter.GetComponent<SkeletonAnimation>().state.AddAnimation(0, "1", true, 0);
            if(_backGround.GetComponent<LoopImageAction>() != null)
            {
                _backGround.GetComponent<LoopImageAction>().LoopPause();
            }
            _floorTween.Pause();
            yield return new WaitForSeconds(1);
            float waitTime = _superPororoCharacter.GetComponent<SpineAnimation>().BlinkAction(BLINK_COUNT);
            yield return new WaitForSeconds(waitTime);
            _backGround.GetComponent<LoopImageAction>().LoopReStart();
            _floorTween.Play();
            Util.GetInstance.IsNotTouched = false;
        }

    }

    /// <summary>
    /// 맵 체크 7개 먹으면 2단계 floor 진입 / 14개 먹으면 3단계 floor 진입 / 20개 먹으면 end floor 진입
    /// </summary>
    /// <returns></returns>
    IEnumerator CoFloorJudge()
    {
        switch (_sunCount)
        {
            case (int)EGameStep.Two:
            case (int)EGameStep.Three:
                {
                    CreateBigSun();
                    _gameStep++;
                    int index = _backGround.GetComponent<LoopImageAction>().CurrentImageIndex();
                    _backGround.GetComponent<LoopImageAction>().LoopStop();
                    yield return new WaitUntil(() => _backGround.GetChild(index + (3 * (_gameStep - 1))).transform.localPosition.y == -720 * 0.5f);
                    
                    for (int i = 3 * (_gameStep - 1); i < _backGround.childCount; i++)
                    {
                        if (i >= 2 + (3 * (_gameStep - 1)))
                        {
                            _backGround.GetChild(i).localPosition -= new Vector3(0, Util.GetInstance.GetDesignSize().y);
                        }
                        _floorTween = _backGround.GetChild(i).DOLocalMoveY(-720 * 2, BACKGROUND_LOOPTIME * 2f).SetRelative();
                    }
                    yield return new WaitForSeconds(BACKGROUND_LOOPTIME * 2f);
                    Destroy(_backGround.GetComponent<LoopImageAction>());
                    _backGroundLoopImageAction = _backGround.gameObject.AddComponent<LoopImageAction>();

                    if(_gameStep == 1)
                    {
                        _backGroundLoopImageAction.CreateLoopImage(LoopImageAction.EType.UpToDownMove, BACKGROUND_LOOPTIME, _loopBackTwo, Util.GetInstance.GetDesignSize() * 0.5f);
                    }
                    else if(_gameStep == 2)
                    {
                        _backGroundLoopImageAction.CreateLoopImage(LoopImageAction.EType.UpToDownMove, BACKGROUND_LOOPTIME, _loopBackThree, Util.GetInstance.GetDesignSize() * 0.5f);
                    }
                    
                    _backGroundLoopImageAction.StartAction();
                }
                break;
            case (int)EGameStep.End:
                {
                    //Game clear
                    _fullParticle.StartAction(Util.GetInstance.GetDesignSize() * 0.5f);
                    StopCoroutine("CoMakeSun");
                    Destroy(_sunObjectPooling);
                    Util.GetInstance.IsNotTouched = true;
                    _superPororoCharacter.GetComponent<GameGauge>().GaugeDestroy(true);
                    for (int i = 0; i < _sunParentTransform.childCount; i++)
                    {
                        _sunParentTransform.GetChild(i).GetComponent<SpriteRenderer>().DOFade(0, 0.5f);
                    }

                    _gameStep++;

                    int index = _backGround.GetComponent<LoopImageAction>().CurrentImageIndex();
                    _backGround.GetComponent<LoopImageAction>().LoopStop();
                    yield return new WaitUntil(() => _backGround.GetChild(index + (3 * (_gameStep - 1))).transform.localPosition.y == -720 * 0.5f);

                    _superPororoCharacter.GetComponent<SpineAnimation>().SetToSetupPoseAndAnimation(_superPororoCharacter.GetComponent<SkeletonAnimation>(),"5");


                    Destroy(_backGround.GetComponent<LoopImageAction>());
                    for (int i = 3 * (_gameStep - 1); i < _backGround.childCount; i++)
                    {
                        _backGround.GetChild(i).DOLocalMoveY(-720 * 2, BACKGROUND_LOOPTIME * 1.5f).SetRelative();
                    }
                    _backGround.GetChild(_backGround.childCount - 1).localPosition -= new Vector3(0, Util.GetInstance.GetDesignSize().y);

                    yield return new WaitForSeconds(BACKGROUND_LOOPTIME * 1.5f);

                    _superPororoCharacter.transform.DOLocalMoveY(Util.GetInstance.GetDesignSize().y * 0.5f, 0.75f);
                    _superPororoCharacter.transform.DOScale(0, 0.75f);
                    yield return new WaitForSeconds(0.75f);
                    _characterEndEffect.StartAction(_superPororoCharacter.transform.localPosition);
                    GameManager.GameEndCallBackEvent.Invoke();
                }
                break;
            default:
                break;
        }
    }

    void CreateBigSun()
    {
        CreateFactory createFactory = new CreateFactory();
        GameObject bigSun = createFactory.CreateGameObject(Constants.EObjectType.Prefab, Constants.EFolderRootType.Prefabs, Constants.EFolderLeafType.Game, Constants.ETag.BigSuccessSun.ToString(), true, _sunParentTransform, 2, new Vector2(-Util.GetInstance.GetDesignSize().x, 0));
        bigSun.AddComponent<UserInputGestureAction>().CreateInputGesture(UserInputGestureAction.EType.Tap, UserInputGestureAction.EJudgeWay.Collider, bigSun, (() => { BigSunTap(bigSun); }));
        bigSun.GetComponent<UserInputGestureAction>()._isNotDestroy = true;
        int moveRand = Random.Range(0, _wayPointDictionary.Count);
        int isReverse = Random.Range(0, _wayPointDictionary.Count);

        if (isReverse == 1)
        {
            Util.GetInstance.Reverse(_wayPointDictionary[moveRand]);
            bigSun.transform.localPosition = new Vector2(Util.GetInstance.GetDesignSize().x, 0);
        }

        StopCoroutine("CoMakeSun");
        bigSun.transform.DOPath(_wayPointDictionary[moveRand], 8, PathType.CatmullRom, PathMode.TopDown2D).OnComplete(() => {
            RemakeSun(bigSun);
        });

        if(isReverse == 1)
        {
            Util.GetInstance.Reverse(_wayPointDictionary[moveRand]);
        }
    }

    void BigSunTap(GameObject bigSun)
    {
        _successParticleBigSun.StartAction(Input.mousePosition);
        bigSun.GetComponent<ObjectAction>().KillTween();
        bigSun.transform.DOScale(1, 0.25f);
        bigSun.GetComponent<ObjectAction>().StartAction(0);

        _bigSunTapCount++;

        if (_bigSunTapCount == BIG_SUN_TAP_COMPLETE_COUNT)
        {
            StartCoroutine(CoBigSunSuccess(bigSun));
            Destroy(bigSun.GetComponent<UserInputGestureAction>());
        }
    }

    IEnumerator CoBigSunSuccess(GameObject bigSun)
    {
        bigSun.transform.DOKill();
        bigSun.transform.DOLocalJump(_superPororoCharacter.transform.localPosition - Util.GetInstance.GetDesignSize_Vector3() * 0.5f + new Vector3(0,50), 100, 1, 0.75f);
        yield return new WaitForSeconds(0.75f);
        bigSun.GetComponent<SpriteRenderer>().DOFade(0, 0.15f).OnComplete(() => {
            RemakeSun(bigSun);
        });

        _superPororoCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "3", false);
        _superPororoCharacter.GetComponent<SkeletonAnimation>().state.AddAnimation(0, "1", true, 0);

        _currentFillAmount += 3 / SUN_COMPLETE_COUNT;
        _sunCount+= 3;
        _characterGauge.DOFillAmount(_currentFillAmount, 0.25f);
    }

    /// <summary>
    /// 큰 태양 지나간 이후 태양들 다시 생성
    /// </summary>
    /// <param name="bigSun"></param>
    void RemakeSun(GameObject bigSun)
    {
        _bigSunTapCount = 0;
        for (int i = 0; i < _sunCreateYPosExist.Length; i++)
        {
            _sunCreateYPosExist[i] = false;
        }
        StartCoroutine("CoMakeSun");
        
        Destroy(bigSun);
    }
}
