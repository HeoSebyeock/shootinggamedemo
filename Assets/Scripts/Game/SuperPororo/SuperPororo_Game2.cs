﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.UI;
using Spine;

public class SuperPororo_Game2 : MonoBehaviour
{
    /// <summary>
    /// 적 행성 단계 
    /// </summary>
    enum EGameStep
    {
        None,
        One,
        Two,
        Three,
        Size,
    }

    enum EEnemyAction
    {
        MoveToPororo,
    }

    /// <summary>
    /// 게임 상태 (주인공이 미는 상태와 밀리는 상태)
    /// </summary>
    enum EGameState
    {
        None,
        Back,
        Push,
    }

    /// <summary>
    /// 오브젝트 위치에 따른 행동
    /// </summary>
    enum ELocationStep
    {
        Left,
        Middle,
        Right,
        Laiser,
    }

    [Header("배경")]
    [SerializeField]
    private Transform _backGround;

    [Header("주인공 캐릭터")]
    [SerializeField]
    private GameObject _superPororoCharacter;

    [Header("적 캐릭터")]
    [SerializeField]
    private GameObject _enemyCharacter;

    [Header("지구")]
    [SerializeField]
    private GameObject _earth;

    [Header("레이저 가이드")]
    [SerializeField]
    private GameObject _laiserGuide;

    [Header("클릭 파티클")]
    [SerializeField]
    private Particles _clickParticle;

    [Header("성공 파티클")]
    [SerializeField]
    private Particles _successParticle;

    private EGameStep _currentGameStep = EGameStep.None;
    private ELocationStep _currentLocationStep = ELocationStep.Middle;
    private EGameState _currentGameState = EGameState.None;

    //게임 게이지
    private GameGauge _gauge;
    private Image _gaugeBar;
    private string[] _gaugeStrs = new string[] {
        "2_h_",
        "2_i_",
        "2_j_",
    };

    //터치 성공 했을 경우
    private bool _isInputSuccess;
    [Header("터치 영역 콜라이더")]
    [SerializeField]
    private GameObject _touchCollider;

    //게이지 액션 트윈
    private Tween _gaugeAction;
    //적 움직이는 시퀀스 액션
    private Sequence _enemyAction;

    private float _pressHintWaitTime = 0;
    private float _pressJudgeTime = 0;

    private const float POWER = 75f;
    private const float LIMIT_DISTANCE = 170f;
    private const float SCALE_POWER = 0.0005f;
    private const float WAIT_FOR_PRESS_TIME = 5;


    // Use this for initialization
    void Start()
    {
        Util.GetInstance.IsNotTouched = true;
        StartCoroutine("CoEnemyAction");
        StartCoroutine("CoNextStep");
        
    }

    // Update is called once per frame
    private void Update()
    {
        if(Input.GetMouseButtonDown(0)){
            if (Util.GetInstance.IsNotTouched)
            {
                return;
            }
            _clickParticle.StartAction(Input.mousePosition);
        }
    }

    #region 영역 내 터치가 들어오는지 확인 
    public void TapCallBack()
    {
        if (Util.GetInstance.IsNotTouched)
        {
            return;
        }

        StopCoroutine("CoTapCallBack");
        StartCoroutine("CoTapCallBack");
    }

    IEnumerator CoTapCallBack()
    {
        _successParticle.StartAction(Input.mousePosition);
        _isInputSuccess = true;
        Judge();
        yield return new WaitForSeconds(0.2f);
        _isInputSuccess = false;
        Judge();
    }
    #endregion 

    /// <summary>
    /// 클리어 후 다음 단계 진입 시
    /// </summary>
    /// <returns></returns>
    IEnumerator CoNextStep()
    {
        if(_currentGameStep == EGameStep.None)
        {
            yield return new WaitForSeconds(3.5f);
        }
        _gauge = gameObject.AddComponent<GameGauge>();
        _gauge.CreateGameGauge(GameGauge.EType.Horizontal, _gaugeStrs[(int)_currentGameStep], new Vector2(656,Util.GetInstance.GetDesignSize().y - 85));

        _currentGameStep++;
        _gaugeBar = _gauge.MakeAndInGaugeImageReturn();
        _isInputSuccess = false;
        _currentGameState = EGameState.None;

        _enemyCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, (1 + (int)EGameStep.Three * ((int)_currentGameStep - 1)).ToString(), true);

        switch (_currentGameStep)
        {
            case EGameStep.Two:
                {
                    
                    _enemyCharacter.transform.DOLocalMoveX(_enemyCharacter.transform.localPosition.x - 45, 0.25f);
                    yield return new WaitForSeconds(0.35f);
                }
                break;
            case EGameStep.Three:
                {
                    _enemyCharacter.transform.DOLocalMoveX(_enemyCharacter.transform.localPosition.x - 85, 0.4f);
                    yield return new WaitForSeconds(0.5f);
                }
                break;
            default:
                break;
        }
        Judge();
        Util.GetInstance.IsNotTouched = false;
        
    }

    /// <summary>
    /// 적이 뽀로로에게 이동
    /// </summary>
    /// <returns></returns>
    IEnumerator CoEnemyAction()
    {
        yield return new WaitForSeconds(2);
        _enemyCharacter.transform.DOLocalMoveX(955, 1.5f).SetEase(Ease.InQuad);
    }

    /// <summary>
    /// 터치에 대한 판단
    /// </summary>
    void Judge()
    {
        //캐릭터가 밀때
        if (_isInputSuccess)
        {
            StopCoroutine("CoBackAction");
            StartCoroutine("CoPushAction");
        }
        //캐릭터가 밀릴때
        else
        {
            StopCoroutine("CoPushAction");
            StartCoroutine("CoBackAction");
        }
    }

    /// <summary>
    /// 캐릭터가 밀때 하는 행동
    /// </summary>
    /// <returns></returns>
    IEnumerator CoPushAction()
    {
        //현재 상태가 밀고있는 상태가 아닌경우
        if (_currentGameState != EGameState.Push)
        {
            _currentGameState = EGameState.Push;
            _superPororoCharacter.GetComponent<SpineAnimation>().SetToSetupPoseAndAnimation(_superPororoCharacter.GetComponent<SkeletonAnimation>(), "2", 2);
            _enemyCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, (2 + (int)EGameStep.Three * ((int)_currentGameStep - 1)).ToString(), true);
            
            while (true)
            {
                _gaugeBar.fillAmount += Time.deltaTime * 0.1f * ((int)EGameStep.Size - (int)_currentGameStep);

                //3단계에서 뽀로로가 레이저를 쏘는 경우
                if(_currentGameStep == EGameStep.Three && _gaugeBar.fillAmount >= 1f)
                {
                    gameObject.GetComponent<GameGauge>().GaugeDestroy(true);
                    _currentLocationStep = ELocationStep.Laiser;
                    //guide
                    _laiserGuide.SetActive(true);
                    _laiserGuide.GetComponent<Guide>().SetGuidePosition(_superPororoCharacter.transform.localPosition + new Vector3(-60,-50));

                    _superPororoCharacter.GetComponent<SpineAnimation>().SetToSetupPoseAndAnimation(_superPororoCharacter.GetComponent<SkeletonAnimation>(), "1");
                    _enemyCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "7", true);
                    //_superPororoCharacter.transform.GetChild(0).gameObject.SetActive(true);

                    Destroy(_touchCollider);
                    _superPororoCharacter.AddComponent<BoxCollider2D>();
                    _superPororoCharacter.AddComponent<UserInputGestureAction>().CreateInputGesture(UserInputGestureAction.EType.Press, UserInputGestureAction.EJudgeWay.Collider, _superPororoCharacter
                        , null, ProgressingButton, PressButtonFail, null, null, 3);
                    StopCoroutine("CoTapCallBack");
                    StopCoroutine("CoPushAction");
                    yield break;
                }

                //clear and next stage
                if (_gaugeBar.fillAmount >= 1f)
                {
                    StopCoroutine("CoTapCallBack");
                    Util.GetInstance.IsNotTouched = true;
                    GetComponent<GameGauge>().GaugeDestroy(true);

                    _superPororoCharacter.GetComponent<SpineAnimation>().SetToSetupPoseAndAnimation(_superPororoCharacter.GetComponent<SkeletonAnimation>(), "1", 1);
                    _enemyCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, (3 + (int)EGameStep.Three * ((int)_currentGameStep - 1)).ToString(),false);
                    yield return new WaitForSeconds(2f);
                    
                    Destroy(GetComponent<GameGauge>());
                    StartCoroutine("CoNextStep");
                    StopCoroutine("CoPushAction");
                    yield break;
                }
                //위치에 따른 상태변화
                switch (_currentLocationStep)
                {
                    case ELocationStep.Left:
                        {
                            if (_earth.transform.localScale.x > 1f)
                            {
                                _earth.transform.localScale -= new Vector3(SCALE_POWER * ((int)EGameStep.Size - (int)_currentGameStep), SCALE_POWER * ((int)EGameStep.Size - (int)_currentGameStep));
                                _backGround.transform.localPosition -= new Vector3(Time.deltaTime * POWER * ((int)EGameStep.Size - (int)_currentGameStep), 0);
                            }
                            else
                            {
                                _currentLocationStep = ELocationStep.Middle;
                            }
                        }
                        break;
                    case ELocationStep.Middle:
                        {
                            _superPororoCharacter.transform.localPosition += new Vector3(Time.deltaTime * POWER * ((int)EGameStep.Size - (int)_currentGameStep), 0);
                            _enemyCharacter.transform.localPosition += new Vector3(Time.deltaTime * POWER * ((int)EGameStep.Size - (int)_currentGameStep), 0);
                            if (_superPororoCharacter.transform.localPosition.x >= Util.GetInstance.GetDesignSize().x * 0.5f + LIMIT_DISTANCE - 120)
                            {
                                _currentLocationStep = ELocationStep.Right;
                            }
                        }
                        break;
                    case ELocationStep.Right:
                        {
                            if (_earth.transform.localScale.x > 0.65f)
                            {
                                _earth.transform.localScale -= new Vector3(SCALE_POWER * 0.5f * ((int)EGameStep.Size - (int)_currentGameStep), SCALE_POWER * 0.5f * ((int)EGameStep.Size - (int)_currentGameStep));
                                _backGround.transform.localPosition -= new Vector3(Time.deltaTime * POWER * ((int)EGameStep.Size - (int)_currentGameStep), 0);
                            }
                        }
                        break;

                    default:
                        break;
                }
                yield return null;
            }
        }
    }

    /// <summary>
    /// 캐릭터가 밀릴때 하는 행동 
    /// </summary>
    /// <returns></returns>
    IEnumerator CoBackAction()
    {
        //현재 상태가 밀리고 있는 상태가 아닌경우
        if (_currentGameState != EGameState.Back)
        {
            _currentGameState = EGameState.Back;
            _superPororoCharacter.GetComponent<SpineAnimation>().SetToSetupPoseAndAnimation(_superPororoCharacter.GetComponent<SkeletonAnimation>(), "3", 3);
            _enemyCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, (1 + (int)EGameStep.Three * ((int)_currentGameStep - 1)).ToString(), true);
            
            while (true)
            {
                //위치에 따른 상태변화
                switch (_currentLocationStep)
                {
                    case ELocationStep.Left:
                        {
                            if (_earth.transform.localScale.x < 1.75f)
                            {
                                _earth.transform.localScale += new Vector3(SCALE_POWER * (int)_currentGameStep, SCALE_POWER * (int)_currentGameStep);
                                _backGround.transform.localPosition += new Vector3(Time.deltaTime * POWER * (int)_currentGameStep, 0);
                            }
                        }
                        break;
                    case ELocationStep.Middle:
                        {
                            _superPororoCharacter.transform.localPosition -= new Vector3(Time.deltaTime * POWER * (int)_currentGameStep, 0);
                            _enemyCharacter.transform.localPosition -= new Vector3(Time.deltaTime * POWER * (int)_currentGameStep, 0);
                            if (_superPororoCharacter.transform.localPosition.x <= Util.GetInstance.GetDesignSize().x * 0.5f - LIMIT_DISTANCE - 120)
                            {
                                _currentLocationStep = ELocationStep.Left;
                            }
                        }
                        break;
                    case ELocationStep.Right:
                        {
                            if (_earth.transform.localScale.x < 1f)
                            {
                                _earth.transform.localScale += new Vector3(SCALE_POWER * 0.5f * (int)_currentGameStep, SCALE_POWER * 0.5f * (int)_currentGameStep);
                                _backGround.transform.localPosition += new Vector3(Time.deltaTime * POWER * (int)_currentGameStep, 0);
                            }
                            else
                            {
                                _currentLocationStep = ELocationStep.Middle;
                            }
                        }
                        break;
                    default:
                        break;
                }
                yield return null;
            }
        }
    }

    #region 뽀로로가 레이저를 쏘고 난 후 행동
    public void PressButtonComplete()
    {
        StartCoroutine("CoCompleteGame");
    }
    
    IEnumerator CoCompleteGame()
    {
        Util.GetInstance.IsNotTouched = true;
        _superPororoCharacter.GetComponent<SpineAnimation>().StopAllCoroutines();
        _superPororoCharacter.transform.GetChild(0).gameObject.SetActive(false);
        _gauge.GaugeDestroy(true);
        _superPororoCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "5", true);
        yield return new WaitForSeconds(1);
        _enemyCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "9", true);
        _superPororoCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "6", false);
        yield return new WaitForSeconds(0.667f);
        _superPororoCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "1", true);
        yield return new WaitForSeconds(2.933f - 0.667f);
        _enemyCharacter.SetActive(false);
        yield return new WaitForSeconds(1f);
        _superPororoCharacter.GetComponent<SkeletonAnimation>().state.ClearTracks();
        _superPororoCharacter.GetComponent<SkeletonAnimation>().Skeleton.SetToSetupPose();
        _superPororoCharacter.transform.DOLocalMoveX(_superPororoCharacter.transform.localPosition.x - 100, 1.5f).SetEase(Ease.InBack);
        yield return new WaitForSeconds(1.5f);
        _superPororoCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "2", true);
        _superPororoCharacter.transform.DOLocalMoveX(Util.GetInstance.GetDesignSize().x + 200, 2.0f).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(3.0f);
        GameManager.GameEndCallBackEvent.Invoke();

    }
    #endregion

    /// <summary>
    /// 레이저 버튼을 누르는 동안에 하는 행동
    /// </summary>
    public void ProgressingButton()
    {
        
        StartCoroutine("CoJudgePressTimer");
        _superPororoCharacter.transform.GetChild(0).gameObject.SetActive(false);
        _enemyCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "8", true);
        TrackEntry trackEntry = _superPororoCharacter.GetComponent<SpineAnimation>().UnReverseAnimation("4");
        
        if (_gaugeAction == null)
        {
            _gaugeAction = _gaugeBar.DOFillAmount(1, trackEntry.Animation.Duration).OnComplete(() => {
                Destroy(_superPororoCharacter.GetComponent<UserInputGestureAction>());
                PressButtonComplete();
            });
            _enemyAction = DOTween.Sequence();
            _enemyAction.AppendInterval(0.5f);
            _enemyAction.Append(_enemyCharacter.transform.DOLocalMoveX(_enemyCharacter.transform.localPosition.x + 185, trackEntry.Animation.Duration - 0.5f));
            
        }
        else
        {
            _gaugeAction.PlayForward();
            _enemyAction.PlayForward();
        }
    }

    /// <summary>
    /// 레이저 버튼을 누르지 않은 상태에서 하는 행동
    /// </summary>
    public void PressButtonFail()
    {
        StopCoroutine("CoJudgePressTimer");
        StopCoroutine("CoPressHintTimer");
        StartCoroutine("CoPressHintTimer");
        _pressJudgeTime = 0;
        _superPororoCharacter.transform.GetChild(0).gameObject.SetActive(true);
        _enemyCharacter.GetComponent<SkeletonAnimation>().state.SetAnimation(0, "7", true);

        _superPororoCharacter.GetComponent<SpineAnimation>().ReverseAnimation();

        _gaugeAction.PlayBackwards();
        if(_enemyAction != null)
        {
            _enemyAction.PlayBackwards();
        }
    }

    IEnumerator CoJudgePressTimer()
    {
        while (true)
        {
            _pressJudgeTime += Time.deltaTime;
            if(_pressJudgeTime >= 0.5f)
            {
                StopCoroutine("CoPressHintTimer");
                _pressHintWaitTime = 0;
            }
            yield return null;
        }
    }

    public void LaiserVisible(bool visible)
    {
        if (visible)
        {
            _superPororoCharacter.transform.GetChild(0).gameObject.SetActive(true);
        }
        else
        {
            //_superPororoCharacter.transform.GetChild(0).gameObject.SetActive(false);
            _laiserGuide.SetActive(false);
            _pressHintWaitTime = 0;
            StartCoroutine("CoPressHintTimer");
        }
    }

    IEnumerator CoPressHintTimer()
    {
        
        while (true)
        {
            _pressHintWaitTime += Time.deltaTime;
            if(_pressHintWaitTime >= WAIT_FOR_PRESS_TIME)
            {
                _laiserGuide.SetActive(true);
                _laiserGuide.GetComponent<Guide>().SetGuidePosition(_superPororoCharacter.transform.localPosition + new Vector3(-60, -50));
                _pressHintWaitTime = 0;
                yield break;
            }
            yield return null;
        }
    }
}
