﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using System;
using DG.Tweening;

/// <summary>
/// 스파인 애니메이션에 사용되는 클래스입니다.
/// </summary>
public class SpineAnimation : MonoBehaviour
{
    private SkeletonAnimation _currentSkeletonAnimation;

    private TrackEntry _currentTrackEntry;

    private void Start()
    {
        _currentSkeletonAnimation = GetComponent<SkeletonAnimation>();
    }

    #region 애니메이션 역재생 / 재생

    private float _animationTime = 0f;

    /// <summary>
    /// 애니메이션 역재생
    /// </summary>
    public void ReverseAnimation()
    {
        StopCoroutine("CoGetBackAnimation");
        StartCoroutine("CoReverseAnimation");
    }

    
    IEnumerator CoReverseAnimation()
    {
        yield return new WaitUntil(() => _currentSkeletonAnimation != null);
        
        while (true)
        {
            _currentTrackEntry.TimeScale = 0f;
            _currentTrackEntry.AnimationLast = 0f;
            
            _currentTrackEntry.TrackTime = _animationTime;

            _currentSkeletonAnimation.skeleton.SetToSetupPose();
            _currentSkeletonAnimation.state.Apply(_currentSkeletonAnimation.skeleton);
            _animationTime -= Time.deltaTime;

            //animation idle
            if (_animationTime < 0)
            {
                _currentSkeletonAnimation.state.ClearTracks();
                _currentSkeletonAnimation.skeleton.SetToSetupPose();
                _currentTrackEntry = _currentSkeletonAnimation.state.SetAnimation(0, "1", true);
                yield break;

                //계속 리버스 할 경우
                //_animationTime = _currentTrackEntry.AnimationEnd;
            }
            yield return null;
        }
    }

    /// <summary>
    /// 애니메이션 순재생
    /// </summary>
    /// <param name="animationName"></param>
    public TrackEntry UnReverseAnimation(string animationName)
    {
        StopCoroutine("CoReverseAnimation");
        _currentTrackEntry = _currentSkeletonAnimation.state.SetAnimation(0, animationName, false);
        StartCoroutine("CoGetBackAnimation");
        return _currentTrackEntry;
    }

    IEnumerator CoGetBackAnimation()
    {
        while (true)
        {
            if (_currentTrackEntry.Animation == null)
            {
                yield break;
            }
            if (_animationTime < _currentTrackEntry.Animation.Duration)
            {
                _animationTime += Time.deltaTime;
                _currentTrackEntry.TrackTime = _animationTime;
            }
            yield return null;
        }
    }
    #endregion

    /// <summary>
    /// 블링크 효과
    /// </summary>
    /// <param name="count"></param>
    /// <returns>블링크 액션 시간</returns>
    public float BlinkAction(int blinkCount, float blinkTime = 0.1f)
    {
        if(GetComponent<SkeletonAnimation>() == null)
        {
            Debug.Log("SkeletonAnimation Component를 오브젝트에 추가해주세요!!");
            return 0;
        }
        StartCoroutine(CoBlinkAction(blinkCount,blinkTime));
        return blinkTime * blinkCount * 3 + blinkTime;
    }

    IEnumerator CoBlinkAction(int blinkCount,float blinkTime)
    {
        int count = 0;
        float time = 0;
        while (count < blinkCount)
        {
            Fade(GetComponent<SkeletonAnimation>(), 0, 0);
            yield return new WaitForSeconds(blinkTime);
            time += blinkTime;
            Fade(GetComponent<SkeletonAnimation>(), 1, 0);
            yield return new WaitForSeconds(blinkTime * 2);
            time += (blinkTime * 2);
            count++;
        }
        yield return new WaitForSeconds(blinkTime);
        time += (blinkTime);
        Debug.Log("time == " + time);
    }

    /// <summary>
    /// Spine의 알파를 duration동안 alpha로 만듭니다.
    /// </summary>
    public void Fade(SkeletonAnimation skeletonAnimation, float alpha, float duration, Action endCallback = null)
    {
        if (duration == 0)
        {
            skeletonAnimation.skeleton.A = alpha;
            //endCallback?.Invoke();
        }
        else
        {
            DOTween.To(() => skeletonAnimation.skeleton.A, x => skeletonAnimation.skeleton.A = x, alpha, duration)
                .OnComplete(() =>
                {
                    endCallback?.Invoke();
                });
        }
    }

    /// <summary>
    /// 스파인 애니메이션을 초기화 및 동작 루프 시킵니다.
    /// </summary>
    public void SetToSetupPoseAndAnimation(SkeletonAnimation skeletonAnimation,string animationName,int trackIndex = 0)
    {
        skeletonAnimation.skeleton.SetToSetupPose();
        skeletonAnimation.state.ClearTracks();
        skeletonAnimation.state.SetAnimation(trackIndex, animationName, true);
    }
}
