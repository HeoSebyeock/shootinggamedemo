﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEditor;

/// <summary>
/// 액션의 정보 값 관리 클래스
/// </summary>

[CreateAssetMenu(fileName = "", menuName = "Scriptable Object/Action Data")]

public class ActionData : ScriptableObject
{
    public enum EType
    {
        Jump,
        Move,
        Rotate,
        Scale,
    }

    public enum EAdverb
    {
        Default,
        Shake,
        Punch,
    }

    [Header("종류")]
    [SerializeField]
    private EType _type;
    public EType Type { get { return _type; } }

    [Header("이동할_위치")]
    [SerializeField]
    private Vector2 _movePosition;
    public Vector2 MovePosition { get { return _movePosition; } set { _movePosition = value; } }
    

    [Header("시간")]
    [SerializeField]
    private float _time;
    public float Time { get { return _time; } }

    [Header("지연시간")]
    [SerializeField]
    private float _actionWaitTime;
    public float ActionWaitTime { get { return _actionWaitTime; } }

    [Header("루프_횟수")]
    [SerializeField]
    private int _loopCount;
    public int LoopCount { get { return _loopCount; } }

    [Header("루프_종류")]
    [SerializeField]
    private LoopType _loopType;
    public LoopType LoopType { get { return _loopType; } }

    [Header("Ease")]
    [SerializeField]
    private Ease _ease;
    public Ease Ease{ get { return _ease; } }

    #region 점프에만 쓰임
    [Header("점프_높이")]
    [Header("!!! JUMP_ACTION !!!")]
    [Space(15)]
    [SerializeField]
    private float _jumpheight;
    public float JumpHeight { get { return _jumpheight; } }

    [Header("점프_횟수")]
    [SerializeField]
    private int _jumpCount;
    public int JumpCount { get { return _jumpCount; } }
    #endregion

    #region 회전에만 쓰임
    [Header("회전 각도")]
    [Header("!!! ROTATE_ACTION !!!")]
    [Space(15)]
    [SerializeField]
    private float _rotateAngle;
    public float RotateAngle { get { return _rotateAngle; } }

    [Header("회전_방식")]
    [SerializeField]
    private RotateMode _rotateMode;
    public RotateMode RotateMode { get { return _rotateMode; } }
    #endregion

    #region 스케일에만 쓰임
    [Header("변경_크기")]
    [Header("!!! SCALE_ACTION !!!")]
    [Space(15)]
    [SerializeField]
    private Vector2 _changeScale;
    public Vector2 ChangeScale { get { return _changeScale; } }
    #endregion

    /// <summary>
    /// 기본이 아닌 액션의 경우
    /// </summary>

    [Header("부사")]
    [Header("!!! NOT_DEFAULT_ACTION !!!")]
    [Space(15)]
    [SerializeField]
    private EAdverb _adverb;
    public EAdverb Adverb { get { return _adverb; } }

    [Header("진동")]
    [Header("*** ShakeAction ***")]
    [Space(15)]
    [SerializeField]
    private int _vibrato;
    public int Vibrato { get { return _vibrato; } }

    [Header("늘어나는길이")]
    [SerializeField]
    private float _strength;
    public float Strength { get { return _strength; } }

    [Header("Shake_Random")]
    [SerializeField]
    private float _shakeRandom;
    public float ShakeRandom { get { return _shakeRandom; } }

    [Header("탄력")]
    [Header("*** PunchAction ***")]
    [Space(15)]
    [SerializeField]
    private float _elastic;
    public float Elastic { get { return _elastic; } }

    

    
}


