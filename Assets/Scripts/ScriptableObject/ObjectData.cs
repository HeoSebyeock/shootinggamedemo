﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 오브젝트의 데이터 정보 값 관리 클래스
/// </summary>

[CreateAssetMenu(fileName = "", menuName = "Scriptable Object/Object Data")]

public class ObjectData : ScriptableObject
{
    [Header("오브젝트_종류")]
    [SerializeField]
    private Constants.EObjectType _objectType;
    public Constants.EObjectType ObjectType { get { return _objectType; } }

    [Header("이미지_이름")]
    [SerializeField]
    private string _imageName;
    public string ImageName { get { return _imageName; } }

    [Header("위치")]
    [SerializeField]
    private Vector2 _position;
    public Vector2 Position { get { return _position; } }

    [Header("크기")]
    [SerializeField]
    private float _scale;
    public float Scale { get { return _scale; } }
}
